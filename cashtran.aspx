﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cashtran.aspx.vb" Inherits="cashtran"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script type="text/jscript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/jscript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=Txt_Account.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetallAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=Txt_AcCode.ClientID %>").val(i.item.val);
                    $("#<%=Txt_Account.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=Txt_Bname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetbankName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1],
                                    label1: item.split('-')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=Txt_BCode.ClientID %>").val(i.item.val);
                    $("#<%=Txt_Bname.ClientID %>").val(i.item.label);
                    $("#<%=Txt_BBranch.ClientID %>").val(i.item.label1);


                },
                minLength: 1

            });
        }); 
    </script>



    <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ctl00_ContentPlaceHolder1_Drl_CashBank").change(function () {
            $("#ctl00_ContentPlaceHolder1_Drl_CashBank option:selected").each(function () {
        console.log("test");

                if ($(this).attr("value") == "0") {
                    $("#tablerow").show();
//                    $(".box").hide();
//                    $(".red").show();
                }
                if ($(this).attr("value") == "1") {
                    $("#tablerow").hide();


//                    $(".green").show();
                }
            });
        });
    });
</script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td colspan="7" align="right">
                <asp:Button ID="btn_back" runat="server" Text="Back" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="Lbl_CBNo" runat="server" Text="CashBook No."></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="Txt_CBNo" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_PayRec" runat="server" Text="Payment/Receipt"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="Drl_PayRec" runat="server">
                    <asp:ListItem Selected="True" Value="True">Payment</asp:ListItem>
                    <asp:ListItem Value="False">Receipt</asp:ListItem>
                </asp:DropDownList>
            </td>
                        <td>
                <asp:Label ID="Lbl_CashBank" runat="server" Text="Cash/Bank"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="Drl_CashBank" runat="server">
                    <asp:ListItem Selected="True" Value="True">Bank</asp:ListItem>
                    <asp:ListItem Value="False">Cash</asp:ListItem>
                </asp:DropDownList>
            </td>
                        <td align="right">
                <asp:Label ID="Lbl_Date" runat="server" Text="Date"></asp:Label>
            </td>
            <td>
                <BDP:BasicDatePicker ID="Txt_Date" runat="server" DisplayType="TextBox"></BDP:BasicDatePicker>
            </td>
        </tr>
    </table>
    <hr />
    <table id="Tbl_Details1">
        <tr>
            <td align="right">
                <asp:Label ID="Lbl_Account" runat="server" Text="Account" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Account" runat="server" Width="200px"></asp:TextBox>
                <asp:TextBox ID="Txt_AcCode" runat="server" Width="50px" ReadOnly="True">0000077</asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_Amount" runat="server" Text="Amount" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Amount" runat="server" Width="100px">165.92</asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_Narration" runat="server" Text="Narration" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Narration" runat="server" Width="200px">AmountPaid</asp:TextBox>
            </td>
            <td>
                <asp:HiddenField ID="hf_serno" runat="server" />
            </td>
        <td>
                <asp:HiddenField ID="hf_savemode" runat="server" Value="0" />
            </td>
        <td>
                <asp:Button ID="btn_Save" runat="server" Text="Add" />
            </td>
        </tr>
        <tr id="tablerow">
            <td align="right">
                <asp:Label ID="Lbl_Bname" runat="server" Text="Bank Name" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Bname" runat="server" Width="200px"></asp:TextBox>
                <asp:TextBox ID="Txt_BCode" runat="server" Width="50px" ReadOnly="True">0000092</asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_BBranch" runat="server" Text="Branch" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_BBranch" runat="server" Width="100px">Vashi</asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_ChqNo" runat="server" Text="Cheque No." Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_ChqNo" runat="server" Width="100px">625984</asp:TextBox>
            </td>
            <td>
                <asp:Label ID="Lbl_ChqDate" runat="server" Text="Cheque Date" Width="100px" 
                    Height="19px"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="Txt_ChqDate" runat="server" DisplayType="TextBox" 
                    Width="100px"></BDP:BasicDatePicker>
            </td>
            <td align="left">
                &nbsp;</td>
        </tr>
        <tr>
        <td colspan="9">
            &nbsp;</td>
        </tr>
        <tr>
        <td colspan="9" align="right">
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
                <asp:Button ID="Btn_ConfirmSave" runat="server" Text="Save" />
        
        </td>
        </tr>
        </table>
    <hr />

</asp:Content>
