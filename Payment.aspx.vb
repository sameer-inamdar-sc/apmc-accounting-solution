﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Payment
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub BindGrid()
        Dim filter As String = ""

        If txtSearchPaymentNo.Text <> "" Then
            filter = "and t.TransactionMasterID like '" & txtSearchPaymentNo.Text & "%'"
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and t.date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If

        filter = filter & " and rp=" & Page.Request.QueryString("type")
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()
        Dim cmd As New SqlCommand("select ROW_NUMBER() OVER (ORDER BY t.transactionmasterid asc) AS 'Sr. No.', t.transactionmasterid tr,CONVERT(VARCHAR(10), t.date, 105) AS date,a.acname,case when transtype=0 then 'Bank' when transtype=1 then 'Cash' end 'Transaction Type',sum(d.amount) amount from transactionmaster t left join acmast a on t.AcCode=a.acccd left join TransactionDetails d on t.TransactionMasterID=d.TransactionMasterID " & filter & " and d.amount<>0 group by t.TransactionMasterID,date,acname,TransType", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
        'ClearConrols()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        ''  BindGrid()


        If Not IsPostBack Then

            BindGrid()
            setheaders()
          
            'Panel2.Visible = False
            'txt_fromDate.SelectedDate = Date.Now.AddDays(-7)
            'txt_toDate.SelectedDate = Date.Today

        End If
    End Sub
    Public Sub setheaders()
        If Page.Request.QueryString("type") = "0" Then
            Lbl_TransactionNo.Text = "Receipt No."
            Lbl_Header.Text = "Receipt Details"
            GridView1.HeaderRow.Cells(1).Text = "Receipt No."
        ElseIf Page.Request.QueryString("type") = "1" Then
            Lbl_TransactionNo.Text = "Voucher No."
            Lbl_Header.Text = "Payment Details"
            GridView1.HeaderRow.Cells(1).Text = "Voucher No."

        End If
    End Sub

    Public query As String, constr As String

    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        If e.CommandName = "e" Then
            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("~/paytran.aspx?type=" & Page.Request.QueryString("type") & "&id=" & Page.Request.QueryString("id") & srno)

        End If
        If e.CommandName = "d" Then
            con.Open()
            Dim intResponse As Integer
            intResponse = MsgBox("Do You Want to Delete Whole Data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            Dim srno As Integer = e.CommandArgument
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd As New SqlCommand("delete from transactiondetails where TransactionMasterID=" & srno & "delete from transactionmaster where TransactionMasterID=" & srno & "", con)
                cmd.ExecuteNonQuery()
                query = "ledger_Delete"
                Dim com As New SqlCommand(query, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.AddWithValue("@serno", srno)
                com.Parameters.AddWithValue("@compid", Session("compid"))
                com.Parameters.AddWithValue("@doccd", "RC")
                com.ExecuteNonQuery()

                MsgBox("Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
                con.Close()
                BindGrid()
            End If
        End If

    End Sub

    Protected Sub Btn_Search_Click(sender As Object, e As System.EventArgs) Handles Btn_Search.Click
        BindGrid()
        setheaders()

    End Sub

    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Response.Redirect("paytran.aspx?type=" & Page.Request.QueryString("type"))
    End Sub

    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        Dim cell As TableCell = e.Row.Cells(0)
        'Remove cell
        e.Row.Cells.RemoveAt(0)
        'Add at the end
        e.Row.Cells.Add(cell)
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri

    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        ''        Dim url As String = HttpContext.Current.Request.Url.AbsoluteUri

    End Sub
End Class
