﻿Imports System
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System.Globalization
Partial Class mjmast
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub btn_save_Click(sender As Object, e As System.EventArgs) Handles btn_save.Click
        con.Open()

        If HiddenField1.Value = "0" Then
            Dim cmd1 = New SqlCommand("select * from mjmast where mjcd = '" & txt_code.Text & "'", con)
            Dim mjcd As String = cmd1.ExecuteScalar()
            If mjcd <> "" Then
                MsgBox("Major Code Already Exist", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Save Error")
                ClearConrols()
                Exit Sub
            End If
            Dim cmd2 = New SqlCommand("select * from mjmast where mjname = '" & txt_description.Text & "'", con)
            Dim mjname As String = cmd2.ExecuteScalar()
            If mjname <> "" Then
                MsgBox("Major Name Already Exist", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Save Error")
                ClearConrols()
                Exit Sub
            End If
            Dim s As String
            s = "insert into mjmast(mjcd,mjname,grcd) values('" & txt_code.Text.Trim().ToUpper() & "','" & txt_description.Text.Trim() & "','" & drlgrcd.SelectedValue.Trim() & "')"
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
        Else
            Dim s As String
            s = "update mjmast set mjname='" & txt_description.Text.Trim() & "', grcd='" & drlgrcd.SelectedValue & "' where mjcd='" & txt_code.Text.Trim() & "'"
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
        End If
        ClearConrols()
        MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Save")
        txt_description.Focus()
        txt_code.Text = ""
        txt_description.Text = ""
        con.Close()
        BindGrid()
        'End If
    End Sub
    Protected Sub btn_search_Click(sender As Object, e As System.EventArgs) Handles btn_search.Click
        BindGrid()
        ClearConrols()
    End Sub
    Public Sub bindgrouptype()

        Dim cmd1 As New SqlCommand("select grcd,grcd+'-'+grtype com from grtype", con)

        Dim da As New SqlDataAdapter(cmd1)

        Dim dt As New DataTable()
        da.Fill(dt)
        drlgrcd.DataSource = dt
        drlgrcd.DataTextField = "com"
        drlgrcd.DataValueField = "grcd"
        drlgrcd.DataBind()
        drlgrcd.Items.Insert(0, "Select")

    End Sub
    Public Sub BindGrid()
        Dim filter As String = ""
        If txt_SearchCode.Text <> "" Then
            filter = "and mjcd like '" & txt_SearchCode.Text & "%'"
        End If
        If txt_SearchDescription.Text <> "" Then
            filter = filter & "and mjname like '" & txt_SearchDescription.Text & "%'"
        End If
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If
        con.Open()
        Dim cmd As New SqlCommand("Select upper(mjmast.mjcd) mjcd, upper(mjmast.mjname) mjname, mjmast.grcd+' - ' +grtype.grtype type from mjmast left join grtype on mjmast.grcd=grtype.grcd " & filter, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
    End Sub
    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        ClearConrols()
        txt_code.Enabled = True
        txt_code.Focus()
        drlgrcd.SelectedIndex = 0
        HiddenField1.Value = "0"
        Me.ModalPopupExtender2.Show()
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then
            txt_code.Text = ""
            Dim mjcd As String = e.CommandArgument
            HiddenField1.Value = mjcd
            txt_description.Focus()
            con.Open()
            Dim cmd1 As New SqlCommand("select upper(mjmast.mjcd) mjcd, upper(mjmast.mjname) mjname,grtype.grcd,grtype.grtype from mjmast left join grtype on mjmast.grcd=grtype.grcd where mjcd= '" & mjcd & "' ", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)
            Dim culture1 As New CultureInfo("pt-BR")
            txt_code.Text = custTable.Rows(0).Item(0)
            txt_code.Enabled = False
            txt_description.Text = custTable.Rows(0).Item(1)
            drlgrcd.SelectedValue = custTable.Rows(0).Item("grcd")
            con.Close()
            Me.ModalPopupExtender2.Show()
        End If
        If e.CommandName = "d" Then
            con.Open()
            Dim mjcd = e.CommandArgument
            HiddenField1.Value = mjcd
            Dim d As String
            Dim f As Integer
            d = "select COUNT(acmast.mjcd) from mjmast left join acmast on acmast.mjcd=mjmast.mjcd where acmast.mjcd='" + mjcd.ToString() + "'"
            Dim cmd3 As New SqlCommand(d, con)
            f = cmd3.ExecuteScalar()
            If f = 0 Then
                Dim cmd1 As New SqlCommand("delete from dbo.mjmast where dbo.mjmast.mjcd='" + mjcd.ToString() + "'", con)
                cmd1.ExecuteNonQuery()
                con.Close()
                MsgBox("Major Code Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
                BindGrid()
            Else
                MsgBox("Major is user in Account Master", MsgBoxStyle.OkOnly + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "In use...")

            End If
        End If
    End Sub
    Public Sub ClearConrols()
        txt_description.Focus()
        txt_description.Text = ""
        txt_code.Text = ""
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            HiddenField1.Value = "0"
            ClearConrols()
            'Panel1.Visible = True
            txt_code.Text = ""
            bindgrouptype()
            txt_description.Text = ""
            BindGrid()
        End If
    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

  
   
End Class
