﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesInvoice.aspx.vb" Inherits="SalesInvoice" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sales Invoice</title>
    <style>
        .bottom
        {
            position: absolute;
            bottom: 0px;
        }
        
        th
        {
            border-bottom: 1px solid;
            border-left: 1px solid;
        }
        th:first-child
        {
            border-left: 0px solid;
        }
        th:last-child
        {
            border-right: 0px solid;
        }
    </style>
</head>
<body>
    <asp:Label ID="lblPageString" runat="server" EnableViewState="false" Width="100%"></asp:Label>
</body>
</html>
<%--<script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=printPanel.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>--%>
<%--<asp:Panel ID="printPanel" runat="server" Width="100%">
        <center>
            <table style="width: 75%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td align="left">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Address.JPG" ImageAlign="Left" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/img/logo.png" />
                    </td>
                </tr>
            </table>
            <hr style="width: 75%" />
            <center>
                Invoice</center>
            <hr style="width: 75%" />
            <table style="width: 75%" border="2">
                <tr>
                    <td>
                        <table style="width: 70%">
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Address"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="City"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="Phone"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table style="width: 70%">
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Invoice No."></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblInvoiceNo" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label10" runat="server" Text="Date"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label12" runat="server" Text="Order No."></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblOrderNo" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label14" runat="server" Text="Ref. No."></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataText="Data not found !!!"
                HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" HorizontalAlign="Center"
                PagerStyle-HorizontalAlign="Right" PageSize="2" ShowFooter="true" Width="75%">
                <Columns>
                    <asp:BoundField DataField="tserno" HeaderText="Sr No." ItemStyle-HorizontalAlign="Center"
                        ReadOnly="true" Visible="false" ItemStyle-Width="25%" />
                    <asp:BoundField DataField="qty" HeaderText="Quantity" ItemStyle-HorizontalAlign="Right"
                        ReadOnly="true" />
                    <asp:BoundField DataField="item" HeaderText="Item Name" ItemStyle-HorizontalAlign="Center"
                        ReadOnly="true" />
                    <asp:TemplateField FooterStyle-HorizontalAlign="Right" HeaderText="Rate" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblRate" runat="server" Text='<%# Eval("rate") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblGrandTotal" runat="server" Text="Total"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField FooterStyle-HorizontalAlign="Right" HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label15" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblGrandTotal0" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="False" Font-Italic="False" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <br />
            <table style="width: 75%" border="2">
                <tr>
                    <td valign="top" align="center" class="style7">
                        <asp:Label ID="Label11" runat="server" Text="Payment Details"></asp:Label>
                    </td>
                    <td style="width: 20%">
                        <table style="width: 100%; height: 91px;">
                            <tr>
                                <td class="style8" align="left">
                                    <asp:Label ID="Label13" runat="server" Text="Total"></asp:Label>
                                </td>
                                <td align="right" class="style4">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8" align="left">
                                    <asp:Label ID="Label3" runat="server" Text="APMC"></asp:Label>
                                </td>
                                <td align="right" class="style4">
                                    <asp:Label ID="lblAPMC" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8" align="left">
                                    <asp:Label ID="Label17" runat="server" Text="Round Up"></asp:Label>
                                </td>
                                <td align="right" class="style4">
                                    <asp:Label ID="lblRound" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style8" align="left">
                                    <asp:Label ID="Label19" runat="server" Text="Net Bill Amount"></asp:Label>
                                </td>
                                <td align="right" class="style4">
                                    <asp:Label ID="lblGTInvoice" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
        </center>
    </asp:Panel>
    <asp:Button ID="btnPrint" runat="server" Text="Print" Width="125px" Height="25px"
        OnClientClick="PrintPanel();" />--%>
<%--<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style4
        {
            width: 199px;
        }
        .style7
        {
            width: 40%;
        }
        .style8
        {
            width: 450px;
        }
    </style>
</asp:Content>--%>
