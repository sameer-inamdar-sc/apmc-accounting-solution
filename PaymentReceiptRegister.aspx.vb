﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing
Partial Class PaymentReceiptRegister
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim id, acname, frdate, todate, type As String
        id = Page.Request.QueryString("id")
        acname = Page.Request.QueryString("acname")
        frdate = Page.Request.QueryString("frdate")
        todate = Page.Request.QueryString("todate")
        type = Page.Request.QueryString("type")
        ShowPaymentRegister(id, acname, frdate, todate, type)
    End Sub
    Protected Sub ShowPaymentRegister(ByVal id As String, ByVal acname As String, ByVal frdate As String, ByVal todate As String, ByVal type As String)
        Dim filter As String = ""

        If id <> "" Then
            filter = "and m.TransactionMasterID like '" & id & "%'"
        End If

        If acname <> "" Then
            filter = filter & "and a.acname like '" & acname & "%'"
        End If

        If (frdate <> "") And (todate <> "") Then
            filter = filter & "and m.date BETWEEN '" & frdate & "' AND '" & todate & "'"
        End If

        filter = filter & " and rp=" & type
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()
        Dim cmd As New SqlCommand("select CONVERT(VARCHAR(10), m.date, 105) AS Date,m.TransactionMasterID 'Voucher No.',a.acname 'Account Name',case when TransType=0 then SUM(ISNULL(d.amount,0)) end Cash,case when TransType=1 then SUM(ISNULL(d.amount,0)) end Bank,SUM(ISNULL(d.amount,0)) Total,cb.acname 'Cash/Bank' from TransactionMaster m join acmast a on m.AcCode=a.acccd join TransactionDetails d on m.TransactionMasterID=d.TransactionMasterID join acmast cb on m.casbankcode=cb.acccd " & filter & " group by a.acname,date,m.TransactionMasterID,TransType,cb.acname", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetPaymentRegisterd(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
        con.Close()
        'If Page.Request.QueryString("type") = "0" Then
        '    GridView1.HeaderRow.Cells(1).Text = "Receipt No."
        'ElseIf Page.Request.QueryString("type") = "1" Then
        '    GridView1.HeaderRow.Cells(1).Text = "Voucher No."
        'End If
        'ClearConrols()
    End Sub
    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetPaymentRegisterd(ByVal dt As DataTable) As String

        Dim strb As New StringBuilder
        Dim CompDt = New DataTable
        CompDt = GetCompInfo()
        'strb.Append("<head><style>")
        'strb.Append("table {border-collapse: collapse;}")
        'strb.Append("table, td, th {border: 1px solid black;}")
        'strb.Append("</style></head>")
        strb.Append("<div id='printDiv'>")
        strb.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        strb.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        strb.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        strb.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        strb.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        strb.Append("<tr><td>&nbsp;</td></tr>")
        strb.Append("<tr><td colspan=8>PAYMENT/RECIEPT REGISTER</td></tr>")
        strb.Append("<tr><td>&nbsp;</td></tr>")
        strb.Append("<tr><td>&nbsp;</td></tr>")
        strb.Append("<tr><td width='130px' align='right'>From Date: </td><td style='width: 130px;' align='left'>01/01/2014</td><td style='width: 130px;' align='right'>To Date: </td><td style='width: 130px;' align='left'>31/01/2014</td><td>&nbsp;</td><td>&nbsp;</td><td style='width: 130px;' align='right'>Date: </td><td style='width: 130px;' align='left'>15/02/2014</td></tr>")
        strb.Append("<tr><td colspan=8>")
        strb.Append("<table width='100%' border='1' cellpadding='0' cellspacing='0' style=;border-collapse:collapse;'><tr style='background-color: darkorchid;'>") '
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(0).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(1).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(2).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(3).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(4).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(5).ColumnName & "</td>")
        strb.Append("<td style='padding-left: 10px;'>" & dt.Columns(6).ColumnName & "</td>")
        strb.Append("</tr>")

        Dim cashTotal As New Decimal
        cashTotal = 0
        Dim BankTotal As New Decimal
        BankTotal = 0
        Dim Total As New Decimal
        Total = 0

        For Each tr As DataRow In dt.Rows
            strb.Append("<tr>")

            strb.Append("<td style='padding-left: 10px;'>" & tr.Item(0).ToString() & "</td>")
            strb.Append("<td style='padding-left: 10px;'>" & tr.Item(1).ToString & "</td>")
            strb.Append("<td style='padding-left: 10px;'>" & tr.Item(2).ToString & "</td>")
            strb.Append("<td align='Right' style='padding-right: 10px;'>" & tr.Item(3).ToString & "</td>")
            strb.Append("<td align='Right' style='padding-right: 10px;'>" & tr.Item(4).ToString & "</td>")
            strb.Append("<td align='Right' style='padding-right: 10px;'>" & tr.Item(5).ToString & "</td>")
            strb.Append("<td style='padding-left: 10px;'>" & tr.Item(6).ToString & "</td>")
            strb.Append("</tr>")
            If tr.Item(3).ToString() <> "" Then
                cashTotal += Convert.ToDecimal(tr.Item(3).ToString())
            End If
            If tr.Item(4).ToString() <> "" Then
                BankTotal += Convert.ToDecimal(tr.Item(4).ToString())
            End If

            Total += Convert.ToDecimal(tr.Item(5).ToString())

        Next
        strb.Append("<tr>")
        strb.Append("<td colspan='3' align='Center'>Total</td>")
        strb.Append("<td align='Right' style='padding-right: 10px;'>" & IIf(cashTotal <> 0, cashTotal, "&nbsp;") & "</td>")
        strb.Append("<td align='Right' style='padding-right: 10px;'>" & IIf(BankTotal <> 0, BankTotal, "&nbsp;") & "</td>")
        strb.Append("<td align='Right' style='padding-right: 10px;'>" & IIf(Total <> 0, Total, "&nbsp;") & "</td>")
        strb.Append("<td>&nbsp;</td>")


        strb.Append("</tr></table>")
        strb.Append("</td></tr>")
        strb.Append("</table>")




        Return strb.ToString()

    End Function
End Class
