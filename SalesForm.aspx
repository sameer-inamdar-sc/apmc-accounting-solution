﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesForm.aspx.vb" Inherits="SalesForm"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        function PrintPanel() {
            document.getElementById('divPrint').style.display = 'inline'
            var panel = document.getElementById("<%=printPanel.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Invoice</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById('divPrint').style.display = 'none'
            document.getElementById('<%=lblGT.ClientID%>').style.display = 'none';
            $('#ctl00_ContentPlaceHolder1_txtGrandTotal').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtGrossAmt').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtRoundUp').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtNetAmt').attr("readonly", true)
            //$('#ctl00_ContentPlaceHolder1_txtAcName').attr("disabled", true)
            $('#ctl00_ContentPlaceHolder1_txtChallanDate_TextBox').attr("readonly", true)



            $("#<%=txtAcName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtAcCode.ClientID %>").val(i.item.val);
                    $("#<%=hfAccode.ClientID %>").val(i.item.val);
                    $("#<%=hfAcName.ClientID %>").val(i.item.label);

                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        function GrandTotal() {

            var l = document.getElementById('<%=lblGT.ClientID%>').innerHTML;
            if (l.length < 1)
                l = 0;

            var t = Math.round((parseFloat(l)) * 100) / 100;
            document.getElementById('<%=txtGrandTotal.ClientID%>').value = t.toFixed(2);

            var percent = Math.round(t * (1 / 100) * 100) / 100
            document.getElementById('<%=hfAPMC1perc.ClientID%>').value = percent.toFixed(2);
            document.getElementById('<%=txtAPMC.ClientID%>').value = document.getElementById('<%=hfAPMC1perc.ClientID%>').value

            var apmc = document.getElementById('<%=txtAPMC.ClientID%>').value

            var gross = Math.round((parseFloat(t) + parseFloat(apmc)) * 100) / 100;
            document.getElementById('<%=txtGrossAmt.ClientID%>').value = gross.toFixed(2);

            var net = Math.ceil(gross);
            document.getElementById('<%=txtNetAmt.ClientID%>').value = net.toFixed(2);

            var roundup = Math.round((net - gross) * 100) / 100;
            document.getElementById('<%=txtRoundUp.ClientID%>').value = roundup.toFixed(2);
            return false;
        }
    </script>
    <script type="text/javascript">
        function GT() {
            var gt = document.getElementById('<%=lblGT.ClientID%>').innerHTML;
            var gt1 = Math.round(gt * 100) / 100;
            document.getElementById('<%=txtGrandTotal.ClientID%>').value = gt1.toFixed(2);

            var ap = document.getElementById('<%=txtAPMC.ClientID%>').value;
            if (ap.length < 1)
                ap = 0;
            var apmc = parseFloat(ap).toFixed(2);
            document.getElementById('<%=txtAPMC.ClientID%>').value = apmc;

            var ga = parseFloat(gt) + parseFloat(ap);
            document.getElementById('<%=txtGrossAmt.ClientID%>').value = ga.toFixed(2);

            var na = Math.ceil(ga);
            document.getElementById('<%=txtNetAmt.ClientID%>').value = na.toFixed(2);

            var ru = parseFloat(na) - parseFloat(ga);
            document.getElementById('<%=txtRoundUp.ClientID%>').value = ru.toFixed(2);
            return false;




        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtChallanNo.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetChallanNoForSales") %>',
                        data: "{ 'prefix': '" + request.term + "','acccd' : '" + $("#<%=hfAccode.ClientID %>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0]

                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtChallanNo.ClientID %>").val(i.item.label);
                    $("#<%=hfChallanNo.ClientID %>").val(i.item.label);

                },
                minLength: 1

            });
        }); 
    </script>
    <%--<script type="text/javascript">
    function setSessionAcccd() 
    {
        var acd = document.getElementById("ctl00_ContentPlaceHolder1_hfAccode").value;
        <%Session["accode"] = acd;%>
        document.getElementById("ctl00_ContentPlaceHolder1_txtS").text = sessionStorage.setItem(acd);
    }
    </script>--%>
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Sales Form" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" align="right" Height="20px" Width="60px" />
            </td>
        </tr>
    </table>
    <div>
        <table align="left">
            <tr>
                <td>
                    <asp:TextBox ID="Txtcompid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtuserid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtdoccd" runat="server" Enabled="False" Visible="False" Width="49px">SA</asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtmjcd" runat="server" Enabled="False" Visible="False" Width="49px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Panel ID="panel1" runat="server" Width="100%">
            <table class="style1">
                <tr>
                    <td class="style10">
                        <asp:Label ID="Label3" runat="server" Text="Sales Invoice No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtSrNo" runat="server" Enabled="false" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label22" runat="server" Text="Lot No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtLotNo" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label24" runat="server" Text="Invoice Date"></asp:Label>
                    </td>
                    <td class="style10">
                        <BDP:BasicDatePicker ID="txtdate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtdate"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style11">
                        <asp:Label ID="Label25" runat="server" Text="Challan Date"></asp:Label>
                    </td>
                    <td class="style11">
                        <BDP:BasicDatePicker ID="txtChallanDate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                    </td>
                </tr>
                <tr>
                    <td class="style10">
                        <asp:Label ID="Label7" runat="server" Text="Account Name"></asp:Label>
                        <asp:HiddenField ID="hfAccode" runat="server" />
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtAcName" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="hfAcName" runat="server" />
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label23" runat="server" Text="A/c Code"></asp:Label>
                        <asp:HiddenField ID="hfSalTranId" runat="server" Value="0" />
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtAcCode" runat="server" Enabled="False"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label10" runat="server" Text="Challan No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtChallanNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtChallanNo"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hfChallanNo" runat="server" />
                    </td>
                    <td class="style11">
                        <asp:Button ID="btnChallanDtls" runat="server" Text="Get Challan Details" ValidationGroup="Save" />
                    </td>
                    <td class="style11">
                        <asp:HiddenField ID="hfItemId" runat="server" />
                        <asp:HiddenField ID="hfItemname" runat="server" />
                    </td>
                </tr>
                <caption>
                    <hr>
                </caption>
            </table>
        </asp:Panel>
    </div>
    <asp:Panel ID="panel2" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
            AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
            EmptyDataRowStyle-HorizontalAlign="Center" PageSize="11" HeaderStyle-BackColor="Black"
            HeaderStyle-ForeColor="White" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="tserno" HeaderText="Sr No." ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="item" HeaderText="Item Name" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="qty" HeaderText="Quantity" ReadOnly="true" ItemStyle-HorizontalAlign="Right" />
                <asp:BoundField DataField="weight" HeaderText="Weight" ReadOnly="true" ItemStyle-HorizontalAlign="Right" />
                <%--<asp:BoundField DataField="rate" HeaderText="Rate" ReadOnly="true" />--%>
                <%--<asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="true" />--%>
                <asp:TemplateField HeaderText="Rate" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="lblRate" runat="server" Text='<%# Eval("rate") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblPageTotal" runat="server" Text="Page Total"></asp:Label>
                        <br />
                        <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                    </FooterTemplate>
                    <%--<FooterStyle HorizontalAlign="Right" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Amount" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        <br />
                        <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                    </FooterTemplate>
                    <%--<FooterStyle HorizontalAlign="Right" />--%>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
        <br />
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label27" runat="server" Text="Grand ToTal"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtGrandTotal" runat="server" onchange="GrandTotal()"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="APMC"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtAPMC" runat="server" onchange="GT()"></asp:TextBox>
                    <asp:HiddenField ID="hfAPMC1perc" runat="server" />
                </td>
                <td>
                    <asp:Label ID="Label26" runat="server" Text="Gross Amount"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtGrossAmt" runat="server" onchange="GrandTotal()"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Round Up"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtRoundUp" runat="server" onchange="GrandTotal()"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Net Amount"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNetAmt" runat="server" onchange="GrandTotal()"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="lblGT" runat="server"></asp:Label>
        <asp:Button ID="btnSaveConfirm" runat="server" Text="Save Sales Invoice" />
        &nbsp;<%--<asp:Button ID="btnPrint" runat="server" Text="Print Sales Invoice" Visible="False" />--%></asp:Panel>
    <asp:Panel ID="printPanel" runat="server" Width="100%">
        <div id="divPrint">
            <center>
                <table style="width: 75%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/Address.JPG" ImageAlign="Left" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/img/logo.png" />
                        </td>
                    </tr>
                </table>
                <hr style="width: 75%" />
                <center>
                    Invoice</center>
                <hr style="width: 75%" />
                <table style="width: 75%" border="2">
                    <tr>
                        <td>
                            <table style="width: 70%">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Address"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Text="City"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label11" runat="server" Text="Phone"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table style="width: 70%">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label12" runat="server" Text="Invoice No."></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInvoiceNo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label13" runat="server" Text="Date"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Text="Order No."></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOrderNo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label15" runat="server" Text="Ref. No."></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRefNo" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                    EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataText="Data not found !!!"
                    HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" HorizontalAlign="Center"
                    PagerStyle-HorizontalAlign="Right" PageSize="2" ShowFooter="true" Width="75%">
                    <Columns>
                        <asp:BoundField DataField="tserno" HeaderText="Sr No." ItemStyle-HorizontalAlign="Center"
                            ReadOnly="true" Visible="false" ItemStyle-Width="25%" />
                        <asp:BoundField DataField="qty" HeaderText="Quantity" ItemStyle-HorizontalAlign="Right"
                            ReadOnly="true" />
                        <asp:BoundField DataField="item" HeaderText="Item Name" ItemStyle-HorizontalAlign="Left"
                            ReadOnly="true" />
                        <asp:TemplateField FooterStyle-HorizontalAlign="Right" HeaderText="Rate" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblRate" runat="server" Text='<%# Eval("rate") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblGrandTotal" runat="server" Text="Total"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField FooterStyle-HorizontalAlign="Right" HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblGrandTotal0" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center" />
                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                    <HeaderStyle BackColor="#333333" Font-Bold="False" Font-Italic="False" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                    <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                    <SortedDescendingHeaderStyle BackColor="#242121" />
                </asp:GridView>
                <br />
                <table style="width: 75%" border="2">
                    <tr>
                        <td valign="top" align="center" class="style7">
                            <asp:Label ID="Label16" runat="server" Text="Payment Details"></asp:Label>
                        </td>
                        <td style="width: 20%">
                            <table style="width: 100%; height: 91px;">
                                <tr>
                                    <td class="style8" align="left">
                                        <asp:Label ID="Label17" runat="server" Text="Total"></asp:Label>
                                    </td>
                                    <td align="right" class="style4">
                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style8" align="left">
                                        <asp:Label ID="Label18" runat="server" Text="APMC"></asp:Label>
                                    </td>
                                    <td align="right" class="style4">
                                        <asp:Label ID="lblAPMC" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style8" align="left">
                                        <asp:Label ID="Label19" runat="server" Text="Round Up"></asp:Label>
                                    </td>
                                    <td align="right" class="style4">
                                        <asp:Label ID="lblRound" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style8" align="left">
                                        <asp:Label ID="Label20" runat="server" Text="Net Bill Amount"></asp:Label>
                                    </td>
                                    <td align="right" class="style4">
                                        <asp:Label ID="lblGTInvoice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
            </center>
        </div>
    </asp:Panel>
</asp:Content>
