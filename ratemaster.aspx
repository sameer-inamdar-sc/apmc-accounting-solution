﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ratemaster.aspx.vb" Inherits="ratemaster"
    MasterPageFile="~/MasterPage.master" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtacname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <%--<style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style1
        {
            height: 15px;
            font-size: x-small;
        }
        .style4
        {
            width: 1296px;
        }
        .style8
        {
        }
        .style9
        {
            width: 130px;
        }
    </style>--%>
    <table width="100%">
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Ratemaster"></asp:Label>
            </td>
            <td align="right" colspan="2">
                &nbsp;
            </td>
            <td>
            </td>
            <td>
            </td>
            <td align="right" colspan="2">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                &nbsp;
                <asp:HiddenField ID="hfacname" runat="server" />
            </td>
            <td align="left">
                <asp:HiddenField ID="hfacccd" runat="server" />
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="Label3" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
                <asp:TextBox ID="txtacname" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="left">
                <asp:Button ID="BtnShow" runat="server" Text="Show" />
            </td>
        </tr>
    </table>
    <hr />
    <asp:Panel ID="panel1" runat="server">
        <asp:GridView ID="gridmaster" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
            AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
            EmptyDataRowStyle-HorizontalAlign="Center" PageSize="15" HeaderStyle-BackColor="Black"
            HeaderStyle-ForeColor="White">
            <Columns>
                <asp:BoundField HeaderText="Sr. No." DataField="serno">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Account Code" DataField="acccd">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Account Name" DataField="acname"></asp:BoundField>
                <asp:BoundField HeaderText="Date" DataField="date">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>
                        <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                            CommandArgument='<%#Eval("serno")%>' />
                        <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                            OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("serno")%>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        </asp:GridView>
    </asp:Panel>
</asp:Content>
