﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Itmexp.aspx.vb" Inherits="Itmexp" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <title></title>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    </html>
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#TextBox1").datepicker();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtitemname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetItems") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfItemId.ClientID %>").val(i.item.val);
                    $("#<%=hfItemname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <%-- <script type="text/javascript">
     $(document).ready(function () {
         $("#<%=txtItemName.ClientID %>").autocomplete({
             autoFocus: true,
             source: function (request, response) {
                 $.ajax({
                     url: '<%=ResolveUrl("~/Service.asmx/GetItems") %>',
                     data: "{ 'prefix': '" + request.term + "'}",
                     dataType: "json",
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     success: function (data) {
                     if (data.d.length > 0) {
                         response($.map(data.d, function (item) {
                             return {
                                 label: item.split('-')[0],
                                 val: item.split('-')[1]
                             };
                            }
                        }
                        else {
                            response([{ label: 'No results found.', val: -1}]);
                        }
                        
                         }))
                     },
//                     error: function (response) {
//                         alert(response.responseText);
//                     },
//                     failure: function (response) {
//                         alert(response.responseText);
//                     }
                 });
             },
             select: function (e, i) {
                 $("#<%=hfItemId.ClientID %>").val(i.item.val);
                 $("#<%=hfItemname.ClientID %>").val(i.item.label);


             },
             minLength: 1

         });
     }); 
    </script>--%>
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
        <style type="text/css">
        .hs
        {
            padding-right: 10px;
        }
        </style>
    <table class="style4" width="100%">
        <tr>
            <td colspan="2" style="padding-left: 5px;" align="left">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#6600CC"
                    Text="Item Expenses Master"></asp:Label>
            </td>
            <td colspan="3" style="padding-left: 5px;" align="left">
                <asp:HiddenField ID="hfItemId" runat="server" />
            </td>
            <td colspan="2" style="padding-left: 5px;" align="left">
                <asp:HiddenField ID="hfItemname" runat="server" />
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" OnClick="Btn_Add_Click" runat="server" ImageUrl="img/add.png"
                    onmouseover="this.src='Img/AddHover.png'" onmouseout="this.src='Img/Add.png'">
                </asp:ImageButton>
            </td>
        </tr>
        <tr>
            <td align="right" class="style8">
                <asp:Label ID="lbl_SearchItmID" runat="server" Text="Item Code"></asp:Label>
            </td>
            <td align="left" class="style8" colspan="2">
                <asp:TextBox ID="txt_SearchCode" runat="server" class="decimal"></asp:TextBox>
            </td>
            <td align="right" class="style8">
                <asp:Label ID="lbl_SearchItmName" runat="server" Text="Item Name"></asp:Label>
            </td>
            <td align="left" class="style8" colspan="2">
                <asp:TextBox ID="txt_SearchItmName" runat="server"></asp:TextBox>
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="left" class="style9">
                <asp:Button ID="Btn_Search" runat="server" Text="Search" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="12">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="12">
                <asp:Panel ID="Panel1" runat="server">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="serno"
                        HorizontalAlign="Center" AllowPaging="True" Width="100%" EmptyDataText="Data not found !!!"
                        EmptyDataRowStyle-HorizontalAlign="Center" PageSize="15" PagerStyle-HorizontalAlign="Right" HeaderStyle-BackColor="Black"
                        HeaderStyle-ForeColor="White">
                        <Columns>
                            <asp:BoundField DataField="itmcd" HeaderText="Item Code" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                            </asp:BoundField>
                            <asp:BoundField DataField="item" HeaderText="Item Name" />
                            <asp:BoundField DataField="date" HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="comm" HeaderText="Commission" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                            </asp:BoundField>
                            <asp:BoundField DataField="hamali" HeaderText="Hamali" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs"  />
                            </asp:BoundField>
                            <asp:BoundField DataField="levy" HeaderText="Levy" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs"  />
                            </asp:BoundField>
                            <asp:BoundField DataField="tolai" HeaderText="Tolai" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs"  />
                            </asp:BoundField>
                            <asp:BoundField DataField="vatav" HeaderText="Vatav" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                            </asp:BoundField>
                            <asp:BoundField DataField="APMC" HeaderText="APMC" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                            </asp:BoundField>
                            <asp:BoundField DataField="maplevy" HeaderText="Mapadi Levy" ItemStyle-HorizontalAlign="Right">
                                <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                        CommandArgument='<%#Eval("serno")%>' />
                                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                        OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("serno")%>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <RowStyle Height="21px" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style1
        {
            height: 15px;
        }
        .style2
        {
            font-size: xx-small;
        }
        .style3
        {
            color: red;
        }
        .style4
        {
            width: 1296px;
        }
        .style8
        {
        }
        .style9
        {
            width: 130px;
        }
    </style>
    <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <br />
    <br />
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;
    <asp:Button ID="zhol" runat="server" Style="display: none;" />
    <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="zhol"
        PopupControlID="Panel2" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="Panel2" runat="server" Width="800px" ForeColor="Black" Font-Bold="true"
        BackColor="White" Font-Size="X-Large" Font-Names="Book Antiqua" Height="307px"
        BorderColor="White" BorderStyle="Outset" BorderWidth="6px" Style="z-index: 100000">
        <div id="dialog" title="Add Item Expenses" class="dialog" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller">
            <table style="line-height: 15px" width="800">
                <tr>
                    <td height="15" style="line-height: 15px" align="center" colspan="6">
                        &nbsp;
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6" height="15" style="line-height: 15px">
                        <asp:Label ID="lbl_popuphead" runat="server" Font-Bold="True" Font-Italic="True"
                            Font-Names="Bookman Old Style" Font-Overline="True" Font-Size="X-Large" Font-Underline="True"
                            Text="Item Expenses Details"></asp:Label>
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" class="style1" colspan="6" style="line-height: 15px">
                    </td>
                    <td class="style1" style="line-height: 15px">
                    </td>
                </tr>
                <tr>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label1" runat="server" Text="Ser No."></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txtserno" runat="server" AutoPostBack="True" Enabled="False" Style="text-align: right;"
                            Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Txtserno"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td height="15" style="line-height: 15px">
                    </td>
                    <td height="15" style="line-height: 15px">
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="lbl_Date" runat="server" Text="Date" Width="120px"></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <BDP:BasicDatePicker ID="txt_date" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy"
                            TabIndex="1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_date"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td height="10px" style="line-height: 15px">
                    </td>
                </tr>
                <tr>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label16" runat="server" Text="Item Name" Width="120px"></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px;">
                        <asp:ToolkitScriptManager ID="ScriptManager1" runat="server" />
                        <div>
                            <asp:TextBox ID="txtItemName" runat="server" TabIndex="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtItemName"
                                ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                        </div>
                    </td>
                    <td height="15" style="line-height: 15px;">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label17" runat="server" Text="Commission" Width="120px"></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txtcomm" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="3" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Txtcomm"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Txtcomm"
                            ValidationExpression="[0-9]*\.?[0-9]*" ValidationGroup="save">time pass</asp:RegularExpressionValidator>
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label18" runat="server" Text="Hamali" Width="120px"></asp:Label>
                    </td>
                    <td style="line-height: 15px;" height="15">
                        <asp:TextBox ID="Txthamali" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="4" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Txthamali"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td style="line-height: 15px;" height="15">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px" width="120">
                        <asp:Label ID="Tolai0" runat="server" Text="Tolai"></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txttolai" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="5" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="Txttolai"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label19" runat="server" Text="Levy" Width="120px"></asp:Label>
                    </td>
                    <td style="line-height: 15px;" height="15">
                        <asp:TextBox ID="Txtlevy" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="6" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Txtlevy"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td style="line-height: 15px;" height="15">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:Label ID="Label20" runat="server" Text="APMC"></asp:Label>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txtapmc" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="7" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="Txtapmc"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 15px" class="style1">
                        <asp:Label ID="Label21" runat="server" Text="Mapadi Levy" Width="120px"></asp:Label>
                    </td>
                    <td style="line-height: 15px;" class="style1">
                        <asp:TextBox ID="Txtmaplevy" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="8" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Txtmaplevy"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td style="line-height: 15px;" class="style1">
                        &nbsp;
                    </td>
                    <td style="line-height: 15px" class="style1">
                        &nbsp;
                    </td>
                    <td style="line-height: 15px" class="style1">
                        <asp:Label ID="Label22" runat="server" Text="Vatav"></asp:Label>
                    </td>
                    <td style="line-height: 15px" class="style1">
                        <asp:TextBox ID="Txtvatav" runat="server" onfocus="disableautocompletion(this.id)"
                            Class="decimal" Style="text-align: right;" TabIndex="9" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="Txtvatav"
                            ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td style="line-height: 15px" class="style1">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td height="15" style="line-height: 15px">
                        &nbsp;
                    </td>
                    <td style="line-height: 15px;" height="15">
                        &nbsp;
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                    <td style="line-height: 15px;" height="15">
                        <asp:TextBox ID="txtitmcd" runat="server" onfocus="disableautocompletion(this.id)"
                            Visible="False" Width="40px"></asp:TextBox>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txtcompid" runat="server" Visible="False" Width="40px"></asp:TextBox>
                    </td>
                    <td height="15" style="line-height: 15px">
                        <asp:TextBox ID="Txtuserid" runat="server" Visible="False" Width="40px"></asp:TextBox>
                    </td>
                    <td height="15" style="line-height: 15px" class="style2">
                        All fields mark with <span class="style3">*</span> are mandatory
                    </td>
                    <td height="10px" style="line-height: 15px">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="7" height="15" style="line-height: 15px">
                        <asp:Button ID="btn_save" runat="server" Text="Save" TabIndex="10" ValidationGroup="save" />
                        <asp:Button ID="Btn_Cancle" runat="server" Text="Cancel" TabIndex="11" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
