﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class ChallanForm
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
         
            txtAcName.Focus()

            txtdate.SelectedDate = Now.ToString
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            con.Open()
            sernofun()
            con.Close()

            Dim sr As String = CType(Page.Request.QueryString("id"), String)
            If Not sr = String.Empty Then
                con.Open()

                Dim cmd As New SqlCommand("select challanMast.serno,challanMast.lotno,challanMast.challanNo, challanMast.date, challanMast.acccd, acmast.acname from challanMast left JOIN acmast on challanMast.acccd = acmast.acccd where challanMast.serno=" & sr, con)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable()
                da.Fill(dt)
                'GridView3.DataSource = dt
                'GridView3.DataBind()
                con.Close()
                txtSrNo.Text = sr
                txtItemName.Focus()

                txtChallanNo.Text = dt.Rows(0).Item("challanNo")
                txtdate.SelectedDate = dt.Rows(0).Item("date")
                hfAccode.Value = dt.Rows(0).Item("acccd")
                txtLotNo.Text = dt.Rows(0).Item("lotno")
                txtAcName.Text = dt.Rows(0).Item("acname")
                txtAcCode.Text = dt.Rows(0).Item("acccd")
                'hfAccode.Value = txtAcCode.Text
                txtItemName.Text = ""
                txtQty.Text = "0"
                txtWeight.Text = "0"
                txtRate.Text = "0.0"
                txtAmt.Text = "0.0"

                'txtGrossAmt.Text = dt.Rows(0).Item("grossamt")
                'txtRoundup.Text = dt.Rows(0).Item("roundoff")
                'txtNetAmt.Text = dt.Rows(0).Item("netamt")

                BindGrid()
                

            End If
        End If
    End Sub

    Public Sub sernofun()
        Dim cmd As New SqlCommand("select max(serno) from challanMast where doccd='" & Txtdoccd.Text.Trim() & "'", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            txtSrNo.Text = a.ToString()
            txtChallanNo.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            txtSrNo.Text = a.ToString()
            txtChallanNo.Text = a.ToString()
        End If
        txtLotNo.Text = Txtdoccd.Text.Trim() + "-" + txtSrNo.Text.Trim().ToString()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("ChallanMain.aspx")
    End Sub

    Public Sub ClearControls()
        txtItemName.Text = ""
        txtQty.Text = "0"
        txtWeight.Text = "0"
        txtRate.Text = "0.00"
        txtAmt.Text = "0.00"
        txtItemName.Focus()
    End Sub

    Public Sub ClearAll()
        con.Open()
        sernofun()
        con.Close()
        txtChallanNo.Text = ""
        txtdate.SelectedDate = Now.ToString()
        txtAcName.Text = ""
        txtAcCode.Text = ""
        'txtFooterAmt.Text = "0.00"
        txtChallanNo.Focus()
        ClearControls()

    End Sub

    Protected Sub BindGrid()
        Dim amt As Double
        'Dim filter As String = ""
        Dim cmd As New SqlCommand(" select challanTran.TransID,challanTran.tserno,item.item,challanTran.qty,challanTran.weight,challanTran.rate,challanTran.amount from challanTran left join item on challanTran.itmcd=item.itmcd where serno= " & txtSrNo.Text, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        For i = 0 To dt.Rows.Count - 1
            amt += Convert.ToDouble(dt.Rows(i).Item("amount"))    ' ITEM 2 IS THE PRICE.
        Next
        lblGT.Text = amt.ToString("#0.00")

        GridView1.DataSource = dt
        GridView1.DataBind()
        Page.ClientScript.RegisterStartupScript _
            (Me.GetType(), "", "GrandTotal();", True)

    End Sub

    Protected Sub btnSaveChallan_Click(sender As Object, e As System.EventArgs) Handles btnSaveChallan.Click
        Dim tserno, serno, compid, chNo, itmcd, qty As Integer
        Dim uid, dccd, acccd, lotno, year As String
        Dim tamt, weight, Rate, amt As Double
        Dim dat As Date
        Dim createdOn As DateTime
        con.Open()
        If hfItemname.Value <> txtItemName.Text Then
            MsgBox("Item not found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Item Name Check")
            txtItemName.Text = ""
            txtItemName.Focus()
            Exit Sub
        Else
            txtItemName.Focus()
        End If

        'If hfInsert.Value = "0" Then

        year = CType(Session("coyear"), String)

        serno = Integer.Parse(txtSrNo.Text)
        compid = Integer.Parse(Txtcompid.Text)
        uid = Txtuserid.Text
        dccd = Txtdoccd.Text
        chNo = Integer.Parse(txtChallanNo.Text)
        dat = DateTime.ParseExact(txtdate.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        acccd = hfAccode.Value
        lotno = txtLotNo.Text
        tamt = totalAmt
        createdOn = Now.ToString()


        InsertChalMast(serno, compid, uid, dccd, chNo, dat, acccd, lotno, tamt, createdOn, year)

        If hfChallanTranID.Value = "0" Then
            Dim dt As New DataTable()
            dt.Columns.Add("tserno", GetType(Integer))
            dt.Columns.Add("item", GetType(String))
            dt.Columns.Add("qty", GetType(Integer))
            dt.Columns.Add("weight", GetType(Double))
            dt.Columns.Add("rate", GetType(Double))
            dt.Columns.Add("amount", GetType(Double))
            
            Dim cmd As New SqlCommand("select max(tserno) from challanTran where serno='" & txtSrNo.Text.Trim() & "'", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1

            Else
                a = Convert.ToInt32(obj)
                a = a + 1

            End If
            dt.Rows.Add(a, hfItemId.Value, Convert.ToInt64(txtQty.Text), Convert.ToDouble(txtWeight.Text), Convert.ToDouble(txtRate.Text), Convert.ToDouble(Request.Form(txtAmt.UniqueID)))
            serno = txtSrNo.Text
            tserno = Integer.Parse(a)
            itmcd = hfItemId.Value
            qty = Integer.Parse(Convert.ToInt64(txtQty.Text))
            weight = Double.Parse(Convert.ToDouble(txtWeight.Text))
            Rate = Double.Parse(Convert.ToDouble(txtRate.Text))
            amt = Double.Parse(Convert.ToDouble(Request.Form(txtAmt.UniqueID)))
            'GridView3.DataSource = dt
            'GridView3.DataBind()

            InsertRowsInGrid(serno, tserno, itmcd, qty, weight, Rate, amt)


        Else
            Dim s As String
            s = "update challanTran set itmcd=" & hfItemId.Value & " ,qty=" & txtQty.Text & ",weight=" & txtWeight.Text & ",rate=" & txtRate.Text & ",amount=" & txtAmt.Text & " where transid=" & hfChallanTranID.Value & ""
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
            hfChallanTranID.Value = "0"

        End If
        BindGrid()
        con.Close()

        ClearControls()
        txtAcCode.Text = hfAccode.Value
        Page.ClientScript.RegisterStartupScript _
            (Me.GetType(), "", "GrandTotal();", True)
    End Sub

    Private Sub InsertChalMast(ByVal serno As Integer, ByVal compid As Integer, ByVal userid As String, ByVal doccd As String, ByVal challanNo As Integer, ByVal dat As Date, ByVal acccd As String, ByVal lotno As String, ByVal tamt As Double, ByVal CreatedOn As DateTime, ByVal year As String)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("if not exists(select * from challanMast where serno=@serno) begin insert into challanMast (serno, compid, userid, doccd, challanNo, date, acccd, lotno,tamount,CreatedOn,YearId)" &
                                        " values (@serno,@compid, @userid, @doccd, @challanNo, @date, @acccd, @lotno,0.00,@CreatedOn,@YearId) end", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@serno", serno)
                cmd.Parameters.AddWithValue("@compid", compid)
                cmd.Parameters.AddWithValue("@userid", userid)
                cmd.Parameters.AddWithValue("@doccd", doccd)
                cmd.Parameters.AddWithValue("@challanNo", challanNo)
                cmd.Parameters.AddWithValue("@date", dat)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Parameters.AddWithValue("@lotno", lotno)
                cmd.Parameters.AddWithValue("@tamount", tamt)
                cmd.Parameters.AddWithValue("@CreatedOn", CreatedOn)
                cmd.Parameters.AddWithValue("@YearId", year)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

    Private Sub InsertRowsInGrid(ByVal serno As Integer, ByVal tserno As Integer, ByVal itmcd As String, ByVal qty As Double, ByVal weight As Double, ByVal rate As Double, ByVal amt As Double)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("insert into challanTran (serno, tserno, itmcd, qty, weight, rate, amount)" &
                                        " values (@serno, @tserno, @itmcd, @qty, @weight, @rate, @amount)", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@serno", serno)
                cmd.Parameters.AddWithValue("@tserno", tserno)
                cmd.Parameters.AddWithValue("@itmcd", itmcd)
                cmd.Parameters.AddWithValue("@qty", qty)
                cmd.Parameters.AddWithValue("@weight", weight)
                cmd.Parameters.AddWithValue("@rate", rate)
                cmd.Parameters.AddWithValue("@amount", amt)

                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then
            ''txtSrNo.Text = ""
            Dim challanTranId As Integer = e.CommandArgument
            hfChallanTranID.Value = challanTranId
            con.Open()
            Dim cmd1 As New SqlCommand("SELECT ChallanTran.TransID, ChallanTran.itmcd, item.item, item.unit, ChallanTran.qty, ChallanTran.weight, ChallanTran.rate, ChallanTran.amount from ChallanTran left JOIN item on ChallanTran.itmcd = item.itmcd WHERE TransID=" + challanTranId.ToString(), con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim pmastTable As DataTable = ds1.Tables(0)

            hfItemQty.Value = pmastTable.Rows(0).Item("unit")
            hfItemId.Value = pmastTable.Rows(0).Item("itmcd")
            txtItemName.Text = pmastTable.Rows(0).Item("item")
            hfItemname.Value = pmastTable.Rows(0).Item("item")
            txtQty.Text = pmastTable.Rows(0).Item("qty")
            txtWeight.Text = pmastTable.Rows(0).Item("weight")
            txtRate.Text = pmastTable.Rows(0).Item("rate")
            txtAmt.Text = pmastTable.Rows(0).Item("amount")

            'Dim culture1 As New CultureInfo("pt-BR")

            'txt_date.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)

            ''code for drlitmcd to fetch value

            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            BindGrid()

            con.Close()

        End If
        If e.CommandName = "d" Then
           
            con.Open()
            Dim challanTranId As Integer = e.CommandArgument

            Dim intResponse As Integer
            intResponse = MsgBox("Are you sure you want to delete this details ??", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd1 As New SqlCommand("delete from challanTran where TransID=" + challanTranId.ToString() + "", con)
                cmd1.ExecuteNonQuery()

            Else
                Response.Redirect("~/ChallanForm.aspx?id=" & txtSrNo.Text)
            End If
            con.Close()
            BindGrid()

            MsgBox("Challan details Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information + MsgBoxStyle.MsgBoxSetForeground, "Delete")
            
            'ClearConrols()

        End If
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Dim totalAmt As Double
    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)


        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)

            GridView1.Columns(5).FooterText = totalAmt.ToString("#0.00") & "<br/>" & lblGT.Text

            e.Row.Cells(5).Text = totalAmt.ToString("#0.00") & "<br/>" & lblGT.Text
            e.Row.Cells(5).Font.Bold = True

        End If
    End Sub

    Protected Sub btnSaveConfirmChallan_Click(sender As Object, e As System.EventArgs) Handles btnSaveConfirmChallanWRate.Click
        con.Open()
        Dim a As Double
        Dim s, updBy As String
        Dim updOn As DateTime
        a = lblGT.Text
        updBy = Txtuserid.Text
        updOn = Now.ToString()
        s = "update challanMast set challanNo=" & txtChallanNo.Text & ",date='" & txtdate.SelectedDate.ToString() & "',tamount=" & a & ",grossamt=" & txtGrossAmt.Text & ",roundoff=" & txtRoundUp.Text & ",netamt=" & txtNetAmt.Text & ",LastUpdatedOn='" & updOn & "',LastUpdatedBy='" & updBy & "' where serno=" & txtSrNo.Text
        Dim cmd As New SqlCommand(s, con)
        cmd.ExecuteNonQuery()
        con.Close()

        Dim res As Integer
        res = MsgBox("Your data has been saved Successfully " & vbCrLf & "Do you want to print Challan ", MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel + MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Data Saved")
        If res = MsgBoxResult.Yes Then

            Dim url As String = "ChallanInvoice.aspx?id=" & txtSrNo.Text & "-type=withRate"
            Dim sb As New StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.open('")
            sb.Append(url)
            sb.Append("');")
            sb.Append("</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), _
                      "script", sb.ToString())
            BindGrid()

        ElseIf res = MsgBoxResult.Cancel Then
            Response.Redirect("~/ChallanForm.aspx?id=" & txtSrNo.Text)
        ElseIf res = MsgBoxResult.No Then
            Response.Redirect("~/ChallanForm.aspx")
        End If

        'ClearAll()


    End Sub

    Public query As String, constr As String

    Protected Sub btnSaveConfirmChallanWoRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveConfirmChallanWoRate.Click
        con.Open()
        Dim a As Double
        Dim s, updBy As String
        Dim updOn As DateTime
        a = lblGT.Text
        updBy = Txtuserid.Text
        updOn = Now.ToString()
        s = "update challanMast set challanNo=" & txtChallanNo.Text & ",date='" & txtdate.SelectedDate.ToString() & "',tamount=" & a & ",grossamt=" & txtGrossAmt.Text & ",roundoff=" & txtRoundUp.Text & ",netamt=" & txtNetAmt.Text & ",LastUpdatedOn='" & updOn & "',LastUpdatedBy='" & updBy & "' where serno=" & txtSrNo.Text
        Dim cmd As New SqlCommand(s, con)
        cmd.ExecuteNonQuery()
        con.Close()

        Dim res As Integer
        res = MsgBox("Your data has been saved Successfully " & vbCrLf & "Do you want to print Challan ", MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel + MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Data Saved")
        If res = MsgBoxResult.Yes Then

            Dim url As String = "ChallanInvoice.aspx?id=" & txtSrNo.Text & "-type=withoutRate"
            Dim sb As New StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.open('")
            sb.Append(url)
            sb.Append("');")
            sb.Append("</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), _
                      "script", sb.ToString())
            BindGrid()

        ElseIf res = MsgBoxResult.Cancel Then
            Response.Redirect("~/ChallanForm.aspx?id=" & txtSrNo.Text)
        ElseIf res = MsgBoxResult.No Then
            Response.Redirect("~/ChallanForm.aspx")
        End If
    End Sub
End Class
