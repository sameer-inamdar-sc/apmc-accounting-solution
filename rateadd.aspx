﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="rateadd.aspx.vb" Inherits="rateadd"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtacname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txt_itmName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetItems") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txt_itmcode.ClientID %>").val(i.item.val);
                    $("#<%=hfItemId.ClientID %>").val(i.item.val);
                    $("#<%=hfItemname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker();
        });
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style25
        {
            width: 1290px;
        }
        .style26
        {
            width: 161px;
        }
        .hs
        {
            padding-right: 10px;
        }
   
    </style>
    <asp:Panel ID="panel1" runat="server" Width="100%">
    <hr />
       
        <table class="style25">
            <tr>
                <td align="left" colspan="7">
                    <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Rate Details"></asp:Label>
                </td>
                <td align="right">
                    <asp:Button ID="btn_back" runat="server" Text="Back" />
                </td>
            </tr>
            <tr>
                <td align="right" class="style26">
                    &nbsp;
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label3" runat="server" Text="Sr. No."></asp:Label>
                </td>
                <td align="left" class="style26">
                    <asp:TextBox ID="txt_serno" runat="server" Enabled="False" Width="50px"></asp:TextBox>
                    <asp:TextBox ID="Txtuserid" runat="server" Visible="False" Width="50px"></asp:TextBox>
                    <asp:TextBox ID="Txtcompid" runat="server" Visible="False" Width="50px"></asp:TextBox>
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label2" runat="server" Text="Account Name"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtacname" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtacname"
                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label4" runat="server" Text="Date"></asp:Label>
                </td>
                <td align="left" class="style26">
                    <BDP:BasicDatePicker ID="txtdate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox"
                        Width="100px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdate"
                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
                <td align="left" class="style26">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>

    <hr />
    <asp:Panel ID="panel2" runat="server" Width="100%">
        <table width="100%" class="style25">
            <tr>
                <td align="right" class="style26">
                    <asp:HiddenField ID="hfsrno" runat="server" />
                </td>
                <td align="right" class="style26">
                    <asp:HiddenField ID="hfitmserno" runat="server" />
                </td>
                <td align="left" class="style26">
                    <asp:HiddenField ID="hfItemId" runat="server" />
                </td>
                <td align="right" class="style26">
                    <asp:HiddenField ID="hfItemname" runat="server" />
                </td>
                <td align="left" class="style26">
                    <asp:HiddenField ID="hfsaveupdate" runat="server" Value="0" />
                </td>
                <td align="right" class="style26">
                    <asp:HiddenField ID="hfacname" runat="server" />
                </td>
                <td align="left" class="style26">
                    <asp:HiddenField ID="hfacccd" runat="server" />
                </td>
                <td class="style26">
                    <asp:HiddenField ID="hfupdate" runat="server" Value="0" />
                </td>
            </tr>
            <tr>
                <td align="right" class="style26">
                    <asp:Label ID="Label8" runat="server" Text="Sr.No." Visible="False"></asp:Label>
                    <asp:TextBox ID="txt_tserno" runat="server" Visible="False" Width="100px"></asp:TextBox>
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label5" runat="server" Text="Item Code"></asp:Label>
                </td>
                <td align="left" class="style26">
                    <asp:TextBox ID="txt_itmcode" runat="server" Enabled="false" Width="100px"></asp:TextBox>
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label6" runat="server" Text="Item Name"></asp:Label>
                </td>
                <td align="left" class="style26">
                    <asp:TextBox ID="txt_itmName" runat="server" Width="100px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt_itmName" ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
                <td align="right" class="style26">
                    <asp:Label ID="Label7" runat="server" Text="Rate"></asp:Label>
                </td>
                <td align="left" class="style26">
                    <asp:TextBox ID="txtrate" runat="server" class="decimal" Width="100px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txtrate" ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
                <td class="style26">
                    <asp:Button ID="btn_addnew" runat="server" Text="Save" ValidationGroup="Save" />
                </td>
            </tr>
        </table>
    <hr />
    
    </asp:Panel>
    <asp:Panel ID="panel3" runat="server">
        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
            AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
            PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
            <Columns>
                <asp:BoundField HeaderText="srmast" DataField="serno" Visible="false"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:BoundField HeaderText="itmserno" DataField="itmserno" Visible="false"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:BoundField HeaderText="Sr. No." DataField="tserno"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:BoundField HeaderText="Item Code" DataField="itmcd"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:BoundField HeaderText="Item Name" DataField="item"></asp:BoundField>
                <asp:BoundField HeaderText="Unit" DataField="unit"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:BoundField HeaderText="Rate" DataField="rate"><ItemStyle HorizontalAlign="Right" CssClass="hs"  /></asp:BoundField>
                <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>
                        <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                            CommandArgument='<%#Eval("itmserno")%>' />
                        <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                            CommandArgument='<%#Eval("itmserno")%>' />
                            <%--OnClientClick="return confirm('Are you sure want to Delete?');"--%>
                    </ItemTemplate><ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
    </asp:Panel>
    <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false">
        <Columns>
        <asp:BoundField DataField="Row" HeaderText="Row" />
        <asp:TemplateField HeaderText="Shiba1">
        <ItemTemplate>
            <asp:TextBox ID="shibashish1" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Shiba2">
        <ItemTemplate>
            <asp:TextBox ID="shibashish2" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Shiba3">
        <ItemTemplate>
            <asp:TextBox ID="shibashish3" runat="server" ></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>--%><%--<asp:Button ID="AddTextbox" runat="server" Text="AddTextbox" 
            onclick="AddTextbox_Click" />--%>
</asp:Content>
