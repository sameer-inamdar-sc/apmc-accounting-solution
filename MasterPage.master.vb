﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Label1.Text = Session("coname") + " - " + Session("coyear")
        Label2.Text = Session("uname") + " " + Session("authority")
        'lbldatetime.Text = DateTime.Now.ToString("MM-dd-yyyy h:mm:ss tt")
        If Session("userid") = "" Or Session("compid") = "" Then
            Response.Redirect("login.aspx")
        End If

        If Session("authority").ToString().ToLower() = "user" Then
            report.Visible = False
            Utility.Visible = False
        End If
    End Sub

    'Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbldatetime.Tick
    '    lbldatetime.Text = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss tt")
    'End Sub
End Class

