﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Partial Class SalesForm
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtAcName.Focus()

            txtdate.SelectedDate = Now.ToString
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            con.Open()
            sernofun()
            con.Close()

            Dim sr As String = CType(Page.Request.QueryString("id"), String)
            If Not sr = String.Empty Then
                con.Open()
                'btnPrint.Visible = True
                txtChallanNo.Enabled = False
                txtAcName.Enabled = False

                hfSalTranId.Value = sr

                Dim cmd As New SqlCommand("select smast.serno,smast.lotno,smast.challanNo,smast.ChallanDate, smast.InvoiceDate, smast.acccd, a.acname,a.srplace,p.place + ' ' + p.atpost + ' ' + p.tal + ' ' + p.dist as address,cast(cellno1 as varchar(20)) + '/ ' + cast(cellno2 as varchar(20)) as phoneNo, smast.tamount, smast.grossamt, smast.apmc, smast.roundoff, smast.netamt from smast left JOIN acmast a on smast.acccd = a.acccd join place p on a.srplace=p.serno where smast.serno=" & sr, con)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable()
                da.Fill(dt)

                con.Close()
                txtSrNo.Text = sr

                txtChallanNo.Text = dt.Rows(0).Item("challanNo")
                hfChallanNo.Value = dt.Rows(0).Item("challanNo")
                txtdate.SelectedDate = dt.Rows(0).Item("InvoiceDate")
                txtChallanDate.SelectedDate = dt.Rows(0).Item("ChallanDate")
                hfAccode.Value = dt.Rows(0).Item("acccd")
                txtLotNo.Text = dt.Rows(0).Item("lotno")
                txtAcName.Text = dt.Rows(0).Item("acname")
                txtAcCode.Text = dt.Rows(0).Item("acccd")
                lblGT.Text = dt.Rows(0).Item("tamount")
                txtGrandTotal.Text = dt.Rows(0).Item("tamount")
                txtAPMC.Text = dt.Rows(0).Item("apmc")
                txtGrossAmt.Text = dt.Rows(0).Item("grossamt")
                txtRoundUp.Text = dt.Rows(0).Item("roundoff")
                txtNetAmt.Text = dt.Rows(0).Item("netamt")
                lblName.Text = dt.Rows(0).Item("acname")
                lblAddress.Text = dt.Rows(0).Item("address")
                lblPhone.Text = dt.Rows(0).Item("phoneNo")
                lblInvoiceNo.Text = dt.Rows(0).Item("serno")
                lblDate.Text = dt.Rows(0).Item("InvoiceDate")
                lblOrderNo.Text = dt.Rows(0).Item("lotno")

                BindGrid()
                Page.ClientScript.RegisterStartupScript _
                    (Me.GetType(), "", "GT();", True)

            End If
        End If
    End Sub

    Protected Sub BindGrid()
        Dim cmd As New SqlCommand(" select saltran.TransID,saltran.serno,saltran.tserno,item.item,saltran.qty,saltran.weight,saltran.rate,saltran.amount from saltran left join item on saltran.itmcd=item.itmcd where serno= " & txtSrNo.Text, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        'For i = 0 To dt.Rows.Count - 1
        '    amt += Convert.ToDouble(dt.Rows(i).Item("amount"))    ' ITEM 2 IS THE PRICE.
        'Next
        'lblGT.Text = amt.ToString("#0.00")

        GridView1.DataSource = dt
        GridView1.DataBind()

        GridView2.DataSource = dt
        GridView2.DataBind()

        lblTotal.Text = lblGT.Text
        lblAPMC.Text = txtAPMC.Text
        lblRound.Text = txtRoundUp.Text
        lblGTInvoice.Text = txtNetAmt.Text

        'Page.ClientScript.RegisterStartupScript _
        '    (Me.GetType(), "", "GrandTotal();", True)

    End Sub

    Public Sub sernofun()
        Dim cmd As New SqlCommand("select max(serno) from smast where doccd='" & Txtdoccd.Text.Trim() & "'", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            txtSrNo.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            txtSrNo.Text = a.ToString()
        End If
        txtLotNo.Text = Txtdoccd.Text.Trim() + "-" + txtSrNo.Text.Trim().ToString()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("Sales.aspx")
    End Sub


    Protected Sub btnChallanDtls_Click(sender As Object, e As System.EventArgs) Handles btnChallanDtls.Click
        con.Open()
        If txtChallanNo.Text <> hfChallanNo.Value Then
            MsgBox("Challan No. not found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Challan No. Check")
            txtAcName.Text = ""
            txtAcCode.Text = ""
            txtChallanNo.Text = ""
            txtAcName.Focus()
            Exit Sub
        Else
            Dim s As String
            s = "select c.challanNo,c.date,c.acccd,a.acname,c.tamount from challanMast c join acmast a on c.acccd=a.acccd where " & "challanNo=" & txtChallanNo.Text
            Dim cmd As New SqlCommand(s, con)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)

            'txtAcName.Text = dt.Rows(0).Item("acname")
            txtChallanDate.SelectedDate = dt.Rows(0).Item("date")
            txtAcCode.Text = dt.Rows(0).Item("acccd")
            hfAccode.Value = dt.Rows(0).Item("acccd")
            lblGT.Text = dt.Rows(0).Item("tamount")
            BindGridChallan()

            con.Close()
            'txtAcCode.Text = hfAccode.Value
            Page.ClientScript.RegisterStartupScript _
                (Me.GetType(), "", "GrandTotal();", True)
        End If
    End Sub

    Protected Sub BindGridChallan()

        Dim s As String
        s = "select ct.tserno,ct.itmcd,i.item,ct.qty,ct.weight,ct.rate,ct.amount from challanMast cm join challanTran ct on cm.serno=ct.serno join item i on i.itmcd=ct.itmcd where " & " challanNo=" & txtChallanNo.Text
        Dim cmd1 As New SqlCommand(s, con)
        Dim da1 As New SqlDataAdapter(cmd1)
        Dim dt1 As New DataTable
        da1.Fill(dt1)
        GridView1.DataSource = dt1
        GridView1.DataBind()

    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGridChallan()
    End Sub

    Dim totalAmt As Double
    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)


        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)

            GridView1.Columns(5).FooterText = totalAmt.ToString("#0.00") & "<br/>" & lblGT.Text

            e.Row.Cells(5).Text = totalAmt.ToString("#0.00") & "<br/>" & lblGT.Text
            e.Row.Cells(5).Font.Bold = True

        End If
    End Sub

    Protected Sub btnSaveConfirm_Click(sender As Object, e As System.EventArgs) Handles btnSaveConfirm.Click
        SaveSalesInvoice()
        BindGrid()

        Dim res As Integer
        res = MsgBox("Your data has been saved Successfully " & vbCrLf & "Do you want to print Invoice ", MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel + MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Data Saved")
        If res = MsgBoxResult.Yes Then
            
            Dim url As String = "SalesInvoice.aspx?id=" & txtSrNo.Text
            Dim sb As New StringBuilder()
            sb.Append("<script type = 'text/javascript'>")
            sb.Append("window.open('")
            sb.Append(url)
            sb.Append("');")
            sb.Append("</script>")
            ClientScript.RegisterStartupScript(Me.GetType(), _
                      "script", sb.ToString())
            BindGrid()

        ElseIf res = MsgBoxResult.Cancel Then
            Response.Redirect("~/SalesForm.aspx?id=" & txtSrNo.Text)
        ElseIf res = MsgBoxResult.No Then
            Response.Redirect("~/SalesForm.aspx")
        End If
        'ClearAll()
        'Response.Redirect("~/SalesForm.aspx")

        'btnPrint.Visible = True
    End Sub

    Public Sub SaveSalesInvoice()
        Dim serno, compid, chno As Integer
        Dim uid, dccd, acccd, lotno, year As String
        Dim tamount, apmc As Double
        Dim Invdat, chDate As Date
        Dim createdOn As DateTime

        year = CType(Session("coyear"), String)
        serno = txtSrNo.Text
        compid = Integer.Parse(Txtcompid.Text)
        uid = Txtuserid.Text
        dccd = Txtdoccd.Text
        Invdat = DateTime.ParseExact(txtdate.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        acccd = hfAccode.Value
        lotno = txtLotNo.Text
        chno = txtChallanNo.Text
        tamount = lblGT.Text
        chDate = DateTime.ParseExact(txtChallanDate.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        createdOn = Now.ToString()
        apmc = txtAPMC.Text

        InsertSalesMast(serno, compid, uid, dccd, Invdat, acccd, lotno, chno, tamount, chDate, createdOn, year, apmc)

        If hfSalTranId.Value = 0 Then
            Dim s As String
            s = "select ct.tserno,ct.itmcd,ct.qty,ct.weight,ct.rate,ct.amount from challanMast cm join challanTran ct on cm.serno=ct.serno where " & " challanNo=" & txtChallanNo.Text
            Dim cmd As New SqlCommand(s, con)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds1 As New DataSet
            da.Fill(ds1)
            Dim dt As DataTable = ds1.Tables(0)

            For i = 0 To dt.Rows.Count - 1
                Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
                Using con As New SqlConnection(constr)
                    Using cmd1 As New SqlCommand("INSERT INTO saltran(serno,tserno,itmcd,qty,weight,rate,amount) VALUES(@serno,@tserno,@itmcd,@qty,@weight,@rate,@amount)", con)
                        cmd1.Parameters.AddWithValue("@serno", Convert.ToInt32(txtSrNo.Text))
                        cmd1.Parameters.AddWithValue("@tserno", Convert.ToInt32(dt.Rows(i).Item("tserno")))
                        cmd1.Parameters.AddWithValue("@itmcd", Convert.ToInt32(dt.Rows(i).Item("itmcd")))
                        cmd1.Parameters.AddWithValue("@qty", Convert.ToDouble(dt.Rows(i).Item("qty")))
                        cmd1.Parameters.AddWithValue("@weight", Convert.ToDouble(dt.Rows(i).Item("weight")))
                        cmd1.Parameters.AddWithValue("@rate", Convert.ToDouble(dt.Rows(i).Item("rate")))
                        cmd1.Parameters.AddWithValue("@amount", Convert.ToDouble(dt.Rows(i).Item("amount")))
                        con.Open()
                        cmd1.ExecuteScalar()
                        con.Close()
                    End Using
                End Using
            Next
        End If

        hfSalTranId.Value = "1"
        Dim s1, updBy As String
        Dim updOn As DateTime
        updBy = Txtuserid.Text
        updOn = Now.ToString()
        s1 = "update smast set grossamt=" & txtGrossAmt.Text & ",apmc=" & txtAPMC.Text & ",roundoff=" & txtRoundUp.Text & ",netamt=" & txtNetAmt.Text & ",LastUpdatedOn='" & updOn & "',LastUpdatedBy='" & updBy & "' where " & " serno=" & txtSrNo.Text
        Dim cmd2 As New SqlCommand(s1, con)
        con.Open()
        cmd2.ExecuteNonQuery()
        con.Close()
        InsertInLedgerDr()
        InsertInLedgerCr()
    End Sub
    Public query As String

    Public Sub InsertInLedgerDr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtSrNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", txtdate.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd1", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "DR")
        com.Parameters.AddWithValue("@acccd2", "0000063") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", txtNetAmt.Text.ToString())
        com.Parameters.AddWithValue("@chqno", "")
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", txtLotNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Public Sub InsertInLedgerCr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtSrNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", txtdate.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd2", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "CR")
        com.Parameters.AddWithValue("@acccd1", "0000063") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", txtNetAmt.Text.ToString())
        com.Parameters.AddWithValue("@chqno", "")
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", txtLotNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Private Sub InsertSalesMast(ByVal serno As Integer, ByVal compid As Integer, ByVal userid As String, ByVal doccd As String, ByVal dat As Date, ByVal acccd As String, ByVal lotno As String, ByVal chno As Integer, ByVal tamount As Double, ByVal chDate As Date, ByVal CreatedOn As DateTime, ByVal year As String, ByVal apmc As Double)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("if not exists(select * from smast where serno=@serno) begin insert into smast (serno, compid, userid, doccd, InvoiceDate, acccd, lotno,challanNo,ChallanDate,tamount,CreatedOn,YearId,apmc)" &
                                        " values (@serno,@compid, @userid, @doccd, @InvoiceDate, @acccd, @lotno,@challanNo,@ChallanDate,@tamount,@CreatedOn,@YearId,@apmc) end", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@serno", serno)
                cmd.Parameters.AddWithValue("@compid", compid)
                cmd.Parameters.AddWithValue("@userid", userid)
                cmd.Parameters.AddWithValue("@doccd", doccd)
                cmd.Parameters.AddWithValue("@InvoiceDate", dat)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Parameters.AddWithValue("@lotno", lotno)
                cmd.Parameters.AddWithValue("@challanNo", chno)
                cmd.Parameters.AddWithValue("@ChallanDate", chDate)
                cmd.Parameters.AddWithValue("@tamount", tamount)
                cmd.Parameters.AddWithValue("@CreatedOn", CreatedOn)
                cmd.Parameters.AddWithValue("@YearId", year)
                cmd.Parameters.AddWithValue("@apmc", apmc)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub

    Private Sub ClearAll()
        con.Open()
        sernofun()
        con.Close()
        txtdate.SelectedDate = Now.ToString()
        txtChallanDate.Clear()
        txtAcCode.Text = ""
        txtAcName.Text = ""
        txtChallanNo.Text = ""
        hfAccode.Value = ""
        hfAcName.Value = ""

    End Sub

    Dim tAmt As Double
    Protected Sub GridView2_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            tAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(tAmt)


        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            tAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(tAmt)

            GridView2.Columns(4).FooterText = tAmt.ToString("#0.00")

            e.Row.Cells(4).Text = tAmt.ToString("#0.00")
            e.Row.Cells(4).Font.Bold = True
            e.Row.Cells(3).Font.Bold = True
        End If
    End Sub


    'Protected Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
    '    Dim url As String = "SalesInvoice.aspx?id=" & txtSrNo.Text
    '    Dim sb As New StringBuilder()
    '    sb.Append("<script type = 'text/javascript'>")
    '    sb.Append("window.open('")
    '    sb.Append(url)
    '    sb.Append("');")
    '    sb.Append("</script>")
    '    ClientScript.RegisterStartupScript(Me.GetType(), _
    '              "script", sb.ToString())
    '    BindGrid()
    '    'Response.Redirect("~/SalesInvoice.aspx?id=" & txtSrNo.Text)
    'End Sub
End Class
