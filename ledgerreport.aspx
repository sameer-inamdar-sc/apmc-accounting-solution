﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ledgerreport.aspx.vb"
 Inherits="ledgerreport" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   <!--This is Auto completeTextbox javascript/jquery code---------------------------------------->
   <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
   <script type="text/jscript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/jscript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
  
   <%--<script type="text/javascript">
       $(document).ready(function () {
           $("#<%=txtSearchAcName.ClientID %>").autocomplete({
             //  autoFocus: true,
               source: function (request, response) {
                   $.ajax({
                       url: '<%=ResolveUrl("~/Service.asmx/GetledgerAcName") %>',
                       //data: "{ 'prefix': '" + request.term + "'}",
                       data: "{ 'prefix': '" + request.term + "','acccd' : '" + $("#<%=txtacccd.ClientID %>").val() + "'}",
                       dataType: "json",
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       success: function (data) {
                           response($.map(data.d, function (item) {
                               return {
                                   label: item.split('-')[0],
                                   val: item.split('-')[1]
                               }
                           }))
                       },
                       error: function (response) {
                           alert(response.responseText);
                       },
                       failure: function (response) {
                           alert(response.responseText);
                       }
                   });
               },
               select: function (e, i) {
                   $("#<%=txtSearchAcName.ClientID %>").val(i.item.val);
                   $("#<%=txtacccd.ClientID %>").val(i.item.val);
                //   $("#<%=hfacccd.ClientID %>").val(i.item.label);
                   $("#<%=hfSearchAcName.ClientID %>").val(i.item.label);

               },

               minLength: 1

           }); 

       }); 
      
    </script>--%>
    

    <!--End This is Auto completeTextbox javascript/jquery code---------------------------------------->
    <table width="100%">
        <tr>
            <td colspan="5" align="left">
                <asp:Label ID="Label2" runat="server" Text="Ledger Report" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right" colspan="5">
                <asp:LinkButton ID="Btn_Print" href="javascript:__doPostBack('Btn_Print_Click','')" runat="server" Text="Print" />
            </td>
        </tr>
    </table>
    <hr />
    <table width="100%">
        <tr>
            <%--<td align="left">
                <asp:Label ID="Label3" runat="server" Text="Sales Invoice no."></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
            </td>--%>
            <td align="left">
                <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
                                
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchAcName" runat="server" class="autosuggest"></asp:TextBox>                   
                <!--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSearchAcName"
                    ErrorMessage="*"></asp:RequiredFieldValidator> -->
                   
            </td>
            <td align="left">
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtacccd" runat="server" Width="81px" Enabled="false">0000077</asp:TextBox>
            </td>
       
            <td>
                    </td>
                    
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
            </td>
            <td align="left">
                <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
            </td>
            <td align="left">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" />
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                <asp:HiddenField ID="hfSearchAcName" runat="server" /></td>
            <td align="left">
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="hfacccd" runat="server" /></td>
       
            <td>
                    &nbsp;</td>
                    
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
        </tr>
    </table>
    <div>
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
    </div>
</asp:Content>

