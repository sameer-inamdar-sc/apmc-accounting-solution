﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing

Partial Class SalesRegister
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Public dt As DataTable
    Dim filter As String = ""
    Dim sbPageString As New StringBuilder()

    Protected Sub btnGenerateReport_Click(sender As Object, e As System.EventArgs) Handles btnGenerateReport.Click
        ShowSalesRegister()
    End Sub

    Public Sub ShowSalesRegister()
        filter = ""
        If txtSearchInvoice.Text <> "" Then
            filter = "and m.serno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and invoiceDate BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "select m.serno,m.challanNo,m.invoiceDate,m.acccd,a.acname,m.lotno,m.tamount,m.grossamt,m.apmc,m.roundoff,m.netamt,t.transid,t.tserno,t.itmcd,i.item,t.qty,t.weight,t.rate,t.amount from smast m join saltran t on m.serno=t.serno join item i on i.itmcd=t.itmcd join acmast a on a.acccd=m.acccd " & filter & " ORDER BY serno DESC"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)

        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetSalesRegister(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
    End Sub

    Public Function GetSalesRegister(ByVal dt As DataTable) As String
        Dim a As Integer
        Dim amt As Double = 0
        Dim pd As New PrintDocument()
        Dim pg As PageSettings = pd.DefaultPageSettings
        Dim StrPrinter As String = ""
        pg.Landscape = True
        pg.Margins.Top = 30
        pg.Margins.Bottom = 30
        pg.Margins.Left = 30
        pg.Margins.Right = 30

        ''pg = New PageSettings(sbPageString)

        pg.PaperSize = New System.Drawing.Printing.PaperSize(System.Drawing.Printing.PaperKind.A4, 210, 297) ''Paper Size in MiliMeter
        pg.PrinterSettings.PrinterName = StrPrinter

       
        sbPageString.Append("<style> .bottom{position: absolute;bottom: 0px;}th{border-bottom: 1px solid;border-left: 1px solid;}th:first-child{border-left: 0px solid;}</style>")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;border-bottom: 0px solid;'><tr align='center'><th width='3%'>Sr No.</th><th width='2%'>Invoice No.</th><th width='5%'>Challan No</th><th width='8%'>Invoice date</th><th width='7%'>Account Code</th><th width='20%'>Account Name</th><th width='2%'>Sr No.</th><th width='12%'>Item Name</th><th width='7%'>Quantity</th><th width='10%'>Weight</th><th width='9%'>Rate</th><th width='15%'>Amount</th></tr>")

        For i = 0 To dt.Rows.Count - 1
            a = a + 1
            Dim invDate As String
            invDate = dt.Rows(i).Item("invoiceDate")

            Dim invoiceDateArray As String()
            invoiceDateArray = invDate.Split("/")

            invDate = invoiceDateArray(1) & "/" & invoiceDateArray(0) & "/" & invoiceDateArray(2)


            sbPageString.Append("<tr><td width='3%'>" & a & "</td><td width='2%'>" & dt.Rows(i).Item("serno") & "</td><td width='5%'>" & dt.Rows(i).Item("challanNo") & "</td><td width='8%' align='center'>" & invDate & "</td><td width='7%'>" & dt.Rows(i).Item("acccd") & "</td><td width='20%'>" & dt.Rows(i).Item("acname") & "</td><td width='2%' align='center'>" & dt.Rows(i).Item("tserno") & "</td><td width='12%'>" & dt.Rows(i).Item("item") & "</td><td width='7%' align='right'>" & dt.Rows(i).Item("qty") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("weight") & "</td><td width='9%' align='right'>" & dt.Rows(i).Item("rate") & "</td><td width='15%' align='right'>" & dt.Rows(i).Item("amount") & "</td></tr>")

            amt += Convert.ToDouble(dt.Rows(i).Item("amount"))
        Next

        sbPageString.Append("<tr style='font-weight: bold;border: 1px solid;border-top: 0px solid;'><td width='85%' colspan='11' align='right' style='border-top: 1px solid;border-right: 1px solid;border-left: 1px solid;'>Total</td><td width='15%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & amt.ToString("#0.00") & "</td></tr></table>")

        'sbPageString.Append("<br><caption><hr /></caption><table width='50%' style='font-weight: bold;'><tr><td>APMC</td><td>" & dt.Rows(0).Item("apmc") & "</td></tr><tr><td>Net Amount</td><td>" & dt.Rows(0).Item("netamt") & "</td></tr></table>")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")

        Return sbPageString.ToString()
    End Function
    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Protected Sub Btn_Print_Click(sender As Object, e As System.EventArgs) Handles Btn_Print.Click
        Dim fd, td, today As String
        fd = txtSearchFromDate.SelectedDate
        td = txtSearchToDate.SelectedDate
        today = Now.ToShortDateString()

        Dim fromDateArray, toDateArray, todayArray As String()
        fromDateArray = fd.Split("/")
        toDateArray = td.Split("/")
        todayArray = today.Split("/")
        today = todayArray(1) & "/" & todayArray(0) & "/" & todayArray(2)

        If txtSearchFromDate.Text = "" Then
            fd = Nothing
        Else
            fd = fromDateArray(1) & "/" & fromDateArray(0) & "/" & fromDateArray(2)
        End If
        If txtSearchToDate.Text = "" Then
            td = Nothing
        Else
            td = toDateArray(1) & "/" & toDateArray(0) & "/" & toDateArray(2)
        End If
        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        sbPageString.Append("<table><tr><td width='10%'>From Date </td><td width='10%'>" & fd & "</td><td width='10%'>To Date </td><td width='55%'>" & td & "</td><td width='10%'>Print Date </td><td width='10%'>" & today & "</td></tr></table><caption><hr /></caption>")

        If txtSearchInvoice.Text <> "" Then
            filter = "and m.serno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and invoiceDate BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "select m.serno,m.challanNo,m.invoiceDate,m.acccd,a.acname,m.lotno,m.tamount,m.grossamt,m.apmc,m.roundoff,m.netamt,t.transid,t.tserno,t.itmcd,i.item,t.qty,t.weight,t.rate,t.amount from smast m join saltran t on m.serno=t.serno join item i on i.itmcd=t.itmcd join acmast a on a.acccd=m.acccd " & filter & " ORDER BY serno DESC"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)
        GetSalesRegister(dt)

        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(sbPageString.ToString())
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), "Print", sb.ToString())
        sbPageString.Clear()
        btnGenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim s1 As String
            s1 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s1, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")


            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txtSearchFromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txtSearchToDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)
        End If
    End Sub
End Class
