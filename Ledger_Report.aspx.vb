﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic
Imports System.Drawing.Printing
Imports System.Web.Services
Partial Class Ledger_Report
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Dim filter As String = ""
    Dim dt As DataTable
    Dim sbPageString As New StringBuilder()



   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim s1 As String
            s1 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s1, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")


            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txtFromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txtToDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)
        End If
    End Sub
    Public Function GetLedgerRegister(ByVal dt As Object) As String
        Dim a As Integer
        Dim cramt, dramt As Double
        Dim acname As String
        acname = ""
        sbPageString.Append("<style> .bottom{position: absolute;bottom: 0px;}th{border-bottom: 1px solid;border-left: 1px solid;}th:first-child{border-left: 0px solid;}</style>")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;border-bottom: 0px solid;'><tr align='center' ><th width='3%'>Sr No.</th><th width='5%'>Date</th><th width='20%'>Cr Account</th><th width='10%'>Cr Amount</th><th width='10%'>Date</th><th width='10%'>Dr Account</th><th width='10%'>Dr Amount</th></tr>")
        For i = 0 To dt.Rows.Count - 1
            a = a + 1
            'If acname = dt.Rows(i).Item("acname") Then
            '    dt.Rows(i).Item("acname") = ""
            'Else
            '    acname = dt.Rows(i).Item("acname")

            'End If
            'Dim billDate As String
            'billDate = dt.Rows(i).Item("invoiceDate")

            'Dim billDateArray As String()
            'billDateArray = billDate.Split("/")

            'billDate = billDateArray(1) & "/" & billDateArray(0) & "/" & billDateArray(2)
            cramt += Convert.ToDouble(dt.Rows(i).Item("column2"))
            dramt += Convert.ToDouble(dt.Rows(i).Item("column4"))

            sbPageString.Append("<tr><td width='3%' align='center'>" & a & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & dt.Rows(i).Item("column1") & "</td><td width='20%' style='padding-left: 8px;'>" & dt.Rows(i).Item("crefno") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("column2") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("column3") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("drefno") & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & dt.Rows(i).Item("column4") & "</td></tr>")
            'amt += Convert.ToDouble(dt.Rows(i).Item("netamt"))
        Next
        sbPageString.Append("<tr style='font-weight: bold;'><td width='74%' colspan='2' align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & "</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & cramt.ToString("#0.00") & "</td><td  align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & "</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & dramt.ToString("#0.00") & "</td></tr></table>")

        Return sbPageString.ToString()
    End Function
    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Protected Sub Btn_Print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Print.Click
        Dim fd, td, today As String
        fd = txtfromdate.SelectedDate
        td = txttodate.SelectedDate
        today = Now.ToShortDateString()

        Dim fromDateArray, toDateArray, todayArray As String()
        fromDateArray = fd.Split("/")
        toDateArray = td.Split("/")
        todayArray = today.Split("/")
        today = todayArray(1) & "/" & todayArray(0) & "/" & todayArray(2)

        If txtfromdate.Text = "" Then
            fd = Nothing
        Else
            fd = fromDateArray(1) & "/" & fromDateArray(0) & "/" & fromDateArray(2)
        End If
        If txttodate.Text = "" Then
            td = Nothing
        Else
            td = toDateArray(1) & "/" & toDateArray(0) & "/" & toDateArray(2)
        End If

        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        sbPageString.Append("<table><tr><td width='10%'>From Date </td><td width='10%'>" & fd & "</td><td width='10%'>To Date </td><td width='55%'>" & td & "</td><td width='10%'>Print Date </td><td width='10%'>" & today & "</td></tr></table><caption><hr /></caption>")
        sbPageString.Append("<table><tr><td width='20%'>Account Name    </td><td width='30%'>" & txtacname.Text & "</td><td width='40%'></td><td width='10%'></td><td width='10%'>Place </td><td width='30%'>" & hfacccd.Value.Trim() & "</td></tr></table><caption><hr /></caption>")

        If txtacname.Text <> "" Then
            filter = "and a.acname like '" & txtacname.Text & "%'"
        End If

        If (txtfromdate.Text <> "" Or txtfromdate.SelectedDateFormatted.ToString() <> "") And (txttodate.Text <> "" Or txttodate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and invoiceDate BETWEEN '" & txtfromdate.SelectedDate & "' AND '" & txttodate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "ledgerreport"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@fdate", txtfromdate.SelectedDate)
        cmd.Parameters.AddWithValue("@tdate", txttodate.SelectedDate)
        cmd.Parameters.AddWithValue("@acccd", hfacccd.Value.Trim())
        da.Fill(dt)

        GetLedgerRegister(dt)



        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(sbPageString.ToString())
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), "Print", sb.ToString())
        sbPageString.Clear()
    End Sub
End Class
