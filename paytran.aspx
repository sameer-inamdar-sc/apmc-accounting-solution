﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="paytran.aspx.vb" Inherits="paytran"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtAcName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcc") %>',
                        data: "{ 'prefix': '" + request.term + "', 'prefix1': '" + $("#<%=hftype.ClientID %>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtAcCode.ClientID %>").val(i.item.val);
                    $("#<%=hfAccode.ClientID %>").val(i.item.val);
                    $("#<%=hfAcName.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });

        });

 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Txt_Amt").change(function () {
                $("#ctl00_ContentPlaceHolder1_GridView1 tr:last-child td:last-child")[0].innerHTML = this.value;
                var _this = this;
                var ramt = _this.value;
                $("#ctl00_ContentPlaceHolder1_GridView1 tr").each(function () {
                    var textBox = $(this).find("input[type='text']");

                    $(textBox).change(function () {
                        var total = 0;
                        $("#ctl00_ContentPlaceHolder1_GridView1 tr").each(function () {
                            var textBox1 = $(this).find("input[type='text']");
                            if (textBox1.length) {
                                total = total + parseInt(textBox1[0].value);
                            }

                            $("#ctl00_ContentPlaceHolder1_GridView1 tr:last-child td:last-child")[0].innerHTML = total;
                        });
                        });

                        var amount = $(this)[0].children[3].innerHTML;
                        var paidamt = $(this)[0].children[4].innerHTML;
                        var bal = amount - paidamt;
                        if (_this.value >= 0) {

                            if (bal < _this.value) {
                                textBox.val(bal)
                                _this.value = _this.value - bal;
                            }
                            else {
                                if (bal >= _this.value) {
                                    textBox.val(_this.value)
                                    _this.value = _this.value - bal;
                                }
                                else return;


                            }
                        }
                        else textBox.val("0");
                    });
                    _this.value = ramt;
                });
            });
            </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtbankname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetbankName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfacccd1.ClientID %>").val(i.item.val);
                    $("#<%=hfacname1.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });

            $("#Div_ChequeDetails").hide();

            $("#<%=drlcb.ClientID %>").change(function () {
                if ($("#ctl00_ContentPlaceHolder1_drlcb")[0].value == "Bn") {
                    $("#Div_ChequeDetails").show();
                }
                else $("#Div_ChequeDetails").hide();

            })
        }); 
    </script>
    <script type="text/javascript">

      
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style10
        {
            width: 1267px;
        }
        .style18
        {
        }
        .style19
        {
            width: 127px;
        }
        .style20
        {
            width: 115px;
        }
        .style21
        {
            width: 52px;
        }
        .style22
        {
            height: 30px;
        }
        .style23
        {
            width: 127px;
            height: 30px;
        }
    </style>
    <table class="style10">
        <tr>
            <td align="left" class="style20">
                <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Payment"></asp:Label>
            </td>
            <td align="left" colspan="5" height="10px" style="margin-left: 40px">
                &nbsp;<asp:TextBox ID="txtserno" runat="server" Enabled="False" Width="70px"></asp:TextBox>
                &nbsp;<asp:TextBox ID="Txtcompid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                <asp:TextBox ID="Txtuserid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
            </td>
            <td class="style19" align="right">
                &nbsp;</td>
            <td class="style19" align="left">
                &nbsp;</td>
            <td align="right" class="style19">
                <asp:Button ID="btnBack" runat="server" Text="Back" align="right" Height="20px" Width="60px" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style20">
                <asp:Label ID="Label3" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtAcName" runat="server" Width="250px" Style="margin-left: 2px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAcName"
                    ErrorMessage="*" ValidationGroup="save"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtAcCode" runat="server" Enabled="False" Width="70px"></asp:TextBox>
            </td>
            <td align="right" class="style18">
                <asp:Label ID="Label4" runat="server" Text="Date"></asp:Label>
            </td>
            <td align="left" class="style19">
                <BDP:BasicDatePicker ID="txt_Date" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="right" class="style19">
                <asp:HiddenField ID="hftype" runat="server" Value="0" />
            </td>
            <td class="style19">
                <asp:HiddenField ID="hfAccode" runat="server" Value="0" />
            </td>
            <td class="style19">
                <asp:HiddenField ID="hfAcName" runat="server" />
            </td>
            <td align="right" class="style19">
                <asp:HiddenField ID="hftransactionmasterid" runat="server" Value="0" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style20">
                <asp:Label ID="Label5" runat="server" Text="Cash / Bank"></asp:Label>
            </td>
            <td align="left" class="style18">
                <asp:DropDownList ID="drlcb" runat="server" Enabled="False">
                    <asp:ListItem Selected="True" Value="Cs">Cash</asp:ListItem>
                    <asp:ListItem Value="Bn">Bank</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right" class="style21">
                &nbsp;
            </td>
            <td align="right" class="style18">
                <asp:Label ID="Label6" runat="server" Text="Amount"></asp:Label>
            </td>
            <td align="left" class="style19">
                <asp:TextBox ID="Txt_Amt" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td align="right" class="style19">
                <asp:Button ID="Btn_GetBills" runat="server" Text="Get Bills" ValidationGroup="save" />
            </td>
            <td class="style19">
                <asp:Button ID="Btn_clear" runat="server" Text="Reset" />
            </td>
            <td class="style19">
                <asp:HiddenField ID="hfacname1" runat="server" />
            </td>
            <td align="right" class="style19">
                <asp:HiddenField ID="hfacccd1" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <div id="Div_ChequeDetails" visible="True">
        <table width="100%">
            <tr>
                <td align="right" class="style22">
                    <asp:Label ID="Label7" runat="server" Text="Bank Name"></asp:Label>
                </td>
                <td align="left" class="style22">
                    <asp:TextBox ID="txtbankname" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td align="right" class="style22">
                    <asp:Label ID="Label18" runat="server" Text="Bank Branch" Visible="False"></asp:Label>
                    &nbsp;
                </td>
                <td align="right" class="style22">
                    <asp:TextBox ID="txtbankBranch" runat="server" Width="200px" Visible="False"></asp:TextBox>
                    &nbsp;
                </td>
                <td align="right" class="style22">
                    <asp:Label ID="Label17" runat="server" Text="Cheque No."></asp:Label>
                </td>
                <td align="right" class="style23">
                    <asp:TextBox ID="txtchqno0" runat="server" Width="50px" Style="margin-bottom: 0px"></asp:TextBox>
                </td>
                <td align="right" class="style23">
                    <asp:Label ID="Label16" runat="server" Text="Cheque Date"></asp:Label>
                </td>
                <td class="style23">
                    <BDP:BasicDatePicker ID="txtdate0" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                </td>
            </tr>
        </table>
    </div>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="true" OnRowDataBound="GridView1_RowDataBound"
        HorizontalAlign="Center" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
        EmptyDataRowStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White"
        BorderStyle="None" ShowHeaderWhenEmpty="True" ShowFooter="True">
        <Columns>
            <asp:TemplateField HeaderText="Balance" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Style="text-align: right;" Height="10px"
                        Width="50"></asp:TextBox>
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle HorizontalAlign="Center" />
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle Height="10px" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="BtnSave" runat="server" Text="Save" align="right" Height="20px" Width="60px"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td align="right">
                &nbsp;
            </td>
            <td align="right">
                <asp:Label ID="Lbl_unpaid" runat="server" Text="Unpaid Amount :  " 
                    Visible="False"></asp:Label>
                &nbsp;
            </td>
            <td align="left">
                <asp:Label ID="Lbl_unpaidAmt" runat="server" Text="0.00" Visible="False"></asp:Label>
                &nbsp;
            </td>
        </tr>
    </table>
    <asp:GridView ID="gridview2" runat="server" AutoGenerateColumns="true" OnRowDataBound="GridView2_RowDataBound"
        HorizontalAlign="Center" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
        EmptyDataRowStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White"
        BorderStyle="None" ShowHeaderWhenEmpty="True" ShowFooter="True">
        <Columns>
            <asp:TemplateField HeaderText="Balance" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                        OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("serno")%>' />
                </ItemTemplate>
                <FooterTemplate>
                </FooterTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle HorizontalAlign="Center" />
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle Height="10px" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>
</asp:Content>
