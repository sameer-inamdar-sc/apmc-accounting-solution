﻿
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Class PurchaseForm
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Me.btnBack.Attributes.Add("onclick", "return ConfirmSubmit();")

        If Not Page.IsPostBack Then
            hfConfirmClickeOrNot.Value = "0"
            txtBillNo.Focus()
            btnSaveTran.Visible = False
            txtdate.SelectedDate = Now().ToString
            txtAPMC.Text = String.Format("{0:n2}", 0.0)
            txtRate0.Text = String.Format("{0:n2}", 0.0)

            'txtTolai.Text = String.Format("{0:n2}", 0.0)
            'txtRoundup.Text = String.Format("{0:n2}", 0.0)

            Txtmjcd.Text = "L4"
            con.Open()
            sernofun()
            con.Close()
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")

            Dim sr As String = CType(Page.Request.QueryString("id"), String)
            If Not sr = String.Empty Then
                con.Open()

                Dim cmd As New SqlCommand("select pmast.pbillno, pmast.date, pmast.acccd, acmast.acname, pmast.lotno, pmast.tbharai, pmast.tutrai, pmast.ttolai, pmast.tmotorfrt, pmast.trnscd, (SELECT acmast.acname FROM acmast INNER JOIN pmast ON acmast.acccd = pmast.trnscd WHERE pmast.serno=" & sr & ") as transName , pmast.grossamt, pmast.roundoff, purtran.amount+pmast.tbharai+pmast.tutrai+lapmc+lmaplevy+lhamali+pmast.ttolai+pmast.tmotorfrt netamt from purtran right join pmast on purtran.serno= pmast.serno left join acmast on pmast.acccd=acmast.acccd left join item on purtran.itmcd=item.itmcd where pmast.serno=" & sr, con)

                Dim da As New SqlDataAdapter(cmd)
                Dim dt As New DataTable()
                da.Fill(dt)
                'GridView3.DataSource = dt
                'GridView3.DataBind()
                con.Close()
                txtSrNo.Text = sr
                txtItemName.Focus()

                txtBillNo.Text = dt.Rows(0).Item("pbillno")
                txtdate.SelectedDate = dt.Rows(0).Item("date")
                hfAccode.Value = dt.Rows(0).Item("acccd")
                txtLotNo.Text = dt.Rows(0).Item("lotno")
                txtAcName.Text = dt.Rows(0).Item("acname")
                txtAcCode.Text = dt.Rows(0).Item("acccd")
                hfAccode.Value = txtAcCode.Text
                txtItemName.Text = ""
                txtQty.Text = "0"
                txtWeight.Text = "0"
                txtRate0.Text = "0.00"
                txtAmt.Text = "0.00"
                txtBharai.Text = dt.Rows(0).Item("tbharai")
                txtUtarai.Text = dt.Rows(0).Item("tutrai")
                txtTolai.Text = dt.Rows(0).Item("ttolai")
                txtFreight.Text = dt.Rows(0).Item("tmotorfrt")
                hfTransId.Value = dt.Rows(0).Item("trnscd")
                If hfTransId.Value <> "" Then
                    txtTransName.Text = dt.Rows(0).Item("transName")
                End If
                txtGrossAmt.Text = dt.Rows(0).Item("grossamt")
                txtRoundup.Text = dt.Rows(0).Item("roundoff")
                txtNetAmt.Text = dt.Rows(0).Item("netamt")

                BindGrid()
                'txtFooterAmt.Text = GridView3.Columns(5).FooterText
                'txtFooterAPMC.Text = GridView3.Columns(6).FooterText
                'txtFooterHamali.Text = GridView3.Columns(7).FooterText
                'txtFooterMapLevy.Text = GridView3.Columns(8).FooterText

                'GridView3.DataSource = dt
                'GridView3.DataBind()

                'sr = String.Empty
                btnSaveTran.Visible = True

            End If
        End If

    End Sub
    Public Sub sernofun()
        Dim cmd As New SqlCommand("select max(serno) from pmast where doccd='" & Txtdoccd.Text.Trim() & "'", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            txtSrNo.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            txtSrNo.Text = a.ToString()
        End If
        txtLotNo.Text = Txtdoccd.Text.Trim() + "-" + txtSrNo.Text.Trim().ToString()
    End Sub


    Protected Sub txtBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("purchase.aspx")
    End Sub

    Protected Sub btnSaveInGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInGrid.Click


        'If hfInsert.Value <> "0" Then
        '    con.Open()
        '    Dim cmd1 As New SqlCommand("select MAX(tserno) from purtran where serno=" & txtSrNo.Text, con)
        '    i = cmd1.ExecuteScalar()
        '    con.Close()
        'End If
        Dim d As Double = 0
        Dim uid, dccd, acccd, lotno, trnscd As String
        Dim dat As Date
        Dim tserno, serno, compid, itmcd, qty, pbill As Integer
        Dim weight, Rate, mapLevy As Double
        con.Open()
        If hfItemname.Value <> txtItemName.Text Then
            MsgBox("Item not found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Item Name Check")
            txtItemName.Text = ""
            txtItemName.Focus()
            Exit Sub
        Else
            txtItemName.Focus()
        End If

        'If hfInsert.Value = "0" Then
        Dim year As String
        year = CType(Session("coyear"), String)

        serno = Integer.Parse(txtSrNo.Text)
        compid = Integer.Parse(Txtcompid.Text)
        uid = Txtuserid.Text
        dccd = Txtdoccd.Text
        pbill = Integer.Parse(txtBillNo.Text)

        dat = DateTime.ParseExact(txtdate.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        acccd = hfAccode.Value
        lotno = txtLotNo.Text
        'hfInsert.Value = "1"
        InsertPurMast(serno, compid, uid, dccd, pbill, dat, acccd, lotno)
        'End If

        If hfPurTranID.Value = "0" Then
            Dim dt As New DataTable()
            dt.Columns.Add("tserno", GetType(Integer))
            dt.Columns.Add("item", GetType(String))
            dt.Columns.Add("qty", GetType(Integer))
            dt.Columns.Add("weight", GetType(Double))
            dt.Columns.Add("rate", GetType(Double))
            dt.Columns.Add("amount", GetType(Double))
            dt.Columns.Add("lapmc", GetType(Double))
            dt.Columns.Add("lhamali", GetType(Double))
            dt.Columns.Add("lmaplevy", GetType(Double))

            Dim cmd As New SqlCommand("select max(tserno) from purtran where serno='" & txtSrNo.Text.Trim() & "'", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1

            Else
                a = Convert.ToInt32(obj)
                a = a + 1

            End If
            dt.Rows.Add(a, hfItemId.Value, Convert.ToInt64(txtQty.Text), Convert.ToDouble(txtWeight.Text), Convert.ToDouble(txtRate0.Text), Convert.ToDouble(Request.Form(txtAmt.UniqueID)), Convert.ToDouble(Request.Form(hfAPMC.UniqueID)), Convert.ToDouble(Request.Form(hfHamali.UniqueID)), Convert.ToDouble(Request.Form(hfMapLevy.UniqueID)))
            serno = txtSrNo.Text
            tserno = Integer.Parse(a)
            itmcd = hfItemId.Value
            qty = Integer.Parse(Convert.ToInt64(txtQty.Text))
            weight = Double.Parse(Convert.ToDouble(txtWeight.Text))
            Rate = Double.Parse(Convert.ToDouble(txtRate0.Text))
            amt = Double.Parse(Convert.ToDouble(Request.Form(txtAmt.UniqueID)))
            apmc = Double.Parse(Convert.ToDouble(Request.Form(hfAPMC.UniqueID)))
            hamali = Double.Parse(Convert.ToDouble(Request.Form(hfHamali.UniqueID)))
            mapLevy = Double.Parse(Convert.ToDouble(Request.Form(hfMapLevy.UniqueID)))
            'GridView3.DataSource = dt
            'GridView3.DataBind()

            InsertRows(serno, tserno, itmcd, qty, weight, Rate, amt, apmc, hamali, mapLevy)


        Else
            Dim s As String
            s = "update purtran set itmcd=" & hfItemId.Value & " ,qty=" & txtQty.Text & ",weight=" & txtWeight.Text & ",rate=" & txtRate0.Text & ",amount=" & txtAmt.Text & ",lapmc=" & hfAPMC.Value & ",lmaplevy=" & hfMapLevy.Value & ",lhamali=" & hfHamali.Value & " where transid=" & hfPurTranID.Value & ""
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
            hfPurTranID.Value = "0"

        End If
        BindGrid()
        'ClearConrols()
        ' MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Save")
        con.Close()


        btnSaveTran.Visible = True
        ClearConrols()
        txtAcCode.Text = hfAccode.Value
    End Sub
    Protected Sub BindGrid()
        Dim amt, ap, ham, map As Double
        Dim filter As String = ""
        Dim cmd As New SqlCommand("select purtran.TransID,purtran.tserno,item.item,purtran.qty,purtran.weight,purtran.rate,purtran.amount,purtran.lapmc,purtran.lhamali,purtran.lmaplevy from purtran left join item on purtran.itmcd=item.itmcd where serno= " & txtSrNo.Text, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        For i = 0 To dt.Rows.Count - 1
            amt += Convert.ToDouble(dt.Rows(i).Item("amount"))    ' ITEM 2 IS THE PRICE.
            ap += Convert.ToDouble(dt.Rows(i).Item("lapmc"))
            ham += Convert.ToDouble(dt.Rows(i).Item("lhamali"))
            map += Convert.ToDouble(dt.Rows(i).Item("lmaplevy"))
        Next
        lblAmount.Text = amt.ToString("#0.00")
        lblAPMC.Text = ap.ToString("#0.00")
        lblHamali.Text = ham.ToString("#0.00")
        lblMapLevy.Text = map.ToString("#0.0000")

        GridView3.DataSource = dt
        GridView3.DataBind()
        Page.ClientScript.RegisterStartupScript _
            (Me.GetType(), "", "GrandTotal();", True)

    End Sub
    Protected Sub btnSaveTran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTran.Click
        con.Open()
        Dim a, ap, h, ml As Double
        a = lblAmount.Text
        ap = lblAPMC.Text
        h = lblHamali.Text
        ml = lblMapLevy.Text
        If txtTransName.Text = "" Then
            hfTransId.Value = ""
        End If
        If txtBharai.Text = "" Then
            txtBharai.Text = "0.00"
        End If
        If txtUtarai.Text = "" Then
            txtUtarai.Text = "0.00"
        End If
        If txtTolai.Text = "" Then
            txtTolai.Text = "0.00"
        End If
        If txtFreight.Text = "" Then
            txtFreight.Text = "0.00"
        End If
        Dim s As String
        s = "update pmast set pbillno=" & txtBillNo.Text & ",date='" & txtdate.SelectedDate & "', ttolai=" & txtTolai.Text & ",tbharai=" & txtBharai.Text & ",tutrai=" & txtUtarai.Text & ",tmotorfrt=" & txtFreight.Text & ",grossamt=" & txtGrossAmt.Text & ",roundoff=" & txtRoundup.Text & ",netamt=" & txtNetAmt.Text & ",tamount=" & a & ",tapmc=" & ap & ",thamali=" & h & ",tmapLevy=" & ml & " ,trnscd='" & hfTransId.Value & "' where serno=" & txtSrNo.Text
        Dim cmd As New SqlCommand(s, con)
        cmd.ExecuteNonQuery()
        con.Close()

        InsertInLedgerCr()
        InsertInLedgerDr()



        MsgBox("Your data has been saved Successfully ", MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly + MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Data Saved")

        ClearAll()

        If (String.IsNullOrEmpty(Page.Request.QueryString("id"))) Then
            Response.Redirect("PurchaseForm.aspx")
        Else
            Response.Redirect("Purchase.aspx")

        End If
        hfConfirmClickeOrNot.Value = "1"
        ClearAll()
        'hfInsert.Value = "0"
    End Sub

    Public query As String, constr As String

    Public Sub InsertInLedgerCr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtSrNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", txtdate.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd1", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "CR")
        com.Parameters.AddWithValue("@acccd2", "0000062") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", txtNetAmt.Text.ToString())
        com.Parameters.AddWithValue("@chqno", "")
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", txtLotNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Public Sub InsertInLedgerDr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtSrNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", txtdate.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd2", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "DR")
        com.Parameters.AddWithValue("@acccd1", "0000062") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", txtNetAmt.Text.ToString())
        com.Parameters.AddWithValue("@chqno", "")
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", txtLotNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
  
    Private Sub InsertRows(ByVal serno As Integer, ByVal tserno As Integer, ByVal itmcd As String, ByVal qty As Double, ByVal weight As Double, ByVal rate As Double, ByVal amt As Double, ByVal apmc As Double, ByVal hamali As Double, ByVal mapLevy As Double)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("insert into purtran (serno, tserno, itmcd, qty, weight, rate, amount, lapmc, lmaplevy, lhamali)" &
                                        " values (@serno, @tserno, @itmcd, @qty, @weight, @rate, @amount, @lapmc, @lmaplevy, @lhamali)", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@serno", serno)
                cmd.Parameters.AddWithValue("@tserno", tserno)
                cmd.Parameters.AddWithValue("@itmcd", itmcd)
                cmd.Parameters.AddWithValue("@qty", qty)
                cmd.Parameters.AddWithValue("@weight", weight)
                cmd.Parameters.AddWithValue("@rate", rate)
                cmd.Parameters.AddWithValue("@amount", amt)
                'cmd.Parameters.AddWithValue("@lbharai", bharai)
                'cmd.Parameters.AddWithValue("@lutrai", utrai)
                cmd.Parameters.AddWithValue("@lapmc", apmc)
                cmd.Parameters.AddWithValue("@lmaplevy", mapLevy)
                cmd.Parameters.AddWithValue("@lhamali", hamali)
                'cmd.Parameters.AddWithValue("@ltolai", tolai)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub

    Private Sub InsertPurMast(ByVal serno As Integer, ByVal compid As Integer, ByVal userid As String, ByVal doccd As String, ByVal pbillno As Integer, ByVal dat As Date, ByVal acccd As String, ByVal lotno As String)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("if not exists(select * from pmast where serno=@serno) begin insert into pmast (serno, compid, userid, doccd, pbillno, date, acccd, lotno,tamount ,tapmc ,thamali ,tmapLevy ,ttolai ,tbharai ,tutrai ,tmotorfrt ,grossamt ,roundoff ,netamt,trnscd)" &
                                        " values (@serno,@compid, @userid, @doccd, @pbillno, @date, @acccd, @lotno,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00 ,0.00,'') end", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@serno", serno)
                cmd.Parameters.AddWithValue("@compid", compid)
                cmd.Parameters.AddWithValue("@userid", userid)
                cmd.Parameters.AddWithValue("@doccd", doccd)
                cmd.Parameters.AddWithValue("@pbillno", pbillno)
                cmd.Parameters.AddWithValue("@date", dat)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Parameters.AddWithValue("@lotno", lotno)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

    Public Sub ClearConrols()
        txtItemName.Text = ""
        txtQty.Text = "0"
        txtWeight.Text = "0"
        txtRate0.Text = "0.00"
        txtAmt.Text = "0.00"
        txtAPMC.Text = "0.00"
        txtHamali.Text = "0.00"

        txtItemName.Focus()
    End Sub

    Public Sub ClearAll()
        con.Open()
        sernofun()
        con.Close()
        txtBillNo.Text = ""
        txtdate.SelectedDate = Now.ToString()
        txtAcName.Text = ""
        txtAcCode.Text = ""
        txtBharai.Text = "0.00"
        txtUtarai.Text = "0.00"
        txtTolai.Text = "0.00"
        txtFreight.Text = "0.00"
        txtTransName.Text = ""
        'txtFooterAmt.Text = "0.0"
        'txtFooterAPMC.Text = "0.0"
        'txtFooterHamali.Text = "0.0"
        'txtFooterMapLevy.Text = "0.0"
        txtGrossAmt.Text = "0.00"
        txtRoundup.Text = "0.00"
        txtBillNo.Focus()
        txtNetAmt.Text = "0.00"

        ClearConrols()

    End Sub

    'Protected Sub GridView3_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowCreated
    '    'Dim dt As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
    '    '    GridView3.Columns(5).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("amt")).Sum().ToString()
    '    '    GridView3.Columns(6).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("lbharai")).Sum().ToString()
    '    '    GridView3.Columns(7).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("lutrai")).Sum().ToString()
    '    '    GridView3.Columns(8).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("lapmc")).Sum().ToString()
    '    '    GridView3.Columns(9).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("lhamali")).Sum().ToString()
    '    '    GridView3.Columns(10).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("lmaplevy")).Sum().ToString()
    '    '    GridView3.Columns(11).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("ltolai")).Sum().ToString()
    '    '    GridView3.Columns(12).FooterText = dt.AsEnumerable().[Select](Function(x) x.Field(Of Double)("netamt")).Sum().ToString()
    'End Sub
    Dim totalAmt, totalbharai, totalutrai, totalapmc, totalhamali, totallevy, totaltolai, totalnetamt As Double
    Public amt, bharai, utrai, apmc, hamali, levy, tolai, netamt As Double

    Protected Sub GridView3_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView3.RowDataBound
        'Dim dt As DataTable = DirectCast(ViewState("CurrentTable"), DataTable)
        If e.Row.RowType = DataControlRowType.DataRow Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)

            totalapmc += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lapmc"))
            'amt = Convert.ToDouble(totalapmc)

            totalhamali += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lhamali"))
            'hamali = Convert.ToDouble(totalhamali)

            totallevy += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lmaplevy"))
            'levy = Convert.ToDouble(totallevy)


        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
            'amt = Convert.ToDouble(totalAmt)

            totalapmc += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lapmc"))
            'amt = Convert.ToDouble(totalapmc)

            totalhamali += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lhamali"))
            'hamali = Convert.ToDouble(totalhamali)

            totallevy += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "lmaplevy"))
            'levy = Convert.ToDouble(totallevy)

            GridView3.Columns(5).FooterText = totalAmt.ToString("#0.00") & "<br/>" & lblAmount.Text
            GridView3.Columns(6).FooterText = totalapmc.ToString("#0.00") & "<br/>" & lblAPMC.Text
            GridView3.Columns(7).FooterText = totalhamali.ToString("#0.00") & "<br/>" & lblHamali.Text
            GridView3.Columns(8).FooterText = totallevy.ToString("#0.0000") & "<br/>" & lblMapLevy.Text


            e.Row.Cells(5).Text = totalAmt.ToString("#0.00") & "<br/>" & lblAmount.Text
            e.Row.Cells(5).Font.Bold = True

            e.Row.Cells(6).Text = totalapmc.ToString("#0.00") & "<br/>" & lblAPMC.Text
            e.Row.Cells(6).Font.Bold = True

            e.Row.Cells(7).Text = totalhamali.ToString("#0.00") & "<br/>" & lblHamali.Text
            e.Row.Cells(7).Font.Bold = True

            e.Row.Cells(8).Text = totallevy.ToString("#0.0000") & "<br/>" & lblMapLevy.Text
            e.Row.Cells(8).Font.Bold = True


        End If
    End Sub


    Protected Sub GridView3_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        If e.CommandName = "e" Then
            ''txtSrNo.Text = ""
            Dim purTranId As Integer = e.CommandArgument
            hfPurTranID.Value = purTranId
            con.Open()
            Dim cmd1 As New SqlCommand("SELECT transid,purtran.itmcd,item.item,item.unit,qty,weight,rate,amount,lapmc,lmaplevy,lhamali,(select trnscd from pmast where serno=" & txtSrNo.Text & ") as trnscd FROM purtran join item on purtran.itmcd=item.itmcd WHERE transid=" + purTranId.ToString, con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim pmastTable As DataTable = ds1.Tables(0)
            'Dim pTranTable As DataTable = ds1.Tables(1)

            hfItemQty.Value = pmastTable.Rows(0).Item("unit")
            hfItemId.Value = pmastTable.Rows(0).Item("itmcd")
            txtItemName.Text = pmastTable.Rows(0).Item("item")
            hfItemname.Value = pmastTable.Rows(0).Item("item")
            txtQty.Text = pmastTable.Rows(0).Item("qty")
            txtWeight.Text = pmastTable.Rows(0).Item("weight")
            txtRate0.Text = pmastTable.Rows(0).Item("rate")
            txtAmt.Text = pmastTable.Rows(0).Item("amount")
            txtAPMC.Text = pmastTable.Rows(0).Item("lapmc")
            hfAPMC.Value = pmastTable.Rows(0).Item("lapmc")
            txtMapLevy.Text = pmastTable.Rows(0).Item("lmaplevy")
            hfMapLevy.Value = pmastTable.Rows(0).Item("lmaplevy")
            txtHamali.Text = pmastTable.Rows(0).Item("lhamali")
            hfHamali.Value = pmastTable.Rows(0).Item("lhamali")
            hfTransId.Value = pmastTable.Rows(0).Item("transid")

            Dim cmd2 As New SqlCommand("select item,item.itmcd,unit,date,hamali,tolai,APMC,maplevy from item inner join itmexp on item.itmcd=itmexp.itmcd where item like '" & txtItemName.Text & "%' and date=(select max(date) from itmexp inner join item on item.itmcd=itmexp.itmcd and item like '" & txtItemName.Text & "%')", con)
            Dim da2 As New SqlDataAdapter(cmd2)
            Dim dt2 As New DataTable()
            da2.Fill(dt2)

            hfItemExpAPMC.Value = dt2.Rows(0).Item("APMC")
            hfItemExpMapLevy.Value = dt2.Rows(0).Item("maplevy")
            hfItemExpTolai.Value = dt2.Rows(0).Item("tolai")
            hfItmExpHamali.Value = dt2.Rows(0).Item("hamali")

            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            BindGrid()

            con.Close()

        End If
        If e.CommandName = "d" Then
            ''  If MsgBox("Are you sure want to Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Delete Confirmation") = MsgBoxResult.Yes Then
            ' execute command
            con.Open()
            Dim purTranId As Integer = e.CommandArgument
            'hfInsert.Value = srno
            Dim intResponse As Integer
            intResponse = MsgBox("Are you sure you want to delete this details ??", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd1 As New SqlCommand("delete from purtran where transid=" + purTranId.ToString() + "", con)
                cmd1.ExecuteNonQuery()
                Dim count As Integer
                Dim cmd2 As New SqlCommand("select count( serno ) from purtran where serno=" & txtSrNo.Text & "", con)
                count = cmd2.ExecuteScalar
                If count = 0 Then
                    Dim cmd As New SqlCommand("delete from pmast where serno=" & txtSrNo.Text & "", con)
                    cmd.ExecuteNonQuery()
                End If
            Else
                Response.Redirect("~/PurchaseForm.aspx?id=" & txtSrNo.Text)
            End If
            con.Close()
            BindGrid()

            MsgBox("Purchase details Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information + MsgBoxStyle.MsgBoxSetForeground, "Delete")

            ClearConrols()
            'Else
            '    Exit Sub
            'End If
        End If
    End Sub

    Protected Sub GridView3_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView3.PageIndexChanging
        GridView3.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    'Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
    '    If hfConfirmClickeOrNot.Value = "0" Then
    '        MsgBox("You havn't comfirm the transaction. Do you want to confirm transaction...", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo, "Confirm Transaction")
    '    End If
    'End Sub
End Class
