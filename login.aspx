﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
        function disableautocompletion(id) {
            var passwordControl = document.getElementById(id);
            passwordControl.setAttribute("autocomplete", "off");
        }
    </script>
    
</head>
<body background="img/background01.jpg">
    <form id="form1" runat="server">
    <div>
    <br />
        <asp:Panel ID="Panel1" runat="server">
      
        <table cellpadding="0" cellspacing="0" border="1"><tr><td>
       <table height="100px" width="500px">
          <tr><td> <asp:Label ID="Label1" runat="server" Text="User Id"></asp:Label></td>
              <td>  <asp:TextBox ID="txtuserid" runat="server" onfocus="disableautocompletion(this.id)" TabIndex="1"></asp:TextBox></td>
              <td>
                  <asp:Label ID="Label4" runat="server" Text="Password"></asp:Label>
              </td>
              <td><asp:TextBox ID="txtpass" runat="server" 
                                  TextMode="Password" TabIndex="2"></asp:TextBox></td>
          </tr>
          <tr><td colspan="4" align="center">
              <asp:Button ID="btnsignin" runat="server" Text="Sign In" TabIndex="3" 
                  Font-Bold="True" Font-Size="Small" ForeColor="#000099" Height="31px" /> &nbsp;&nbsp;&nbsp; 
              &nbsp;&nbsp;&nbsp;
             <asp:Button ID="btncancel" runat="server" Text="Cancel" Font-Bold="True" 
                  Font-Size="Small" ForeColor="#000099" Height="31px" />
              </td>
           </tr>
       </table></td></tr></table>
         </asp:Panel>
      <br /><br />
      
       <table height="150px" width="650px">
          <tr>
          <td> <asp:Label ID="Label2" runat="server" Text="Select Company " Visible="False"></asp:Label></td>
          <td>  
              <asp:DropDownList ID="drlcomplist" runat="server" 
                           Visible="False" AutoPostBack="true" Width="300px" TabIndex="4">
              </asp:DropDownList></td>
          <td> 
              <asp:TextBox ID="txtcoid" runat="server"
                                  Visible="False" Width="50px"></asp:TextBox></td>
          </tr>    
          <tr>
            <td> <asp:Label ID="Label3" runat="server" Text="Select Financial Year" 
                  Visible="False"></asp:Label></td>
            <td><asp:DropDownList ID="drlyearlist" runat="server" 
                        Visible="False" AutoPostBack="true" Width="300px" TabIndex="5">
                </asp:DropDownList></td>
            <td> <asp:TextBox ID="txtyearid" runat="server" 
                       Visible="False"></asp:TextBox></td>
          </tr>  
          <tr>
          <td colspan="3" align="center">
              <asp:Button ID="btnnext" runat="server" 
                      Text="Next" Visible="False" TabIndex="6" Font-Bold="True" 
                  Font-Size="Small" ForeColor="#000099" Height="31px" Width="60px" />
              <asp:Button ID="btncreatecomp" runat="server" Text="Create Company" 
                  PostBackUrl="~/createcomp.aspx" Visible="False" Font-Bold="True" 
                  Font-Size="Small" ForeColor="#000099" Height="31px" />
              <asp:Button ID="btncreate" Visible="false" runat="server" Text="Create User" Font-Bold="True" 
                  Font-Size="Small" ForeColor="#000099" Height="31px" />
          </td>
          </tr>
       </table>
        
        
       
        
        
    
    </div>
    </form>
</body>
</html>
