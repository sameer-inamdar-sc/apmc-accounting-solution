﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Partial Class opbal
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            con.Open()
            Dim cmdStr As String = "select count(*) from openbal"
            cmdStr += " where compid='" + Txtcompid.Text.Trim() & "'"

            Dim cmd As New SqlCommand(cmdStr, con)

            Dim i As Integer
            i = Convert.ToInt16(cmd.ExecuteScalar())

            Dim cmd3 As New SqlCommand("select max(acccd) from openbal", con)
            Dim a As Integer
            Dim obj As Object = cmd3.ExecuteScalar()
            If obj Is DBNull.Value Then
                a = 0
            Else
                a = Convert.ToInt32(obj)
            End If

            Dim cmd4 As New SqlCommand("select max(acccd) from acmast", con)
            Dim a1 As Integer
            Dim obj1 As Object = cmd4.ExecuteScalar()
            a1 = Convert.ToInt32(obj1)

            If a = 0 Then
                Dim cmd2 As New SqlCommand("insert into openbal(acccd,acname,mjcd,opbal,sign12,compid,userid,grcd,lf) select acccd,acname,mjcd,0.00,'CR'," & Txtcompid.Text.Trim() & ",'" & Txtuserid.Text.Trim() & "',grcd,0 from acmast", con)
                cmd2.ExecuteNonQuery()
            ElseIf a1 > a Then
                Dim cmd5 As New SqlCommand("insert into openbal(acccd,acname,mjcd,opbal,sign12,compid,userid,grcd,lf) select acccd,acname,mjcd,0.00,'CR'," & Txtcompid.Text.Trim() & ",'" & Txtuserid.Text.Trim() & "',grcd,0 from acmast where acccd>" & a & "", con)
                cmd5.ExecuteNonQuery()
            ElseIf a = a1 Then
            End If

            If i = 0 Then
                btnupdate.Enabled = True

            Else
                btnupdate.Enabled = True

            End If

            drlmjname.Items.Clear()
            Dim cmd1 As New SqlCommand("select mjcd,mjname from mjmast", con)
            Dim dr As SqlDataReader = cmd1.ExecuteReader()
            While dr.Read()
                Dim item As String = dr.GetSqlString(0).Value
                Dim item1 As String = dr.GetSqlString(1).Value
                drlmjname.Items.Add(New ListItem(item + "-" + item1))
            End While
            dr.Close()
            drlmjname.Items.Insert(0, "Select Major Code")
            drlmjname.Focus()
            con.Close()
        End If
    End Sub
    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chkTest As CheckBox = DirectCast(sender, CheckBox)
        Dim grdRow As GridViewRow = DirectCast(chkTest.NamingContainer, GridViewRow)
        Dim txtopbal As TextBox = DirectCast(grdRow.FindControl("txtopbal"), TextBox)
        Dim drlsign12 As DropDownList = DirectCast(grdRow.FindControl("drlsign12"), DropDownList)
        If chkTest.Checked Then
            txtopbal.[ReadOnly] = False
            'drlsign12.[ReadOnly] = False
            txtopbal.ForeColor = System.Drawing.Color.Black
            drlsign12.ForeColor = System.Drawing.Color.Black
        Else
            txtopbal.[ReadOnly] = True
            'drlsign12.[ReadOnly] = True
            txtopbal.ForeColor = System.Drawing.Color.Blue
            drlsign12.ForeColor = System.Drawing.Color.Blue
        End If
    End Sub
    Protected Sub drlmjname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlmjname.SelectedIndexChanged
        con.Open()
        If drlmjname.SelectedIndex = 0 Then
            MsgBox("Please Select Any Major Code from List")
            Panel1.Visible = False
            txtmjcd.Text = ""
            drlmjname.Focus()
        Else
            Dim cmd As New SqlCommand("Select substring('" + drlmjname.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlmjname.SelectedItem.Text.Trim() + "')-1)", con)
            Dim da1 As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da1.Fill(ds)
            Dim custTable1 As DataTable = ds.Tables(0)
            txtmjcd.Text = custTable1.Rows(0).Item(0)


            Dim cmd1 As New SqlCommand("SELECT acccd,acname,mjcd,grcd,opbal,sign12 FROM openbal WHERE mjcd = '" & txtmjcd.Text & "'", con)
            Dim i As Integer
            i = Convert.ToInt16(cmd1.ExecuteScalar())
            If i = 0 Then
                Panel1.Visible = False
                MsgBox("No Record Found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal, "No Recorrd Found")
            Else
                Panel1.Visible = True
            End If

        End If
        con.Close()
    End Sub
    Private Sub checkAll()
        For Each row As GridViewRow In GridView1.Rows
            Dim chkUncheck As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim txtopbal As TextBox = DirectCast(row.FindControl("txtopbal"), TextBox)
            Dim drlsign12 As DropDownList = DirectCast(row.FindControl("drlsign12"), DropDownList)
            chkUncheck.Checked = True
            txtopbal.[ReadOnly] = False
            'drlsign12.[ReadOnly] = False
            txtopbal.ForeColor = System.Drawing.Color.Black
            drlsign12.ForeColor = System.Drawing.Color.Black
        Next
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        checkAll()
    End Sub

    Protected Sub btnupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Dim strSql As New StringBuilder(String.Empty)
        Dim cmd As New SqlCommand()
        For i As Integer = 0 To GridView1.Rows.Count - 1
            Dim chkUpdate As CheckBox = DirectCast(GridView1.Rows(i).Cells(0).FindControl("chkSelect"), CheckBox)
            If chkUpdate IsNot Nothing Then
                If chkUpdate.Checked Then
                    Dim strID As String = GridView1.Rows(i).Cells(1).Text
                    Dim stropbal As Integer = DirectCast(GridView1.Rows(i).FindControl("txtopbal"), TextBox).Text

                    Dim strsign12 As String = DirectCast(GridView1.Rows(i).FindControl("drlsign12"), DropDownList).SelectedItem.Text

                    Dim strUpdate As String = "Update openbal set opbal = " + DirectCast(GridView1.Rows(i).FindControl("txtopbal"), TextBox).Text + ",sign12 ='" + strsign12 + "' WHERE acccd ='" + strID + "'"
                    strSql.Append(strUpdate)
                End If
            End If
        Next
        Try
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strSql.ToString()
            cmd.Connection = con
            con.Open()
            cmd.ExecuteNonQuery()
            MsgBox("Record Updated", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Recorrd Updated")
        Catch ex As SqlException
            MsgBox("Please Contact administrater")
        Finally
            con.Close()
        End Try
        drlmjname.Focus()
    End Sub

    
End Class
