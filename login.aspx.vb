﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.SqlClient.SqlDataReader

Partial Class _Default
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub btnsignin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsignin.Click
        con.Open()
        If txtuserid.Text = "" Or txtpass.Text = "" Then
            ClientScript.RegisterStartupScript(Page.GetType(), "Login Error", "<script language='javascript'> alert('Please Enter Your User ID or Password')</script>")
            'MsgBox("Please Enter Your User ID or Password", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Question, "Login Error")
            txtuserid.Focus()
        Else
            Dim cmd As New SqlCommand("select uid,pass from createuser where uid COLLATE Latin1_general_CS_AS =@uid and pass COLLATE Latin1_general_CS_AS =@pass", con)
            Dim da As New SqlDataAdapter(cmd)
            da.SelectCommand.Parameters.Add("@uid", SqlDbType.NVarChar, 50).Value = txtuserid.Text
            da.SelectCommand.Parameters.AddWithValue("@pass", txtpass.Text)
            Dim dt As New DataTable()
            da.Fill(dt)
            da.Dispose()
            If dt.Rows.Count = 0 Then
                '  ClientScript.RegisterStartupScript(Page.GetType(), "Login Error", "alert('Sorry, Wrong Username or Password')", True)
                MsgBox("Sorry, Wrong Username or Password", MsgBoxStyle.MsgBoxSetForeground, "Login Error")
                Call clear()
                txtuserid.Focus()
                Return
            Else
                ' Dim myscript As String = "alert2('Please Enter The Patients Inpatient number','Message From My Custom page','Ok')   window.location.replace(window.location.href);"
                ClientScript.RegisterStartupScript(Page.GetType(), "Login", "alert('Login successful');", True)

                ' MsgBox("Login successful", MsgBoxStyle.MsgBoxSetForeground, "Login")
                Session("userid") = txtuserid.Text.Trim()

                Dim cmd2 As New SqlCommand("select authority,uname from createuser where uid='" + txtuserid.Text + "' and pass='" + txtpass.Text + "' ", con)
                Dim dr1 As SqlDataReader = cmd2.ExecuteReader()
                While dr1.Read()
                    Session("authority") = dr1.GetSqlString(0).Value.Trim()
                    Session("uname") = dr1.GetSqlString(1).Value.Trim()
                End While

                dr1.Close()
                Call clear()

                Label1.Visible = False
                Label4.Visible = False
                txtuserid.Visible = False
                txtpass.Visible = False
                btnsignin.Visible = False
                btncancel.Visible = False
                Panel1.Visible = False

                Label2.Visible = True
                drlcomplist.Visible = True

                Label3.Visible = True
                drlyearlist.Visible = True

                btnnext.Visible = True
                If Session("authority").ToString().ToUpper() = "ADMIN" Or Session("authority").ToString().ToUpper() = "SUPER-ADMIN" Then
                    btncreatecomp.Visible = True
                    btncreate.Visible = True
                End If

                Dim cmd1 As New SqlCommand("select cocd,coname from company", con)
                ' Dim cmd1 As New SqlCommand("select co.cocd,co.coname from company co join createuser cu on co.coid=cu.coid where cu.uname='" & userid_z & "'", con)
                Dim dr As SqlDataReader = cmd1.ExecuteReader()
                While dr.Read()
                    Dim coname As String = dr.GetSqlString(0).Value + "-" + dr.GetSqlString(1).Value            ''Session("cocd") = s
                    drlcomplist.Items.Add(New ListItem(coname))
                    ' drlcomplist.Items.Add(dr("cocd").ToString() + " , " + dr("coname").ToString())
                End While
                dr.Close()
                drlcomplist.Items.Insert(0, "-- Please Select Company --")

                Dim cmd3 As New SqlCommand("select yearid,coyear from finyear", con)
                Dim Adpt As New SqlDataAdapter(cmd3)
                Dim ds As New DataSet()
                If (Adpt.Fill(ds, "logn")) Then
                    drlyearlist.DataSource = ds.Tables(0)
                    drlyearlist.DataTextField = "coyear"
                    drlyearlist.DataValueField = "yearid"
                    drlyearlist.DataBind()
                End If

            End If
        End If

        Dim com As New SqlCommand("select co.coid, co.cocd+'-'+co.coname con from company co", con)
        Dim da2 As New SqlDataAdapter(com)
        Dim dt2 As New DataTable()
        da2.Fill(dt2)
        drlcomplist.DataSource = dt2
        drlcomplist.DataTextField = "con"
        drlcomplist.DataValueField = "coid"
        drlcomplist.DataBind()
        con.Close()
        GetCompany()
        SetCurrentFinYear()
    End Sub

    Public Sub GetCompany()
        Dim userid_z As String
        userid_z = Session("userid")
        Dim com As New SqlCommand("select cu.coid from createuser cu where cu.uid='" & userid_z & "'", con)
        Dim da2 As New SqlDataAdapter(com)
        Dim dt2 As New DataTable()
        da2.Fill(dt2)
        drlcomplist.SelectedValue = dt2.Rows(0)(0).ToString()
        con.Close()
        If Session("authority").ToString().ToUpper() = "USER" Or Session("authority").ToString().ToUpper() = "ADMIN" Then
            drlcomplist.Enabled = False
        End If
    End Sub
    Public Sub SetCurrentFinYear()

        Dim com As New SqlCommand("select yearid from finyear where coyear=rtrim(dbo.GetFinancialYear(getdate()))", con)
        Dim da2 As New SqlDataAdapter(com)
        Dim dt2 As New DataTable()
        da2.Fill(dt2)
        drlyearlist.SelectedValue = dt2.Rows(0)(0).ToString()
        con.Close()

    End Sub



    Protected Sub drlcomplist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlcomplist.SelectedIndexChanged
        con.Open()
        'If drlcomplist.SelectedIndex = 0 Then
        'ClientScript.RegisterStartupScript(Page.GetType(), "Information", "<script language='javascript'> alert('Please select company')</script>")
        'MsgBox("Please select company")
        'Else
        Dim cmd1 As New SqlCommand("select coid from company where cocd=substring('" + drlcomplist.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlcomplist.SelectedItem.Text.Trim() + "')-1)", con)
        'Dim cmd1 As New SqlCommand("select cm.coid, cu.uname from company cm, createuser cu where cocd=substring('" + drlcomplist.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlcomplist.SelectedItem.Text.Trim() + "')-1)", con)
        Dim da As New SqlDataAdapter(cmd1)
        Dim ds1 As New DataSet()
        da.Fill(ds1)
        Dim custTable As DataTable = ds1.Tables(0)
        txtcoid.Text = custTable.Rows(0).Item(0)

        'If drlcomplist.SelectedValue = dt Then
        'drlcomplist.Enabled = False Then
        'End If

        con.Close()
        'End If

    End Sub
    Protected Sub drlyearlist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlyearlist.SelectedIndexChanged
        con.Open()
        If drlyearlist.SelectedIndex = 0 Then
            ClientScript.RegisterStartupScript(Page.GetType(), "Information", "<script language='javascript'> alert('Please select Year')</script>")
            ' MsgBox("Please select Year")
        Else
            Dim cmd1 As New SqlCommand("select yearid from finyear where coyear='" + drlyearlist.SelectedItem.Text + "'", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)
            txtyearid.Text = custTable.Rows(0).Item(0)
        End If
        con.Close()
    End Sub
    Protected Sub btnnext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnext.Click

        con.Open()
        Dim cmd2 As New SqlCommand("select compid from compmast where coid=" + drlcomplist.SelectedValue + " and yearid=" + drlyearlist.SelectedValue + " ", con)
        Dim i As Integer
        i = Convert.ToInt16(cmd2.ExecuteScalar())

        If i = 0 Then
            'ClientScript.RegisterStartupScript(Page.GetType(), "Admin Contact", "<script language='javascript'> alert('Please contact to admin or select proper Company Name or Financial year')</script>")
            ' MsgBox("Please contact to admin or select proper Company Name or Financial year", MsgBoxStyle.MsgBoxSetForeground, "Admin Contact")
            MsgBox("Please Select Valid Financial Year", MsgBoxStyle.SystemModal, "Error")
            Return
            'Response.Redirect("http://google.com")
        Else
            Dim dr1 As SqlDataReader = cmd2.ExecuteReader()
            While dr1.Read()
                Dim a As String = dr1.GetSqlInt32(0).Value
                Session("compid") = a
            End While
            dr1.Close()
        End If
        Session("coname") = drlcomplist.SelectedItem.Text
        Session("coyear") = drlyearlist.SelectedItem.Text
        Response.Redirect("mainpage.aspx")
        con.Close()

    End Sub
    Protected Sub btncreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncreate.Click
        Response.Redirect("createuser.aspx")
    End Sub
    Public Sub clear()
        txtuserid.Text = ""
        txtpass.Text = ""
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtuserid.Focus()
        End If
    End Sub
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        txtuserid.Text = ""
        txtpass.Text = ""
        txtuserid.Focus()
    End Sub

    Protected Sub btncreatecomp_Click(sender As Object, e As System.EventArgs) Handles btncreatecomp.Click
        Response.Redirect("createcomp.aspx")

    End Sub
End Class
