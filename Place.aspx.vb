﻿
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System

Partial Class Place
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)


    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        'Response.Redirect(Place.aspx)
        ClearConrols()
        con.Open()
        Dim cmd As New SqlCommand("select max(serno) from place", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            txt_srno.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            txt_srno.Text = a.ToString()
        End If

        Txtplace.Focus()


        con.Close()
        HiddenField1.Value = "0"
        Me.ModalPopupExtender2.Show()
    End Sub

    'Private Shared Function aspx() As String
    '    Throw New NotImplementedException
    'End Function
    Public Sub ClearConrols()
        Txtplace.Focus()
        Txtplace.Text = ""
        Txtatpost.Text = ""
        Txttal.Text = ""
        Txtdist.Text = ""
        txt_srno.Text = ""

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            ClearConrols()
           
            BindGrid()
        End If
    End Sub

    Protected Sub btnsave_Click(sender As Object, e As System.EventArgs) Handles btnsave.Click
        con.Open()

        If Txtplace.Text = "" Then
            MsgBox("Please Enter Place Name", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            Txtplace.Focus()
        Else

            If HiddenField1.Value = "0" Then
                Dim s As String
                s = "insert into place(serno,place,atpost,tal,dist) values(" & txt_srno.Text & ",'" & Txtplace.Text.Trim() & "','" & Txtatpost.Text.Trim() & "','" & Txttal.Text.Trim() & "','" & Txtdist.Text.Trim() & "')"
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
            Else
                Dim s As String
                s = "update place set place='" & Txtplace.Text.Trim() & "',atpost='" & Txtatpost.Text.Trim() & "',tal='" & Txttal.Text.Trim() & "',dist='" & Txtdist.Text.Trim() & "'where serno=" & txt_srno.Text.Trim & ""
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
            End If

            MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Save")

            Txtplace.Text = ""
            Txtatpost.Text = ""
            Txttal.Text = ""
            Txtdist.Text = ""

            Txtplace.Focus()
            con.Close()

        End If
        BindGrid()
    End Sub
    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        If e.CommandName = "e" Then
            Dim srno As String = e.CommandArgument
            HiddenField1.Value = srno
            con.Open()
            Dim cmd1 As New SqlCommand("select serno,UPPER(place)place,upper(atpost)atpost,UPPER(tal)tal,UPPER(dist)dist from place where serno='" + srno.ToString() + "'", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)
            'code for drlitmcd to fetch value
            txt_srno.Text = custTable.Rows(0).Item(0)
            Txtplace.Text = custTable.Rows(0).Item(1)
            Txtatpost.Text = custTable.Rows(0).Item(2)
            Txttal.Text = custTable.Rows(0).Item(3)
            Txtdist.Text = custTable.Rows(0).Item(4)
            con.Close()
            Me.ModalPopupExtender2.Show()
        End If
        If e.CommandName = "d" Then
            Dim srno As String = e.CommandArgument
            HiddenField1.Value = srno
            con.Open()
            Dim d As String
            Dim f As Integer
            d = "select count(srplace) from acmast where srplace like '" + srno.ToString() + "'"
            Dim cmd3 As New SqlCommand(d, con)
            f = cmd3.ExecuteScalar()
            If f = 0 Then


                Dim cmd1 As New SqlCommand("delete from place where serno='" + srno.ToString() + "'", con)
                cmd1.ExecuteNonQuery()
                con.Close()
                MsgBox("Place Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")

                BindGrid()
            Else
                MsgBox("Place is user in Account Master", MsgBoxStyle.OkOnly + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "In use...")

            End If
        End If

    End Sub
    Protected Sub BindGrid()
        Dim filter As String = ""

        If txt_serachplace.Text <> "" Then
            filter = "and place like '" & txt_serachplace.Text & "%'"
        End If

        If txt_searchpost.Text <> "" Then
            filter = filter & "and atpost like '" & txt_searchpost.Text & "%'"
        End If

        If txt_searchtaluka.Text <> "" Then
            filter = filter & "and tal like '" & txt_searchtaluka.Text & "%'"
        End If

        If txt_searchdist.Text <> "" Then
            filter = filter & "and dist like '" & txt_searchdist.Text & "%'"
        End If

        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If


        con.Open()
        Dim cmd As New SqlCommand("select serno,UPPER(place)place,upper(atpost)atpost,UPPER(tal)tal,UPPER(dist)dist from place " & filter & " ORDER BY serno DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView2.DataSource = dt
        GridView2.DataBind()
        con.Close()
    End Sub

    Protected Sub Btn_Search_Click(sender As Object, e As System.EventArgs) Handles Btn_Search.Click
        BindGrid()
        ClearConrols()
    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView2.PageIndexChanging
        GridView2.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridView2_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GridView2.SelectedIndexChanged

    End Sub
End Class
