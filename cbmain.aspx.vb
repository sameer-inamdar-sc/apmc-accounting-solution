﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Partial Class cbmain
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Txt_Account.Focus()
            Txt_Date.SelectedDate = Date.Today
            Txt_CBNo.Enabled = False
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            btn_Save.Visible = False
            con.Open()
            sr()
            con.Close()


            Dim cbno As String = CType(Page.Request.QueryString("id"), String)
            If Not cbno = String.Empty Then

                con.Open()
                Dim com As New SqlCommand("select cm.cbno,cm.trsnrp,cm.modecasbank,cm.date,ISNULL(cm.bankname,' ')bankname,cm.bankcode from cbmast cm where CM.cbno=" & cbno & " order by Cm.cbno desc", con)
                Dim da2 As New SqlDataAdapter(com)
                Dim dt2 As New DataTable()

                da2.Fill(dt2)
                con.Close()

                Txt_CBNo.Text = dt2.Rows(0).Item("cbno")

                '  Txt_Account.Text = dt2.Rows(0).Item("acname")
                ' Txt_AcCode.Text = dt2.Rows(0).Item("acccd")
                Drl_PayRec.SelectedValue = dt2.Rows(0).Item("trsnrp")
                If dt2.Rows(0).Item("modecasbank") = True Then
                    Cash.Checked = True
                    Bank.Checked = False
                    tablerow.Visible = False
                ElseIf dt2.Rows(0).Item("modecasbank") = False Then
                    Bank.Checked = True
                    Cash.Checked = False
                    tablerow.Visible = True
                End If
                ''    Txt_Amount.Text = dt2.Rows(0).Item("amount")
                Txt_Bname.Text = dt2.Rows(0).Item("bankname")
                Txt_BCode.Text = dt2.Rows(0).Item("bankcode").ToString()
                Txt_Date.SelectedDate = dt2.Rows(0).Item("date").ToString()
                btn_Save.Visible = True
                ''  Txt_BBranch.Text = dt2.Rows(0).Item("branch")
                '' Txt_ChqNo.Text = dt2.Rows(0).Item("chqno")
                ''  Txt_Narration.Text = dt2.Rows(0).Item("narr")


                'Drl_CashBank.DataSource = dt2
                'Drl_CashBank.DataTextField = "acname"
                'Drl_CashBank.DataValueField = "acccd"
                'Drl_CashBank.DataBind()
                'con.Open()
                'Dim cmd As New SqlCommand("select max(cbno) from cbmast", con)
                'Dim a As Integer
                'Dim obj As Object = cmd.ExecuteScalar()
                'If obj Is Nothing Or obj Is DBNull.Value Then
                '    a = 0
                '    a = a + 1
                '    Txt_CBNo.Text = a.ToString()
                'Else
                '    a = Convert.ToInt32(obj)
                '    a = a + 1
                '    Txt_CBNo.Text = a.ToString()
                'End If
                'Txt_Date.SelectedDate = Date.Today

                'Dim cmd1 As New SqlCommand("select max(sr) from cbtran where cbno=" & Txt_CBNo.Text & "", con)
                'Dim b As Integer
                'Dim obj1 As Object = cmd1.ExecuteScalar()
                'If obj1 Is Nothing Or obj1 Is DBNull.Value Then
                '    b = 0
                '    b = b + 1
                '    hf_serno.Value = b.ToString()
                'Else
                '    b = Convert.ToInt32(obj1)
                '    b = b + 1
                '    hf_serno.Value = b.ToString()
                'End If

                BindGrid()
                '    con.Close()

                'If GridView1 Is Nothing Then
                '    Btn_ConfirmSave.Visible = False
                'Else
                '    Btn_ConfirmSave.Visible = True
                'End If


            End If
        End If

        
    End Sub
    Public Sub sr()
        Dim cmd As New SqlCommand("select max(cbno) from cbmast", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            Txt_CBNo.Text = a.ToString()

        Else
            a = Convert.ToInt32(obj)

            a = a + 1
            Txt_CBNo.Text = a.ToString()

        End If
    End Sub
    Protected Sub BindGrid()

        Dim cmd As New SqlCommand(" select ct.sr,cm.cbno,ac.acname,ct.acccd,cm.trsnrp,cm.modecasbank,cm.date,ct.amount, ISNULL(cm.bankname,' ')bankname,cm.bankcode,ct.branch,ct.chqno,ct.chqdate,ct.narr from cbtran ct join acmast ac on ct.acccd=ac.acccd join cbmast cm on ct.cbno=cm.cbno where CM.cbno=" & Txt_CBNo.Text & " order by CT.sr desc", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()


    End Sub
    Public Sub ClearControls()
        Txt_Account.Text = ""
        Txt_AcCode.Text = ""
        Txt_Amount.Text = ""
        Txt_BBranch.Text = ""
        Txt_Narration.Text = ""
        Txt_ChqNo.Text = ""

    End Sub


    Protected Sub btn_back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Response.Redirect("Cashbook.aspx")
    End Sub

    Protected Sub btnsave_Click(sender As Object, e As System.EventArgs) Handles btnsave.Click
        Dim modecasbank, cbno As Integer
        Dim doccode, trsnrp, bankcode, bankname As String
        Dim dat As DateTime
        con.Open()
        cbno = Integer.Parse(Txt_CBNo.Text)
        trsnrp = Drl_PayRec.SelectedValue
        If Cash.Checked = True Then
            modecasbank = 1
        ElseIf Bank.Checked = True Then
            modecasbank = 0
        End If
        dat = Txt_Date.Text
        bankcode = Txt_BCode.Text
        bankname = Txt_Bname.Text
        doccode = Txtdoccd.Text
        Dim cbno1 As String = CType(Page.Request.QueryString("id"), String)
        If Not cbno1 = String.Empty Then
            UpdateCBmast(cbno, dat, doccode, trsnrp, modecasbank, bankcode, bankname)

            'Dim cmd As New SqlCommand("UPDATE cbmast SET date='" & Txt_Date.Text.Trim() & "',trsnrp='" & Drl_PayRec.SelectedValue & "',modecasbank='" & Cash.Checked = "" & "',bankcode='" & Txt_BCode.Text.Trim & "',bankname ='" & Txt_Bname.Text.Trim & "', WHERE cbno =" & cbno1 & " ")
            'Dim da2 As New SqlDataAdapter(cmd)
            'Dim dt2 As New DataTable()

            'da2.Fill(dt2)
            'con.Close()
        Else
            InsertCBmast(cbno, dat, doccode, trsnrp, modecasbank, bankcode, bankname)
            btn_Save.Visible = True

        End If
        BindGrid()
    End Sub
    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        Dim compid, cbno, sr As Integer
        Dim amount As Double
        Dim narr, year, branch, userid, acccd, chqNo, chqdate As String


        con.Open()
        year = CType(Session("coyear"), String)
        cbno = Integer.Parse(Txt_CBNo.Text)
        'trsnrp = Drl_PayRec.SelectedValue
        'If Cash.Checked = True Then
        '    modecasbank = 1
        'ElseIf Bank.Checked = True Then
        '    modecasbank = 0
        'End If
        'dat = Txt_Date.Text

        ' dat = DateTime.ParseExact(Txt_Date.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        compid = Session("compid")
        userid = Session("userid")
        '  bankcode = Txt_BCode.Text
        acccd = Txt_AcCode.Text
        amount = Txt_Amount.Text
        narr = Txt_Narration.Text
        '  bankname = Txt_Bname.Text
        branch = Txt_BBranch.Text
        chqNo = Txt_ChqNo.Text
        chqdate = Txt_ChqDate.Text
        '  doccode = Txtdoccd.Text
        '  InsertCBmast(cbno, dat, doccode, trsnrp, modecasbank)
        If hfChallanTranID.Value = "0" Then
            Dim dt As New DataTable()
            dt.Columns.Add("acccd", GetType(String))
            dt.Columns.Add("amount", GetType(Double))
            dt.Columns.Add("narr", GetType(String))
            dt.Columns.Add("bankcode", GetType(String))
            dt.Columns.Add("bankname", GetType(String))
            dt.Columns.Add("branch", GetType(String))
            dt.Columns.Add("chqno", GetType(String))
            dt.Columns.Add("chqdate", GetType(String))

            Dim cmd As New SqlCommand("select max(sr) from cbtran", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1
            Else
                a = Convert.ToInt32(obj)
                a = a + 1

            End If
            If Bank.Checked = True Then
                dt.Rows.Add(Convert.ToString(Txt_AcCode.Text), Convert.ToDouble(Txt_Amount.Text), Convert.ToString(Txt_Narration.Text), Convert.ToString(Txt_BCode.Text), Convert.ToString(Txt_Bname.Text), Convert.ToString(Txt_BBranch.Text), Convert.ToString(Txt_ChqNo.Text), chqdate)
            Else

                dt.Rows.Add(Convert.ToString(Txt_AcCode.Text), Convert.ToDouble(Txt_Amount.Text), Convert.ToString(Txt_Narration.Text), Convert.ToString(" "), Convert.ToString(" "), Convert.ToString(" "), Convert.ToString(" "), Convert.ToString(" "))
            End If
            sr = Integer.Parse(a)
            compid = Convert.ToInt32(Session("compid"))
            userid = Convert.ToString(Session("userid"))
            cbno = Convert.ToInt32(Txt_CBNo.Text)
            acccd = Convert.ToString(Txt_AcCode.Text)
            amount = Convert.ToDouble(Txt_Amount.Text)
            narr = Convert.ToString(Txt_Narration.Text)
            ' bankcode = Convert.ToString(Txt_BCode.Text)
            ' bankname = Convert.ToString(Txt_Bname.Text)
            branch = Convert.ToString(Txt_BBranch.Text)
            chqNo = Convert.ToString(Txt_ChqNo.Text)
            chqdate = Convert.ToString(Txt_ChqDate.Text)
            'GridView3.DataSource = dt
            'GridView3.DataBind()

            InsertRowsInGrid(sr, compid, userid, cbno, acccd, amount, narr, branch, chqNo, chqdate)
            con.Close()
            InsertInLedgerCr()
            InsertInLedgerDr()
            con.Open()
        Else
            Dim s As String
            s = "update cbtran set account=" & Txt_Account.Text & " ,amount=" & Txt_Amount.Text & ",narr=" & Txt_Narration.Text & ",bankname=" & Txt_Bname.Text & ",branch=" & Txt_BBranch.Text & ",chqNo=" & Txt_ChqNo.Text & ",chqdate=" & Txt_ChqDate.Text & " where transid=" & hfChallanTranID.Value & ""
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
            hfChallanTranID.Value = "0"

        End If

        BindGrid()
        ClearControls()
        con.Close()
    End Sub
    Public Query As String
    'Public Sub InsertInLedger()

    '    Query = "ledger_Insert"
    '    Dim com As New SqlCommand(Query, con)
    '    com.CommandType = CommandType.StoredProcedure
    '    com.Parameters.AddWithValue("@serno", Txt_CBNo.Text.ToString())
    '    com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
    '    com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
    '    com.Parameters.AddWithValue("@doccd", "CB")
    '    com.Parameters.AddWithValue("@tdate", Txt_Date.SelectedDate.ToString())
    '    com.Parameters.AddWithValue("@rp", Drl_PayRec.SelectedValue)
    '    com.Parameters.AddWithValue("@acccd1", Txt_BCode.Text)
    '    com.Parameters.AddWithValue("@sign12", " ")
    '    com.Parameters.AddWithValue("@acccd2", Txt_AcCode.Text)
    '    com.Parameters.AddWithValue("@amount", Txt_Amount.Text)
    '    com.Parameters.AddWithValue("@chqno", Txt_ChqNo.Text)
    '    com.Parameters.AddWithValue("@narrcd", Txt_Narration.Text)
    '    com.Parameters.AddWithValue("@lotno", "CB-" & Txt_CBNo.Text.ToString())
    '    com.Parameters.AddWithValue("@posting", "")
    '    com.ExecuteNonQuery()

    'End Sub

    Private Sub InsertCBmast(ByVal cbno As Integer, ByVal dat As Date, ByVal doccode As String, ByVal trsnrp As String, ByVal modecasbank As Integer, ByVal bankcode As String, ByVal bankname As String)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)

            Using cmd As New SqlCommand("if not exists(select * from cbmast where cbno=@cbno) begin insert into cbmast (cbno,date,doccode,trsnrp,modecasbank,bankcode, bankname)" &
                                        " values (@cbno,@dat,@doccode,@trsnrp,@modecasbank,@bankcode,@bankname) end", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@cbno", cbno)
                cmd.Parameters.AddWithValue("@dat", dat)
                cmd.Parameters.AddWithValue("@doccode", doccode)
                cmd.Parameters.AddWithValue("@trsnrp", trsnrp)
                cmd.Parameters.AddWithValue("@modecasbank", modecasbank)
                If modecasbank = 1 Then
                    cmd.Parameters.AddWithValue("@bankcode", bankcode)
                ElseIf Drl_PayRec.SelectedValue = False Then

                    cmd.Parameters.AddWithValue("@bankcode", DBNull.Value)
                Else

                    cmd.Parameters.AddWithValue("@bankcode", bankcode)


                End If
                cmd.Parameters.AddWithValue("@bankname", bankname)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub
    Private Sub UpdateCBmast(ByVal cbno As Integer, ByVal dat As Date, ByVal doccode As String, ByVal trsnrp As String, ByVal modecasbank As Integer, ByVal bankcode As String, ByVal bankname As String)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
        
            Using cmd As New SqlCommand("update cbmast set date=@dat,doccode=@doccode,trsnrp=@trsnrp,modecasbank=@modecasbank,bankcode=@bankcode, bankname=@bankname where cbno=@cbno ", con)
                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@cbno", cbno)
                cmd.Parameters.AddWithValue("@dat", dat)
                cmd.Parameters.AddWithValue("@doccode", doccode)
                cmd.Parameters.AddWithValue("@trsnrp", trsnrp)
                cmd.Parameters.AddWithValue("@modecasbank", modecasbank)
                If Drl_PayRec.SelectedValue = False Then

                    cmd.Parameters.AddWithValue("@bankcode", DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue("@bankcode", bankcode)


                End If
                cmd.Parameters.AddWithValue("@bankname", bankname)
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub

    Private Sub InsertRowsInGrid(ByVal sr As Integer, ByVal compid As Integer, ByVal userid As String, ByVal cbno As Integer, ByVal acccd As String, ByVal amount As Double, ByVal narr As String, ByVal branch As String, ByVal chqNo As String, ByVal chqdate As String)
        Dim constr As String = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("insert into cbtran (sr,compid,userid,cbno,acccd, amount, narr, branch, chqNo, chqdate)" &
                                        " values (@sr,@compid,@userid,@cbno,@acccd, @amount, @narr, @branch, @chqNo, @chqdate)", con)


                con.Open()
                cmd.CommandType = CommandType.Text
                cmd.Parameters.AddWithValue("@sr", sr)
                cmd.Parameters.AddWithValue("@compid", compid)
                cmd.Parameters.AddWithValue("@userid", userid)
                cmd.Parameters.AddWithValue("@cbno", cbno)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Parameters.AddWithValue("@amount", amount)
                cmd.Parameters.AddWithValue("@narr", narr)
                'If Drl_PayRec.SelectedValue = False Then
                '    cmd.Parameters.AddWithValue("@bankcode", DBNull.Value)
                'Else
                '    cmd.Parameters.AddWithValue("@bankcode", bankcode)


                'End If
                'cmd.Parameters.AddWithValue("@bankname", bankname)
                cmd.Parameters.AddWithValue("@branch", branch)
                cmd.Parameters.AddWithValue("@chqNo", chqNo)

                If chqdate = "" Then
                    cmd.Parameters.AddWithValue("@chqdate", DBNull.Value)
                Else
                    cmd.Parameters.AddWithValue("@chqdate", chqdate)
                End If

                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using
    End Sub

    Public Sub InsertInLedgerCr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", Txt_CBNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", Txt_Date.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd1", Txt_AcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "CR")
        com.Parameters.AddWithValue("@acccd2", "0000062") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", Txt_Amount.Text.ToString())
        com.Parameters.AddWithValue("@chqno", Txt_ChqNo.Text.ToString())
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", Txt_CBNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Public Sub InsertInLedgerDr()
        con.Open()
        query = "ledger_Insert"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", Txt_CBNo.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", Txtdoccd.Text.ToString())
        com.Parameters.AddWithValue("@tdate", Txt_Date.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd2", Txt_AcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", "DR")
        com.Parameters.AddWithValue("@acccd1", "0000062") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", Txt_Amount.Text.ToString())
        com.Parameters.AddWithValue("@chqno", Txt_ChqNo.Text.ToString())
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", Txt_CBNo.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub

    Public Sub Cash_CheckedChanged(sender As Object, e As System.EventArgs) Handles Cash.CheckedChanged
        Txt_Bname.Text = "Cash In Hand"
        Txt_Bname.Enabled = False
        Txt_BCode.Text = "0000097"
        Txt_BCode.Enabled = False
        'Lbl_BBranch.Visible = False
        'Txt_BBranch.Visible = False
        'Lbl_ChqNo.Visible = False
        'Txt_ChqNo.Visible = False
        'Lbl_ChqDate.Visible = False
        'Txt_ChqDate.Visible = False
        tablerow.Visible = False
    End Sub

    Public Sub Bank_CheckedChanged(sender As Object, e As System.EventArgs) Handles Bank.CheckedChanged
        Txt_Bname.Text = ""
        Txt_Bname.Enabled = True
        'Txt_BCode.Enabled = False
        Txt_BCode.Text = ""
        Lbl_BBranch.Visible = True
        Txt_BBranch.Visible = True
        Lbl_ChqNo.Visible = True
        Txt_ChqNo.Visible = True
        Lbl_ChqDate.Visible = True
        Txt_ChqDate.Visible = True
        tablerow.Visible = True
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "d" Then

            Dim cbno = Txt_CBNo.Text
            Dim sr As Integer = e.CommandArgument
            Dim srno As Integer = e.CommandArgument
            Dim count As Integer
            Dim intResponse As Integer
            intResponse = MsgBox("Are you sure you want to delete this details ??", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                con.Open()

                Dim cmd1 As New SqlCommand("delete from cbtran where sr=" + sr.ToString() + "", con)
                cmd1.ExecuteNonQuery()
                'Dim cmd2 As New SqlCommand("select count(sr ) from cbtran where cbno=" & cbno & "", con)
                'count = cmd2.ExecuteScalar
                'If count = 0 Then
                '    Dim cmd As New SqlCommand("delete from cbmast where cbno=" & cbno & "", con)
                '    cmd.ExecuteNonQuery()
                '    Response.Redirect("cashbook.aspx")
                'End If

                con.Close()
            Else
                Exit Sub
            End If

            BindGrid()

            MsgBox("Entry Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information + MsgBoxStyle.MsgBoxSetForeground, "Delete")

            'ClearConrols()

        End If
    End Sub

End Class
