﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChallanInvoice.aspx.vb"
    Inherits="ChallanInvoice" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Challan Invoice</title>
    <style>
        .bottom
        {
            position: absolute;
            bottom: 0px;
        }
        
        th
        {
            border-bottom: 1px solid;
            border-left: 1px solid;
        }
        th:first-child
        {
            border-left: 0px solid;
        }
        th:last-child
        {
            border-right: 0px solid;
        }
        .printButtonClass
        {
            display: none;
        }
    </style>
</head>
<body>
    <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
</body>
</html>
