﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChallanForm.aspx.vb" Inherits="ChallanForm"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById('<%=lblGT.ClientID%>').style.display = 'none';
            document.getElementById('<%=txtRoundUp.ClientID%>').style.display = 'none';
            document.getElementById('<%=txtNetAmt.ClientID%>').style.display = 'none';

            $('#ctl00_ContentPlaceHolder1_txtAmt').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtGrossAmt').attr("readonly", true)

            $("#<%=txtItemName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetItemsForChallan") %>',
                        data: "{ 'prefix': '" + request.term + "','acccd' : '" + $("#<%=hfAccode.ClientID %>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1],
                                    val1: item.split('-')[2],
                                    rate: item.split('-')[3]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfItemId.ClientID %>").val(i.item.val);
                    $("#<%=hfItemname.ClientID %>").val(i.item.label);
                    $("#<%=hfItemQty.ClientID %>").val(i.item.val1);
                    $("#<%=txtRate.ClientID %>").val(i.item.rate);
                    GrandTotal();
                },
                minLength: 1

            });
        });  
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtAcName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetChallanAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtAcCode.ClientID %>").val(i.item.val);
                    $("#<%=hfAccode.ClientID %>").val(i.item.val);
                    $("#<%=hfAcName.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        function GrandTotal() {

            var w = document.getElementById('<%=txtWeight.ClientID%>').value;

            if (w.length < 1)
                w = 0;
            var r = document.getElementById('<%=txtRate.ClientID%>').value;
            if (r.length < 1)
                r = 0;
            var q = document.getElementById('<%=hfItemQty.ClientID%>').value;
            if (q.length < 1)
                q = 0;

            var amt = Math.round(((parseFloat(w) * parseFloat(r)) / parseFloat(q)) * 100) / 100;
            if (isNaN(amt) == true)
                amt = 0.00;
        
            document.getElementById('<%=txtAmt.ClientID%>').value = amt.toFixed(2);
            document.getElementById('<%=txtWeight.ClientID%>').value = parseFloat(Math.round(w * 100) / 100).toFixed(3);
            document.getElementById('<%=txtRate.ClientID%>').value = parseFloat(Math.round(r * 100) / 100).toFixed(2);

            var l = document.getElementById('<%=lblGT.ClientID%>').innerHTML;
            if (l.length < 1)
                l = 0;
            var gross = Math.round((parseFloat(l)) * 100) / 100;
            document.getElementById('<%=txtGrossAmt.ClientID%>').value = gross.toFixed(2);

            var net = Math.ceil(gross);

            document.getElementById('<%=txtNetAmt.ClientID%>').value = net.toFixed(2);
            var roundup = Math.round((net - gross) * 100) / 100;
            document.getElementById('<%=txtRoundUp.ClientID%>').value = roundup.toFixed(2);

        }
    </script>
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Challan Form" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" align="right" Height="20px" Width="60px" />
            </td>
        </tr>
    </table>
    <div>
        <table align="left">
            <tr>
                <td>
                    <asp:TextBox ID="Txtcompid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtuserid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtdoccd" runat="server" Enabled="False" Visible="False" Width="49px">CH</asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Panel ID="panel1" runat="server" Width="100%">
            <table class="style1">
                <tr>
                    <td class="style10">
                        <asp:Label ID="Label3" runat="server" Text="Sr. No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtSrNo" runat="server" Enabled="false" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label22" runat="server" Text="Lot No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtLotNo" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label10" runat="server" Text="Challan No."></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtChallanNo" runat="server" Enabled="false" ReadOnly="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtChallanNo"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style11">
                        <asp:Label ID="Label21" runat="server" Text="Challan Date"></asp:Label>
                    </td>
                    <td class="style11">
                        <BDP:BasicDatePicker ID="txtdate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtdate"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style10">
                        <asp:Label ID="Label7" runat="server" Text="Account Name"></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtAcName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtAcName"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label23" runat="server" Text="A/c Code"></asp:Label>
                    </td>
                    <td class="style10">
                        <asp:TextBox ID="txtAcCode" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="style10">
                        <asp:HiddenField ID="hfAccode" runat="server" />
                    </td>
                    <td class="style10">
                        <asp:HiddenField ID="hfAcName" runat="server" />
                    </td>
                    <td class="style11">
                        <asp:HiddenField ID="hfItemId" runat="server" />
                    </td>
                    <td class="style11">
                        <asp:HiddenField ID="hfItemname" runat="server" />
                    </td>
                </tr>
                <caption>
                    <hr>
                </caption>
            </table>
        </asp:Panel>
        <asp:Panel ID="panel2" runat="server" Width="100%">
            <table width="100%" class="style1">
                <tr>
                    <td class="style2">
                        <asp:Label ID="Label8" runat="server" Text="Item Name"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtItemName"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style2">
                        <asp:Label ID="Label11" runat="server" Text="Quantity"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtQty" runat="server" Style="margin-left: 0px" Text="0"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtQty"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hfItemQty" runat="server" />
                    </td>
                    <td class="style2">
                        <asp:Label ID="Label24" runat="server" Text="Weight"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtWeight" runat="server" Class="decimal" onchange="return GrandTotal();"
                            Text="0.000"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtWeight"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style2">
                        <asp:Label ID="Label26" runat="server" Text="Rate"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtRate" runat="server" Class="decimal" onchange="GrandTotal()">0.0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtRate"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style2">
                        <asp:Label ID="Label27" runat="server" Text="Amount"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtAmt" runat="server" Class="decimal">0.0</asp:TextBox>
                    </td>
                    <td class="style2">
                        <asp:HiddenField ID="hfChallanTranID" runat="server" Value="0" />
                        <asp:Button ID="btnSaveChallan" runat="server" Text="Save Item" ValidationGroup="Save" />
                    </td>
                </tr>
                <caption>
                    <hr />
                </caption>
            </table>
        </asp:Panel>
        <asp:Panel ID="panel3" runat="server">
            <hr />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center"
                AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
                EmptyDataRowStyle-HorizontalAlign="Center" PageSize="2" HeaderStyle-BackColor="Black"
                HeaderStyle-ForeColor="White" ShowFooter="true">
                <Columns>
                    <asp:BoundField DataField="tserno" HeaderText="Sr No." ReadOnly="true" ItemStyle-HorizontalAlign="Center"
                        SortExpression="tserno" />
                    <asp:BoundField DataField="item" HeaderText="Item Name" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="qty" HeaderText="Quantity" ReadOnly="true" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="weight" HeaderText="Weight" ReadOnly="true" ItemStyle-HorizontalAlign="Right" />
                    <%--<asp:BoundField DataField="rate" HeaderText="Rate" ReadOnly="true" />--%>
                    <%--<asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="true" />--%>
                    <asp:TemplateField HeaderText="Rate" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblRate" runat="server" Text='<%# Eval("rate") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblPageTotal" runat="server" Text="Page Total"></asp:Label>
                            <br />
                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                        </FooterTemplate>
                        <%--<FooterStyle HorizontalAlign="Right" />--%>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                        </FooterTemplate>
                        <%--<FooterStyle HorizontalAlign="Right" />--%>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnedit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                CommandArgument='<%#Eval("TransID")%>' />
                            <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                CommandArgument='<%#Eval("TransID")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Gross Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtGrossAmt" runat="server" onchange="GrandTotal()">0.00</asp:TextBox>
                    </td>
                    <%--<td>
                        <asp:Label ID="Label9" runat="server" Text="APMC"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAPMC" runat="server"></asp:TextBox>
                    </td>--%>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Round Up" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRoundUp" runat="server" onchange="GrandTotal()"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Net Amount" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNetAmt" runat="server" onchange="GrandTotal()"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Label ID="lblGT" runat="server"></asp:Label>
            <asp:Button ID="btnSaveConfirmChallanWRate" runat="server" Text="Generate Challan with Rate" />
            &nbsp;<asp:Button ID="btnSaveConfirmChallanWoRate" runat="server" Text="Generate Challan without Rate" />
            <br />
        </asp:Panel>
</asp:Content>
