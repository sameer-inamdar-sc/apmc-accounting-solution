﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/PurchaseForm.aspx.vb"
    Inherits="PurchaseForm" MasterPageFile="~/MasterPage.master" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <script>

        function 

 ConfirmSubmit() {


            if (confirm("Save changes?") == true) return true;
            else return false;
        } 

    </script>
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input#ctl00_ContentPlaceHolder1_txtAPMC").hide();
            document.getElementById("ctl00_ContentPlaceHolder1_txtHamali").style.display = 'none';
            document.getElementById("ctl00_ContentPlaceHolder1_lblAmount").style.display = 'none';
            document.getElementById("ctl00_ContentPlaceHolder1_lblAPMC").style.display = 'none';
            document.getElementById("ctl00_ContentPlaceHolder1_lblHamali").style.display = 'none';
            document.getElementById("ctl00_ContentPlaceHolder1_lblMapLevy").style.display = 'none';
            $('#ctl00_ContentPlaceHolder1_txtAmt').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtGrossAmt').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtRoundup').attr("readonly", true)
            $('#ctl00_ContentPlaceHolder1_txtNetAmt').attr("readonly", true)


            //            $("input#ctl00_ContentPlaceHolder1_txtHamali").hide();
            $("input#ctl00_ContentPlaceHolder1_txtMapLevy").hide();

            $("#<%=txtItemName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetItemsForPurchase") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1],
                                    val1: item.split('-')[2],
                                    
                                    val3: item.split('-')[4],
                                    val4: item.split('-')[5],
                                    val5: item.split('-')[6],
                                    val6: item.split('-')[7]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfItemname.ClientID %>").val(i.item.label);
                    $("#<%=hfItemId.ClientID %>").val(i.item.val);
                    $("#<%=hfItemQty.ClientID %>").val(i.item.val1);

                    $("#<%=hfItmExpHamali.ClientID %>").val(i.item.val3);
                    $("#<%=hfItemExpTolai.ClientID %>").val(i.item.val4);
                    $("#<%=hfItemExpAPMC.ClientID %>").val(i.item.val5);
                    $("#<%=hfItemExpMapLevy.ClientID %>").val(i.item.val6);
                    Sum();
                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtTransName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetTransDtls") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfTransId.ClientID %>").val(i.item.val);
                    $("#<%=hfTransName.ClientID %>").val(i.item.label);
                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtAcName.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetPurchaser") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtAcCode.ClientID %>").val(i.item.val);
                    $("#<%=hfAccode.ClientID %>").val(i.item.val);
                    $("#<%=hfAcName.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript" language="javascript">

        function Sum() {
            

            var w = document.getElementById('<%=txtWeight.ClientID%>').value;
            if (w.length < 1)
                w = 0;
            var r = document.getElementById('<%=txtRate0.ClientID%>').value;
            if (r.length < 1)
                r = 0;
            var q = document.getElementById('<%=hfItemQty.ClientID%>').value;
            if (q.length < 1)
                q = 0;
            document.getElementById('<%=txtWeight.ClientID%>').value = parseFloat(Math.round(w * 100) / 100).toFixed(3);
            var amt = Math.round(((parseFloat(w) * parseFloat(r)) / parseFloat(q)) * 100) / 100;
            document.getElementById('<%=txtAmt.ClientID%>').value = amt;
            document.getElementById('<%=txtRate0.ClientID%>').value = parseFloat(Math.round(r * 100) / 100).toFixed(2);

            var apmc = Math.round((((document.getElementById('<%=txtAmt.ClientID%>').value) * (document.getElementById('<%=hfItemExpAPMC.ClientID%>').value)) / 100) * 100) / 100;
            document.getElementById('<%=hfAPMC.ClientID%>').value = apmc;
            document.getElementById('<%=txtAPMC.ClientID%>').value = apmc;
            var hamali = (document.getElementById('<%=txtQty.ClientID%>').value) * (document.getElementById('<%=hfItmExpHamali.ClientID%>').value);
            document.getElementById('<%=hfHamali.ClientID%>').value = hamali;
            document.getElementById('<%=txtHamali.ClientID%>').value = hamali;
            var levy = Math.round(((document.getElementById('<%=txtWeight.ClientID%>').value) * (document.getElementById('<%=hfItemExpMapLevy.ClientID%>').value)) * 100) / 100;
            document.getElementById('<%=hfMapLevy.ClientID%>').value = levy.toFixed(4);
            document.getElementById('<%=txtMapLevy.ClientID%>').value = levy.toFixed(4);
            
            GrandTotal();

            return false;

        }


        function GrandTotal() {
           
            var amt = document.getElementById('<%=txtAmt.ClientID%>').value;
            document.getElementById('<%=txtAmt.ClientID%>').value = parseFloat(Math.round(amt * 100) / 100).toFixed(2);
            if (amt.length < 1)
                amt = 0;
            var b = document.getElementById('<%=txtBharai.ClientID%>').value;
            document.getElementById('<%=txtBharai.ClientID%>').value = parseFloat(Math.round(b * 100) / 100).toFixed(2);
            if (b.length < 1)
                b = 0;

            var u = document.getElementById('<%=txtUtarai.ClientID%>').value;
            document.getElementById('<%=txtUtarai.ClientID%>').value = parseFloat(Math.round(u * 100) / 100).toFixed(2);
            if (u.length < 1)
                u = 0;
            var apmc = document.getElementById('<%=hfAPMC.ClientID%>').value;
            if (apmc.length < 1)
                apmc = 0;
            var h = document.getElementById('<%=hfHamali.ClientID%>').value;
            if (h.length < 1)
                h = 0;
            var l = document.getElementById('<%=hfMapLevy.ClientID%>').value;
            if (l.length < 1)
                l = 0;
            var t = document.getElementById('<%=txtTolai.ClientID%>').value;
            document.getElementById('<%=txtTolai.ClientID%>').value = parseFloat(Math.round(t * 100) / 100).toFixed(2);
            if (t.length < 1)
                t = 0;
            var f = document.getElementById('<%=txtFreight.ClientID%>').value;
            document.getElementById('<%=txtFreight.ClientID%>').value = parseFloat(Math.round(f * 100) / 100).toFixed(2);
            if (f.length < 1)
                f = 0;

            var f5 = document.getElementById('<%=lblAmount.ClientID%>').innerHTML;
            if (f5.length < 1)
                f5 = 0;
            var f6 = document.getElementById('<%=lblAPMC.ClientID%>').innerHTML;
            if (f6.length < 1)
                f6 = 0;
            var f7 = document.getElementById('<%=lblHamali.ClientID%>').innerHTML;
            if (f7.length < 1)
                f7 = 0;
            var f8 = document.getElementById('<%=lblMapLevy.ClientID%>').innerHTML;
            if (f8.length < 1)
                f8 = 0;


            var gross = Math.round((parseFloat(b) + parseFloat(u) + parseFloat(t) + parseFloat(f) + parseFloat(f5) + parseFloat(f6) + parseFloat(f7) + parseFloat(f8)) * 100) / 100;
            document.getElementById('<%=txtGrossAmt.ClientID%>').value = gross.toFixed(2);
            var net = Math.ceil(gross);
            document.getElementById('<%=txtNetAmt.ClientID%>').value = net.toFixed(2);
            var roundup = Math.round((net - gross) * 100) / 100;
            document.getElementById('<%=txtRoundup.ClientID%>').value = roundup.toFixed(2);

            return false;
            //Sum();
        }


    </script>
    <style type="text/css">
        .hs
        {
            padding-right: 10px;
        }
        .style1
        {
            width: 1318px;
        }
        .style2
        {
            width: 131px;
        }
        .style6
        {
            width: 132px;
        }
        .style7
        {
            width: 190px;
        }
    </style>
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label2" runat="server" Text="Purchase Form" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" align="right" Height="20px" Width="60px" />
            </td>
        </tr>
    </table>
    <div>
        <table align="left">
            <tr>
                <td>
                    <asp:TextBox ID="Txtcompid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtuserid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtdoccd" runat="server" Enabled="False" Visible="False" Width="49px">PU</asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="Txtmjcd" runat="server" Enabled="False" Visible="False" Width="49px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <form id="form1">
        <asp:Panel ID="panel1" runat="server" Width="100%">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Sr. No."></asp:Label>
                    </td>
                    <td colspan="2" style="margin-left: 80px">
                        <asp:TextBox ID="txtSrNo" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label22" runat="server" Text="Lot No."></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLotNo" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td width="100">
                        <asp:Label ID="Label10" runat="server" Text="Bill No."></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBillNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtBillNo"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Label ID="Label21" runat="server" Text="Date"></asp:Label>
                    </td>
                    <td>
                        <BDP:BasicDatePicker ID="txtdate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtdate"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Account Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAcName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtAcName"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="Label23" runat="server" Text="A/c Code"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAcCode" runat="server" Enabled="false"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:HiddenField ID="hfAccode" runat="server" />
                    </td>
                    <td>
                        <asp:HiddenField ID="hfAcName" runat="server" />
                    </td>
                    <td>
                        <asp:HiddenField ID="hfItemId" runat="server" />
                    </td>
                    <td>
                        <asp:HiddenField ID="hfItemname" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <caption>
                    <hr />
                </caption>
            </table>
        </asp:Panel>
        <asp:Panel ID="panel2" runat="server" Width="100%">
            <table width="100%">
                <tr>
                    <td align="right" style="margin-left: 200px">
                        <asp:Label ID="Label8" runat="server" Text="Item Name"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtItemName" runat="server" onchange="return Sum();"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtItemName"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label11" runat="server" Text="Quantity"></asp:Label>
                    </td>
                    <td align="left" style="margin-left: 40px">
                        <asp:TextBox ID="txtQty" runat="server" Style="margin-left: 0px" Text="0"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtQty"
                            ErrorMessage="*" InitialValue="0" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label24" runat="server" Text="Weight"></asp:Label>
                    </td>
                    <td align="left" colspan="4">
                        <asp:TextBox ID="txtWeight" runat="server" Text="0.000" Class="decimal" 
                            onchange="return Sum();"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtWeight"
                            ErrorMessage="*" InitialValue="0" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label26" runat="server" Text="Rate"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtRate0" runat="server" Class="decimal" onchange="return Sum();">0.00</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtRate0"
                            ErrorMessage="*" InitialValue="0.0" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left">
                        <asp:Label ID="Label27" runat="server" Text="Amount"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtAmt" runat="server" onchange="return Sum();">0.00</asp:TextBox>
                    </td>
                    <td align="right">
                        &nbsp;
                    </td>
                    <td align="left">
                        &nbsp;
                    </td>
                    <td align="left">
                        <asp:Button ID="btnSaveInGrid" runat="server" Text="SAVE ITEM" ValidationGroup="Save" />
                    </td>
                    <td align="left">
                        <asp:HiddenField ID="hfItemQty" runat="server" />
                        <asp:HiddenField ID="hfConfirmClickeOrNot" runat="server" Value="0" />
                    </td>
                </tr>
                <caption>
                    <hr />
                    <%--<tr>
                        <td align="center" colspan="13">
                            <hr width="100%" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="right" style="margin-left: 200px">
                            <asp:Label ID="Label29" runat="server" Text="APMC" Visible="False"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAPMC" runat="server" ReadOnly="True" Width="100px" onchange="return GrandTotal();"
                                Text="0.00"></asp:TextBox>
                        </td>
                        <td align="right">
                            <asp:Label ID="Label30" runat="server" Text="Hamali" Visible="False"></asp:Label>
                            <asp:HiddenField ID="hfPurTranID" runat="server" Value="0" />
                        </td>
                        <td align="left" style="margin-left: 40px">
                            <asp:TextBox ID="txtHamali" runat="server" ReadOnly="True" Width="100px" onchange="return GrandTotal();">0.00</asp:TextBox>
                        </td>
                        <td align="right">
                            <asp:Label ID="Label36" runat="server" Text="Mapadi Levy" Visible="False"></asp:Label>
                        </td>
                        <td align="left" class="style7">
                            <asp:TextBox ID="txtMapLevy" runat="server" ReadOnly="True" Width="100px" onchange="return GrandTotal();">0.00</asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:HiddenField ID="hfItemExpAPMC" runat="server" />
                            <asp:HiddenField ID="hfAPMC" runat="server" />
                        </td>
                        <td align="left">
                            <asp:HiddenField ID="hfItmExpHamali" runat="server" />
                            <asp:HiddenField ID="hfHamali" runat="server" />
                        </td>
                        <td align="left">
                            <asp:HiddenField ID="hfItemExpMapLevy" runat="server" />
                            <asp:HiddenField ID="hfMapLevy" runat="server" />
                        </td>
                    </tr>
                </caption>
            </table>
            <hr />
        </asp:Panel>
        <asp:Panel ID="panel3" runat="server">
            <br />
            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" Height="20px"
                HorizontalAlign="Center" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC"
                BorderStyle="None" BorderWidth="1px" PageSize="8" CellPadding="4" ForeColor="Black"
                Width="100%" ShowFooter="true">
                <Columns>
                    <asp:BoundField HeaderText="No." DataField="tserno" ItemStyle-HorizontalAlign="Center">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Item Name" DataField="item"></asp:BoundField>
                    <asp:BoundField HeaderText="Quantity" DataField="qty" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Weight" DataField="weight" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <%--<asp:BoundField HeaderText="Rate" DataField="rate" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Amount" DataField="amount" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="APMC" DataField="lapmc" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Hamali" DataField="lhamali" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Mapadi Levy" DataField="lmaplevy" ItemStyle-HorizontalAlign="right">
                    </asp:BoundField>--%>
                    <asp:TemplateField HeaderText="Rate" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblRate" runat="server" Text='<%# Eval("rate") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblPageTotal" runat="server" Text="Page Total"></asp:Label>
                            <br />
                            
                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label22" runat="server" Text='<%# Eval("amount") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblAmountTotal" runat="server"></asp:Label>
                            <br />
                            
                            <asp:Label ID="lblAmountGrandTotal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="APMC" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label23" runat="server" Text='<%# Eval("lapmc") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblAPMCTotal" runat="server"></asp:Label>
                            <br />
                            
                            <asp:Label ID="lblAPMCGrandTotal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Hamali" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label24" runat="server" Text='<%# Eval("lhamali") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblHamaliTotal" runat="server"></asp:Label>
                            <br />
                            
                            <asp:Label ID="lblHamaliGrandTotal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mapadi Levy" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="Label25" runat="server" Text='<%# Eval("lmaplevy") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblMapLevyTotal" runat="server"></asp:Label>
                            <br />
                           
                            <asp:Label ID="lblMapLevyGrandTotal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actions">
                        <ItemTemplate>
                            <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                CommandArgument='<%#Eval("TransID")%>' />
                            <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                CommandArgument='<%#Eval("TransID")%>' />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataRowStyle HorizontalAlign="Center" />
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" HorizontalAlign="left" />
                <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
            </asp:GridView>
            <table width="100%" runat="server" class="style1">
                <tr>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td class="style6">
                        &nbsp;
                    </td>
                    <td class="style6">
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </td>
                    <td class="style6">
                        <asp:Label ID="lblAPMC" runat="server"></asp:Label>
                    </td>
                    <td class="style6">
                        <asp:Label ID="lblHamali" runat="server"></asp:Label>
                    </td>
                    <td class="style6">
                        <asp:Label ID="lblMapLevy" runat="server"></asp:Label>
                    </td>
                    <td class="style6">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="panel4" runat="server">
            <table width="100%" runat="server">
                <tr>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="Bharai"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBharai" runat="server" Class="decimal" onchange="return GrandTotal();"
                            Width="100px">0.00</asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label28" runat="server" Text="Utrai"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUtarai" runat="server" Class="decimal" onchange="return GrandTotal();"
                            Width="100px">0.00</asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label37" runat="server" Text="Tolai"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTolai" runat="server" Class="decimal" onchange="return GrandTotal();"
                            Width="100px">0.00</asp:TextBox>
                        <asp:HiddenField ID="hfItemExpTolai" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="Label34" runat="server" Text="Freight"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFreight" runat="server" Class="decimal" onchange="return GrandTotal();"
                            Width="100px">0.00</asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label35" runat="server" Text="Transporter Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTransName" runat="server" Width="250px"></asp:TextBox>
                        <asp:HiddenField ID="hfTransId" runat="server" />
                        <asp:HiddenField ID="hfTransName" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="Label40" runat="server" Text="Gross Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtGrossAmt" runat="server" Width="100px" onchange="return GrandTotal();">0.00</asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label38" runat="server" Text="Roundup"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRoundup" runat="server" Width="100px" onchange="return GrandTotal();">0.00</asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label39" runat="server" Text="Net Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNetAmt" runat="server" Width="100px" Height="22px" onchange="return GrandTotal();">0.00</asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:HiddenField ID="hfUpdate" runat="server" Value="0" />
                        <asp:Button ID="btnSaveTran" runat="server" Text="SAVE &amp; CONFIRM" />
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        </form>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
</asp:Content>
