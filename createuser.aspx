﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="createuser.aspx.vb" Inherits="Default2" %>
<% @Import Namespace="System.Web.Mail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
     <script type="text/javascript">
    function disableautocompletion(id)
    { 
    var passwordControl = document.getElementById(id);
    passwordControl.setAttribute("autocomplete", "off");
    }
     
    </script>
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
        .style5
        {
            width: 317px;
            border-style: solid;
            border-width: 3px;
        }
        .style7
        {
            height: 9px;
            width: 198px;
        }
        .style9
        {
            height: 9px;
            width: 176px;
        }
        .style12
        {
            width: 176px;
        }
        .style15
        {
            width: 198px;
        }
        </style>
</head>
<body background="img/background01.jpg">
   <form id="form1" runat="server">
    <div>
    
    
            <asp:Button ID="Button7" runat="server" Text="Add User" />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button 
                ID="Button8" runat="server" Text="Modify User" />
            &nbsp;&nbsp;&nbsp;
    
    
     <asp:Panel runat="server" id="pnl_add">
        <table  cellspacing="0" cellpadding="0" width="300PX" height="250PX" border="1">
        <tr>
        <td>
     <table align="center" height="220px" width="260px" 
                style="margin-right: 47px">
        <tr>
        <td colspan="4">
        <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
        </td>
        </tr>
        <tr>
            <td>
        <asp:Label ID="Label9" runat="server" Text="Company" Font-Bold="True"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        <td>
            <asp:DropDownList ID="ddcomplist" runat="server" style="margin-left: 0px">
            </asp:DropDownList>
            </td>
        </tr>
        <tr>
        <td>
        <asp:Label ID="Label2" runat="server" Text="First Name" Font-Bold="True"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
        <asp:TextBox ID="Txtuname" runat="server" 
                        TabIndex="1" onfocus="disableautocompletion(this.id)"></asp:TextBox>
       <asp:TextBox ID="Txtfrmemailid" runat="server" Width="218px" Enabled="False" 
            Visible="False">sushilj02@gmail.com</asp:TextBox>
        </td>
        </tr>
        <tr>
        <td>
        <asp:Label ID="Label3" runat="server" Text="Last Name" Font-Bold="True"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
         <asp:TextBox ID="Txtsname" runat="server" TabIndex="2" onfocus="disableautocompletion(this.id)"></asp:TextBox>
         <asp:TextBox ID="Txtfrmemailpass" runat="server" Width="218px" Enabled="False" 
            Visible="False">amdfinom</asp:TextBox>
         </td>
      </tr>
      <tr>
      <td>
        <asp:Label ID="Label7" runat="server" Text="Email ID" Font-Bold="True"></asp:Label>
      </td>
      <td>
          &nbsp;</td>
      <td>
          &nbsp;</td>
      <td> 
       <asp:TextBox ID="Txtemailid" runat="server" TabIndex="3" onfocus="disableautocompletion(this.id)"></asp:TextBox>
       <asp:TextBox ID="Txttoemailid" runat="server" Width="218px" Enabled="False" Visible="False">spsmail01@gmail.com</asp:TextBox>
      </td>
      </tr>
      <tr>
      <td>
       <asp:Label ID="Label8" runat="server" Text="Mobile No." Font-Bold="True"></asp:Label>
       </td>
      <td>
          &nbsp;</td>
      <td>
          &nbsp;</td>
       <td>
         <asp:TextBox ID="Txtmobno" runat="server" TabIndex="4" onfocus="disableautocompletion(this.id)"></asp:TextBox>
        <asp:TextBox ID="txtSMTP_Host" runat="server" Width="116px" Enabled="False" Visible="False">smtp.gmail.com</asp:TextBox>
        <asp:TextBox ID="txtSMTP_Port" runat="server" Width="115px" Enabled="False" 
            Visible="False">587</asp:TextBox>
        <asp:TextBox ID="Txtsubject" runat="server" Width="219px" Enabled="False" 
            Visible="False">Requesting for User ID &amp; Password</asp:TextBox>
       </td>
       </tr>
      <tr>
      <td>
       <asp:Label ID="Label11" runat="server" Text="Role" Font-Bold="True"></asp:Label>
       </td>
      <td>
          &nbsp;</td>
      <td>
          &nbsp;</td>
       <td>
           <asp:DropDownList ID="Ddl_role" runat="server">
               <asp:ListItem Selected="True">User</asp:ListItem>
               <asp:ListItem>Admin</asp:ListItem>
           </asp:DropDownList>
       </td>
       </tr>
      <tr>
      <td class="style1">
       <asp:Label ID="Label10" runat="server" Text="UserId" Font-Bold="True"></asp:Label>
       </td>
      <td class="style1">
          </td>
      <td class="style1">
          </td>
       <td class="style1">
           <asp:TextBox ID="txt_userid" runat="server"></asp:TextBox>
       </td>
       </tr>
       <tr>
       <td colspan="4">
           <asp:TextBox ID="Txtmessage" runat="server" Width="215px" TextMode="MultiLine" Enabled="False" Visible="False"></asp:TextBox>
        </td>
        </tr>
       <tr>
       <td colspan="4">
          <asp:Button ID="Button1" runat="server" Text="Create User" TabIndex="5" 
               Width="100px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        </asp:Panel>
           
    </div>
    
    <asp:Panel ID="Panel1" runat="server" >
        
        <table class="style5" align="" frame="box">
            <tr>
                <td class="style12">
                    <asp:Label ID="Label19" runat="server" Text="USERID"></asp:Label>
                </td>
                <td class="style15">
                    <asp:DropDownList ID="ddluser" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label20" runat="server" Text="COMPANY"></asp:Label>
                </td>
                <td class="style15">
                    <asp:DropDownList ID="ddlcomp" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label21" runat="server" Text="FIRSTNAME"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="Txtfname0" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label22" runat="server" Text="LASTNAME"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="Txtlname0" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label23" runat="server" Text="EMAILID"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="Txteid0" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label24" runat="server" Text="MOB NO"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="Txtmno0" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style12">
                    <asp:Label ID="Label25" runat="server" Text="ROLE"></asp:Label>
                </td>
                <td class="style15">
                    <asp:DropDownList ID="ddlrole" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style9">
                    <asp:Button ID="Button5" runat="server" Text="UPDATE" />
                </td>
                <td class="style7">
                    <asp:Button ID="Button6" runat="server" Text="DELETE" />
                </td>
            </tr>
        </table>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </asp:Panel>
    </form>
</body>
</html>
