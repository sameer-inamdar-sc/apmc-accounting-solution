﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Place.aspx.vb" Inherits="Place" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#TextBox1").datepicker();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtatpost.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetDetailsbyatpost") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (Place) {
                                return {
                                    label: Place.split('|')[0],
                                    label1: Place.split('|')[1],
                                    label2: Place.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                    $("#<%=txtatpost.ClientID %>").val(i.item.label);
                    $("#<%=txttal.ClientID %>").val(i.item.label1)
                    $("#<%=txtdist.ClientID %>").val(i.item.label2);


                },
                minLength: 1

            });
        }); 
    </script>
    <script type="text/javascript">
          $(document).ready(function () {
              $("#<%=txttal.ClientID %>").autocomplete({
                  autoFocus: true,
                  source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("~/Service.asmx/GetDetailsbytal") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          contentType: "application/json; charset=utf-8",
                          success: function (data) {
                              response($.map(data.d, function (Place) {
                                  return {
                                      label: Place.split('|')[0],
                                      label1: Place.split('|')[1],
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  },
                  select: function (e, i) {

                      $("#<%=txttal.ClientID %>").val(i.item.label)
                      $("#<%=txtdist.ClientID %>").val(i.item.label1);


                  },
                  minLength: 1

              });
          }); 
    </script>
    <script type="text/javascript">
          $(document).ready(function () {
              $("#<%=txtdist.ClientID %>").autocomplete({
                  autoFocus: true,
                  source: function (request, response) {
                      $.ajax({
                          url: '<%=ResolveUrl("~/Service.asmx/GetDetailsbydist") %>',
                          data: "{ 'prefix': '" + request.term + "'}",
                          dataType: "json",
                          type: "POST",
                          contentType: "application/json; charset=utf-8",
                          success: function (data) {
                              response($.map(data.d, function (Place) {
                                  return {
                                      label: Place.split('|')[0],
                                  }
                              }))
                          },
                          error: function (response) {
                              alert(response.responseText);
                          },
                          failure: function (response) {
                              alert(response.responseText);
                          }
                      });
                  },
                  select: function (e, i) {

                      $("#<%=txtdist.ClientID %>").val(i.item.label);


                  },
                  minLength: 1

              });
          }); 
    </script>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style4
        {
            width: 1296px;
        }
        .style8
        {
        }
        .style9
        {
            width: 130px;
        }
        </style>
    <table class="style4" width="100%">
        <tr>
            <td colspan="3" style="padding-left: 5px;">
                <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Place Master"></asp:Label>
            </td>
            <td colspan="3" style="padding-left: 5px;">
                <asp:ToolkitScriptManager ID="ScriptManager1" runat="server" />
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
                <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="zhol"
                    PopupControlID="Panel1" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
                </asp:ModalPopupExtender>
                <asp:Button ID="zhol" runat="server" Style="display: none;" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8">
                <asp:Label ID="Label9" runat="server" Text="Place" Width="50px"></asp:Label>
            </td>
            <td align="left" class="style8">
                <asp:TextBox ID="txt_serachplace" runat="server" Width="150px"></asp:TextBox>
            </td>
            <td align="right" class="style8" colspan="2">
                <asp:Label ID="Label10" runat="server" Text="Post" Width="50px"></asp:Label>
            </td>
            <td align="left" class="style8">
                <asp:TextBox ID="txt_searchpost" runat="server" Width="150px"></asp:TextBox>
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="Label11" runat="server" Text="Taluka" Width="100px"></asp:Label>
            </td>
            <td align="left" class="style9">
                <asp:TextBox ID="txt_searchtaluka" runat="server" Width="150px"></asp:TextBox>
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label12" runat="server" Text="District" Width="100px"></asp:Label>
            </td>
            <td align="left" class="style9">
                <asp:TextBox ID="txt_searchdist" runat="server" Width="150px"></asp:TextBox>
            </td>
            <td align="left" class="style9">
                <asp:Button ID="Btn_Search" runat="server" Text="Search" Width="50px" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="11">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="11">
                <asp:Panel ID="Panel4" runat="server">
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="serno"
                        HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
                        EmptyDataRowStyle-HorizontalAlign="Center" PageSize="15" HeaderStyle-BackColor="Black"
                        HeaderStyle-ForeColor="White">
                        <Columns>
                            <asp:BoundField DataField="serno" HeaderText="Sr. No." ReadOnly="True" SortExpression="serno"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="place" HeaderText="Place" SortExpression="place" />
                            <asp:BoundField DataField="atpost" HeaderText="Post" SortExpression="atpost" />
                            <asp:BoundField DataField="tal" HeaderText="Taluka" SortExpression="tal" />
                            <asp:BoundField DataField="dist" HeaderText="District" SortExpression="dist" />
                            <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                        CommandArgument='<%#Eval("serno")%>' />
                                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                        OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("serno")%>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server" Width="371px" Style="display: " BackColor="White"
        ForeColor="Black" Font-Bold="true" Font-Size="X-Large" Font-Names="Book Antiqua"
        Height="269px" BorderColor="White" BorderStyle="Outset" BorderWidth="6px">
        <div id="dialog" title="Add Place Master" class="dialog" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller">
            <table align="center" style="width: 369px; height: 160px;">
                <tr>
                    <td align="center" colspan="2" height="15" style="line-height: 50px">
                        <asp:Label ID="lbl_popuphead" runat="server" Font-Bold="True" Font-Italic="True"
                            Font-Names="Bookman Old Style" Font-Overline="True" Font-Size="X-Large" Font-Underline="True"
                            Text="Place Details"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td >

                    </td>
                    <td >

                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="right" >
                        <asp:Label ID="Label13" runat="server" Text="Sr. No." Width="100px"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_srno" runat="server" onfocus="disableautocompletion(this.id)"
                            ReadOnly="True" TabIndex="2" Width="225px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3" align="right">
                        <asp:Label ID="Label4" runat="server" Text="Place" Width="100px"></asp:Label>
                    </td>
                    <td class="style2" align="left">
                        <asp:TextBox ID="Txtplace" runat="server" onfocus="disableautocompletion(this.id)"
                            TabIndex="2" Width="225px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Txtplace"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style3" align="right">
                        <asp:Label ID="Label5" runat="server" Text="At. Post" Width="100px"></asp:Label>
                    </td>
                    <td class="style2" align="left">
                        <asp:TextBox ID="Txtatpost" runat="server" onfocus="disableautocompletion(this.id)"
                            Width="225px" TabIndex="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3" align="right">
                        <asp:Label ID="Label6" runat="server" Text="Tal." Width="100px"></asp:Label>
                    </td>
                    <td class="style2" align="left">
                        <asp:TextBox ID="Txttal" runat="server" onfocus="disableautocompletion(this.id)"
                            Width="225px" TabIndex="4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3" align="right">
                        <asp:Label ID="Label7" runat="server" Text="Dist." Width="100px"></asp:Label>
                    </td>
                    <td class="style2" align="left">
                        <asp:TextBox ID="Txtdist" runat="server" onfocus="disableautocompletion(this.id)"
                            Width="225px" TabIndex="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="line-height: 40px">
                        <asp:Button ID="btnsave" runat="server" Text="Save" TabIndex="6" Width="50px" 
                            ValidationGroup="Save" style="height: 26px" />
                        &nbsp;
                        <asp:Button ID="Btn_Cancle" runat="server" Text="Cancle" Width="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
