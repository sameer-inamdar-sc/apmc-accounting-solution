﻿
Imports System.Data.SqlClient
Imports System.Data
Partial Class SalesMain
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Private Property Query As String

    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Session.Item("Sr No.") = ""
        Response.Redirect("SalesForm.aspx")
    End Sub
    Protected Sub BindGrid()
        Dim filter As String = ""

        If txtSearchInvoice.Text <> "" Then
            filter = "and s.serno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        'If txtSearchItemName.Text <> "" Then
        '    filter = filter & "and item like '" & txtSearchItemName.Text & "%'"
        'End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and InvoiceDate BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "' "
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()

        Dim cmd As New SqlCommand("SELECT s.serno, s.challanNo, CONVERT(VARCHAR(10), InvoiceDate, 105) AS date,a.acccd ,a.acname ,s.lotno ,s.tamount,s.grossamt,s.roundoff,s.netamt FROM smast s join acmast a on  s.acccd=a.acccd " & filter & " ORDER BY serno DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()

    End Sub


    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtSearchFromDate.SelectedDate = Date.Now.AddDays(-7)
            txtSearchToDate.SelectedDate = Date.Today
            BindGrid()
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        con.Open()
        If e.CommandName = "e" Then
            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("~/SalesForm.aspx?id=" & srno)

        End If

        If e.CommandName = "d" Then
            Dim intResponse As Integer
            intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            If intResponse = MsgBoxResult.Yes Then
                Dim Cmd As New SqlCommand("delete from smast where serno=" & srno & "delete from saltran where serno=" & srno, con)
                Cmd.ExecuteNonQuery()

                Query = "ledger_Delete"
                Dim com As New SqlCommand(Query, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.AddWithValue("@serno", srno)
                com.Parameters.AddWithValue("@compid", Session("compid"))
                com.Parameters.AddWithValue("@doccd", "SA")
                com.ExecuteNonQuery()

            ElseIf intResponse = MsgBoxResult.No Then
                Response.Redirect("~/SalesForm.aspx?id=", srno)
            End If
        End If

        con.Close()
        BindGrid()

    End Sub
End Class
