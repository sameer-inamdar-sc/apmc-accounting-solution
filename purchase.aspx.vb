﻿
Imports System.Data.SqlClient
Imports System.Data

Partial Class purchase
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Btn_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Session.Item("Sr No.") = ""
        Response.Redirect("PurchaseForm.aspx")
    End Sub

    Protected Sub BindGrid()
        Dim filter As String = ""

        If txtSearchInvoice.Text <> "" Then
            filter = "and pbillno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        'If txtSearchItemName.Text <> "" Then
        '    filter = filter & "and item like '" & txtSearchItemName.Text & "%'"
        'End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "' "
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()
        Dim cmd As New SqlCommand("SELECT p.serno ,[pbillno] ,CONVERT(VARCHAR(10), date, 105) AS date,a.[acccd] ,a.[acname] ,[lotno] ,sum(pt.amount) tamount,sum(pt.amount+pt.lapmc+pt.lmaplevy+pt.lhamali)netamt FROM [dbo].[pmast] p join [dbo].[acmast] a on  p.[acccd]=a.[acccd] join purtran pt on pt.serno=p.serno " & filter & "  group by tamount,p.serno,pbillno,date,a.acccd,acname,lotno,netamt", con)
        ' Dim cmd As New SqlCommand("SELECT [serno] ,[pbillno] ,CONVERT(VARCHAR(10), date, 105) AS date,a.[acccd] ,a.[acname] ,[lotno] ,[tamount] ,[netamt] FROM [dbo].[pmast] p join [dbo].[acmast] a on  p.[acccd]=a.[acccd] " & filter & " ORDER BY serno DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()

    End Sub




    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtSearchFromDate.SelectedDate = Date.Now.AddDays(-7)
            txtSearchToDate.SelectedDate = Date.Today
            BindGrid()
        End If


    End Sub
    Public query As String, constr As String

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        con.Open()
        If e.CommandName = "e" Then
            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            'Session("Sr No.") = srno
            'HiddenField1.Value = Session("Sr No.").ToString()

            Response.Redirect("~/PurchaseForm.aspx?id=" & srno)

        End If
        If e.CommandName = "d" Then

            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            Dim intResponse As Integer
            intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd As New SqlCommand("delete from pmast where serno=" & srno & "  delete from purtran where serno=" & srno, con)
                cmd.ExecuteNonQuery()
                query = "ledger_Delete"
                Dim com As New SqlCommand(Query, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.AddWithValue("@serno", srno)
                com.Parameters.AddWithValue("@compid", Session("compid"))
                com.Parameters.AddWithValue("@doccd", "PU")
                com.ExecuteNonQuery()
                con.Close()

            ElseIf intResponse = MsgBoxResult.No Then
                Response.Redirect("~/PurchaseForm.aspx?id=" & srno)
           
            End If

            'ClientScript.RegisterStartupScript(Me.[GetType](), "alert", "alert('You can't delete master entry.')", True)
        End If
        con.Close()

        BindGrid()

    End Sub
End Class
