﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Receipt.aspx.vb" Inherits="Receipt"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtacname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcName") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style1
        {
            height: 15px;
            font-size: x-small;
        }
        .style4
        {
            width: 1296px;
        }
        .style8
        {
        }
        .style9
        {
            width: 130px;
        }
    </style>
    <asp:Panel ID="panel1" runat="server">
        <table width="100%">
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Receipt Details"></asp:Label>
                </td>
                <td align="right" colspan="2">
                    &nbsp;
                </td>
                <td>
                </td>
                <td>
                </td>
                <td align="right" colspan="2">
                    <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    &nbsp;
                    <asp:HiddenField ID="hfacname" runat="server" />
                </td>
                <td align="left">
                    <asp:HiddenField ID="hfacccd" runat="server" />
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Receipt No."></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtacname0" runat="server" Width="50px"></asp:TextBox>
                </td>
                <td align="right">
                    <asp:Label ID="Label3" runat="server" Text="Account Name"></asp:Label>
                </td>
                <td align="left" colspan="2">
                    <asp:TextBox ID="txtacname" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td align="right">
                    <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                </td>
                <td align="left" class="style9">
                    <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                </td>
                <td align="right" class="style9">
                    <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                </td>
                <td align="left" class="style9">
                    <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                </td>
                <td align="left">
                    <asp:Button ID="BtnShow" runat="server" Text="Show" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <hr />
        <asp:Panel ID="panel3" runat="server">
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="serno"
                        HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" EmptyDataText="Data not found !!!"
                        EmptyDataRowStyle-HorizontalAlign="Center" PageSize="15" HeaderStyle-BackColor="Black"
                        HeaderStyle-ForeColor="White">
    </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" BackColor="White" Width="542px" ForeColor="Black"
        Font-Bold="true" Font-Size="X-Large" Font-Names="Book Antiqua" Height="227px"
        BorderColor="White" BorderStyle="Outset" BorderWidth="6px">
        <asp:Button ID="zhol" runat="server" Style="display: none;" />
        <%--<asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="zhol"
            PopupControlID="Panel2" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>--%>
        <div id="dialog" title="Add Item Expenses" class="dialog" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller; width: 432px;">
            <table style="width: 432px">
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Receipt No."></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtrecno" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Date"></asp:Label>
                    </td>
                    <td>
                        <BDP:BasicDatePicker ID="txtdate" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Account"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox1" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                        </asp:ToolkitScriptManager>
                        <asp:RadioButton ID="rbcash" runat="server" GroupName="cashbank" Text="Cash" />
                    </td>
                    <td colspan="2">
                        <asp:RadioButton ID="rbbank" runat="server" GroupName="cashbank" Text="Bank" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtamt" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Bank
                    </td>
                    <td>
                        <asp:TextBox ID="txtbank" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Branch
                    </td>
                    <td>
                        <asp:TextBox ID="txtbranch" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cheque No.
                    </td>
                    <td>
                        <asp:TextBox ID="txtchqno" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Cheque Date
                    </td>
                    <td>
                        <BDP:BasicDatePicker ID="txtchqdt" runat="server" DateFormat="dd-MM-yyyy" DisplayType="TextBox" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
