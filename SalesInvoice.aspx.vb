﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing


Partial Class SalesInvoice
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Public dt As DataTable

    Public Sub ShowSalesInvoce()
        'On Error GoTo ErrHand

        sr = CType(Page.Request.QueryString("id"), String)

        Dim s As String
        s = "select s.serno, s.acccd,a.acname,a.srplace,p.place + ' ' + p.atpost + ' ' + p.tal + ' ' + p.dist as address,cast(cellno1 as varchar(20)) + '/ ' + cast(cellno2 as varchar(20)) as phoneNo, s.invoicedate, s.lotno, s.tamount, s.grossamt, s.apmc, s.roundoff, s.netamt from smast s join acmast a on a.acccd=s.acccd join place p on a.srplace=p.serno where s.serno=" & sr
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)



        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetSalesInvoce(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If




        'ErrHand:
        'lblPageString.Text = Err.Description
    End Sub

    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetSalesInvoce(ByVal dt As DataTable) As String
        Dim sbPageString As New StringBuilder()
        Dim pd As New PrintDocument()
        Dim pg As PageSettings = pd.DefaultPageSettings
        Dim StrPrinter As String = ""
        pg.Landscape = True
        pg.Margins.Top = 30
        pg.Margins.Bottom = 30
        pg.Margins.Left = 30
        pg.Margins.Right = 30

        ''pg = New PageSettings(sbPageString)

        pg.PaperSize = New System.Drawing.Printing.PaperSize(System.Drawing.Printing.PaperKind.A4, 210, 297) ''Paper Size in MiliMeter
        pg.PrinterSettings.PrinterName = StrPrinter

        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")

        sbPageString.Append("<script type='text/javascript'>function callme() {window.print();}</script><style type='text/css' media='print'>.noprint{display:none;}</style>")
        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table style='width: 100%'><tr><td><table><tr><td align='left'></td></tr></table></td><td align='right'><img src='./img/logo.png' alt='print' border='0' height='100' width='300'></td></tr>")
        sbPageString.Append("<tr><td><a href='' alt='Print' class='noprint' onclick='callme();'>Print</a></td><td align='right'>" & Now.ToShortDateString & "</td></tr></table></table>")
        sbPageString.Append("<hr style='width: 100%' /><center>Invoice</center><hr style='width: 100%' />")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;' align='center'>")
        sbPageString.Append("<tr><td width='75%' style='border-right: 1px solid;'>")
        sbPageString.Append("<table style='width: 100%;border-spacing: 0px;'><tr><td>Name</td><td>" & dt.Rows(0).Item("acname") & "</td></tr>")
        sbPageString.Append("<tr><td>Address</td><td>" & dt.Rows(0).Item("address") & "</td></tr>")
        sbPageString.Append("<tr><td>City</td><td> </td></tr>")
        sbPageString.Append("<tr><td>Phone</td><td>" & dt.Rows(0).Item("phoneNo") & "</td></tr></table></td>")
        sbPageString.Append("<td width='25%'><table style='width: 100%'><tr><td>Invoice No.</td><td>" & dt.Rows(0).Item("serno") & "</td></tr>")
        sbPageString.Append("<tr><td>Date</td><td>" & dt.Rows(0).Item("invoicedate") & "</td></tr>")
        sbPageString.Append("<tr><td>Order No.</td><td>" & dt.Rows(0).Item("lotno") & "</td></tr>")
        sbPageString.Append("<tr><td>Ref. No.</td><td> </td></tr></table></td></tr></table><br>")

        Dim dummyDataRow = dt.Rows(0)
        PrintReportRows(sbPageString, True, dummyDataRow, dt)

        For Each irow As DataRow In dt.Rows
            PrintReportRows(sbPageString, False, irow, dt)
        Next

        sbPageString.Append("<tr><td valign='top' align='center' style='border: 1px solid;' colspan='5'>Payment Details</td>")
        sbPageString.Append("<td style='width: 20%;border: 1px solid;border-left: 0px solid;'><table style='width: 300px; height: 91px;'><tr><td align='left'>Total</td><td align='right'>" & dt.Rows(0).Item("tamount") & "</td></tr>")
        sbPageString.Append("<tr><td align='left'>APMC</td><td align='right'>" & dt.Rows(0).Item("apmc") & "</td></tr>")
        sbPageString.Append("<tr><td align='left'>Round Up</td><td align='right'>" & dt.Rows(0).Item("roundoff") & "</td></tr>")
        sbPageString.Append("<tr><td align='left'>Net Bill Amount</td><td align='right'>" & dt.Rows(0).Item("netamt") & "</td></tr></table></td></tr></tbody></table>")
        sbPageString.Append("</div>")

        Return sbPageString.ToString()
    End Function
    Dim dt1 As New DataTable
    Private Function PrintReportRows(ByVal sbPageString As StringBuilder, ByVal bPrintHeader As Boolean, ByRef iRow As DataRow, ByVal dt As DataTable) As String
        Dim amt As Double
        If bPrintHeader = True Then
            sbPageString.Append("<table style='width: 100%;border-spacing: 0px;border: 1px solid;border-bottom: 0px solid;' align='center'><tr><th width='10%'><strong>Sr No.</strong></th><th width='40%'><strong>Description</strong></td><th width='10%'><strong>Quantity</strong></th><th width='10%'><strong>Weight</strong></th><th width='10%'><strong>Unit Price</strong></th><th width='20%'><strong>Amount</strong></td></tr>")
        Else
            Dim cmd As New SqlCommand(" select saltran.TransID,saltran.serno,saltran.tserno,item.item,saltran.qty,saltran.weight,saltran.rate,saltran.amount from saltran left join item on saltran.itmcd=item.itmcd where serno= " & sr, con)
            Dim da As New SqlDataAdapter(cmd)

            da.Fill(dt1)
            For i = 0 To dt1.Rows.Count - 1
                sbPageString.Append("<tr><td width='10%' align='center'>" & dt1.Rows(i).Item("tserno") & "</td><td width='40%'>" & dt1.Rows(i).Item("item") & "</td><td width='10%' align='Right'>" & dt1.Rows(i).Item("qty") & "</td><td width='10%' align='Right'>" & dt1.Rows(i).Item("weight") & "<td width='10%' align='Right'>" & dt1.Rows(i).Item("rate") & "</td><td align='Right' width='20%'>" & dt1.Rows(i).Item("amount") & "</td></tr>")
                amt += Convert.ToDouble(dt1.Rows(i).Item("amount"))    ' ITEM 2 IS THE PRICE.
            Next
            sbPageString.Append("<tbody id='footer' class='bottom'><tr><td align='Right' colspan='5' style='border-top: 1px solid;border-right: 1px solid;'><strong>Total</strong></td><td align='Right' width='20%' style='border-top: 1px solid;'><strong>" & amt.ToString("#0.00") & "</strong></td></tr>")
        End If

        Return ""
    End Function


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ShowSalesInvoce()
        End If
    End Sub


    'Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    '    If Not Page.IsPostBack Then
    '        sr = CType(Page.Request.QueryString("id"), String)
    '        If Not sr = String.Empty Then
    '            Dim s As String
    '            s = "select s.serno, s.acccd,a.acname,a.srplace,p.place + ' ' + p.atpost + ' ' + p.tal + ' ' + p.dist as address,cast(cellno1 as varchar(20)) + '/ ' + cast(cellno2 as varchar(20)) as phoneNo, s.invoicedate, s.lotno, s.tamount, s.grossamt, s.apmc, s.roundoff, s.netamt from smast s join acmast a on a.acccd=s.acccd join place p on a.srplace=p.serno where s.serno=" & sr
    '            Dim cmd As New SqlCommand(s, con)
    '            Dim da As New SqlDataAdapter(cmd)
    '            Dim dt As New DataTable()
    '            da.Fill(dt)
    '            lblName.Text = dt.Rows(0).Item("acname")
    '            lblAddress.Text = dt.Rows(0).Item("address")
    '            lblPhone.Text = dt.Rows(0).Item("phoneNo")
    '            lblInvoiceNo.Text = dt.Rows(0).Item("serno")
    '            lblTotal.Text = dt.Rows(0).Item("tamount")
    '            lblAPMC.Text = dt.Rows(0).Item("apmc")
    '            lblRound.Text = dt.Rows(0).Item("roundoff")
    '            lblGTInvoice.Text = dt.Rows(0).Item("netamt")

    '            Dim d As DateTime
    '            d = dt.Rows(0).Item("invoicedate")
    '            lblDate.Text = d.ToString("dd/MM/yyyy")
    '            lblOrderNo.Text = dt.Rows(0).Item("lotno")

    '            BindGrid()
    '            ' Page.ClientScript.RegisterStartupScript _
    '            '(Me.GetType(), "", "GrandTotal();", True)
    '        End If

    '    End If
    'End Sub
    'Protected Sub BindGrid()
    '    Dim cmd As New SqlCommand(" select saltran.TransID,saltran.serno,saltran.tserno,item.item,saltran.qty,saltran.weight,saltran.rate,saltran.amount from saltran left join item on saltran.itmcd=item.itmcd where serno= " & sr, con)
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim dt As New DataTable
    '    da.Fill(dt)
    '    'Dim amt As Double
    '    'For i = 0 To dt.Rows.Count - 1
    '    '    amt += Convert.ToDouble(dt.Rows(i).Item("amount"))    ' ITEM 2 IS THE PRICE.
    '    'Next
    '    'lblTotal.Text = amt.ToString("#0.00")

    '    GridView2.DataSource = dt
    '    GridView2.DataBind()


    'End Sub

    'Dim totalAmt As Double
    'Protected Sub GridView2_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
    '        'amt = Convert.ToDouble(totalAmt)


    '    ElseIf e.Row.RowType = DataControlRowType.Footer Then
    '        totalAmt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "amount"))
    '        'amt = Convert.ToDouble(totalAmt)

    '        GridView2.Columns(4).FooterText = totalAmt.ToString("#0.00")

    '        e.Row.Cells(4).Text = totalAmt.ToString("#0.00")
    '        e.Row.Cells(4).Font.Bold = True
    '        e.Row.Cells(3).Font.Bold = True
    '    End If
    'End Sub

    'Protected Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click
    '    'ClientScript.RegisterStartupScript([GetType](), "print", "window.print();", True)
    '    'Page.ClientScript.RegisterStartupScript _
    '    '      (Me.GetType(), "", "GrandTotal();", True)
    'End Sub


End Class
