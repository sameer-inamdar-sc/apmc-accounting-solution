﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Class Cashbook
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Public query As String, constr As String


    Protected Sub Btn_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Session.Item("cbno") = ""
        Response.Redirect("cbmain.aspx")

    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Bingrid1()
    End Sub

    Protected Sub Bingrid1()

        Dim filter As String = ""

        If txt_Searchcashbook.Text <> "" Then
            filter = " and cbno=" & txt_Searchcashbook.Text & ""
        End If

        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If

        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()

        Dim cmd As New SqlCommand("select cm.cbno, CONVERT(VARCHAR(10), cm.date, 105) AS date,case when cm.trsnrp = 0 then 'Reciept' else 'Payment' end type ,case when cm.modecasbank = 0 then 'Bank' else 'Cash' end mode, cm.Bankname,ISNULL (SUM(ct.amount), 0) Total from cbmast cm left join cbtran ct on cm.cbno=ct.cbno " & filter & "  group by cm.cbno, date, trsnrp, modecasbank, Bankname  order by cbno asc", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()

    End Sub

    'Protected Sub BindGrid2()

    'Dim filter As String = ""

    '    If txt_Searchcashbook.Text <> "" Then
    '        filter = " and cbno" & txt_Searchcashbook.Text & ""
    '    End If

    '    If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
    '        filter = filter & "and date between '" & txt_fromDate.SelectedDate & " ' AND ' " & txt_toDate.SelectedDate & "'"
    '    End If

    '    If filter <> "" Then
    '        filter = Replace(filter, "and", "where", 1, 1)

    '    End If

    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Bingrid1()
        End If

    End Sub
    'Public Sub bindgrid()
    '    con.Open()
    '    Dim cmd As New SqlCommand("select cm.cbno, CONVERT(VARCHAR(10), cm.date, 105) AS date,case when cm.trsnrp = 0 then 'Reciept' else 'Payment' end type ,case when cm.modecasbank = 0 then 'Bank' else 'Cash' end mode, cm.Bankname,ISNULL (SUM(ct.amount), 0) Total from cbmast cm left join cbtran ct on cm.cbno=ct.cbno group by cm.cbno, date, trsnrp, modecasbank, Bankname  order by cbno asc", con)
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim dt As New DataTable
    '    da.Fill(dt)

    '    GridView1.DataSource = dt
    '    GridView1.DataBind()
    '    con.Close()
    'End Sub


    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        con.Open()
        If e.CommandName = "e" Then
            Dim cbno As String = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("~/cbmain.aspx?id=" & cbno)
        End If
        If e.CommandName = "d" Then
            Dim cbno As String = Convert.ToInt32(e.CommandArgument)
            Dim intResponse As Integer
            intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd As New SqlCommand("delete from cbtran where cbno=" & cbno & "  delete from cbmast where cbno=" & cbno, con)
                cmd.ExecuteNonQuery()
                query = "ledger_Delete"
                Dim com As New SqlCommand(query, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.AddWithValue("@serno", cbno)
                com.Parameters.AddWithValue("@compid", Session("compid"))
                com.Parameters.AddWithValue("@doccd", "CB")
                com.ExecuteNonQuery()
            ElseIf intResponse = MsgBoxResult.No Then
                Exit Sub

            End If
            'con.Open()
            'Dim srno As Integer = e.CommandArgument
            'Dim cmd8 As New SqlCommand("select COUNT(serno) from cbtran where cbno=" + srno.ToString() + "", con)
            'Dim a As String
            'a = cmd8.ExecuteScalar()
            'If a = 0 Then
            '    Dim cmd7 As New SqlCommand("delete from cbmast where cbno=" + srno.ToString() + "", con)
            '    cmd7.ExecuteNonQuery()
            '    MsgBox("Your Record Has Been Deleted", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Deleted")
            'Else
            '    MsgBox("Your Record can not delete", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record can not Delete")
            'End If

            con.Close()
            Bingrid1()
        End If
    End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click
        Bingrid1()
    End Sub
End Class
