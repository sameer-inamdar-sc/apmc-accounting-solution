﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="acmast.aspx.vb" Inherits="acmast" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <head id="Head1">
        <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
        <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
        <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
        <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
        <script language="Javascript">
            $(function () {


                $('.decimal').bind('paste', function () {
                    var self = this;
                    setTimeout(function () {
                        if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                    }, 0);
                });

                $('.decimal').keypress(function (e) {
                    var character = String.fromCharCode(e.keyCode)
                    var newValue = this.value + character;
                    if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                        e.preventDefault();
                        return false;
                    }
                });

                function hasDecimalPlace(value, x) {
                    var pointIndex = value.indexOf('.');
                    return pointIndex >= 0 && pointIndex < value.length - x;
                }
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#<%=txtplace.ClientID %>").autocomplete({
                    autoFocus: true,
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/Service.asmx/GetPlace") %>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    select: function (e, i) {
                        $("#<%=hfplaceval.ClientID %>").val(i.item.val);
                        $("#<%=hfplace.ClientID %>").val(i.item.label);

                    },
                    minLength: 1

                });
            }); 
        </script>
        <script type="text/javascript">
            $(function () {
                $("#TextBox1").datepicker();
            });
        </script>
        <style type="text/css">
            .AutoExtender
            {
                font-family: arial, cambria, Helvetica, sans-serif;
                font-size: 10pt;
                font-weight: normal;
                border: solid 1px #006699;
                line-height: 20px;
                padding: 10px;
                background-color: White;
                margin-left: 0px;
            }
            .modalBackground
            {
                background-color: Black;
                filter: alpha(opacity=80);
                opacity: 0.8;
                z-index: 10000;
            }
            .autoCompleteList
            {
                list-style: none outside none;
                border: 1px solid buttonshadow;
                cursor: default;
                padding: 0px;
                margin: 0px;
                z-index: 100009 !important;
            }
            .modalPopup
            {
                background-color: White;
                padding: 10px;
                width: 462px;
                z-index: 1000 !important;
            }
            .AutoExtenderList
            {
                border-bottom: dotted 1px #006699;
                cursor: pointer;
                color: Blue;
            }
            .AutoExtenderHighlight
            {
                color: White;
                background-color: #006699;
                cursor: pointer;
            }
            
            .ui-autocomplete
            {
                display: block;
                z-index: 100009;
            }
            .style4
            {
                width: 1296px;
            }
            .style8
            {
            }
            .style9
            {
                width: 130px;
            }
            .style10
            {
                height: 15px;
            }
            .style12
            {
                width: 299px;
            }
            .style13
            {
                width: 57px;
            }
            .style3
            {
                color: red;
            }
            .style14
            {
                font-size: xx-small;
            }
        </style>
    </head>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="style4" width="100%">
        <tr>
            <td colspan="3" style="padding-left: 5px;">
                <asp:Label ID="Label4" runat="server" Font-Size="Large" ForeColor="#6600CC" Text="Account Master"
                    Font-Bold="True"></asp:Label>
            </td>
            <td colspan="3" style="padding-left: 5px;">
                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8">
                <asp:Label ID="Label18" runat="server" Text="Account Code" Width="150px"></asp:Label>
            </td>
            <td align="left" class="style8">
                <asp:TextBox ID="txt_searchaccode" runat="server" Width="150px">
                </asp:TextBox>
            </td>
            <td align="right" class="style8" colspan="2">
                <asp:Label ID="lbl" runat="server" Text="Account Name" Width="150px"></asp:Label>
            </td>
            <td align="left" class="style8">
                <asp:TextBox ID="txt_searchacname" runat="server" Width="150px">
                </asp:TextBox>
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="Label19" runat="server" Text="Place" Width="100px"></asp:Label>
            </td>
            <td align="left" class="style9">
                <asp:TextBox ID="txt_searchplace" runat="server" Width="150px">
                </asp:TextBox>
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label23" runat="server" Text="Account Type" Width="100px"></asp:Label>
            </td>
            <td align="left" class="style9">
                &nbsp;
                <asp:DropDownList ID="DropDownList1" runat="server" TabIndex="1" Width="250px">
                </asp:DropDownList>
            </td>
            <td align="left" class="style9">
                <asp:Button ID="Btn_search" runat="server" Text="Search" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="11">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="11">
                <asp:Panel ID="Panel4" runat="server">
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="acccd"
                        HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
                        Width="100%" EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
                        PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
                        <Columns>
                            <asp:BoundField DataField="acccd" HeaderText="Account Code" SortExpression="acccd"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="grtype" HeaderText="Account Type" SortExpression="grtype" />
                            <asp:BoundField DataField="acname" HeaderText="Account Name" SortExpression="acname" />
                            <asp:BoundField DataField="mjname" HeaderText="Major Name" SortExpression="mjname" />
                            <asp:BoundField DataField="place" HeaderText="Place" SortExpression="place" />
                            <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                        CommandArgument='<%#Eval("acccd")%>' />
                                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                        OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("acccd")%>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <HeaderStyle BackColor="Black" ForeColor="White" />
                        <PagerStyle HorizontalAlign="Right" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Button ID="zhol" runat="server" Style="display: none;" />
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="zhol"
        PopupControlID="Panel1" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="Panel1" runat="server" BackColor="White" Width="690px" ForeColor="Black"
        Font-Bold="true" Font-Size="X-Large" Font-Names="Book Antiqua" Height="403px"
        BorderColor="White" BorderStyle="Outset" BorderWidth="6px">
        <div id="dialog" title="Add New Account" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller">
            <table width="700">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td class="style13">
                        &nbsp;
                    </td>
                    <td class="style12">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6" height="15" style="line-height: 15px">
                        <asp:Label ID="lbl_popuphead" runat="server" Font-Bold="True" Font-Italic="True"
                            Font-Names="Bookman Old Style" Font-Overline="True" Font-Size="X-Large" Font-Underline="True"
                            Text="Account Details"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td class="style13">
                        &nbsp;
                    </td>
                    <td class="style12" align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" runat="server" Text="Code" Width="100px"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="Txtacccd" runat="server" Enabled="False" onfocus="disableautocompletion(this.id)"
                            Width="100px">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="timpass" runat="server" ControlToValidate="Txtacccd"
                            ErrorMessage="*" ValidationGroup="Save">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td class="style13">
                        <asp:Label ID="Label11" runat="server" Text="Type"></asp:Label>
                    </td>
                    <td class="style12" align="left">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drlgrcd"
                            ErrorMessage="*" InitialValue="Select" ValidationGroup="Save"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="drlgrcd" runat="server" TabIndex="1" Width="250px" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style10" align="right">
                        <asp:Label ID="Label22" runat="server" Text="Account Name" Width="150px"></asp:Label>
                    </td>
                    <td colspan="5" align="left">
                        <asp:TextBox ID="Txtacname" runat="server" onfocus="disableautocompletion(this.id)"
                            TabIndex="2" Width="490px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="timpass6" runat="server" ControlToValidate="Txtacname"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label13" runat="server" Text="L.F. No." Width="100px"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="Txtlf" runat="server" onfocus="disableautocompletion(this.id)" TabIndex="3"
                            Width="100px" class="decimal"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="timpass2" runat="server" ControlToValidate="Txtlf"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td class="style13">
                        &nbsp;
                    </td>
                    <td class="style12" align="left">
                        <asp:TextBox ID="Txtcompid" runat="server" Visible="False" Width="100px"></asp:TextBox>
                        &nbsp;
                        <asp:TextBox ID="Txtuserid" runat="server" Visible="False" Width="100px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label14" runat="server" Text="Place" Width="100px"></asp:Label>
                    </td>
                    <td colspan="5" align="left">
                        <asp:TextBox ID="txtplace" runat="server" Width="490px" TabIndex="4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label15" runat="server" Text="Cell No." Width="100px"></asp:Label>
                    </td>
                    <td colspan="4" width="300" align="left">
                        <asp:TextBox ID="Txtcellno1" runat="server" onfocus="disableautocompletion(this.id)"
                            TabIndex="5" Width="100px" class="decimal"></asp:TextBox>
                        &nbsp;<asp:TextBox ID="Txtcellno2" runat="server" onfocus="disableautocompletion(this.id)"
                            TabIndex="6" Width="100px" class="decimal"></asp:TextBox>
                    </td>
                    <td class="style12" align="left" width="250">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label16" runat="server" Text="Major Code" Width="150px"></asp:Label>
                    </td>
                    <td colspan="5" align="left">
                        <asp:DropDownList ID="drlmjcd" runat="server" TabIndex="7" Width="300px" >
                        </asp:DropDownList>
                        <%--OnSelectedIndexChanged="drlmjcd_SelectedIndexChanged"--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drlmjcd"
                            ErrorMessage="*" InitialValue="Select" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                        <asp:HiddenField ID="HFUpdate" runat="server" Value="0" />
                    </td>
                    <td>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td class="style13">
                        &nbsp;
                    </td>
                    <td height="15" style="line-height: 15px" class="style14">
                        All fields mark with <span class="style3">*</span> are mandatory
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6">
                        <asp:HiddenField ID="hfplace" runat="server" />
                        <asp:HiddenField ID="hfplaceval" runat="server" />
                        <asp:HiddenField ID="hfacname" runat="server" />
                        <asp:HiddenField ID="hfacccd" runat="server" />
                        <asp:Button ID="Btnsave" runat="server" TabIndex="8" Text="Save" Width="50px" ValidationGroup="Save" />
                        &nbsp;
                        <asp:Button ID="Btn_Cancle" runat="server" TabIndex="9" Text="Cancle" Width="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
