﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Globalization

Partial Class item
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub btnsave_Click(sender As Object, e As System.EventArgs) Handles btn_save.Click
        con.Open()

        If txtitem.Text = "" Then
            MsgBox("Please Enter Item Name", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            txtitem.Focus()
        End If

        If txtunit.Text = "" Then
            MsgBox("Please Enter Unit", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            txtunit.Focus()
        End If

        If txttare.Text = "" Then
            MsgBox("Please Enter Tare", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            txttare.Focus()

        Else

            If HiddenField1.Value = "0" Then
                Dim cmdStr As String = "select count(*) from item"

                cmdStr += " where item='" + txtitem.Text.Trim() & "'"

                Dim cmd1 As New SqlCommand(cmdStr, con)

                Dim i As Integer
                i = Convert.ToInt16(cmd1.ExecuteScalar())

                If i = 0 Then
                    txtunit.Focus()
                Else
                    MsgBox("Item Is Already Exist in Your List", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Item Name Check")
                    txtitem.Text = ""
                    txtitem.Focus()
                    Exit Sub
                End If
                Dim s As String
                s = "insert into Item (compid,userid,itmcd,item,unit,tare) values(" & Txtcompid.Text.Trim() & ", '" & Txtuserid.Text.Trim() & "' ," & txtitmid.Text.Trim() & ",'" & txtitem.Text.Trim() & "','" & txtunit.Text.Trim() & "','" & txttare.Text.Trim() & "')"
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
            Else

                Dim cmdStr As String = "select count(*) from item"

                cmdStr += " where item='" + txtitem.Text.Trim() & "' and itmcd!=" & txtitmid.Text.Trim()

                Dim cmd1 As New SqlCommand(cmdStr, con)

                Dim i As Integer
                i = Convert.ToInt16(cmd1.ExecuteScalar())

                If i = 0 Then
                    txtunit.Focus()
                Else
                    MsgBox("Item Is Already Exist in Your List", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Item Name Check")
                    txtitem.Text = ""
                    txtitem.Focus()
                    Exit Sub
                End If


                Dim s As String
                s = "update item set compid=" & Txtcompid.Text.Trim() & ",userid='" & Txtuserid.Text.Trim() & "',item='" & txtitem.Text.Trim() & "', unit='" & txtunit.Text.Trim() & "', tare='" & txttare.Text.Trim() & "' where itmcd=" & txtitmid.Text.Trim() & ""
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
            End If

            ClearConrols()

            MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Record Save")

            txtitem.Focus()
            txtunit.Text = ""
            txttare.Text = ""
            con.Close()
            BindGrid()
        End If


    End Sub

    Protected Sub Btn_Search_Click(sender As Object, e As System.EventArgs) Handles Btn_Search.Click
        BindGrid()
    End Sub
    Protected Sub BindGrid()
        Dim filter As String = ""

        If txt_SearchCode.Text <> "" Then
            filter = "and itmcd like '" & txt_SearchCode.Text & "%'"
        End If

        If txt_SearchDescription.Text <> "" Then
            filter = filter & "and item like '" & txt_SearchDescription.Text & "%'"
        End If

        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()
        Dim cmd As New SqlCommand("select itmcd,upper(item)item,unit,tare from dbo.item " & filter & "ORDER BY itmcd DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
        ClearConrols()
    End Sub

    Public Sub ClearConrols()
        txtitem.Text = ""
        txtitmid.Text = ""
        txtunit.Text = ""
        txttare.Text = ""
        txtitem.Focus()
    End Sub
    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        ClearConrols()
        HiddenField1.Value = "0"
        con.Open()
        Dim cmd As New SqlCommand("select max(itmcd) from item", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            txtitmid.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            txtitmid.Text = a.ToString()
        End If

        txtitem.Focus()
        Txtuserid.Text = Session("userid")
        Txtcompid.Text = Session("compid")

        con.Close()
        Me.ModalPopupExtender2.Show()
    End Sub


    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then
            txtitmid.Text = ""
            Dim itmid As Integer = e.CommandArgument
            HiddenField1.Value = itmid
            con.Open()
            Dim cmd1 As New SqlCommand("select itmcd,compid,userid,upper(item)item,unit,tare from item where itmcd=" + itmid.ToString() + "", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)

            Dim culture1 As New CultureInfo("pt-BR")


            txtitmid.Text = custTable.Rows(0).Item(0)
            txtitem.Text = custTable.Rows(0).Item(3)
            txtunit.Text = custTable.Rows(0).Item(4)
            txttare.Text = custTable.Rows(0).Item(5)
            Txtcompid.Text = custTable.Rows(0).Item(1)
            Txtuserid.Text = custTable.Rows(0).Item(2)


            con.Close()
            Me.ModalPopupExtender2.Show()
        End If
        If e.CommandName = "d" Then
            Dim itmid As Integer = e.CommandArgument
            HiddenField1.Value = itmid
            con.Open()
            Dim cmd4 As New SqlCommand("select COUNT(itmcd) from ratetran where itmcd=" + itmid.ToString() + "", con)
            Dim d As Integer
            d = Convert.ToInt16(cmd4.ExecuteScalar())

            Dim cmd3 As New SqlCommand("select COUNT(itmcd) from itmexp where itmcd=" + itmid.ToString() + "", con)
            Dim i As Integer
            i = Convert.ToInt16(cmd3.ExecuteScalar())

            If i = 0 & d = 0 Then
                MsgBox("You must Delete Expenses Related to this Item or Rate Fixed to Any Account to Delete This Item.", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Delete Error")


            Else
                Dim cmd1 As New SqlCommand("delete from item where itmcd=" + itmid.ToString() + "", con)
                cmd1.ExecuteNonQuery()
                con.Close()
                MsgBox("Item Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
                BindGrid()
            End If

        End If


    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            HiddenField1.Value = "0"
            ClearConrols()
            BindGrid()
        End If
    End Sub
    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
