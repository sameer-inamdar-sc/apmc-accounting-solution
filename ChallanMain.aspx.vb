﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class ChallanMain
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Session.Item("Sr No.") = ""
        Response.Redirect("ChallanForm.aspx")
    End Sub

    Protected Sub BindGrid()
        Dim filter As String = ""

        If txtSearchChallanNo.Text <> "" Then
            filter = "and challanNo= " & txtSearchChallanNo.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        'If txtSearchItemName.Text <> "" Then
        '    filter = filter & "and item like '" & txtSearchItemName.Text & "%'"
        'End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()

        Dim cmd As New SqlCommand("SELECT serno ,challanNo ,CONVERT(VARCHAR(10), date, 105) AS date,a.acccd ,a.acname ,lotno ,tamount ,netamt FROM challanMast c join acmast a on  c.acccd=a.acccd " & filter & " ORDER BY serno DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            txtSearchFromDate.SelectedDate = Date.Now.AddDays(-7)
            txtSearchToDate.SelectedDate = Date.Today
            BindGrid()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Public query As String, constr As String

    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        con.Open()
        If e.CommandName = "e" Then
            Dim q1 As String
            Dim cno, res As Integer
            Dim srno As String = Convert.ToInt32(e.CommandArgument)


            q1 = "select challanNo from challanMast where serno=" & srno
            Dim cmd2 As New SqlCommand(q1, con)

            cno = Integer.Parse(cmd2.ExecuteScalar())

            Dim q2 As String
            q2 = "select c.challanNo from challanMast c inner join smast s on s.challanNo=c.challanNo"
            Dim cmd As New SqlCommand(q2, con)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)

            For i = 0 To dt.Rows.Count - 1
                If cno Like dt.Rows(i).Item("challanNo") Then
                    res = MsgBox("Sales invoice has been generated for this challan" & vbCrLf & "You need to delete sales entry related to" & vbCrLf & " Challan No. " & cno, MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly, "Sales Invoice Genereted")
                End If
            Next
            If res = MsgBoxResult.Ok Then
                
            Else
                Response.Redirect("~/ChallanForm.aspx?id=" & srno)
            End If

        End If
        If e.CommandName = "d" Then

            Dim q1 As String
            Dim cno, res As Integer
            Dim srno As String = Convert.ToInt32(e.CommandArgument)


            q1 = "select challanNo from challanMast where serno=" & srno
            Dim cmd2 As New SqlCommand(q1, con)

            cno = Integer.Parse(cmd2.ExecuteScalar())

            Dim q2 As String
            q2 = "select c.challanNo from challanMast c inner join smast s on s.challanNo=c.challanNo"
            Dim cmd As New SqlCommand(q2, con)
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)

            For i = 0 To dt.Rows.Count - 1
                If cno Like dt.Rows(i).Item("challanNo") Then
                    res = MsgBox("Sales invoice has been generated for this challan" & vbCrLf & "You need to delete sales entry related to" & vbCrLf & " Challan No. " & cno, MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly, "Sales Invoice Genereted")
                End If

            Next
            If res = MsgBoxResult.Ok Then
                'Response.Redirect("~/ChallanMain.aspx?id=" & srno)
            Else
                Dim intResponse As Integer
                intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
                If intResponse = MsgBoxResult.Yes Then
                    Dim cmd1 As New SqlCommand("delete from challanMast where serno=" & srno & "  delete from ChallanTran where serno=" & srno, con)
                    cmd1.ExecuteNonQuery()

                ElseIf intResponse = MsgBoxResult.No Then
                    Response.Redirect("~/ChallanForm.aspx?id=" & srno)

                End If
            End If

        End If
        con.Close()

        BindGrid()
    End Sub
End Class
