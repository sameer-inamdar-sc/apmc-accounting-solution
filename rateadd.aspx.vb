﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Class rateadd
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            con.Open()
            Dim cmd As New SqlCommand("select max(serno) from ratemast", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1
                txt_serno.Text = a.ToString()
            Else
                a = Convert.ToInt32(obj)
                a = a + 1
                txt_serno.Text = a.ToString()
            End If
            txtacname.Focus()
            txtdate.SelectedDate = Date.Today
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")

            con.Close()
            Dim sr As String = CType(Page.Request.QueryString("id"), String)
            If Not sr = String.Empty Then
                hfsaveupdate.Value = 1
                con.Open()
                Dim cmd4 As New SqlCommand("select upper(acmast.acname)acname,date from ratemast join acmast on ratemast.acccd=acmast.acccd where serno=" & sr, con)
                Dim da1 As New SqlDataAdapter(cmd4)
                Dim ds1 As New DataSet()
                da1.Fill(ds1)
                Dim custTable As DataTable = ds1.Tables(0)
                'code for drlitmcd to fetch value
                txtacname.Text = custTable.Rows(0).Item("acname")

                Dim thisDate1 As Date = custTable.Rows(0).Item(1)
                Dim culture1 As New CultureInfo("pt-BR")
                txtdate.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)
                txtacname.Enabled = False
                txtdate.Enabled = False


                Dim cmd3 As New SqlCommand("select ratetran.itmserno,ratetran.tserno,ratetran.itmcd,upper(item.item)item,item.unit,ratetran.rate from ratemast left join ratetran on ratemast.serno=ratetran.serno left join item on ratetran.itmcd=item.itmcd where ratemast.serno=" & sr, con)
                Dim da As New SqlDataAdapter(cmd3)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView3.DataSource = dt
                GridView3.DataBind()
                con.Close()

                con.Close()
                txt_serno.Text = sr

                'GridView3.DataSource = dt
                'GridView3.DataBind()

            End If

        End If

        con.Open()
        Dim cmd1 As New SqlCommand("select max(tserno) from ratetran where serno='" & txt_serno.Text & "'", con)
        Dim b As Integer
        Dim obj1 As Object = cmd1.ExecuteScalar()
        If obj1 Is Nothing Or obj1 Is DBNull.Value Then
            b = 0
            b = b + 1
            txt_tserno.Text = b.ToString()
        Else
            b = Convert.ToInt32(obj1)
            b = b + 1
            txt_tserno.Text = b.ToString()
        End If


        con.Close()
    End Sub
    Protected Sub autoitmserno()
        con.Open()
        Dim cmd5 As New SqlCommand("select max(itmserno) from ratetran", con)
        Dim c As Integer
        Dim obj5 As Object = cmd5.ExecuteScalar()
        If obj5 Is Nothing Or obj5 Is DBNull.Value Then
            c = 0
            c = c + 1
            hfitmserno.Value = c.ToString()
        Else
            c = Convert.ToInt32(obj5)
            c = c + 1
            hfitmserno.Value = c.ToString()
        End If
        con.Close()
    End Sub

    Protected Sub btn_addnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_addnew.Click
        If hfItemname.Value <> txt_itmName.Text Then
            MsgBox("Item not found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Item Name Check")
            txt_itmName.Text = ""
            txt_itmName.Focus()
            Exit Sub
        End If
        If hfupdate.Value = 0 Then
            autoitmserno()
            con.Open()
            If hfsaveupdate.Value = 0 Then
                Dim d As String
                Dim newValue As String = DateTime.ParseExact(txtdate.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
                'cmd1 = String.Format("insert into ratemast (serno,tserno,acccd,date) values('{0}','{1}','{2}','{3}')", txt_serno, hfItemId.Value, Txtcomm.Text, Txthamali.Text, Txttolai.Text, Txtlevy.Text, Txtapmc.Text, Txtmaplevy.Text, Txtvatav.Text, Txtcompid.Text, Txtuserid.Text)
                d = String.Format("insert into ratemast (serno,acccd,date) values('{0}','{1}','{2}')", txt_serno.Text, hfacccd.Value, newValue)

                Dim cmd2 As New SqlCommand(d, con)
                cmd2.ExecuteNonQuery()
                txtacname.Enabled = False
                txtdate.Enabled = False
            End If
            Dim h As String
            Dim f As Integer
            h = "select count(itmcd) from ratetran where serno like '" & txt_serno.Text & "' and itmcd like '" & hfItemId.Value & "'"
            Dim cmd3 As New SqlCommand(h, con)
            f = cmd3.ExecuteScalar()
            If f <> 0 Then
                MsgBox("Item Already Exist in Cart", MsgBoxStyle.OkOnly + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Exist...")
                Exit Sub
            End If
            Dim s As String
            s = String.Format("insert into ratetran (serno,tserno,compid,userid,itmcd,rate,itmserno) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", txt_serno.Text, txt_tserno.Text, Txtcompid.Text, Txtuserid.Text, hfItemId.Value, txtrate.Text, hfitmserno.Value)
            Dim cmd1 As New SqlCommand(s, con)
            cmd1.ExecuteNonQuery()

            con.Close()
            txt_itmName.Text = ""
            txtrate.Text = ""
            'MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Save")
            'Dim z As Integer
            'z = txt_tserno.Text
            'txt_tserno.Text = z + 1
            'hfsrno.Value = txt_tserno.Text
            hfsaveupdate.Value = 1
            txt_itmName.Focus()
        Else
            con.Open()
            Dim a As String
            a = "update ratetran set compid=" & Txtcompid.Text.Trim() & ",userid='" & Txtuserid.Text.Trim() & "',itmcd='" & hfItemId.Value & "', rate=" & txtrate.Text.Trim() & " where itmserno=" & hfitmserno.Value & ""
            Dim cmd1 As New SqlCommand(a, con)
            cmd1.ExecuteNonQuery()
            con.Close()
            MsgBox("Your Record Has Been Updated", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Updated")
            hfupdate.Value = 0
        End If
        bindgrid()
        txt_itmcode.Text = ""
        hfItemId.Value = ""
        txt_itmName.Text = ""
        txtrate.Text = ""

    End Sub
    Protected Sub bindgrid()
        con.Open()
        Dim cmd As New SqlCommand("select ratemast.serno, ratetran.itmserno, ratetran.tserno, ratetran.itmcd,upper(item.item)item,item.unit,ratetran.rate from ratemast left join ratetran on ratemast.serno=ratetran.serno left join item on ratetran.itmcd=item.itmcd where ratemast.serno= " & txt_serno.Text & "", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        'Dim totalRowsCount As Integer = GridView3.Rows.Count
        da.Fill(dt)
        GridView3.DataSource = dt
        GridView3.DataBind()
        con.Close()

    End Sub

    Protected Sub btn_back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Response.Redirect("ratemaster.aspx")

    End Sub
    Protected Sub delmastrecord()
        Dim cmd8 As New SqlCommand("delete from ratemast where serno=" & txt_serno.Text & "", con)
        cmd8.ExecuteNonQuery()
        Response.Redirect("ratemaster.aspx")
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView3.RowCommand
        If e.CommandName = "e" Then
            Dim srno As Integer = e.CommandArgument
            con.Open()
            Dim cmd1 As New SqlCommand("select ratetran.itmserno, ratetran.itmcd,upper(item.item)item,ratetran.rate from ratetran join item on ratetran.itmcd=item.itmcd where itmserno=" + srno.ToString() + "", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)


            'code for drlitmcd to fetch value
            hfitmserno.Value = custTable.Rows(0).Item(0)
            txt_itmcode.Text = custTable.Rows(0).Item(1)
            hfItemId.Value = custTable.Rows(0).Item(1)
            txt_itmName.Text = custTable.Rows(0).Item(2)
            hfItemname.Value = custTable.Rows(0).Item(2)
            txtrate.Text = custTable.Rows(0).Item(3)
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")

            con.Close()
            hfupdate.Value = 1
            hfsaveupdate.Value = 1
        End If
        If e.CommandName = "d" Then
            con.Open()
            Dim srno As Integer = e.CommandArgument

            'Dim cmd As New SqlCommand("select * from ratetran join ratemast on ratetran.serno=ratemast.serno where ratemast.serno= " & txt_serno.Text & "", con)
            'cmd.ExecuteScalar()
            'Dim totalRowsCount As Integer = GridView3.Rows.Count
            Dim cmd1 As New SqlCommand("  select COUNT(ratetran.tserno) from ratetran join ratemast on ratetran.serno=ratemast.serno where ratemast.serno=" & txt_serno.Text & "", con)
            Dim a As String
            a = cmd1.ExecuteScalar()
            If a = 1 Then
                Dim intResponse As Integer
                intResponse = MsgBox("you have Not Item rate for this Account except this, Do you want to Delete Master Record?", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Master Record")
                If intResponse = MsgBoxResult.Yes Then
                    Dim cmd7 As New SqlCommand("delete from ratetran where itmserno=" + srno.ToString() + "", con)
                    cmd7.ExecuteNonQuery()
                    delmastrecord()
                    MsgBox("Your Record Has Been Deleted", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Deleted")

                Else
                    MsgBox("You can't Delete this Record", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Record Not Deleted")
                End If
            Else
                Dim intResponse As Integer
                intResponse = MsgBox("Are you sure want to Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Delete")

                If intResponse = MsgBoxResult.Yes Then
                    Dim cmd7 As New SqlCommand("delete from ratetran where itmserno=" + srno.ToString() + "", con)
                    cmd7.ExecuteNonQuery()

                    MsgBox("Your Record Has Been Deleted", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Deleted")
                Else
                    Exit Sub
                End If

                con.Close()
                bindgrid()


            End If

        End If
    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView3.PageIndexChanging
        GridView3.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub

End Class
