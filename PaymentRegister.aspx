﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentRegister.aspx.vb"
    Inherits="PaymentRegister" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
        .autoCompleteList
        {
            list-style: none outside none;
            border: 1px solid buttonshadow;
            cursor: default;
            padding: 0px;
            margin: 0px;
            z-index: 100009 !important;
        }
        .modalPopup
        {
            background-color: White;
            padding: 10px;
            width: 462px;
            z-index: 1000 !important;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .ui-autocomplete
        {
            display: block;
            z-index: 100009;
        }
        .style10
        {
            width: 1267px;
        }
        .style18
        {
            width: 126px;
        }
        .style19
        {
            width: 127px;
        }
    </style>
    <table width="100%" class="style10">
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="Lbl_Header" runat="server" Font-Size="Large" ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right" colspan="2">
                &nbsp;
            </td>
            <td class="style18">
            </td>
            <td class="style19">
            </td>
            <td align="right" colspan="2">
                <asp:LinkButton ID="BtnPrint" runat="server" Text="Print" ValidationGroup="z" />
            </td>
        </tr>
        <tr><td></td></tr>
        <tr>
            <td class="style18">
                <asp:Label ID="Lbl_TransactionNo" runat="server"></asp:Label>
            </td>
            <td class="style15">
                <asp:TextBox ID="txtSearchPaymentNo" runat="server" Width="101px" Height="22px"></asp:TextBox>
            </td>
            <td align="right" class="style18">
                <asp:Label ID="Label3" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtSearchAcName" runat="server" Width="300px" Height="21px" Style="margin-bottom: 0px"></asp:TextBox>
            </td>
            <td align="right" class="style19">
                <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left" class="style19">
                <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_fromDate" ErrorMessage="*" ValidationGroup="z"></asp:RequiredFieldValidator>
            </td>
            <td align="right" class="style19">
                <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left" class="style19">
                <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_toDate" ErrorMessage="*" ValidationGroup="z"></asp:RequiredFieldValidator>
            </td>
            <td align="left" class="style19">
                    <asp:Button ID="GenerateReport" runat="server" Text="Generate Report" 
                        ValidationGroup="z" />
            </td>
        </tr>
    </table>
    <hr />
    <asp:Panel ID="panel1" runat="server">
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" 
    Width="100%"></asp:Label>
    </asp:Panel>
</asp:Content>
