﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.Data
Imports System.Net
Imports System.Net.Mail
Imports ASPSnippets.SmsAPI

Module modFuncAndProced
    Public Sub sendEmail(ByVal PstrRecip As String, ByVal PstrSubj As String, ByVal PstrMsg As String, _
                         ByVal PstrFromAdd As String, ByVal PstrPass As String, ByVal PstrSMTP_Host As String, _
                         ByVal PstrSMTP_Port As Integer, ByVal PstrUid As String)
        Try
            Dim SmtpServer As New System.Net.Mail.SmtpClient()
            Dim mail As New System.Net.Mail.MailMessage()

            SmtpServer.EnableSsl = True
            SmtpServer.Credentials = New  _
            Net.NetworkCredential(PstrFromAdd, PstrPass)
            SmtpServer.Port = PstrSMTP_Port
            SmtpServer.Host = PstrSMTP_Host
            mail = New System.Net.Mail.MailMessage()
            mail.From = New System.Net.Mail.MailAddress(PstrFromAdd, "Test Message")
            mail.To.Add(PstrRecip)
            mail.Subject = PstrSubj
            mail.Body = PstrMsg
            SmtpServer.Send(mail)
            MsgBox("Mail Sent!", MsgBoxStyle.Information, "Sent")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
   
    Public Sub sendEmail1(ByVal PstrRecip As String, ByVal PstrSubj As String, ByVal PstrMsg As String, _
                         ByVal PstrFromAdd As String, ByVal PstrPass As String, ByVal PstrSMTP_Host As String, _
                         ByVal PstrSMTP_Port As Integer)
        Try
            Dim SmtpServer As New System.Net.Mail.SmtpClient()
            Dim mail As New System.Net.Mail.MailMessage()

            SmtpServer.EnableSsl = True
            SmtpServer.Credentials = New  _
            Net.NetworkCredential(PstrFromAdd, PstrPass)
            SmtpServer.Port = PstrSMTP_Port
            SmtpServer.Host = PstrSMTP_Host
            mail = New System.Net.Mail.MailMessage()
            mail.From = New System.Net.Mail.MailAddress(PstrFromAdd, "Test Message")
            mail.To.Add(PstrRecip)
            mail.Subject = PstrSubj
            mail.Body = PstrMsg
            SmtpServer.Send(mail)
            MsgBox("Mail Sent!", MsgBoxStyle.Information, "Sent")
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub
End Module
Partial Class Default2
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        con.Open()
        If Me.Txtuname.Text.Trim = "" Or Me.Txtsname.Text.Trim = "" Or Me.Txtemailid.Text.Trim = "" Or Me.Txtmobno.Text.Trim = "" Or Me.txt_userid.Text.Trim = "" Then
            MsgBox("All fields are required!", MsgBoxStyle.Exclamation, "Insufficient Data")
            Return
        End If
        Dim cmd As New SqlCommand("select * from createuser where uid='" & txt_userid.Text & "'", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            MsgBox("UserId already exists. ", MsgBoxStyle.Exclamation, "Insufficient Data")
            Return
        End If


        Dim UserPassword As String = CreateRandomPassword(6)

    
        
    'Txtmessage.Text = "User Name = " + Txtuname.Text.Trim.ToString() + vbNewLine + "Surname = " + Txtsname.Text.Trim.ToString() + vbNewLine + " Email ID = " + Txtemailid.Text.Trim.ToString() + vbNewLine + " Mobile No. = " + Txtmobno.Text.Trim.ToString()

    ' Call sendEmail(Me.Txttoemailid.Text.Trim, Me.Txtsubject.Text.Trim, Me.Txtmessage.Text.Trim, Me.Txtfrmemailid.Text.Trim, _
    ' Me.Txtfrmemailpass.Text.Trim, Me.txtSMTP_Host.Text.Trim, CInt(Me.txtSMTP_Port.Text.Trim), Me.txt_userid.Text.Trim)

        cmd = New SqlCommand("insert into createuser(skey,uname,sname,emailid,mob,coid,authority,uid,pass) values(" & TextBox1.Text & ",'" & Txtuname.Text & "','" & Txtsname.Text & "','" & Txtemailid.Text & "','" & Txtmobno.Text & "'," & ddcomplist.SelectedValue & ",'" & Ddl_role.SelectedValue & "','" & txt_userid.Text & "','" & UserPassword & "') ", con)
        cmd.ExecuteNonQuery()
        Txtmessage.Text = "Welcome " + vbNewLine + ddcomplist.SelectedItem.Text.Trim() + vbNewLine + "User ID = " + txt_userid.Text.Trim.ToString() + vbNewLine + "Password=" & UserPassword
        Call sendEmail1(Me.Txtemailid.Text.Trim, "Hello ", Txtmessage.Text, "siddhicomputer786@gmail.com", _
          "rescue786", "smtp.gmail.com", "587")
        Try
            SMS.APIType = SMSGateway.Site2SMS
            SMS.MashapeKey = "dPHVeeqKv9mshGM7Lg9Phu6FZaKep1vgbcgjsnpd0mbngkeBhX"
            SMS.Username = "9870642594"
            SMS.Password = "441064"
            SMS.SendSms(Txtmobno.Text.Trim(), Txtmessage.Text.Trim())

        Catch ex As Exception
            MsgBox(ex.Message.ToString())
        End Try

        MsgBox("Data saved")
        Session("name") = Txtuname.Text
        TextBox1.Text = ""
        Txtuname.Text = ""
        Txtsname.Text = ""
        Txtemailid.Text = ""
        Txtmobno.Text = ""
        txt_userid.Text = ""
        Response.Redirect("newuser.aspx")

        con.Close()


    'send_mail()
    End Sub


    Public Shared Function CreateRandomPassword(PasswordLength As Integer) As String
        Dim _allowedChars As String = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ"
        Dim randNum As New Random()
        Dim chars As Char() = New Char(PasswordLength - 1) {}
        Dim allowedCharCount As Integer = _allowedChars.Length
        For i As Integer = 0 To PasswordLength - 1
            chars(i) = _allowedChars(CInt((_allowedChars.Length) * randNum.NextDouble()))
        Next
        Return New String(chars)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Panel1.Visible = False
            pnl_add.Visible = False
            con.Open()
            Dim cmd As New SqlCommand("select max(skey) from createuser", con)
            Dim a As Integer
            a = Convert.ToInt32(cmd.ExecuteScalar())
            a = a + 1
            TextBox1.Text = a.ToString()

            'Dim cmd1 As New SqlCommand("select cocd, coname, coid from company", con)
            '' Dim cmd1 As New SqlCommand("select co.cocd,co.coname from company co join createuser cu on co.coid=cu.coid where cu.uname='" & userid_z & "'", con)
            'Dim dr As SqlDataReader = cmd1.ExecuteReader()
            'While dr.Read()
            '    Dim coname As String = dr.GetSqlString(0).Value + "-" + dr.GetSqlString(1).Value            ''Session("cocd") = s
            '    ddcomplist.Items.Add(New ListItem(coname))
            '    ' drlcomplist.Items.Add(dr("cocd").ToString() + " , " + dr("coname").ToString())
            'End While

            'dr.Close()
            ddcomplist.Items.Insert(0, "-- Please Select Company --")
            ' Dim com As New SqlCommand("select co.coid, co.cocd+'-'+co.coname con,cu.coid from company co join createuser cu on co.coid=cu.coid", con)
            Dim com As New SqlCommand("select co.coid, co.cocd+'-'+co.coname con from company co", con)
            Dim da2 As New SqlDataAdapter(com)
            Dim dt2 As New DataTable()
            da2.Fill(dt2)
            con.Close()
            ddcomplist.DataSource = dt2
            ddcomplist.DataTextField = "con"
            ddcomplist.DataValueField = "coid"
            ddcomplist.DataBind()
            GetCompany()
        End If
        

    End Sub

    
    Public Sub GetCompany()
        Dim userid_z As String
        userid_z = Session("userid")
        Dim com As New SqlCommand("select cu.coid from createuser cu where cu.uid='" & userid_z & "'", con)
        Dim da2 As New SqlDataAdapter(com)
        Dim dt2 As New DataTable()
        da2.Fill(dt2)
        ddcomplist.SelectedValue = dt2.Rows(0)(0).ToString()
        con.Close()
        If Session("authority").ToString().ToUpper() = "ADMIN" Or Session("authority").ToString().ToUpper() = "USER" Then
            ddcomplist.Enabled = False
        End If
    End Sub
    'Protected Sub send_email()
    '    Try
    '        Dim mail As String
    '        mail = Txttoemailid.Text.Trim()
    '        Dim mailMessage As New MailMessage()
    '        mailMessage.[To].Add("mail")
    '        'mailMessage.IsBodyHtml = True
    '        'mailMessage.Body = "Hello <b>world!</b>"
    '        mailMessage.From = New MailAddress("sushilj02@gmail.com")
    '        mailMessage.Subject = "ASP.NET e-mail test"
    '        mailMessage.Body = "Hello world," & vbLf & vbLf & "This is an ASP.NET test e-mail!"
    '        Dim smtpClient As New SmtpClient("smtp.your-isp.com")
    '        smtpClient.Send(mailMessage)
    '        Response.Write("E-mail sent!")
    '    Catch ex As Exception
    '        Response.Write("Could not send the e-mail - error: " + ex.Message)
    '    End Try
    'End Sub

    'Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
    '    SMS.APIType = SMSGateway.Site2SMS
    '    SMS.MashapeKey = "<Mashape API Key>"
    '    SMS.Username = txt_userid.Text.Trim()
    '    ' SMS.Password = txtPassword.Text.Trim()
    '    'If txtRecipientNumber.Text.Trim().IndexOf(",") = -1 Then
    '    'Single SMS
    '    ''SMS.SendSms(txtRecipientNumber.Text.Trim(), Txtmessage.Text.Trim())
    '    'Else
    '    'Multiple SMS
    '    ' Dim numbers As List(Of String) = txtRecipientNumber.Text.Trim().Split(","c).ToList()
    '    'SMS.SendSms(numbers, Txtmessage.Text.Trim())
    '    SMS.SendSms(Txtmobno.Text.Trim(), Txtmessage.Text.Trim())
    '    'End If
    'End Sub



    Protected Sub Button8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button8.Click
        Panel1.Visible = True
        pnl_add.Visible = False
        con.Open()
        Dim cmd1 As New SqlCommand("select uid from createuser where authority <>'SUPER-ADMIN' ", con)
        Dim da As New SqlDataAdapter(cmd1)
        Dim dt As New DataTable
        da.Fill(dt)
        con.Close()
        ddluser.DataSource = dt
        ddluser.DataTextField = "uid"
        ddluser.DataValueField = "uid"
        ddluser.DataBind()
        ddluser.Items.Insert(0, "select user")



        Dim comm As New SqlCommand("select authority from createuser where authority<>'SUPER-ADMIN' group by authority", con)
        Dim da3 As New SqlDataAdapter(comm)
        Dim dt3 As New DataTable()
        da3.Fill(dt3)
        con.Close()
        ddlrole.DataSource = dt3
        ddlrole.DataTextField = "authority"
        ddlrole.DataValueField = "authority"
        ddlrole.DataBind()
        ' ddlrole.Items.Insert(0, "select user")


        ClearControls()


    End Sub

    Protected Sub Button7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button7.Click
        pnl_add.Visible = True
        Panel1.Visible = False
        ClearControls1()
    End Sub

    Protected Sub ddluser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddluser.SelectedIndexChanged
        If ddluser.SelectedIndex = 0 Then
            MsgBox("Please select User")
            ClearControls()
        Else
            con.Open()
            Dim cmd As New SqlCommand("select coid,uname,sname,emailid,mob,authority from createuser where uid='" + ddluser.SelectedItem.Text + "'", con)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da.Fill(ds)
            Dim custTable As DataTable = ds.Tables(0)
            ddlcomp.Text = custTable.Rows(0).Item(0)
            Txtfname0.Text = custTable.Rows(0).Item(1)
            Txtlname0.Text = custTable.Rows(0).Item(2)
            Txteid0.Text = custTable.Rows(0).Item(3).ToString()
            Txtmno0.Text = custTable.Rows(0).Item(4).ToString()
            Ddlrole.Text = custTable.Rows(0).Item(5).ToString()

            ddlcomp.Enabled = False
            Dim com As New SqlCommand("select co.coid, co.cocd+'-'+co.coname con from company co", con)
            Dim da2 As New SqlDataAdapter(com)
            Dim dt2 As New DataTable()
            da2.Fill(dt2)
            con.Close()
            ddlcomp.DataSource = dt2
            ddlcomp.DataTextField = "con"
            ddlcomp.DataValueField = "coid"
            ddlcomp.DataBind()
            ddlcomp.Items.Insert(0, "select company")

            con.Close()
        End If
    End Sub
    Public Sub ClearControls()
        ' ddlcomp.Text = ""
        Txtfname0.Text = ""
        Txtlname0.Text = ""
        Txteid0.Text = ""
        Txtmno0.Text = ""
        ' ddlrole.Text = ""
       
    End Sub
    Public Sub ClearControls1()
        Txtuname.Text = ""
        Txtsname.Text = ""
        Txtemailid.Text = ""
        Txtmobno.Text = ""
        txt_userid.Text = ""
       
    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        If ddluser.SelectedIndex = 0 Then
            MsgBox("Please select User")
        Else
            If (MsgBox("Are you Sure to Update?", MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Update")) = MsgBoxResult.Yes Then
                con.Open()
                Dim s As String
                s = "update createuser set coid=" & ddlcomp.Text.Trim() & ",uname='" & Txtfname0.Text.Trim() & "',sname='" & Txtlname0.Text.Trim() & "',emailid='" & Txteid0.Text.Trim() & "',mob='" & Txtmno0.Text.Trim() & "',authority='" & ddlrole.Text.Trim() & "'  where uid='" & ddluser.SelectedValue() & "' "
                '  s = s & "; update compmast set cocd='" & txtcocd1.Text.Trim() & "',coname='" & txtconame1.Text.Trim() & "' where coid=" & txtcoid2.Text.Trim() & " "
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                MsgBox("Record Updated")
                ClearControls()
                ddluser.SelectedIndex = 0
                ddlrole.SelectedIndex = 0
                con.Close()
            Else
                ClearControls()
                ddluser.SelectedIndex = 0
                Exit Sub

            End If

        End If
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        If ddluser.SelectedIndex = 0 Then
            MsgBox("Please select User")
        Else
            If (MsgBox("Are you Sure to Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Update")) = MsgBoxResult.Yes Then
                con.Open()
                Dim s As String
                s = "delete from createuser where uid='" & ddluser.SelectedValue() & "'"
                's = "delete from createuser where coid=" & ddlcomp.Text.Trim() & ",uname='" & Txtfname0.Text.Trim() & "',sname='" & Txtlname0.Text.Trim() & "',emailid='" & Txteid0.Text.Trim() & "',mob=" & Txtmno0.Text.Trim() & ",authority='" & ddlrole.Text.Trim() & "'  where uid='" & ddluser.SelectedValue() & "' "
                Dim cmd As New SqlCommand(s, con)
                cmd.ExecuteNonQuery()
                MsgBox("Record Deleted")
                ClearControls()
                ddluser.SelectedIndex = 0
                con.Close()
            Else
                ClearControls()
                ddluser.SelectedIndex = 0
                Exit Sub

            End If

        End If
    End Sub
End Class

