﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing
Partial Class PurchaseRegister
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Public dt As DataTable
    Dim filter As String = ""
    Dim sbPageString As New StringBuilder()


    Protected Sub GenerateReport_Click(sender As Object, e As System.EventArgs) Handles GenerateReport.Click
        ShowPurchaseRegister()
    End Sub

    Public Sub ShowPurchaseRegister()
        If txtSearchInvoice.Text <> "" Then
            filter = "and m.pbillno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "select m.serno,m.pbillno,m.date,m.acccd,a.acname,m.lotno,m.tamount,m.tapmc,m.thamali,m.tmapLevy,m.ttolai,m.tbharai,m.tutrai,m.tmotorfrt,m.grossamt,m.roundoff,m.netamt,t.transid,t.tserno,t.itmcd,i.item,t.qty,t.weight,t.rate,t.amount,t.lapmc,t.lmaplevy,t.lhamali from pmast m join purtran t on m.serno=t.serno join item i on i.itmcd=t.itmcd join acmast a on a.acccd=m.acccd " & filter & " ORDER BY m.serno DESC"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)
        filter = ""
        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetPurchaseRegister(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
    End Sub

    Public Function GetPurchaseRegister(ByVal dt As DataTable) As String
        Dim a As Integer
        Dim amt, apmc, maplevy, hamali, expences, e, n, net As Double

        sbPageString.Append("<style> .bottom{position: absolute;bottom: 0px;}th{border-bottom: 1px solid;border-left: 1px solid;}th:first-child{border-left: 0px solid;}</style>")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;border-bottom: 0px solid;'><tr align='center' ><th width='3%'>Sr No.</th><th width='5%'>Bill No</th><th width='8%'>Bill date</th><th width='5%'>Account Code</th><th width='20%'>Account Name</th><th width='15%'>Item Name</th><th width='5%'>Quantity</th><th width='6%'>Weight</th><th width='7%'>Rate</th><th width='10%'>Amount</th><th width='5%'>Expences</th><th width='5%'>Net Amount</th></tr>")
        For i = 0 To dt.Rows.Count - 1
            a = a + 1
            

            Dim billDate As String
            billDate = dt.Rows(i).Item("date")

            Dim billDateArray As String()
            billDateArray = billDate.Split("/")

            billDate = billDateArray(1) & "/" & billDateArray(0) & "/" & billDateArray(2)

            e = dt.Rows(i).Item("lapmc") + dt.Rows(i).Item("lmaplevy") + dt.Rows(i).Item("lhamali")
            n = dt.Rows(i).Item("amount") + e

          

            For j = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("lotno") = dt.Rows(j).Item("lotno") Then
                    sbPageString.Append("<tr><td>" & dt.Rows(i).Item("lotno") & "</td></tr>")
                    GoTo lot
                End If
            Next
lot:
            sbPageString.Append("<tr><td width='3%'>" & a & "</td><td width='5%'>" & dt.Rows(i).Item("pbillno") & "</td><td width='8%' align='center'>" & billDate & "</td><td width='5%'>" & dt.Rows(i).Item("acccd") & "</td><td width='20%'>" & dt.Rows(i).Item("acname") & "</td><td width='15%'>" & dt.Rows(i).Item("item") & "</td><td width='5%' align='right'>" & dt.Rows(i).Item("qty") & "</td><td width='6%' align='right'>" & dt.Rows(i).Item("weight") & "</td><td width='7%' align='right'>" & dt.Rows(i).Item("rate") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("amount") & "</td><td width='5%' align='right'>" & e.ToString("#0.0000") & "</td><td width='5%' align='right'>" & n.ToString("#0.00") & "</td></tr>")
            amt += Convert.ToDouble(dt.Rows(i).Item("amount"))
            'apmc += Convert.ToDouble(dt.Rows(i).Item("lapmc"))
            'maplevy += Convert.ToDouble(dt.Rows(i).Item("lmaplevy"))
            'hamali += Convert.ToDouble(dt.Rows(i).Item("lhamali"))
            expences += e
            net += n

        Next

        sbPageString.Append("<tr style='font-weight: bold;border: 1px solid;border-top: 0px solid;' class='bottom'><td width='74%' colspan='9' align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & amt.ToString("#0.00") & "</td><td width='5%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & expences.ToString("#0.0000") & "</td><td width='5%' align='right' style='border-top: 1px solid;'>" & net.ToString("#0.00") & "</td></tr></table>")
        'sbPageString.Append("<br><caption><hr /></caption><table width='100%' style='font-weight: bold;'><tr><td align='right' width='7%'>APMC : </td><td width='6%'>" & apmc & "</td><td align='right' width='12%'>Mapadi Levy : </td><td width='6%'>" & maplevy.ToString("#0.0000") & "</td><td align='right' width='7%'>Hamali : </td><td width='6%'>" & hamali & "</td><td align='right' width='6%'>Tolai : </td><td width='6%'>" & dt.Rows(0).Item("ttolai") & "</td><td align='right' width='6%'>Bharai : </td><td width='6%'>" & dt.Rows(0).Item("tbharai") & "</td><td align='right' width='6%'>Utrai : </td><td width='6%'>" & dt.Rows(0).Item("tutrai") & "</td><td align='right' width='7%'>Frieght : </td><td width='6%'>" & dt.Rows(0).Item("tmotorfrt") & "</td></tr></table><table width='100%' style='font-weight: bold;'><tr><td width='10%'>Net Amount</td><td width='10%'>" & dt.Rows(0).Item("netamt") & "</td></tr></table>")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        sbPageString.Append("")
        Return sbPageString.ToString()
    End Function

    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Protected Sub BtnPrint_Click(sender As Object, e As System.EventArgs) Handles BtnPrint.Click
        Dim fd, td, today As String
        fd = txtSearchFromDate.SelectedDate
        td = txtSearchToDate.SelectedDate
        today = Now.ToShortDateString()

        Dim fromDateArray, toDateArray, todayArray As String()
        fromDateArray = fd.Split("/")
        toDateArray = td.Split("/")
        todayArray = today.Split("/")
        today = todayArray(1) & "/" & todayArray(0) & "/" & todayArray(2)

        If txtSearchFromDate.Text = "" Then
            fd = Nothing
        Else
            fd = fromDateArray(1) & "/" & fromDateArray(0) & "/" & fromDateArray(2)
        End If
        If txtSearchToDate.Text = "" Then
            td = Nothing
        Else
            td = toDateArray(1) & "/" & toDateArray(0) & "/" & toDateArray(2)
        End If

        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        sbPageString.Append("<table><tr><td width='10%'>From Date </td><td width='10%'>" & fd & "</td><td width='10%'>To Date </td><td width='55%'>" & td & "</td><td width='10%'>Print Date </td><td width='10%'>" & today & "</td></tr></table><caption><hr /></caption>")

        If txtSearchInvoice.Text <> "" Then
            filter = "and m.pbillno= " & txtSearchInvoice.Text & ""
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "select m.serno,m.pbillno,m.date,m.acccd,a.acname,m.lotno,m.tamount,m.tapmc,m.thamali,m.tmapLevy,m.ttolai,m.tbharai,m.tutrai,m.tmotorfrt,m.grossamt,m.roundoff,m.netamt,t.transid,t.tserno,t.itmcd,i.item,t.qty,t.weight,t.rate,t.amount,t.lapmc,t.lmaplevy,t.lhamali from pmast m join purtran t on m.serno=t.serno join item i on i.itmcd=t.itmcd join acmast a on a.acccd=m.acccd " & filter & " ORDER BY serno DESC"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)
        filter = ""
        GetPurchaseRegister(dt)

        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(sbPageString.ToString())
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), "Print", sb.ToString())
        sbPageString.Clear()
        GenerateReport_Click(sender, e)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim s1 As String
            s1 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s1, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")


            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txtSearchFromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txtSearchToDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)

        End If
    End Sub
End Class
