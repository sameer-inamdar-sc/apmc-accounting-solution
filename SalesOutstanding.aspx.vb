﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports System.Web.Services
Partial Class SalesOutstanding
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Public dt As DataTable
    Dim filter As String = ""
    Dim sbPageString As New StringBuilder()



    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim s1 As String
            s1 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s1, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")


            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txtSearchFromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txtSearchToDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)
        End If
    End Sub


    Protected Sub btnGenerateReport_Click(sender As Object, e As System.EventArgs) Handles btnGenerateReport.Click
        ShowPurchaseRegister()
    End Sub
    Public Sub ShowPurchaseRegister()

        'If txtSearchInvoice.Text <> "" Then
        '    filter = "and m.serno= " & txtSearchInvoice.Text & ""
        'End If

        If txtSearchAcName.Text <> "" Then
            filter = "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and invoiceDate BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        s = "select s.serno,s.InvoiceDate,s.acccd,a.acname,s.netamt,s.amtrecd,(s.netamt-s.amtrecd) amtbalance from smast s join acmast a on a.acccd=s.acccd " & filter & " ORDER BY a.acname,s.serno "
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)

        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetSalesRegister(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
    End Sub
    Public Function GetSalesRegister(ByVal dt As DataTable) As String
        Dim a As Integer
        Dim amt, amtbalance, amtrecd As Double
        Dim acname As String
        acname = ""

        sbPageString.Append("<style> .bottom{position: absolute;bottom: 0px;}th{border-bottom: 1px solid;border-left: 1px solid;}th:first-child{border-left: 0px solid;}</style>")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;border-bottom: 0px solid;'><tr align='center' ><th width='3%'>Sr No.</th><th width='5%'>Date</th><th width='20%'>Account Name</th><th width='10%'>Amount</th><th width='10%'>Paid Amount</th><th width='10%'>Balance Amount</th><th width='10%'>total amount</th><th width='2%'>Days</th></tr>")
        For i = 0 To dt.Rows.Count - 1
            a = a + 1
            If acname = dt.Rows(i).Item("acname") Then
                dt.Rows(i).Item("acname") = ""

            Else
                acname = dt.Rows(i).Item("acname")
                If i <> 0 Then
                    sbPageString.Append("<tr style='font-weight: bold;'><td colspan='4' align='right'>Phone No</td><td  align='right'>Total </td> <td align='right'> " & amtbalance.ToString("#0.00") & "</td><td>&nbsp;</td><td>&nbsp;</td> </tr>")
                End If
                amtbalance = 0
            End If
            Dim billDate As String
            billDate = dt.Rows(i).Item("invoiceDate")

            Dim billDateArray As String()
            billDateArray = billDate.Split("/")

            billDate = billDateArray(1) & "/" & billDateArray(0) & "/" & billDateArray(2)

            ' sbPageString.Append("<tr><td width='3%' align='center'>" & a & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & billDate & "</td><td width='20%' style='padding-left: 8px;'>" & dt.Rows(i).Item("acname") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("netamt") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("amtrecd") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("amtbalance") & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & dt.Rows(i).Item("total amount") & "</td><td width='2%' align='right'>" & dt.Rows(i).Item("days") & "</td></tr>")
            amt += Convert.ToDouble(dt.Rows(i).Item("netamt"))
            amtrecd += Convert.ToDouble(dt.Rows(i).Item("amtrecd"))
            amtbalance += Convert.ToDouble(dt.Rows(i).Item("amtbalance"))
            sbPageString.Append("<tr><td width='3%' align='center'>" & a & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & billDate & "</td><td width='20%' style='padding-left: 8px;'>" & dt.Rows(i).Item("acname") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("netamt") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("amtrecd") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("amtbalance") & "</td><td  align='right'> " & amtbalance.ToString("#0.00") & "</td><td width='2%' align='right'>" & dt.Rows(i).Item("days") & "</td></tr>")



            '            
            '            net += n

        Next
        sbPageString.Append("<tr style='font-weight: bold;'><td colspan='4' align='right'>Phone No</td><td  align='right'>Total </td> <td align='right'> " & amtbalance.ToString("#0.00") & "</td><td>&nbsp;</td><td>&nbsp;</td> </tr>")

        amtbalance = 0
        sbPageString.Append("<tr style='font-weight: bold;'><td width='74%' colspan='5' align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & amt.ToString("#0.00")- amtrecd.ToString & "</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & "</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & "</td><td width='2%' align='right' style='border-top: 1px solid;'>" & "</td></tr></table>")

        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        Return sbPageString.ToString()
    End Function
    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Protected Sub Btn_Print_Click(sender As Object, e As System.EventArgs) Handles Btn_Print.Click
        Dim fd, td, today As String
        fd = txtSearchFromDate.SelectedDate
        td = txtSearchToDate.SelectedDate
        today = Now.ToShortDateString()

        Dim fromDateArray, toDateArray, todayArray As String()
        fromDateArray = fd.Split("/")
        toDateArray = td.Split("/")
        todayArray = today.Split("/")
        today = todayArray(1) & "/" & todayArray(0) & "/" & todayArray(2)

        If txtSearchFromDate.Text = "" Then
            fd = Nothing
        Else
            fd = fromDateArray(1) & "/" & fromDateArray(0) & "/" & fromDateArray(2)
        End If
        If txtSearchToDate.Text = "" Then
            td = Nothing
        Else
            td = toDateArray(1) & "/" & toDateArray(0) & "/" & toDateArray(2)
        End If
        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        sbPageString.Append("<table><tr><td width='10%'>From Date </td><td width='10%'>" & fd & "</td><td width='10%'>To Date </td><td width='55%'>" & td & "</td><td width='10%'>Print Date </td><td width='10%'>" & today & "</td></tr></table><caption><hr /></caption>")

        If txtSearchAcName.Text <> "" Then
            filter = "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and invoiceDate BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        ''  s = "select s.serno,s.challanNo,s.InvoiceDate,s.acccd,a.acname,s.lotno,s.netamt,s.amtrecd,(s.netamt-s.amtrecd) amtbalance from smast s join acmast a on a.acccd=s.acccd " & filter & " and netamt<>amtrecd ORDER BY s.acccd,s.invoiceDate "
        ' s = "select a.acname, s.InvoiceDate,sum(s.netamt)netamt,sum(isnull(td.amount,0))amtrecd,sum(isnull(s.netamt,0))-sum(isnull(td.amount,0))amtbalance,'' 'total amount',DATEDIFF(day,s.invoicedate,GETDATE())'days' from acmast a join smast s on a.acccd=s.acccd left join TransactionDetails td on td.pmastid=s.serno group by a.acname, s.InvoiceDate order by a.acname"
        s = "select s.serno,a.acname,s.InvoiceDate,sum(s.netamt)netamt,sum(isnull(td.amount,0))amtrecd,sum(isnull(s.netamt,0))-sum(isnull(td.amount,0))amtbalance,'' 'total amount',DATEDIFF(day,s.invoicedate,GETDATE())'days' from acmast a join smast s on a.acccd=s.acccd left join TransactionDetails td on td.pmastid=s.serno  " & filter & "  group by a.acname ,s.InvoiceDate,s.serno order by a.acname"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)

        GetSalesRegister(dt)



        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(sbPageString.ToString())
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), "Print", sb.ToString())
        sbPageString.Clear()
        ''btnGenerateReport_Click(sender, e)
    End Sub
End Class
