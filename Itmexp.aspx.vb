﻿
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Class Itmexp
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetItems(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select item,itmcd from item where " & "item like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("item"), sdr("itmcd")))
                    End While
                End Using
                conn.Close()
                If customers.Count = 0 Then
                    MsgBox("Item Not Found", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Item Expenses")
                    txtItemName.Text = String.Empty

                End If
            End Using
            Return customers.ToArray()
        End Using
    End Function

    '<System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()>
    'Public Shared Function GetCompletionList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
    '    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    '    con.Open()
    '    Dim cmd1 As New SqlCommand("Select Item from itmexp where item like '" & prefixText & "%'", con)
    '    Dim da1 As New SqlDataAdapter(cmd1)
    '    Dim ds1 As New DataSet()
    '    da1.Fill(ds1)

    '    Dim dt As DataTable = ds1.Tables(0)

    '    Dim Items() As String = New String(dt.Rows.Count - 1) {}
    '    Dim i As Integer = 0
    '    For Each dr In dt.Rows
    '        Items.SetValue(dr("item").ToString(), i)
    '        i = i + 1
    '    Next
    '    Return Items
    '    con.Close()
    'End Function
    Protected Sub btn_save_Click(sender As Object, e As System.EventArgs) Handles btn_save.Click
        con.Open()
        Dim newValue As String = DateTime.ParseExact(txt_date.Text, "dd'-'MM'-'yyyy", Nothing).ToString("MM'/'dd'/'yyyy")
        If hfItemname.Value <> txtItemName.Text Then
            MsgBox("Item not found", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal, "Item Name Check")
            txtItemName.Text = ""
            txtItemName.Focus()
            Exit Sub
        Else
            txtItemName.Focus()
        End If
        If HiddenField1.Value = "0" Then
            Dim s As String
            s = String.Format("insert into Itmexp (date,serno,itmcd,comm,hamali,tolai,levy,apmc,maplevy,vatav,compid,userid) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')", newValue, Txtserno.Text, hfItemId.Value, Txtcomm.Text, Txthamali.Text, Txttolai.Text, Txtlevy.Text, Txtapmc.Text, Txtmaplevy.Text, Txtvatav.Text, Txtcompid.Text, Txtuserid.Text)
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
        Else
            Dim s As String
            s = "update itmexp set compid=" & Txtcompid.Text.Trim() & ",userid='" & Txtuserid.Text.Trim() & "',date='" & newValue & "',itmcd=" & hfItemId.Value & ",comm=" & Txtcomm.Text.Trim() & ",hamali=" & Txthamali.Text.Trim() & ",levy=" & Txtlevy.Text.Trim() & ",tolai=" & Txttolai.Text.Trim() & ",vatav=" & Txtvatav.Text.Trim() & ",apmc=" & Txtapmc.Text.Trim() & ",maplevy=" & Txtmaplevy.Text.Trim() & " where serno=" & Txtserno.Text.Trim() & " "
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
        End If
        ClearConrols()
        MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Save")
        con.Close()
        BindGrid()
    End Sub

    Protected Sub Btn_Search_Click(sender As Object, e As System.EventArgs) Handles Btn_Search.Click
        BindGrid()
    End Sub

    Protected Sub BindGrid()
        Dim filter As String = ""
        If txt_SearchCode.Text <> "" Then
            filter = "and dbo.item.itmcd= " & txt_SearchCode.Text & ""
        End If

        If txt_SearchItmName.Text <> "" Then
            filter = filter & "and item like '" & txt_SearchItmName.Text & "%'"
        End If

        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If
        con.Open()
        Dim cmd As New SqlCommand("SELECT CONVERT(VARCHAR(10), dbo.itmexp.date, 105) AS date, dbo.itmexp.serno,upper(dbo.item.item)item,dbo.item.itmcd,dbo.itmexp.comm,dbo.itmexp.hamali,dbo.itmexp.levy,dbo.itmexp.tolai,dbo.itmexp.vatav,dbo.itmexp.APMC,dbo.itmexp.maplevy FROM dbo.itmexp INNER JOIN dbo.item ON dbo.item.itmcd = dbo.itmexp.itmcd " & filter & " ORDER BY date ASC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
        ClearConrols()
    End Sub


    Public Sub ClearConrols()
        txt_date.SelectedValue = ""
        hfItemId.Value = ""
        Txtserno.Text = ""
        txtItemName.Text = ""
        txtItemName.Focus()
        Txtcomm.Text = ""
        Txthamali.Text = ""
        Txttolai.Text = ""
        Txtlevy.Text = ""
        Txtapmc.Text = ""
        Txtmaplevy.Text = ""
        Txtvatav.Text = ""

    End Sub

    Protected Sub Btn_Add_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        ClearConrols()
        con.Open()
        HiddenField1.Value = "0"
        Dim cmd As New SqlCommand("select max(serno) from itmexp", con)
        Dim a As Integer
        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            Txtserno.Text = a.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            Txtserno.Text = a.ToString()
        End If
        txt_date.SelectedDate = Date.Today
        Txtuserid.Text = Session("userid")
        Txtcompid.Text = Session("compid")
        con.Close()
        'Response.Redirect(Itmexp.aspx)
        Me.ModalPopupExtender2.Show()
    End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then
            Txtserno.Text = ""
            Dim srno As Integer = e.CommandArgument
            HiddenField1.Value = srno
            con.Open()
            Dim cmd1 As New SqlCommand("select serno,itmexp.compid,itmexp.userid,date,itmexp.itmcd,upper(item.item)item,comm,hamali,levy,tolai,vatav,APMC,maplevy from itmexp inner join item on itmexp.itmcd=item.itmcd where serno=" + srno.ToString() + "", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)

            Dim thisDate1 As Date = custTable.Rows(0).Item(3)
            Dim culture1 As New CultureInfo("pt-BR")

            txt_date.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)

            'code for drlitmcd to fetch value

            Txtserno.Text = custTable.Rows(0).Item("serno")
            hfItemId.Value = custTable.Rows(0).Item("itmcd")
            hfItemname.Value = custTable.Rows(0).Item("item")
            txtItemName.Text = custTable.Rows(0).Item("item")
            Txtcomm.Text = custTable.Rows(0).Item("comm")
            Txthamali.Text = custTable.Rows(0).Item("hamali")
            Txtlevy.Text = custTable.Rows(0).Item("levy")
            Txttolai.Text = custTable.Rows(0).Item("tolai")
            Txtvatav.Text = custTable.Rows(0).Item("vatav")
            Txtapmc.Text = custTable.Rows(0).Item("APMC")
            Txtmaplevy.Text = custTable.Rows(0).Item("maplevy")
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")

            con.Close()

            Me.ModalPopupExtender2.Show()

        End If
        If e.CommandName = "d" Then
            ''  If MsgBox("Are you sure want to Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Delete Confirmation") = MsgBoxResult.Yes Then
            ' execute command
            con.Open()
            Dim srno As Integer = e.CommandArgument
            HiddenField1.Value = srno
            Dim cmd1 As New SqlCommand("delete from dbo.itmexp where dbo.itmexp.serno=" + srno.ToString() + "", con)
            cmd1.ExecuteNonQuery()
            con.Close()
            MsgBox("Item Expenses Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
            BindGrid()
            'Else
            '    Exit Sub
            'End If


        End If


    End Sub


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            ClearConrols()
            Panel1.Visible = True
            Txthamali.Text = String.Format("{0:n2}", 0.0)
            Txtlevy.Text = String.Format("{0:n2}", 0.0)
            Txttolai.Text = String.Format("{0:n2}", 0.0)
            Txtvatav.Text = String.Format("{0:n2}", 0.0)
            Txtapmc.Text = String.Format("{0:n2}", 0.0)
            Txtmaplevy.Text = String.Format("{0:n4}", 0.0)
            txtitmcd.Text = ""
            'txt_fromDate.SelectedDate = Date.Now.AddDays(-7)
            'txt_toDate.SelectedDate = Date.Today
            BindGrid()
        End If


    End Sub



    Private Shared Function aspx() As String
        Throw New NotImplementedException
    End Function



    Protected Sub GridView1_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class


