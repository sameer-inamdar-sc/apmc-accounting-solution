﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class PaymentRegister
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim s1 As String
            s1 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s1, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")


            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txt_fromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txt_toDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)
        End If
        If Not IsPostBack Then
            If Page.Request.QueryString("type") = "0" Then
                Lbl_TransactionNo.Text = "Receipt No."
                Lbl_Header.Text = "Receipt Details"
            ElseIf Page.Request.QueryString("type") = "1" Then
                Lbl_TransactionNo.Text = "Voucher No."
                Lbl_Header.Text = "Payment Details"
            End If

        End If
    End Sub
    Protected Sub ShowPaymentRegister()
        Dim filter As String = ""

        If txtSearchPaymentNo.Text <> "" Then
            filter = "and m.TransactionMasterID like '" & txtSearchPaymentNo.Text & "%'"
        End If

        If txtSearchAcName.Text <> "" Then
            filter = filter & "and a.acname like '" & txtSearchAcName.Text & "%'"
        End If

        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and m.date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If

        filter = filter & " and rp=" & Page.Request.QueryString("type")
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        con.Open()
        Dim cmd As New SqlCommand("select CONVERT(VARCHAR(10), m.date, 105) AS Date,m.TransactionMasterID 'Voucher No.',a.acname 'Account Name',case when TransType=0 then SUM(ISNULL(d.amount,0)) end Cash,case when TransType=1 then SUM(ISNULL(d.amount,0)) end Bank,SUM(ISNULL(d.amount,0)) Total,cb.acname 'Cash/Bank' from TransactionMaster m join acmast a on m.AcCode=a.acccd join TransactionDetails d on m.TransactionMasterID=d.TransactionMasterID join acmast cb on m.casbankcode=cb.acccd " & filter & " group by a.acname,date,m.TransactionMasterID,TransType,cb.acname", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)

        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetPaymentRegisterd(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
        con.Close()
        'If Page.Request.QueryString("type") = "0" Then
        '    GridView1.HeaderRow.Cells(1).Text = "Receipt No."
        'ElseIf Page.Request.QueryString("type") = "1" Then
        '    GridView1.HeaderRow.Cells(1).Text = "Voucher No."
        'End If
        'ClearConrols()
    End Sub


    Protected Sub GenerateReport_Click(sender As Object, e As System.EventArgs) Handles GenerateReport.Click
        ShowPaymentRegister()
    End Sub
    Public Function GetPaymentRegisterd(ByVal dt As DataTable) As String

        Dim strb As New StringBuilder

        strb.Append("<table width='100%' border='1'><tr>")
        strb.Append("<td>" & dt.Columns(0).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(1).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(2).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(3).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(4).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(5).ColumnName & "</td>")
        strb.Append("<td>" & dt.Columns(6).ColumnName & "</td>")
        strb.Append("</tr>")

        Dim cashTotal As New Decimal
        cashTotal = 0
        Dim BankTotal As New Decimal
        BankTotal = 0
        Dim Total As New Decimal
        Total = 0

        For Each tr As DataRow In dt.Rows
            strb.Append("<tr>")

            strb.Append("<td>" & tr.Item(0).ToString() & "</td>")
            strb.Append("<td>" & tr.Item(1).ToString & "</td>")
            strb.Append("<td>" & tr.Item(2).ToString & "</td>")
            strb.Append("<td align='Right'>" & tr.Item(3).ToString & "</td>")
            strb.Append("<td align='Right'>" & tr.Item(4).ToString & "</td>")
            strb.Append("<td align='Right'>" & tr.Item(5).ToString & "</td>")
            strb.Append("<td>" & tr.Item(6).ToString & "</td>")
            strb.Append("</tr>")
            If tr.Item(3).ToString() <> "" Then
                cashTotal += Convert.ToDecimal(tr.Item(3).ToString())
            End If
            If tr.Item(4).ToString() <> "" Then
                BankTotal += Convert.ToDecimal(tr.Item(4).ToString())
            End If

            Total += Convert.ToDecimal(tr.Item(5).ToString())

        Next
        strb.Append("<tr>")
        strb.Append("<td colspan='3' align='Center'>Total</td>")
        strb.Append("<td align='Right'>" & IIf(cashTotal <> 0, cashTotal, "&nbsp;") & "</td>")
        strb.Append("<td align='Right'>" & IIf(BankTotal <> 0, BankTotal, "&nbsp;") & "</td>")
        strb.Append("<td align='Right'>" & IIf(Total <> 0, Total, "&nbsp;") & "</td>")
        strb.Append("<td>&nbsp;</td>")


        strb.Append("</tr></table>")

        Return strb.ToString()

    End Function

    Protected Sub BtnPrint_Click(sender As Object, e As System.EventArgs) Handles BtnPrint.Click
        Dim url As String = "PaymentReceiptRegister.aspx?id=" & txtSearchPaymentNo.Text & "&acname=" & txtSearchAcName.Text & "&frdate=" & txt_fromDate.Text & "&todate=" & txt_toDate.Text & "&type=" & Page.Request.QueryString("type")
        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.open('")
        sb.Append(url)
        sb.Append("');")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.GetType(), _
                  "script", sb.ToString())
    End Sub
End Class
