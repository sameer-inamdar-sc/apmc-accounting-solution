﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing.Printing
Partial Class CashBookRegister
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim sr As String
    Public dt As DataTable
    Dim filter As String = ""
    Dim sbPageString As New StringBuilder()
    'Protected Sub BindGrid()
    '    Dim filter As String = ""

    '    If ddlSearchCashBooklNo.Text <> "" Then
    '        filter = "and t.TransactionMasterID like '" & ddlSearchCashBooklNo.Text & "%'"
    '    End If

    '    If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
    '        filter = filter & "and t.date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
    '    End If

    '    ''filter = filter & " and modecasbank=" & Page.Request.QueryString("type")
    '    If filter <> "" Then
    '        filter = Replace(filter, "and", "where", 1, 1)
    '    End If

    '    con.Open()
    '    Dim cmd As New SqlCommand("select date,doccode,acname,modecasbank,trsnrp,isnull(SUM(cbtran.amount),0)amount from cbmast left join cbtran on cbmast.cbno=cbtran.cbno join acmast on cbtran.acccd=acmast.acccd  where modecasbank=1 group by cbmast.cbno,chqdate,doccode,acname,modecasbank,trsnrp,date union all select date,doccd,acname,TransType,rp,(select SUM(amount) from TransactionDetails where TransactionMasterID=1)amount from TransactionMaster join acmast on TransactionMaster.AcCode=acmast.acccd where TransType=0 ", con)
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim dt As New DataTable
    '    da.Fill(dt)
    '    GetCashBookRegister(dt)
    '    con.Close()
    '    'ClearConrols()
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.Request.QueryString("type") = 1 Then
            lbl_header.Text = "Cash Book Register"
        Else
            lbl_header.Text = "Bank Book Register"
        End If
        If Not Page.IsPostBack Then
            If Page.Request.QueryString("type") = 1 Then
                con.Open()
                ddlSearchCashBooklNo.Enabled = False
                Dim com As New SqlCommand("select acname,acccd from acmast where acccd=0000097", con)
                Dim da2 As New SqlDataAdapter(com)
                Dim dt2 As New DataTable()
                da2.Fill(dt2)
                con.Close()
                ddlSearchCashBooklNo.DataSource = dt2
                ddlSearchCashBooklNo.DataTextField = "acname"
                ddlSearchCashBooklNo.DataValueField = "acccd"
                ddlSearchCashBooklNo.DataBind()
            Else
                ddlSearchCashBooklNo.Enabled = True
                Dim com As New SqlCommand("select acname,acccd from acmast where mjcd='A3' and acno <> '97' ", con)
                Dim da2 As New SqlDataAdapter(com)
                Dim dt2 As New DataTable()
                da2.Fill(dt2)
                con.Close()
                ddlSearchCashBooklNo.DataSource = dt2
                ddlSearchCashBooklNo.DataTextField = "acname"
                ddlSearchCashBooklNo.DataValueField = "acccd"
                ddlSearchCashBooklNo.DataBind()
            End If
            ''   BindGrid()
            Dim s2 As String
            s2 = "select CONVERT(VARCHAR(10), fdate, 103) AS fdate,CONVERT(VARCHAR(10), tdate, 103) AS tdate,coyear from finyear where coyear='" & Session("coyear") & "'"
            Dim cmdf As New SqlCommand(s2, con)
            Dim daf As New SqlDataAdapter(cmdf)
            Dim dtf = New DataTable
            daf.Fill(dtf)

            Dim fd, td As String
            fd = dtf.Rows(0).Item("fdate")
            td = dtf.Rows(0).Item("tdate")
            Dim fdArray, tdArray As String()
            fdArray = fd.Split("/")
            tdArray = td.Split("/")

            txtSearchFromDate.SelectedDate = fdArray(1) & "-" & fdArray(0) & "-" & fdArray(2)
            txtSearchToDate.SelectedDate = tdArray(1) & "-" & tdArray(0) & "-" & tdArray(2)
        End If
    End Sub
    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        ShowCashBookRegister()
    End Sub
    Public Sub ShowCashBookRegister()

        'If txtSearchInvoice.Text <> "" Then
        '    filter = "and m.serno= " & txtSearchInvoice.Text & ""
        'End If

        If ddlSearchCashBooklNo.Text <> "" Then
            filter = "and j.serno like '" & ddlSearchCashBooklNo.Text & "%'"
        End If
        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and jm.Date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If

        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim s As String
        '  s = "select s.serno,s.InvoiceDate,s.acccd,a.acname,s.netamt,s.amtrecd,(s.netamt-s.amtrecd) amtbalance from smast s join acmast a on a.acccd=s.acccd " & filter & " ORDER BY a.acname,s.serno "
        ' s = "select jm.date,j.doccd+' '+convert(nvarchar,j.serno) serno,ac.acname,j.cramt,j.dramt from journal j join journalmast jm on j.serno=jm.serno join acmast ac on ac.acccd=j.acccd " & filter & " order by j.serno"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        da.Fill(dt)

        If dt.Rows.Count > 0 Then
            lblPageString.Text = GetCashBookRegister(dt)
        Else
            lblPageString.Text = "There are no records to print."
        End If
    End Sub
    Private Sub CashBookRegister()
        Throw New NotImplementedException
    End Sub
    Public Function GetCashBookRegister(ByVal dt As DataTable) As String
        Dim a As Integer
        'Dim amount As Double
        Dim cramt As Double
        Dim dramt As Double


        ' acname = ""


        sbPageString.Append("<style> .bottom{position: absolute;bottom: 0px;}th{border-bottom: 1px solid;border-left: 1px solid;}th:first-child{border-left: 0px solid;}</style>")
        sbPageString.Append("<table style='width: 100%;border: 1px solid;border-spacing: 0px;border-bottom: 0px solid;'><tr align='center' ><th width='10%'>Date</th><th width='20%'>DocCode</th><th width='20%'>AcName</th><th width='25%'>cramt</th><th width='25%'>dramt</th></tr>")
        For i = 0 To dt.Rows.Count - 1
            a = a + 1



            cramt += Convert.ToDouble(dt.Rows(i).Item("cramt"))

            dramt += Convert.ToDouble(dt.Rows(i).Item("dramt"))

            'trsnrp += Convert.ToDouble(dt.Rows(i).Item("trsnrp"))


            'If acname = dt.Rows(i).Item("acname") Then
            'dt.Rows(i).Item("acname") = ""
            'Else
            'acname = dt.Rows(i).Item("acname")

            'End If
            ' ''Dim billDate As String
            ' ''billDate = dt.Rows(i).Item("Date")

            ' ''Dim billDateArray As String()
            ' ''billDateArray = billDate.Split("/")

            ' ''billDate = billDateArray(1) & "/" & billDateArray(0) & "/" & billDateArray(2)

            'sbPageString.Append("<tr><td width='3%' align='center'>" & a & "</td><td width='5%' align='right' style='padding-right: 10px;'>" & billDate & "</td><td width='20%' style='padding-left: 8px;'>" & dt.Rows(i).Item("acname") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("cramt") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("dramt") & "</td></tr>")
            sbPageString.Append("<tr><td align='center'>" & dt.Rows(i).Item("pDate") & "</td><td align='center'>" & dt.Rows(i).Item("DocCd") & "</td><td width='20%' style='padding-left: 8px;'>" & dt.Rows(i).Item("acname") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("cramt") & "</td><td width='10%' align='right'>" & dt.Rows(i).Item("drAmt") & "</td></tr>")


            'If Not IsDBNull(dt.Rows(i).Item("cramt")) Then
            '    reciptamt += Convert.ToDouble(dt.Rows(i).Item("cramt"))
            'End If
            'If Not IsDBNull(dt.Rows(i).Item("dramt")) Then
            '    paymentamt += Convert.ToDouble(dt.Rows(i).Item("dramt"))
            'End If




            'amtbalance += Convert.ToDouble(dt.Rows(i).Item("amtbalance"))


            '            
            '            net += n

        Next

        'sbPageString.Append("<tr style='font-weight: bold;'><td width='74%' colspan='5' align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td></tr></table>")
        sbPageString.Append("<tr style='font-weight: bold;'><td width='74%' colspan='3' align='right' style='border-top: 1px solid;border-right: 1px solid;'>Total</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & cramt.ToString("#0.00") & "</td><td width='10%' align='right' style='border-top: 1px solid;border-right: 1px solid;'>" & dramt.ToString("#0.00") & "</td></tr></table>")

        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        '        sbPageString.Append("")
        Return sbPageString.ToString()
    End Function
    Public Function GetCompInfo() As DataTable
        Dim s As String
        s = "GetCompInfo"
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@compid", Session("compid"))
        da.Fill(dt)
        Return dt
    End Function
    Protected Sub Btn_Print_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Print.Click
        Dim fd, td, today As String
        fd = txtSearchFromDate.SelectedDate
        td = txtSearchToDate.SelectedDate
        today = Now.ToShortDateString()

        Dim fromDateArray, toDateArray, todayArray As String()
        fromDateArray = fd.Split("/")
        toDateArray = td.Split("/")
        todayArray = today.Split("/")
        today = todayArray(1) & "/" & todayArray(0) & "/" & todayArray(2)

        If txtSearchFromDate.Text = "" Then
            fd = Nothing
        Else
            fd = fromDateArray(1) & "/" & fromDateArray(0) & "/" & fromDateArray(2)
        End If
        If txtSearchToDate.Text = "" Then
            td = Nothing
        Else
            td = toDateArray(1) & "/" & toDateArray(0) & "/" & toDateArray(2)
        End If
        Dim CompDt = New DataTable
        CompDt = GetCompInfo()

        sbPageString.Append("<div id='printDiv'>")
        sbPageString.Append("<table align='center'><tr><td>" & CompDt.Rows(0)("coname") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compadd") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compwebsite") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("email") & "</td></tr>")
        sbPageString.Append("<tr><td>" & CompDt.Rows(0)("compphone") & "</td></tr></table><br>")
        sbPageString.Append("<tr><td>" & lbl_header.Text & "<br></td></tr>")
        sbPageString.Append("<tr><td>" & ddlSearchCashBooklNo.SelectedItem.ToString() & "</td></tr>")
        sbPageString.Append("<table><tr><td width='10%'>From Date </td><td width='10%'>" & fd & "</td><td width='10%'>To Date </td><td width='55%'>" & td & "</td><td width='10%'>Print Date </td><td width='10%'>" & today & "</td></tr></table><caption><hr /></caption>")

        If ddlSearchCashBooklNo.Text <> "" Then
            filter = "and j.serno like '" & ddlSearchCashBooklNo.Text & "%'"
        End If

        If (txtSearchFromDate.Text <> "" Or txtSearchFromDate.SelectedDateFormatted.ToString() <> "") And (txtSearchToDate.Text <> "" Or txtSearchToDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and jm.date BETWEEN '" & txtSearchFromDate.SelectedDate & "' AND '" & txtSearchToDate.SelectedDate & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If

        Dim cmd As New SqlCommand

        Dim s As String
        s = "CashBankReport"
        cmd = New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt = New DataTable
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@mode", Page.Request.QueryString("type"))
        cmd.Parameters.AddWithValue("@acccd", ddlSearchCashBooklNo.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@fdate", txtSearchFromDate.SelectedDate)
        cmd.Parameters.AddWithValue("@tdate", txtSearchToDate.SelectedDate)
        da.Fill(dt)


        GetCashBookRegister(dt)



        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(sbPageString.ToString())
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), "Print", sb.ToString())
        sbPageString.Clear()
        ''btnGenerateReport_Click(sender, e)
    End Sub

End Class

