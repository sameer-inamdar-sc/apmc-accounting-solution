﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Class code
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("KomalConnectionString").ConnectionString)
    Public Sub showval()

        drlcashcd.Items.Clear()
        Dim cmd1 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr1 As SqlDataReader = cmd1.ExecuteReader()
        While dr1.Read()
            Dim item As String = dr1.GetSqlString(0).Value
            drlcashcd.Items.Add(New ListItem(item))
        End While
        dr1.Close()
        drlcashcd.Items.Insert(0, "Select")

        drlbankcd.Items.Clear()
        Dim cmd2 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr2 As SqlDataReader = cmd2.ExecuteReader()
        While dr2.Read()
            Dim item As String = dr2.GetSqlString(0).Value
            drlbankcd.Items.Add(New ListItem(item))
        End While
        dr2.Close()
        drlbankcd.Items.Insert(0, "Select")

        drlpl.Items.Clear()
        Dim cmd3 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr3 As SqlDataReader = cmd3.ExecuteReader()
        While dr3.Read()
            Dim item As String = dr3.GetSqlString(0).Value
            drlpl.Items.Add(New ListItem(item))
        End While
        dr3.Close()
        drlpl.Items.Insert(0, "Select")

        drlcommision.Items.Clear()
        Dim cmd4 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr4 As SqlDataReader = cmd4.ExecuteReader()
        While dr4.Read()
            Dim item As String = dr4.GetSqlString(0).Value
            drlcommision.Items.Add(New ListItem(item))
        End While
        dr4.Close()
        drlcommision.Items.Insert(0, "Select")

        drl1.Items.Clear()
        Dim cmd5 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr5 As SqlDataReader = cmd5.ExecuteReader()
        While dr5.Read()
            Dim item As String = dr5.GetSqlString(0).Value
            drl1.Items.Add(New ListItem(item))
        End While
        dr5.Close()
        drl1.Items.Insert(0, "Select")

        drl2.Items.Clear()
        Dim cmd6 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr6 As SqlDataReader = cmd6.ExecuteReader()
        While dr6.Read()
            Dim item As String = dr6.GetSqlString(0).Value
            drl2.Items.Add(New ListItem(item))
        End While
        dr6.Close()
        drl2.Items.Insert(0, "Select")

        drlincome.Items.Clear()
        Dim cmd7 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr7 As SqlDataReader = cmd7.ExecuteReader()
        While dr7.Read()
            Dim item As String = dr7.GetSqlString(0).Value
            drlincome.Items.Add(New ListItem(item))
        End While
        dr7.Close()
        drlincome.Items.Insert(0, "Select")

        drlmotorfrt.Items.Clear()
        Dim cmd8 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr8 As SqlDataReader = cmd8.ExecuteReader()
        While dr8.Read()
            Dim item As String = dr8.GetSqlString(0).Value
            drlmotorfrt.Items.Add(New ListItem(item))
        End While
        dr8.Close()
        drlmotorfrt.Items.Insert(0, "Select")

        drlpurchase.Items.Clear()
        Dim cmd9 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr9 As SqlDataReader = cmd9.ExecuteReader()
        While dr9.Read()
            Dim item As String = dr9.GetSqlString(0).Value
            drlpurchase.Items.Add(New ListItem(item))
        End While
        dr9.Close()
        drlpurchase.Items.Insert(0, "Select")

        drlsales.Items.Clear()
        Dim cmd10 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr10 As SqlDataReader = cmd10.ExecuteReader()
        While dr10.Read()
            Dim item As String = dr10.GetSqlString(0).Value
            drlsales.Items.Add(New ListItem(item))
        End While
        dr10.Close()
        drlsales.Items.Insert(0, "Select")

        drldiff.Items.Clear()
        Dim cmd11 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr11 As SqlDataReader = cmd11.ExecuteReader()
        While dr11.Read()
            Dim item As String = dr11.GetSqlString(0).Value
            drldiff.Items.Add(New ListItem(item))
        End While
        dr11.Close()
        drldiff.Items.Insert(0, "Select")

        drlroundoff.Items.Clear()
        Dim cmd12 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr12 As SqlDataReader = cmd12.ExecuteReader()
        While dr12.Read()
            Dim item As String = dr12.GetSqlString(0).Value
            drlroundoff.Items.Add(New ListItem(item))
        End While
        dr12.Close()
        drlroundoff.Items.Insert(0, "Select")

        drlvadar.Items.Clear()
        Dim cmd13 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr13 As SqlDataReader = cmd13.ExecuteReader()
        While dr13.Read()
            Dim item As String = dr13.GetSqlString(0).Value
            drlvadar.Items.Add(New ListItem(item))
        End While
        dr13.Close()
        drlvadar.Items.Insert(0, "Select")

        drlkasar.Items.Clear()
        Dim cmd14 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr14 As SqlDataReader = cmd14.ExecuteReader()
        While dr14.Read()
            Dim item As String = dr14.GetSqlString(0).Value
            drlkasar.Items.Add(New ListItem(item))
        End While
        dr14.Close()
        drlkasar.Items.Insert(0, "Select")

        drlvatav.Items.Clear()
        Dim cmd15 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr15 As SqlDataReader = cmd15.ExecuteReader()
        While dr15.Read()
            Dim item As String = dr15.GetSqlString(0).Value
            drlvatav.Items.Add(New ListItem(item))
        End While
        dr15.Close()
        drlvatav.Items.Insert(0, "Select")

        drlbankchgs.Items.Clear()
        Dim cmd16 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr16 As SqlDataReader = cmd16.ExecuteReader()
        While dr16.Read()
            Dim item As String = dr16.GetSqlString(0).Value
            drlbankchgs.Items.Add(New ListItem(item))
        End While
        dr16.Close()
        drlbankchgs.Items.Insert(0, "Select")

        drl3.Items.Clear()
        Dim cmd17 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr17 As SqlDataReader = cmd17.ExecuteReader()
        While dr17.Read()
            Dim item As String = dr17.GetSqlString(0).Value
            drl3.Items.Add(New ListItem(item))
        End While
        dr17.Close()
        drl3.Items.Insert(0, "Select")

        drl4.Items.Clear()
        Dim cmd18 As New SqlCommand("select acname from acmast order by acname", con)
        Dim dr18 As SqlDataReader = cmd18.ExecuteReader()
        While dr18.Read()
            Dim item As String = dr18.GetSqlString(0).Value
            drl4.Items.Add(New ListItem(item))
        End While
        dr18.Close()
        drl4.Items.Insert(0, "Select")


    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
            Panel1.Visible = True
            drlcashcd.Focus()

            con.Open()

            Dim cmdStr As String = "select count(*) from code"

            cmdStr += " where compid='" + Txtcompid.Text.Trim() & "'"

            Dim cmd As New SqlCommand(cmdStr, con)

            Dim i As Integer
            i = Convert.ToInt16(cmd.ExecuteScalar())

            btnUpdate.Enabled = False
            btnsave.Enabled = True

            If i = 0 Then

                showval()

            Else
                btnUpdate.Enabled = True
                btnsave.Enabled = False

                showval()
                getval()
            End If

            con.Close()
        End If
    End Sub
    Public Sub getval()
        Dim cmd1 As New SqlCommand("Select * from code where compid='" + Txtcompid.Text + "'", con)
        Dim da1 As New SqlDataAdapter(cmd1)
        Dim ds As New DataSet()
        da1.Fill(ds)
        Dim custTable1 As DataTable = ds.Tables(0)

        If custTable1.Rows(0).Item(2) = Nothing Then
            drlcashcd.Text = "Select"
        Else
            Dim cmd2 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(2) + "'", con)
            Dim da2 As New SqlDataAdapter(cmd2)
            Dim ds2 As New DataSet()
            da2.Fill(ds2)
            Dim custTable2 As DataTable = ds2.Tables(0)
            drlcashcd.Text = custTable2.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(3) = Nothing Then
            drlbankcd.Text = "Select"
        Else
            Dim cmd3 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(3) + "'", con)
            Dim da3 As New SqlDataAdapter(cmd3)
            Dim ds3 As New DataSet()
            da3.Fill(ds3)
            Dim custTable3 As DataTable = ds3.Tables(0)
            drlbankcd.Text = custTable3.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(4) = Nothing Then
            drlmotorfrt.Text = "Select"
        Else
            Dim cmd4 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(4) + "'", con)
            Dim da4 As New SqlDataAdapter(cmd4)
            Dim ds4 As New DataSet()
            da4.Fill(ds4)
            Dim custTable4 As DataTable = ds4.Tables(0)
            drlmotorfrt.Text = custTable4.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(5) = Nothing Then
            drlpl.Text = "Select"
        Else
            Dim cmd5 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(5) + "'", con)
            Dim da5 As New SqlDataAdapter(cmd5)
            Dim ds5 As New DataSet()
            da5.Fill(ds5)
            Dim custTable5 As DataTable = ds5.Tables(0)
            drlpl.Text = custTable5.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(6) = Nothing Then
            drlcommision.Text = "Select"
        Else
            Dim cmd6 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(6) + "'", con)
            Dim da6 As New SqlDataAdapter(cmd6)
            Dim ds6 As New DataSet()
            da6.Fill(ds6)
            Dim custTable6 As DataTable = ds6.Tables(0)
            drlcommision.Text = custTable6.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(7) = Nothing Then
            drlincome.Text = "Select"
        Else
            Dim cmd7 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(7) + "'", con)
            Dim da7 As New SqlDataAdapter(cmd7)
            Dim ds7 As New DataSet()
            da7.Fill(ds7)
            Dim custTable7 As DataTable = ds7.Tables(0)
            drlincome.Text = custTable7.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(8) = Nothing Then
            drlpurchase.Text = "Select"
        Else
            Dim cmd8 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(8) + "'", con)
            Dim da8 As New SqlDataAdapter(cmd8)
            Dim ds8 As New DataSet()
            da8.Fill(ds8)
            Dim custTable8 As DataTable = ds8.Tables(0)
            drlpurchase.Text = custTable8.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(9) = Nothing Then
            drlsales.Text = "Select"
        Else
            Dim cmd9 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(9) + "'", con)
            Dim da9 As New SqlDataAdapter(cmd9)
            Dim ds9 As New DataSet()
            da9.Fill(ds9)
            Dim custTable9 As DataTable = ds9.Tables(0)
            drlsales.Text = custTable9.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(10) = Nothing Then
            drldiff.Text = "Select"
        Else
            Dim cmd10 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(10) + "'", con)
            Dim da10 As New SqlDataAdapter(cmd10)
            Dim ds10 As New DataSet()
            da10.Fill(ds10)
            Dim custTable10 As DataTable = ds10.Tables(0)
            drldiff.Text = custTable10.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(11) = Nothing Then
            drlroundoff.Text = "Select"
        Else
            Dim cmd11 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(11) + "'", con)
            Dim da11 As New SqlDataAdapter(cmd11)
            Dim ds11 As New DataSet()
            da11.Fill(ds11)
            Dim custTable11 As DataTable = ds11.Tables(0)
            drlroundoff.Text = custTable11.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(12) = Nothing Then
            drlvadar.Text = "Select"
        Else
            Dim cmd12 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(12) + "'", con)
            Dim da12 As New SqlDataAdapter(cmd12)
            Dim ds12 As New DataSet()
            da12.Fill(ds12)
            Dim custTable12 As DataTable = ds12.Tables(0)
            drlvadar.Text = custTable12.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(13) = Nothing Then
            drlkasar.Text = "Select"
        Else
            Dim cmd13 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(13) + "'", con)
            Dim da13 As New SqlDataAdapter(cmd13)
            Dim ds13 As New DataSet()
            da13.Fill(ds13)
            Dim custTable13 As DataTable = ds13.Tables(0)
            drlkasar.Text = custTable13.Rows(0).Item(0)
        End If


        If custTable1.Rows(0).Item(14) = Nothing Then
            drlvatav.Text = "Select"
        Else
            Dim cmd14 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(14) + "'", con)
            Dim da14 As New SqlDataAdapter(cmd14)
            Dim ds14 As New DataSet()
            da14.Fill(ds14)
            Dim custTable14 As DataTable = ds14.Tables(0)
            drlvatav.Text = custTable14.Rows(0).Item(0)
        End If

        If custTable1.Rows(0).Item(15) = Nothing Then
            drlbankchgs.Text = "Select"
        Else
            Dim cmd15 As New SqlCommand("select acname from acmast where acccd='" + custTable1.Rows(0).Item(15) + "'", con)
            Dim da15 As New SqlDataAdapter(cmd15)
            Dim ds15 As New DataSet()
            da15.Fill(ds15)
            Dim custTable15 As DataTable = ds15.Tables(0)
            drlbankchgs.Text = custTable15.Rows(0).Item(0)
        End If

    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        con.Open()

        sessionval()

        Dim s As String
        s = "update code set userid='" & Txtuserid.Text.Trim() & "',cashcd='" & Session("cashcd") & "',bankcd='" & Session("bankcd") & "',motorfrt='" & Session("motorfrt") & "',pl='" & Session("pl") & "',commission='" & Session("commission") & "',income='" & Session("income") & "',purchase='" & Session("purchase") & "',sales='" & Session("sales") & "',diff='" & Session("diff") & "',roundoff='" & Session("roundoff") & "',vadar='" & Session("vadar") & "',kasar='" & Session("kasar") & "',vatav='" & Session("vatav") & "',bankchgs='" & Session("bankchgs") & "' where compid=" & Txtcompid.Text.Trim() & " "
        Dim cmd As New SqlCommand(s, con)
        cmd.ExecuteNonQuery()
        MsgBox("Your Record Has Been Updated", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Update")

        showval()
        getval()
        con.Close()
    End Sub
    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        con.Open()

        sessionval()
        Dim cmd As New SqlCommand("insert into code values('" & Txtcompid.Text.Trim() & "','" & Txtuserid.Text.Trim() & "','" & Session("cashcd") & "','" & Session("bankcd") & "','" & Session("motorfrt") & "','" & Session("pl") & "','" & Session("commission") & "','" & Session("income") & "','" & Session("purchase") & "','" & Session("sales") & "','" & Session("diff") & "','" & Session("roundoff") & "','" & Session("vadar") & "','" & Session("kasar") & "','" & Session("vatav") & "','" & Session("bankchgs") & "')", con)
        cmd.ExecuteNonQuery()
        MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Saved")

        btnsave.Enabled = False
        btnUpdate.Enabled = True

        showval()
        getval()

        con.Close()
    End Sub
    Public Sub sessionval()
        If drlcashcd.SelectedIndex = 0 Then
            Session("cashcd") = Nothing
        Else
            Dim cmd1 As New SqlCommand("select acccd from acmast where acname='" + drlcashcd.SelectedItem.Text + "'", con)
            Dim da1 As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da1.Fill(ds1)
            Dim custTable1 As DataTable = ds1.Tables(0)
            Session("cashcd") = custTable1.Rows(0).Item(0)
        End If

        If drlbankcd.SelectedIndex = 0 Then
            Session("bankcd") = Nothing
        Else
            Dim cmd2 As New SqlCommand("select acccd from acmast where acname='" + drlbankcd.SelectedItem.Text + "'", con)
            Dim da2 As New SqlDataAdapter(cmd2)
            Dim ds2 As New DataSet()
            da2.Fill(ds2)
            Dim custTable2 As DataTable = ds2.Tables(0)
            Session("bankcd") = custTable2.Rows(0).Item(0)
        End If

        If drlpl.SelectedIndex = 0 Then
            Session("pl") = Nothing
        Else
            Dim cmd3 As New SqlCommand("select acccd from acmast where acname='" & drlpl.SelectedItem.ToString() & "'", con)
            Dim da3 As New SqlDataAdapter(cmd3)
            Dim ds3 As New DataSet()
            da3.Fill(ds3)
            Dim custTable3 As DataTable = ds3.Tables(0)
            Session("pl") = custTable3.Rows(0).Item(0)
        End If

        If drlcommision.SelectedIndex = 0 Then
            Session("commission") = Nothing
        Else
            Dim cmd4 As New SqlCommand("select acccd from acmast where acname='" + drlcommision.SelectedItem.Text + "'", con)
            Dim da4 As New SqlDataAdapter(cmd4)
            Dim ds4 As New DataSet()
            da4.Fill(ds4)
            Dim custTable4 As DataTable = ds4.Tables(0)
            Session("commission") = custTable4.Rows(0).Item(0)
        End If

        If drlincome.SelectedIndex = 0 Then
            Session("income") = Nothing
        Else
            Dim cmd5 As New SqlCommand("select acccd from acmast where acname='" + drlincome.SelectedItem.Text + "'", con)
            Dim da5 As New SqlDataAdapter(cmd5)
            Dim ds5 As New DataSet()
            da5.Fill(ds5)
            Dim custTable5 As DataTable = ds5.Tables(0)
            Session("income") = custTable5.Rows(0).Item(0)
        End If

        If drlmotorfrt.SelectedIndex = 0 Then
            Session("motorfrt") = Nothing
        Else
            Dim cmd6 As New SqlCommand("select acccd from acmast where acname='" + drlmotorfrt.SelectedItem.Text + "'", con)
            Dim da6 As New SqlDataAdapter(cmd6)
            Dim ds6 As New DataSet()
            da6.Fill(ds6)
            Dim custTable6 As DataTable = ds6.Tables(0)
            Session("motorfrt") = custTable6.Rows(0).Item(0)
        End If

        If drlpurchase.SelectedIndex = 0 Then
            Session("purchase") = Nothing
        Else
            Dim cmd7 As New SqlCommand("select acccd from acmast where acname='" + drlpurchase.SelectedItem.Text + "'", con)
            Dim da7 As New SqlDataAdapter(cmd7)
            Dim ds7 As New DataSet()
            da7.Fill(ds7)
            Dim custTable7 As DataTable = ds7.Tables(0)
            Session("purchase") = custTable7.Rows(0).Item(0)
        End If

        If drlsales.SelectedIndex = 0 Then
            Session("sales") = Nothing
        Else
            Dim cmd8 As New SqlCommand("select acccd from acmast where acname='" + drlsales.SelectedItem.Text + "'", con)
            Dim da8 As New SqlDataAdapter(cmd8)
            Dim ds8 As New DataSet()
            da8.Fill(ds8)
            Dim custTable8 As DataTable = ds8.Tables(0)
            Session("sales") = custTable8.Rows(0).Item(0)
        End If

        If drldiff.SelectedIndex = 0 Then
            Session("diff") = Nothing
        Else

            Dim cmd9 As New SqlCommand("select acccd from acmast where acname='" + drldiff.SelectedItem.Text + "'", con)
            Dim da9 As New SqlDataAdapter(cmd9)
            Dim ds9 As New DataSet()
            da9.Fill(ds9)
            Dim custTable9 As DataTable = ds9.Tables(0)
            Session("diff") = custTable9.Rows(0).Item(0)
        End If

        If drlroundoff.SelectedIndex = 0 Then
            Session("roundoff") = Nothing
        Else
            Dim cmd10 As New SqlCommand("select acccd from acmast where acname='" + drlroundoff.SelectedItem.Text + "'", con)
            Dim da10 As New SqlDataAdapter(cmd10)
            Dim ds10 As New DataSet()
            da10.Fill(ds10)
            Dim custTable10 As DataTable = ds10.Tables(0)
            Session("roundoff") = custTable10.Rows(0).Item(0)
        End If

        If drlvadar.SelectedIndex = 0 Then
            Session("vadar") = Nothing
        Else
            Dim cmd11 As New SqlCommand("select acccd from acmast where acname='" + drlvadar.SelectedItem.Text + "'", con)
            Dim da11 As New SqlDataAdapter(cmd11)
            Dim ds11 As New DataSet()
            da11.Fill(ds11)
            Dim custTable11 As DataTable = ds11.Tables(0)
            Session("vadar") = custTable11.Rows(0).Item(0)
        End If

        If drlkasar.SelectedIndex = 0 Then
            Session("kasar") = Nothing
        Else
            Dim cmd12 As New SqlCommand("select acccd from acmast where acname='" + drlkasar.SelectedItem.Text + "'", con)
            Dim da12 As New SqlDataAdapter(cmd12)
            Dim ds12 As New DataSet()
            da12.Fill(ds12)
            Dim custTable12 As DataTable = ds12.Tables(0)
            Session("kasar") = custTable12.Rows(0).Item(0)
        End If

        If drlvatav.SelectedIndex = 0 Then
            Session("vatav") = Nothing
        Else
            Dim cmd13 As New SqlCommand("select acccd from acmast where acname='" + drlvatav.SelectedItem.Text + "'", con)
            Dim da13 As New SqlDataAdapter(cmd13)
            Dim ds13 As New DataSet()
            da13.Fill(ds13)
            Dim custTable13 As DataTable = ds13.Tables(0)
            Session("vatav") = custTable13.Rows(0).Item(0)
        End If

        If drlbankchgs.SelectedIndex = 0 Then
            Session("bankchgs") = Nothing
        Else
            Dim cmd14 As New SqlCommand("select acccd from acmast where acname='" + drlbankchgs.SelectedItem.Text + "'", con)
            Dim da14 As New SqlDataAdapter(cmd14)
            Dim ds14 As New DataSet()
            da14.Fill(ds14)
            Dim custTable14 As DataTable = ds14.Tables(0)
            Session("bankchgs") = custTable14.Rows(0).Item(0)
        End If
    End Sub

End Class
