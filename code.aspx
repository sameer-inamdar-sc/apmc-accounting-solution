﻿
<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="code.aspx.vb" Inherits="code" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <div>
   
    <p align="left">
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Larger" 
            ForeColor="#6600CC" Text="Code Master"> </asp:Label> 
    </p>
    
    <table>
    <tr>
    <td>
    <asp:TextBox ID="Txtcompid" runat="server" Width="54px" Visible="False" 
            Enabled="False"></asp:TextBox>
    <asp:TextBox ID="Txtuserid" runat="server" Width="54px" Visible="False" 
            Enabled="False"></asp:TextBox>
    </td>
    </tr>
    </table>
         
    <asp:Panel ID="Panel1" runat="server" Height="400px" Visible="False">
        <table border="1" align="center" height="425px" width="900px" cellspacing="0">
        <tr>
        <td>
        <table width="875px" height="400px" align="center">
       
          
    <tr>
    <td>
    <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Cash Code"></asp:Label>
    </td> 
    <td>
     <asp:DropDownList ID="drlcashcd" runat="server" TabIndex="1" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Bank Code"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlbankcd" runat="server" TabIndex="2" AutoPostBack="true" Width="275px">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td>
    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Profit &amp; Loss A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlpl" runat="server" TabIndex="3" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td> 
    <td>
    <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Commision A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlcommision" runat="server" TabIndex="4" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
   <td>
   <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Income A/c."></asp:Label>
   </td>
   <td>
   <asp:DropDownList ID="drlincome" runat="server" TabIndex="5" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
   </td>
   <td>
   <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Motor Freight"></asp:Label>   
   </td>
   <td>
   <asp:DropDownList ID="drlmotorfrt" runat="server" TabIndex="6" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
   </td>
   </tr>
        
    <tr>
    <td>
    <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Purchase A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlpurchase" runat="server" TabIndex="7" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Sales A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlsales" runat="server" TabIndex="8" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td>
    <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Diff. A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drldiff" runat="server" TabIndex="9" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label15" runat="server" Font-Bold="True" Text="Round Off. A/c."></asp:Label>    
    </td>
    <td>
    <asp:DropDownList ID="drlroundoff" runat="server" TabIndex="10" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td>
    <asp:Label ID="Label16" runat="server" Font-Bold="True" Text="Vadar A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlvadar" runat="server" TabIndex="11" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Kasar A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlkasar" runat="server" TabIndex="12" Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td>
    <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="Vatav A/c."></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlvatav" runat="server" TabIndex="13" 
            Width="275px" AutoPostBack="true">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label19" runat="server" Font-Bold="True" Text="Bank Charges"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drlbankchgs" runat="server" TabIndex="14" AutoPostBack="true" Width="275px">
        </asp:DropDownList>
    </td>
    </tr>
       
    <tr>
    <td>
    <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Other 1"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drl1" runat="server" TabIndex="15" Width="275px">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Other 2"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drl2" runat="server" TabIndex="16" Width="275px">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td>
    <asp:Label ID="Label20" runat="server" Font-Bold="True" Text="Other 3"></asp:Label>    
    </td>
    <td>
    <asp:DropDownList ID="drl3" runat="server" TabIndex="17" Width="275px">
        </asp:DropDownList>
    </td>
    <td>
    <asp:Label ID="Label21" runat="server" Font-Bold="True" Text="Other 4"></asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="drl4" runat="server" TabIndex="18" Width="275px">
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr height="45px">
    <td colspan="4" align="center">
    <asp:Button ID="btnUpdate" runat="server" Text="Update" TabIndex="19" />
    <asp:Button ID="btnsave" runat="server" Text="Save" />
    </td>
    </tr>
    
    </table>
    </td>
    </tr>
    </table>
       </asp:Panel> 
    </div>
</asp:Content>

