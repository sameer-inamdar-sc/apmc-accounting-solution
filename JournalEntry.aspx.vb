﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.Timer
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq

Partial Class JournalEntry
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Public query As String, constr As String
    Protected Sub BtnbackClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Response.Redirect("Journal.aspx")

    End Sub

    Protected Sub Bindgrid()
        Dim cmd As New SqlCommand("select journal.tserno,acmast.acname,journal.rp,journal.dramt,journal.cramt,journal.narrcd,acmast.acccd from journal left join acmast on journal.acccd=acmast.acccd where journal.serno=" & txtjeno.Text & " order by serno desc", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sr As String = CType(Page.Request.QueryString("id"), String)

            If sr = String.Empty Then


                con.Open()
                'HiddenField1.Value = "0"
                Dim cmd As New SqlCommand("select max(serno) from journal", con)
                Dim a As Integer
                Dim obj As Object = cmd.ExecuteScalar()
                If obj Is Nothing Or obj Is DBNull.Value Then
                    a = 0
                    a = a + 1
                    txtjeno.Text = a.ToString()
                Else
                    a = Convert.ToInt32(obj)
                    a = a + 1
                    txtjeno.Text = a.ToString()
                End If
                txtDate.SelectedDate = Date.Today




            Else
                con.Open()
                Dim cmd4 As New SqlCommand("select serno,date,narr from journalmast where serno=" & sr, con)
                Dim da1 As New SqlDataAdapter(cmd4)
                Dim ds1 As New DataSet()
                da1.Fill(ds1)
                Dim custTable As DataTable = ds1.Tables(0)
                'code for drlitmcd to fetch value
                txtjeno.Text = custTable.Rows(0).Item(0)
                txtnarration.Text = custTable.Rows(0).Item(2)
                Dim thisDate1 As Date = custTable.Rows(0).Item(1)
                Dim culture1 As New CultureInfo("pt-BR")
                txtDate.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)
                txtjeno.Enabled = False
                txtDate.Enabled = False


                Dim cmd3 As New SqlCommand("select journal.tserno,acmast.acname,journal.rp,journal.dramt,journal.cramt,journal.narrcd,acmast.acccd from journal left join acmast on journal.acccd=acmast.acccd where journal.serno=" & sr, con)
                Dim da As New SqlDataAdapter(cmd3)
                Dim dt As New DataTable
                da.Fill(dt)
                GridView1.DataSource = dt
                GridView1.DataBind()
                con.Close()

                con.Close()
                txtjeno.Text = sr


                'GridView1.DataSource = dt
                'GridView1.DataBind()
                hfmaster.Value = 1
            End If
            Txtuserid.Text = Session("userid")
            Txtcompid.Text = Session("compid")
        End If

        txtacname.Focus()

    End Sub

    Dim totdramt, totcramt As Double

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'totdramt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "dramt"))
            If IsDBNull(DataBinder.Eval(e.Row.DataItem, "dramt")) Then
                GridView1.Columns(3).FooterText = "0.00"
            Else
                totdramt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "dramt"))
            End If
            'totcramt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "cramt"))
            If IsDBNull(DataBinder.Eval(e.Row.DataItem, "cramt")) Then
                GridView1.Columns(4).FooterText = "0.00"
            Else
                totcramt += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "cramt"))
            End If
            GridView1.Columns(3).FooterText = totdramt.ToString("#0.00")
            GridView1.Columns(4).FooterText = totcramt.ToString("#0.00")

        End If
    End Sub
    Public query1 As String

    

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        con.Open()
        Dim thisDate1 As Date = txtDate.SelectedDate
        Dim culture1 As New CultureInfo("pt-BR")

        txtDate.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)


        If hfmaster.Value = 0 Then
            Dim m As New SqlCommand("insert into journalmast (serno,date,narr) values ( " & txtjeno.Text & ",'" & thisDate1 & "','" & txtnarration.Text & "')", con)
            m.ExecuteNonQuery()
        ElseIf hfmaster.Value = 1 Then
            Dim m As New SqlCommand("update journalmast set date='" & txtDate.SelectedDate & "',narr='" & txtnarration.Text & "' where serno=" & txtjeno.Text & "", con)
            m.ExecuteNonQuery()
        End If
        If hfupdate.Value = 0 Then

            Dim cmd3 As New SqlCommand("select count (acmast.acname) from journal left join acmast on journal.acccd=acmast.acccd where journal.serno=" & txtjeno.Text & " and acname='" & txtacname.Text & "'", con)
            cmd3.ExecuteScalar()
            If cmd3.ExecuteScalar() <> 0 Then
                MsgBox("Account Name Already Exist", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Exist")
                txtacname.Text = ""
                txtamount.Text = ""
                Exit Sub
            End If
            Dim cmd As New SqlCommand("select max(tserno) from journal where serno=" & txtjeno.Text & "", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1
                hfserno.Value = a.ToString()
            Else
                a = Convert.ToInt32(obj)
                a = a + 1
                hfserno.Value = a.ToString()
            End If
        End If

        query = "InsertJournal"
        Dim com As New SqlCommand(query, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtjeno.Text.ToString())
        com.Parameters.AddWithValue("@tserno", hfserno.Value.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        'com.Parameters.AddWithValue("@doccd", thisDate.ToString())
        com.Parameters.AddWithValue("@date", thisDate1.ToString())
        com.Parameters.AddWithValue("@rp", drlrecpay.SelectedValue.ToString())
        com.Parameters.AddWithValue("@acccd", hfacccd.Value.ToString())
        'com.Parameters.AddWithValue("@crdr", thisDate.ToString())
        If drlrecpay.SelectedIndex = 0 Then
            com.Parameters.AddWithValue("@dramt", txtamount.Text.ToString())
        Else
            com.Parameters.AddWithValue("@cramt", txtamount.Text.ToString())

        End If
        com.Parameters.AddWithValue("@narrcd", txtnarration.Text.ToString())
        Dim p, q, r, s As Double
        ''update
        If GridView1.Columns(3).FooterText <> "" Or 0.0 Then
            q = Convert.ToDouble(GridView1.Columns(3).FooterText)
        Else
            q = GridView1.Columns(3).FooterText = "0.00"

        End If

        r = txtamount.Text
        p = q + r

        If GridView1.Columns(4).FooterText <> "" Or 0.0 Then
            s = Convert.ToDouble(GridView1.Columns(4).FooterText)
        Else
            s = GridView1.Columns(4).FooterText = "0.00"

        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim v As Integer
        Dim u As New SqlCommand("select count(dramt)from journal where serno=" & txtjeno.Text & "", con)
        v = u.ExecuteScalar()
        If drlrecpay.SelectedValue = "CR" And v > 1 Then
            If q > txtamount.Text Then
                MsgBox("Amount Should be more than ₹" & q & "", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
                txtamount.Text = q
                Exit Sub
            End If
        End If

        Dim x As Integer
        Dim w As New SqlCommand("select count(cramt)from journal where serno=" & txtjeno.Text & "", con)
        x = w.ExecuteScalar()
        If drlrecpay.SelectedValue = "DR" And x > 1 Then
            If s > txtamount.Text Then
                MsgBox("Amount Should be more than ₹" & s & "", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
                txtamount.Text = s
                Exit Sub
            End If
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If p > s And drlrecpay.Enabled = False And drlrecpay.SelectedIndex = 0 And hfupdate.Value = 0 Then
            MsgBox("Exceed", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
            txtamount.Text = s - q
            txtamount.Focus()

            Exit Sub
        End If
        If q < s + r And drlrecpay.Enabled = False And drlrecpay.SelectedIndex = 1 And hfupdate.Value = 0 Then
            txtamount.Text = q - s
            MsgBox("Amount Shouldn't more then ₹" & txtamount.Text & "", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
            txtamount.Focus()

            Exit Sub
        End If
        If s < q + r And drlrecpay.Enabled = False And drlrecpay.SelectedIndex = 0 And hfupdate.Value = 0 Then
            txtamount.Text = s - q
            MsgBox("Amount Shouldn't more then ₹" & txtamount.Text & "", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
            txtamount.Focus()

            Exit Sub
        End If

        If p < s And drlrecpay.Enabled = False And drlrecpay.SelectedIndex = 1 And hfupdate.Value = 0 Then
            MsgBox("Exceed", MsgBoxStyle.Information + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Exceed")
            txtamount.Text = q - s
            txtamount.Focus()
            Exit Sub
        End If
        com.ExecuteNonQuery()

        If GridView1.Columns(3).FooterText <> "" Then
            q = Convert.ToDouble(GridView1.Columns(3).FooterText)
        Else
            q = GridView1.Columns(3).FooterText = "0.00"
        End If
        r = txtamount.Text
        If drlrecpay.SelectedValue = "DR" Then
            p = q + r
        Else
            p = s + r
        End If
        If GridView1.Columns(4).FooterText <> "" Then
            s = Convert.ToDouble(GridView1.Columns(4).FooterText)
        Else
            s = GridView1.Columns(4).FooterText = "0.00"
        End If

        Dim i As Integer
        Dim f As New SqlCommand("select count(dramt)from journal where serno=" & txtjeno.Text & "", con)
        i = f.ExecuteScalar()

        Dim g As Integer
        Dim h As New SqlCommand("select count(cramt)from journal where serno=" & txtjeno.Text & "", con)
        g = h.ExecuteScalar()
        Dim j, k As Integer
        Bindgrid()
        j = Convert.ToDouble(GridView1.Columns(3).FooterText)
        k = Convert.ToDouble(GridView1.Columns(4).FooterText)

        If drlrecpay.SelectedValue = "CR" And v > i And g > 1 Then
            txtamount.Text = (s + r) - q
        ElseIf drlrecpay.SelectedValue = "CR" And i = 0 And g >= 1 Then
            txtamount.Text = txtamount.Text
            drlrecpay.SelectedValue = "DR"
        ElseIf drlrecpay.SelectedValue = "DR" And i >= 1 And g = 0 Then
            txtamount.Text = j
            drlrecpay.SelectedValue = "CR"
        ElseIf drlrecpay.SelectedValue = "CR" And i >= 1 And g = 1 Or i = 1 And g >= 1 Then
            If k > j Then
                txtamount.Text = k - j
                drlrecpay.SelectedValue = "DR"
            Else
                txtamount.Text = j - k
                drlrecpay.SelectedValue = "CR"
                'drlrecpay
            End If
        ElseIf drlrecpay.SelectedValue = "DR" And v = 1 And x = 1 Then
            If k > j Then
                txtamount.Text = k - j
            Else
                txtamount.Text = j - k
            End If
        ElseIf drlrecpay.SelectedValue = "DR" And v > 1 And x > 1 Then
            txtamount.Text = s - (q + r)
        End If




        'If drlrecpay.SelectedValue = "DR" And s = 0.0 Then
        '    txtamount.Text = 0
        'End If
        'If drlrecpay.SelectedValue = "CR" And q = 0.0 Then
        '    txtamount.Text = 0
        'End If


        Dim cmd1 As New SqlCommand("select convert (int,sum(dramt)) from journal where serno=" & txtjeno.Text & "", con)
        Dim b As Integer
        If IsDBNull(cmd1.ExecuteScalar()) Then
            b = 0
        Else
            b = cmd1.ExecuteScalar()
        End If
        Dim cmd2 As New SqlCommand("select convert (int,sum(cramt)) from journal where serno=" & txtjeno.Text & "", con)
        Dim c As Integer
        If IsDBNull(cmd2.ExecuteScalar()) Then
            c = 0
        Else
            c = cmd2.ExecuteScalar()
        End If

        If b <> 0 And c <> 0 And b > c Then
            drlrecpay.SelectedValue = "CR"
            drlrecpay.Enabled = False


        End If
        con.Close()
        If b <> 0 And c <> 0 And b < c Then
            drlrecpay.SelectedValue = "DR"
            drlrecpay.Enabled = False

        End If
        If b = c Then
            txtjeno.Enabled = False
            txtDate.Enabled = False
            txtacname.Enabled = False
            drlrecpay.Enabled = False
            txtamount.Enabled = False

            MsgBox("Entry has been tally", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Tally")
            Label9.Visible = True
            saveconfirm.Visible = True
            txtnarration.Visible = True


        End If
        'Bindgrid()
        txtacname.Text = ""
        txtnarration.Text = ""
        txtacname.Focus()
        If txtacname.Enabled = False Then
            txtnarration.Focus()
        End If
        hfmaster.Value = 1
        hfupdate.Value = 0

    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then
            hfupdate.Value = 1
            hfserno.Value = ""
            drlrecpay.Enabled = True
            Dim srno As Integer = e.CommandArgument
            'HiddenField1.Value = srno
            con.Open()
            Dim cmd1 As New SqlCommand("select journal.tserno,rp,casbank,journal.acccd,acname,cramt,dramt from journal left join acmast on journal.acccd=acmast.acccd where journal.tserno=" + srno.ToString() + " and serno=" & txtjeno.Text & " ", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)


            'code for drlitmcd to fetch value
            hfserno.Value = custTable.Rows(0).Item("tserno")
            hfacccd.Value = custTable.Rows(0).Item("acccd")
            drlrecpay.SelectedValue = custTable.Rows(0).Item("rp")
            txtacname.Text = custTable.Rows(0).Item("acname")
            If IsDBNull(custTable.Rows(0).Item("cramt")) Then
                txtamount.Text = custTable.Rows(0).Item("dramt")
            End If
            If IsDBNull(custTable.Rows(0).Item("dramt")) Then
                txtamount.Text = custTable.Rows(0).Item("cramt")
            End If
            Dim i As Integer
            Dim f As New SqlCommand("select count(dramt)from journal where serno=" & txtjeno.Text & "", con)
            i = f.ExecuteScalar()

            Dim g As Integer
            Dim h As New SqlCommand("select count(cramt)from journal where serno=" & txtjeno.Text & "", con)
            g = h.ExecuteScalar()

            con.Close()
            txtacname.Enabled = True
            txtamount.Enabled = True
            'drlrecpay.Enabled = True
            If drlrecpay.SelectedValue = "DR" And i > 1 And g = 1 Then
                drlrecpay.Enabled = False
            ElseIf drlrecpay.SelectedValue = "CR" And g > 1 And i = 1 Then
                drlrecpay.Enabled = False
            Else
                drlrecpay.Enabled = True
            End If

        End If
        If e.CommandName = "d" Then
            con.Open()
            Dim srno As Integer = e.CommandArgument
            Dim cmd1 As New SqlCommand("delete from journal where tserno=" + srno.ToString() + " and serno=" & txtjeno.Text & "", con)
            cmd1.ExecuteNonQuery()
            MsgBox("Entry Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
            txtacname.Enabled = True
            txtamount.Enabled = True
            drlrecpay.Enabled = True
            Dim r As Integer
            Dim cmd As New SqlCommand("select COUNT(tserno) from journal where serno=" & txtjeno.Text & "", con)
            r = cmd.ExecuteScalar()
            If r = 0 Then
                Dim cmd2 As New SqlCommand("delete from journalmast where serno=" & txtjeno.Text & "", con)
                cmd2.ExecuteNonQuery()
                MsgBox("Last Entry Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")

                Response.Redirect("Journal.aspx")
            End If
            con.Close()

            Bindgrid()


        End If


    End Sub
    Public Sub InsertInLedger(ByVal accd1 As String, ByVal accd2 As String, ByVal Amt As Decimal, ByVal sign12 As String)
        con.Open()
        query1 = "ledger_Insert"
        Dim com As New SqlCommand(query1, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtjeno.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", "JE") '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@tdate", txtDate.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", "")
        com.Parameters.AddWithValue("@acccd1", accd1)
        com.Parameters.AddWithValue("@sign12", sign12)
        com.Parameters.AddWithValue("@acccd2", accd2) '' this is to be dynamic from code table
        com.Parameters.AddWithValue("@amount", Amt)
        com.Parameters.AddWithValue("@chqno", "")
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", "JE-" & txtjeno.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
   
    Protected Sub saveconfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveconfirm.Click
        con.Open()
        Dim m As New SqlCommand("update journalmast set date='" & txtDate.SelectedDate & "',narr='" & txtnarration.Text & "' where serno=" & txtjeno.Text & "", con)
        m.ExecuteNonQuery()
        con.Close()

        con.Open()
        Dim cmd As New SqlCommand("select top 1 * from(select top 1 acccd,cramt amt,rp from journal where serno=" & txtjeno.Text & " and rp='CR' order by cramt desc) a union select * from ( select top 1 acccd,dramt,rp from journal where serno=" & txtjeno.Text & " and rp='DR' order by dramt desc)b  order by amt desc", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        con.Close()
        Dim MaxAmtAcccd As String = dt.Rows(0)(0)
        Dim MaxAmtSign12 As String = dt.Rows(0)(2)


        For Each row As GridViewRow In GridView1.Rows

            Dim accd As String = DirectCast(row.Cells(1).FindControl("hf_accCode"), HiddenField).Value
            Dim CurrentSign12 As String = row.Cells(2).Text

            If MaxAmtSign12 = CurrentSign12 Then
                Continue For
            Else
                InsertInLedger(MaxAmtAcccd, accd, IIf(CurrentSign12 = "DR", row.Cells(3).Text, row.Cells(4).Text), MaxAmtSign12)
                InsertInLedger(accd, MaxAmtAcccd, IIf(CurrentSign12 = "DR", row.Cells(3).Text, row.Cells(4).Text), CurrentSign12)
            End If

        Next


        MsgBox("updated", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "updated")
        Response.Redirect("Journal.aspx")
    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Bindgrid()
    End Sub

    Protected Sub txtamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtamount.TextChanged
       
    End Sub
End Class
