﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Drawing
Imports System.Web.UI
Imports System.Web.UI.WebControls


Partial Class acmast
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Dim a As Integer
    Dim b As String

    Protected Sub BindGrid()
        Dim filter As String = ""

        If txt_searchaccode.Text <> "" Then
            filter = "and acccd like '%" & txt_searchaccode.Text & "%'"
        End If

        If txt_searchacname.Text <> "" Then
            filter = filter & "and acname like '" & txt_searchacname.Text & "%'"
        End If

        If txt_searchplace.Text <> "" Then
            filter = filter & "and place.place like '" & txt_searchplace.Text & "%'"
        End If

        If DropDownList1.SelectedIndex <> 0 Then
            filter = filter & "and grtype.grtype like '" & DropDownList1.SelectedItem.Text & "'"
        End If


        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If
        con.Open()
        Dim cmd As New SqlCommand("select acmast.acccd,grtype.grtype,upper(acmast.acname)acname,upper(mjmast.mjname)mjname,upper(place.place)place from acmast left join grtype on acmast.grcd=grtype.grcd left join mjmast on acmast.mjcd=mjmast.mjcd left join place on acmast.srplace=place.serno " & filter & " ORDER BY acccd DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView2.DataSource = dt
        GridView2.DataBind()
        con.Close()
    End Sub

    Protected Sub Btn_Add_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_Add.Click
        HiddenField1.Value = "0"
        ClearConrols()
        drlmjcd.Items.Insert(0, "Select")
        Txtuserid.Text = Session("userid")
        Txtcompid.Text = Session("compid")
        Txtacname.Text = ""
        Txtlf.Text = ""
        Txtcellno1.Text = ""
        Txtcellno2.Text = ""
        con.Open()
        Dim cmd As New SqlCommand("select max(acccd) from acmast", con)

        Dim obj As Object = cmd.ExecuteScalar()
        If obj Is Nothing Or obj Is DBNull.Value Then
            a = 0
            a = a + 1
            If a <= 9 Then
                b = "000000" + a.ToString()
            ElseIf a > 9 And a <= 99 Then
                b = "00000" + a.ToString()
            ElseIf a > 99 And a <= 999 Then
                b = "0000" + a.ToString()
            ElseIf a > 999 And a <= 9999 Then
                b = "000" + a.ToString()
            ElseIf a > 9999 And a <= 99999 Then
                b = "00" + a.ToString()
            ElseIf a > 99999 And a <= 999999 Then
                b = "0" + a.ToString()
            End If
            Session("acno") = a
            Txtacccd.Text = b.ToString()
        Else
            a = Convert.ToInt32(obj)
            a = a + 1
            If a <= 9 Then
                b = "000000" + a.ToString()
            ElseIf a > 9 And a <= 99 Then
                b = "00000" + a.ToString()
            ElseIf a > 99 And a <= 999 Then
                b = "0000" + a.ToString()
            ElseIf a > 999 And a <= 9999 Then
                b = "000" + a.ToString()
            ElseIf a > 9999 And a <= 99999 Then
                b = "00" + a.ToString()
            ElseIf a > 99999 And a <= 999999 Then
                b = "0" + a.ToString()
            End If
            Session("acno") = a
            Txtacccd.Text = b.ToString()
        End If

        'bindgrouptype()

        'bindmajorcode()

        'drlgrcd.Focus()
        'drlgrcd.Items.Clear()
        'Dim cmd1 As New SqlCommand("select grcd,grtype from grtype", con)
        'Dim dr As SqlDataReader = cmd1.ExecuteReader()
        'While dr.Read()
        '    Dim item As String = dr.GetSqlString(0).Value
        '    Dim item1 As String = dr.GetSqlString(1).Value

        '    drlgrcd.Items.Add(New ListItem(item + "-" + item1))
        'End While
        'dr.Close()
        'drlgrcd.Items.Insert(0, "Select")

        'drlmjcd.Items.Clear()
        'Dim cmd2 As New SqlCommand("select mjcd,mjname from mjmast", con)
        'Dim dr1 As SqlDataReader = cmd2.ExecuteReader()
        'While dr1.Read()
        '    Dim item As String = dr1.GetSqlString(0).Value
        '    Dim item1 As String = dr1.GetSqlString(1).Value

        '    drlmjcd.Items.Add(New ListItem(item + "-" + item1))
        'End While
        'dr1.Close()
        'drlmjcd.Items.Insert(0, "Select")

        con.Close()
        Me.ModalPopupExtender1.Show()

    End Sub

    Protected Sub drlgrcd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlgrcd.SelectedIndexChanged
        If drlgrcd.SelectedItem.Text = "Select" Then
            drlmjcd.Enabled = False
        Else


            con.Open()

            Dim cmd As New SqlCommand("Select substring('" + drlgrcd.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlgrcd.SelectedItem.Text.Trim() + "')-1)", con)
            Dim da1 As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()
            da1.Fill(ds)

            Dim custTable1 As DataTable = ds.Tables(0)
            Session("grcd") = custTable1.Rows(0).Item(0)
            Dim a = cmd.ExecuteScalar()
            Dim cmd2 As New SqlCommand("select mjmast.mjcd, mjmast.mjcd+' - '+mjmast.mjname mjname from grtype right join mjmast on mjmast.grcd=grtype.grcd where grtype.grcd = '" & a & "'", con)
            'Dim cmd2 As New SqlCommand("select grtype.grcd+'-'+grtype.grtype com from mjmast left join grtype on mjmast.grcd=grtype.grcd where mjmast.mjcd = '" & a & "'", con)
            Dim da2 As New SqlDataAdapter(cmd2)
            Dim ds1 As New DataSet()
            da2.Fill(ds1)
            Dim custtable2 As DataTable = ds1.Tables(0)
            'Session("mjcd") = custtable2.Rows(0).Item(0)
            'Dim b = cmd2.ExecuteScalar()
            'drlgrcd.Items.Clear()
            drlmjcd.DataSource = custtable2
            drlmjcd.DataTextField = "mjname"
            drlmjcd.DataValueField = "mjcd"
            drlmjcd.DataBind()
            drlmjcd.Items.Insert(0, "Select")
            con.Close()
        End If
        Me.ModalPopupExtender1.Show()

    End Sub
    'Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
    '    con.Open()
    '    Dim cmd As New SqlCommand("Select substring('" + drlgrcd.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlgrcd.SelectedItem.Text.Trim() + "')-1)", con)
    '    Dim da1 As New SqlDataAdapter(cmd)
    '    Dim ds As New DataSet()
    '    da1.Fill(ds)
    '    Dim custTable1 As DataTable = ds.Tables(0)
    '    Session("grcd") = custTable1.Rows(0).Item(0)

    '    con.Close()
    'End Sub
    'Protected Sub drlmjcd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drlmjcd.SelectedIndexChanged
    '    con.Open()
    '    Dim cmd As New SqlCommand("Select substring('" + drlmjcd.SelectedItem.Text.Trim() + "',1,charindex('-','" + drlmjcd.SelectedItem.Text.Trim() + "')-1)", con)
    '    Dim da1 As New SqlDataAdapter(cmd)
    '    Dim ds As New DataSet()
    '    da1.Fill(ds)
    '    Dim custTable1 As DataTable = ds.Tables(0)
    '    Session("mjcd") = custTable1.Rows(0).Item(0)


    '    Dim a = cmd.ExecuteScalar()
    '    Dim cmd2 As New SqlCommand("select grtype.grcd from mjmast left join grtype on mjmast.grcd=grtype.grcd where mjmast.mjcd = '" & a & "'", con)
    '    'Dim cmd2 As New SqlCommand("select grtype.grcd+'-'+grtype.grtype com from mjmast left join grtype on mjmast.grcd=grtype.grcd where mjmast.mjcd = '" & a & "'", con)
    '    Dim b = cmd2.ExecuteScalar()
    '    'drlgrcd.Items.Clear()
    '    drlgrcd.SelectedValue = b
    '    con.Close()
    '    Me.ModalPopupExtender1.Show()

    'End Sub

    Protected Sub Btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsave.Click
        con.Open()

        Dim lfvalue As String = Txtlf.Text
        If String.IsNullOrEmpty(Txtacname.Text.Trim()) Then
            MsgBox("Please Enter Account Name", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            Txtacname.Focus()
            'ElseIf String.IsNullOrEmpty(Txtcompid.Text.Trim() Or Txtuserid.Text.Trim()) Then
            'MsgBox("Your Session Has Been Expired,You have to Login Again", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            ' Response.Redirect("login.aspx")
        ElseIf drlgrcd.SelectedIndex = 0 Then
            MsgBox("Please select Code", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            drlgrcd.Focus()
            Exit Sub
        ElseIf drlmjcd.SelectedIndex = 0 Then
            MsgBox("Please select Code", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical, "Save Error")
            drlmjcd.Focus()
            Exit Sub

        Else
        End If
        If Txtlf.Text = "" Then
            lfvalue = 0
        End If
        If Txtcellno1.Text = "" Then
            Txtcellno1.Text = 0
        End If
        If Txtcellno2.Text = "" Then
            Txtcellno2.Text = 0
        End If
        If txtplace.Text <> "" Then

            If hfplaceval.Value = "" Or hfplace.Value <> txtplace.Text Then
                MsgBox("Place Not Found", MsgBoxStyle.OkOnly + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Place")
                Exit Sub
            End If


        End If
        Dim d As String
        Dim f As Integer
        d = "select count(acname) from acmast where acname like '" & Txtacname.Text.Trim() & "'"
        Dim cmd3 As New SqlCommand(d, con)
        f = cmd3.ExecuteScalar()
        If f = 0 Then

            Dim s As String
            s = "insert into acmast(acccd,compid,userid,acno,grcd,acname,srplace,mjcd,lf,cellno1,cellno2) values('" & Txtacccd.Text.Trim() & "'," & Txtcompid.Text.Trim() & ",'" & Txtuserid.Text.Trim() & "'," & Session("acno") & ",'" & drlgrcd.SelectedValue & "','" & Txtacname.Text.Trim() & "'," & IIf(hfplaceval.Value = "" Or txtplace.Text = "", "NULL", hfplaceval.Value) & ",'" & drlmjcd.SelectedValue & "'," & lfvalue & "," & Txtcellno1.Text & "," & Txtcellno2.Text & ")"
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()
        ElseIf HFUpdate.Value = "1" Then
            Dim s As String
            s = "update acmast set compid='" & Txtcompid.Text.Trim() & "',userid='" & Txtuserid.Text.Trim() & "',acno=" & Session("acno") & ",grcd='" & drlgrcd.SelectedValue & "',acname='" & Txtacname.Text.Trim() & "',srplace=" & IIf(hfplaceval.Value = "" Or txtplace.Text = "", "NULL", hfplaceval.Value) & ",mjcd='" & drlmjcd.SelectedValue & "',lf=" & lfvalue & ",cellno1=" & Txtcellno1.Text & ",cellno2=" & Txtcellno2.Text & "where acccd=" & Txtacccd.Text.Trim() & ""
            Dim cmd As New SqlCommand(s, con)
            cmd.ExecuteNonQuery()

        Else
        MsgBox("Account Already Exist", MsgBoxStyle.OkOnly + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Duplicate")

        Exit Sub
        End If

        ClearConrols()





        MsgBox("Your Record Has Been Saved", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information, "Record Save")
        Dim cmd1 As New SqlCommand("select max(acno) from acmast", con)

        Dim obj As Object = cmd1.ExecuteScalar()
        a = Convert.ToInt32(obj)
        a = a + 1
        If a <= 9 Then
            b = "000000" + a.ToString()
        ElseIf a > 9 And a <= 99 Then
            b = "00000" + a.ToString()
        ElseIf a > 99 And a <= 999 Then
            b = "0000" + a.ToString()
        ElseIf a > 999 And a <= 9999 Then
            b = "000" + a.ToString()
        ElseIf a > 9999 And a <= 99999 Then
            b = "00" + a.ToString()
        ElseIf a > 99999 And a <= 999999 Then
            b = "0" + a.ToString()
        End If
        Session("acno") = a
        Txtacccd.Text = b.ToString()
        drlgrcd.Focus()
        drlgrcd.SelectedIndex = 0
        Txtacname.Text = ""
        txtplace.Text = 0
        'drlmjcd.SelectedIndex = 0
        Txtlf.Text = ""
        Txtcellno1.Text = ""
        Txtcellno2.Text = ""
        con.Close()
        BindGrid()


    End Sub
    Public Sub ClearConrols()
        Txtacname.Focus()
        Txtacccd.Text = ""
        Txtacname.Text = ""
        Txtlf.Text = ""
        txtplace.Text = ""
        Txtcellno1.Text = ""
        Txtcellno2.Text = ""
        'drlmjcd.Items.Insert(0, "Select")
        drlmjcd.Items.Clear()
        drlgrcd.SelectedIndex = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not Page.IsPostBack Then
                HiddenField1.Value = "0"
                bindgrouptype()
                'bindmajorcode()
                bindgrouptypesearch()
                ClearConrols()
                BindGrid()
            End If

        End If
    End Sub

    Public Sub bindgrouptype()

        Dim cmd1 As New SqlCommand("select grcd,grcd+'-'+grtype com from grtype", con)

        Dim da As New SqlDataAdapter(cmd1)

        Dim dt As New DataTable()
        da.Fill(dt)
        drlgrcd.DataSource = dt
        drlgrcd.DataTextField = "com"
        drlgrcd.DataValueField = "grcd"
        drlgrcd.DataBind()
        drlgrcd.Items.Insert(0, "Select")

    End Sub
    Public Sub bindgrouptypesearch()
        con.Open()
        Dim cmd1 As New SqlCommand("select grtype from grtype", con)

        Dim da As New SqlDataAdapter(cmd1)

        Dim dt As New DataTable()
        da.Fill(dt)
        DropDownList1.DataSource = dt
        DropDownList1.DataTextField = "grtype"
        DropDownList1.DataValueField = "grtype"
        'DropDownList1.SelectedIndex = "grtype"
        DropDownList1.DataBind()
        DropDownList1.Items.Insert(0, "Select")
        con.Close()
    End Sub
    'Public Sub bindmajorcode()

    '    Dim cmd2 As New SqlCommand("select mjcd,mjcd+'-'+mjname com from mjmast", con)
    '    Dim da = New SqlDataAdapter(cmd2)
    '    Dim dt = New DataTable()
    '    da.Fill(dt)
    '    drlmjcd.DataSource = dt
    '    drlmjcd.DataTextField = "com"
    '    drlmjcd.DataValueField = "mjcd"
    '    drlmjcd.DataBind()
    '    drlmjcd.Items.Insert(0, "Select")



    'End Sub

    'Protected Sub Btn_Add_click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click

    '    Me.ModalPopupExtender1.Show()
    'End Sub

    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView2.RowCommand
        If e.CommandName = "e" Then
            Txtacccd.Text = ""
            hfplaceval.Value = ""
            txtplace.Text = ""
            Dim srno As Integer = e.CommandArgument
            HiddenField1.Value = srno
            HFUpdate.Value = "1"
            con.Open()
            Dim cmd1 As New SqlCommand("select acmast.acno, acmast.compid, acmast.userid,grtype.grcd,acmast.lf,acmast.cellno1, acmast.cellno2, mjmast.mjcd,acmast.acccd,grtype.grtype,upper(acmast.acname)acname,upper(mjmast.mjname)mjname,place.serno, upper(place.place)place from acmast left join grtype on acmast.grcd=grtype.grcd left join mjmast on acmast.mjcd=mjmast.mjcd left join place on acmast.srplace=place.serno where acccd =" + srno.ToString() + "", con)
            Dim da As New SqlDataAdapter(cmd1)
            Dim ds1 As New DataSet()
            da.Fill(ds1)
            Dim custTable As DataTable = ds1.Tables(0)
            Session("acno") = custTable.Rows(0).Item("acno")
            Txtacccd.Text = custTable.Rows(0).Item("acccd")
            Txtcompid.Text = custTable.Rows(0).Item("compid")
            Txtuserid.Text = custTable.Rows(0).Item("userid")
            drlgrcd.SelectedValue = custTable.Rows(0).Item("grcd")
            Txtacname.Text = custTable.Rows(0).Item("acname")
            If (Not DBNull.Value.Equals((custTable.Rows(0).Item("serno")))) Then
                hfplaceval.Value = custTable.Rows(0).Item("serno")
                hfplace.Value = custTable.Rows(0).Item("place")
                txtplace.Text = custTable.Rows(0).Item("place")
            End If
            drlmjcd.SelectedValue = custTable.Rows(0).Item("mjcd")
            Txtlf.Text = custTable.Rows(0).Item("lf")
            Txtcellno1.Text = custTable.Rows(0).Item("cellno1")
            Txtcellno2.Text = custTable.Rows(0).Item("cellno2")
            con.Close()
            Me.ModalPopupExtender1.Show()
        End If
        If e.CommandName = "d" Then
            con.Open()
            Dim srno As Integer = e.CommandArgument

            Dim cmd5 As New SqlCommand("select COUNT(ratemast.acccd) from ratemast left join acmast ON ratemast.acccd = acmast.acccd where ratemast.acccd=" + srno.ToString() + "", con)
            Dim i As Integer
            i = Convert.ToInt16(cmd5.ExecuteScalar())

            If i = 0 Then
                Dim cmd1 As New SqlCommand("Delete from acmast where acccd=" + srno.ToString() + "", con)
                cmd1.ExecuteNonQuery()
                con.Close()
                MsgBox("Account Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")

                BindGrid()
            Else
                MsgBox("You must First Delete Rate Fixed with This Account.", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal, "Delete Error")
            End If
        End If
    End Sub

    Protected Sub Btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_search.Click
        BindGrid()
        ClearConrols()

    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView2.PageIndexChanging
        GridView2.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


End Class
