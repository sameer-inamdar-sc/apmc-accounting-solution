﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChallanRegister.aspx.vb"
    Inherits="ChallanRegister" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <html>
    <head>
        <title>Challan Register</title>
    </head>
    <body>
        <br />
        <table width="100%">
            <tr>
                <td colspan="5" align="left">
                    <asp:Label ID="Label2" runat="server" Text="Challan Register" Font-Bold="True" Font-Size="Large"
                        ForeColor="#6600CC"></asp:Label>
                </td>
                <td align="right" colspan="5">
                    <asp:LinkButton ID="Btn_Print" runat="server" Text="Print" 
                        ValidationGroup="show" />
                </td>
            </tr>
        </table>
        <hr />
        <table class="style1">
            <tr>
                <td align="right" class="style11">
                    <asp:Label ID="Label3" runat="server" Text="Challan No."></asp:Label>
                </td>
                <td align="left" class="style11">
                    <asp:TextBox ID="txtSearchChallanNo" runat="server"></asp:TextBox>
                </td>
                <td align="right" class="style12">
                    <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
                </td>
                <td align="left" class="style12">
                    <asp:TextBox ID="txtSearchAcName" runat="server" Height="16px"></asp:TextBox>
                </td>
                <td align="right" class="style10">
                    <asp:Label ID="Label5" runat="server" Text="From Date"></asp:Label>
                </td>
                <td align="left" class="style10">
                    <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
                        DisplayType="TextBox" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtSearchFromDate" ErrorMessage="*" ValidationGroup="show"></asp:RequiredFieldValidator>
                </td>
                <td align="right" class="style10">
                    <asp:Label ID="Label6" runat="server" Text="To Date"></asp:Label>
                </td>
                <td align="left" class="style10">
                    <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
                        DisplayType="TextBox" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtSearchToDate" ErrorMessage="*" ValidationGroup="show"></asp:RequiredFieldValidator>
                </td>
                <td align="center" class="style12">
                    <asp:Button ID="btnReport" runat="server" Text="Generate Report" 
                        ValidationGroup="show" />
                </td>
            </tr>
        </table>
        <div>
            <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
            <br />
            <br />
        </div>
    </body>
    </html>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1
        {
            width: 1300px;
        }
        .style10
        {
            width: 216px;
        }
        .style11
        {
            width: 259px;
        }
        .style12
        {
            width: 260px;
        }
    </style>
</asp:Content>
