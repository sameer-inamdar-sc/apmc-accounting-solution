﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="mjmast.aspx.vb" Inherits="mjmast" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <link href="Scripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#TextBox1").datepicker();
        });
    </script>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        .style13
        {
            color: #FF3300;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td colspan="2">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#6600CC"
                    Text="Major Code Master"></asp:Label>
            </td>
            <td colspan="3">
                <asp:HiddenField ID="hfItemId" runat="server" />
            </td>
            <td colspan="2">
                <asp:HiddenField ID="hfItemname" runat="server" />
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" EnableTheming="True" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_searchcode" runat="server" Text="Code"></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txt_SearchCode" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="lbl_searchname" runat="server" Text="Name"></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txt_SearchDescription" runat="server"></asp:TextBox>
            </td>
            <td align="right" colspan="2">
                &nbsp;
            </td>
            <td align="left">
                &nbsp;
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="left">
                &nbsp;
            </td>
            <td align="left">
                <asp:Button ID="btn_search" runat="server" Text="Search" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="12">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="12">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PagerStyle-HorizontalAlign="Right" Width="100%" AutoGenerateColumns="False"
                    DataKeyNames="mjcd" HorizontalAlign="Center" EmptyDataText="Data not found !!!"
                    EmptyDataRowStyle-HorizontalAlign="Center" PageSize="15" HeaderStyle-BackColor="Black"
                    HeaderStyle-ForeColor="White">
                    <Columns>
                        <asp:BoundField DataField="mjcd" HeaderText="Major Code" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="mjname" HeaderText="Major Name" />
                        <asp:BoundField DataField="type" HeaderText="Group Type" />
                        <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                    CommandArgument='<%#Eval("mjcd")%>' />
                                <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                    OnClientClick="return confirm('Are you sure want to Delete?');" CommandArgument='<%#Eval("mjcd")%>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BorderStyle="None" HorizontalAlign="Center" />
                    <EmptyDataRowStyle BorderStyle="None" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:ToolkitScriptManager ID="toolscriptmanager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:Button ID="jhol" runat="server" Style="display: none;" />
    <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="jhol"
        PopupControlID="Panel2" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="Panel2" runat="server" BackColor="White" ForeColor="Black" Font-Bold="true"
        Font-Size="X-Large" Font-Names="Book Antiqua" BorderColor="White" BorderWidth="6px"
        Height="200px" Width="450px" BorderStyle="Outset">
        <div id="dialog" title="Add Major Code" class="dialog" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller">
            <table align="center" width="450px">
                <tr>
                    <td align="center" colspan="4">
                        <asp:Label ID="lbl_popuphead" runat="server" Font-Bold="True" Font-Italic="True"
                            Font-Names="Bookman Old Style" Font-Overline="True" Font-Size="X-Large" Font-Underline="True"
                            Text="Major Code Details"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="75" align="left">
                        <asp:Label ID="lbl_code" runat="server" Text="Code"></asp:Label>
                    </td>
                    <td width="50" align="left" colspan="2">
                        <asp:TextBox ID="txt_code" runat="server" Width="25px" MaxLength="2" Rows="1"></asp:TextBox>
                        &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_code"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td width="250">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="75" align="left">
                        <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label>
                    </td>
                    <td colspan="3" align="left">
                        <asp:TextBox ID="txt_description" runat="server" Width="300px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_description"
                            ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td width="75" align="left">
                        <asp:Label ID="Label5" runat="server" Text="Type"></asp:Label>
                    </td>
                    <td colspan="3" align="left">
                        <asp:DropDownList ID="drlgrcd" runat="server" Width="300px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drlgrcd"
                            ErrorMessage="*" ValidationGroup="Save" InitialValue="Select"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </td>
                    <td colspan="2">
                        <span class="style13">*</span>&nbsp;<span>All Fields are Mandetory&nbsp; </span>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="btn_save" runat="server" Text="Save" ValidationGroup="Save" Width="50px" />
                        &nbsp;<asp:Button ID="btn_Cancle" runat="server" Text="Cancle" Width="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
