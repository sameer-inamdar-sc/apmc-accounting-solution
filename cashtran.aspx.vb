﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Partial Class Cashtran
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            con.Open()
            Dim cmd As New SqlCommand("select max(cbno) from cbmast", con)
            Dim a As Integer
            Dim obj As Object = cmd.ExecuteScalar()
            If obj Is Nothing Or obj Is DBNull.Value Then
                a = 0
                a = a + 1
                Txt_CBNo.Text = a.ToString()
            Else
                a = Convert.ToInt32(obj)
                a = a + 1
                Txt_CBNo.Text = a.ToString()
            End If
            Txt_Date.SelectedDate = Date.Today

            Dim cmd1 As New SqlCommand("select max(sr) from cbtran where cbno=" & Txt_CBNo.Text & "", con)
            Dim b As Integer
            Dim obj1 As Object = cmd1.ExecuteScalar()
            If obj1 Is Nothing Or obj1 Is DBNull.Value Then
                b = 0
                b = b + 1
                hf_serno.Value = b.ToString()
            Else
                b = Convert.ToInt32(obj1)
                b = b + 1
                hf_serno.Value = b.ToString()
            End If
            BindGrid()

            con.Close()

            Dim sr As String = CType(Page.Request.QueryString("id"), String)
            If Not sr = String.Empty Then
                'con.Open()
                'Dim cmd4 As New SqlCommand("select cbno,date from cbmast where cbno=" & sr, con)
                'Dim da1 As New SqlDataAdapter(cmd4)
                'Dim ds1 As New DataSet()
                'da1.Fill(ds1)
                'Dim custTable As DataTable = ds1.Tables(0)
                ''code for drlitmcd to fetch value
                'Txt_CBNo.Text = custTable.Rows(0).Item(0)
                'Dim thisDate1 As Date = custTable.Rows(0).Item(1)
                'Dim culture1 As New CultureInfo("pt-BR")
                'Txt_Date.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)
                ''txt_date.SelectedDate = thisDate1.ToString("MM/dd/yyyy", culture1)


                'Txt_CBNo.Enabled = False
                'Txt_Date.Enabled = False



                'con.Close()

                'Txt_CBNo.Text = sr

                ''GridView1.DataSource = dt
                ''GridView1.DataBind()
                ''hfupdate.Value = 1
                ''txtbankname.Text = "cash in Hand"
            End If

        End If
        'If GridView1 Is Nothing Then
        '    Btn_ConfirmSave.Visible = True
        'Else
        '    Btn_ConfirmSave.Visible = False
        'End If
    End Sub
   
    Public Sub Bindgrid()

        Dim com As New SqlCommand("select cbno, date, doccode, (trsnrp)TransactionType, modecasbank from cbmast", con)
        Dim da As New SqlDataAdapter(com)
        Dim dt As New DataTable()
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()

    End Sub
    Protected Sub GridView2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "e" Then



        ElseIf e.CommandName = "d" Then

        End If


    End Sub

    Protected Sub btn_back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_back.Click
        Response.Redirect("Cashbook.aspx")
    End Sub

    Protected Sub btn_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Save.Click
        If ViewState("dt") IsNot Nothing Then

            Dim dt As New DataTable()
            dt = ViewState("dt")
            If hf_savemode.Value = 0 Then
                dt.Rows.Add(hf_serno.Value, Txt_CBNo.Text, Txt_Account.Text, Txt_AcCode.Text, Drl_PayRec.SelectedValue, Drl_CashBank.SelectedValue, Txt_Date.SelectedDate, Txt_Amount.Text, Txt_Bname.Text, Txt_BCode.Text, Txt_BBranch.Text, Txt_CBNo.Text, Txt_ChqDate.SelectedDate, Txt_Narration.Text)
            End If
            'Dim dcColumn As DataColumn
            'dcColumn = New DataColumn()
            'dcColumn.DataType = Type.GetType("System.String")
            BindGrid()
        End If
    End Sub

    Protected Sub Btn_ConfirmSave_Click(sender As Object, e As System.EventArgs) Handles Btn_ConfirmSave.Click

    End Sub

    Protected Sub GridView2_RowCommand1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        con.Open()
        If e.CommandName = "e" Then
            Dim serno As String = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("~/Cashbook.aspx?id" & serno)

        End If

        If e.CommandName = "d" Then

            Dim serno As String = Convert.ToInt32(e.CommandArgument)
            Dim intResponse As Integer
            intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            If intResponse = MsgBoxResult.Yes Then
                Dim cmd As New SqlCommand("")
            End If
        End If
        con.Close()

    End Sub
End Class
