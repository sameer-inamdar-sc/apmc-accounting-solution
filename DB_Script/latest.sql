USE [komal]
GO
/****** Object:  ForeignKey [FK__acmast__compid__43D61337]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[acmast] DROP CONSTRAINT [FK__acmast__compid__43D61337]
GO
/****** Object:  ForeignKey [FK__code__compid__5812E165]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[code] DROP CONSTRAINT [FK__code__compid__5812E165]
GO
/****** Object:  ForeignKey [FK__compmast__coid__0EA330E9]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[compmast] DROP CONSTRAINT [FK__compmast__coid__0EA330E9]
GO
/****** Object:  ForeignKey [FK__compmast__yearid__76B698BF]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[compmast] DROP CONSTRAINT [FK__compmast__yearid__76B698BF]
GO
/****** Object:  ForeignKey [FK__item__compid__02FC7413]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[item] DROP CONSTRAINT [FK__item__compid__02FC7413]
GO
/****** Object:  ForeignKey [FK__journal__compid__1BC821DD]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[journal] DROP CONSTRAINT [FK__journal__compid__1BC821DD]
GO
/****** Object:  ForeignKey [FK__openbal__compid__5BE37249]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[openbal] DROP CONSTRAINT [FK__openbal__compid__5BE37249]
GO
/****** Object:  ForeignKey [FK__pmast__compid__2CF2ADDF]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[pmast] DROP CONSTRAINT [FK__pmast__compid__2CF2ADDF]
GO
/****** Object:  ForeignKey [FK__purtran__compid__245D67DE]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[purtran.old] DROP CONSTRAINT [FK__purtran__compid__245D67DE]
GO
/****** Object:  ForeignKey [FK__ratetran__compid__625A9A57]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[ratetran] DROP CONSTRAINT [FK__ratetran__compid__625A9A57]
GO
/****** Object:  ForeignKey [FK__smast__compid__5AB9788F]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[smast] DROP CONSTRAINT [FK__smast__compid__5AB9788F]
GO
/****** Object:  ForeignKey [FK__stock__compid__2739D489]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[stock] DROP CONSTRAINT [FK__stock__compid__2739D489]
GO
/****** Object:  ForeignKey [FK__payment__compid__4B7734FF]    Script Date: 01/11/2015 19:06:02 ******/
ALTER TABLE [dbo].[TransactionMaster] DROP CONSTRAINT [FK__payment__compid__4B7734FF]
GO
/****** Object:  StoredProcedure [dbo].[abcd]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[abcd]
GO
/****** Object:  StoredProcedure [dbo].[billnowise]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[billnowise]
GO
/****** Object:  StoredProcedure [dbo].[CashBankReport]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[CashBankReport]
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[ledgerreport]
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport_old]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[ledgerreport_old]
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport_temp]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[ledgerreport_temp]
GO
/****** Object:  StoredProcedure [dbo].[PandLreport]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PandLreport]
GO
/****** Object:  StoredProcedure [dbo].[PR_acmast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_acmast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_code_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_code_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_code_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_code_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_item_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_journal_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_journal_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journal_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journal_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journal_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journal_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_pmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_pmast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran.old_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_openbal_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratetran_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[InsertJournal]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[InsertJournal]
GO
/****** Object:  StoredProcedure [dbo].[PR_stock_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_stock_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_stock_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_stock_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_TransactionMaster_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_TransactionMaster_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[TransactionMaster_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[TransactionMaster_Insert]
GO
/****** Object:  StoredProcedure [dbo].[TransactionMaster_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[TransactionMaster_UpdateByPK]
GO
/****** Object:  Table [dbo].[smast]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[smast] DROP CONSTRAINT [FK__smast__compid__5AB9788F]
GO
DROP TABLE [dbo].[smast]
GO
/****** Object:  Table [dbo].[TransactionMaster]    Script Date: 01/11/2015 19:06:02 ******/
ALTER TABLE [dbo].[TransactionMaster] DROP CONSTRAINT [FK__payment__compid__4B7734FF]
GO
DROP TABLE [dbo].[TransactionMaster]
GO
/****** Object:  Table [dbo].[stock]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[stock] DROP CONSTRAINT [FK__stock__compid__2739D489]
GO
DROP TABLE [dbo].[stock]
GO
/****** Object:  Table [dbo].[pmast]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[pmast] DROP CONSTRAINT [FK__pmast__compid__2CF2ADDF]
GO
DROP TABLE [dbo].[pmast]
GO
/****** Object:  Table [dbo].[purtran.old]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[purtran.old] DROP CONSTRAINT [FK__purtran__compid__245D67DE]
GO
DROP TABLE [dbo].[purtran.old]
GO
/****** Object:  Table [dbo].[ratetran]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[ratetran] DROP CONSTRAINT [FK__ratetran__compid__625A9A57]
GO
DROP TABLE [dbo].[ratetran]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_compmast_UpdateByPK]
GO
/****** Object:  Table [dbo].[openbal]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[openbal] DROP CONSTRAINT [FK__openbal__compid__5BE37249]
GO
DROP TABLE [dbo].[openbal]
GO
/****** Object:  Table [dbo].[item]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[item] DROP CONSTRAINT [FK__item__compid__02FC7413]
GO
DROP TABLE [dbo].[item]
GO
/****** Object:  Table [dbo].[code]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[code] DROP CONSTRAINT [FK__code__compid__5812E165]
GO
DROP TABLE [dbo].[code]
GO
/****** Object:  StoredProcedure [dbo].[GetCompInfo]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[GetCompInfo]
GO
/****** Object:  Table [dbo].[journal]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[journal] DROP CONSTRAINT [FK__journal__compid__1BC821DD]
GO
DROP TABLE [dbo].[journal]
GO
/****** Object:  Table [dbo].[acmast]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[acmast] DROP CONSTRAINT [FK__acmast__compid__43D61337]
GO
DROP TABLE [dbo].[acmast]
GO
/****** Object:  StoredProcedure [dbo].[AddNewFinancialYear]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[AddNewFinancialYear]
GO
/****** Object:  StoredProcedure [dbo].[cbmast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[cbmast_UpdateByPK]
GO
/****** Object:  Table [dbo].[compmast]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[compmast] DROP CONSTRAINT [FK__compmast__coid__0EA330E9]
GO
ALTER TABLE [dbo].[compmast] DROP CONSTRAINT [FK__compmast__yearid__76B698BF]
GO
DROP TABLE [dbo].[compmast]
GO
/****** Object:  StoredProcedure [dbo].[PR_crdr_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_crdr_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_crdr_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_crdr_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_createuser_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_finyear_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_grtype_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_cbmast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_cbmast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_cbmast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_cbmast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_cbtran_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_cbtran_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_challanMast_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_ChallanTran_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_journalmast_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ledrep_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ledrep_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_ledrep_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ledrep_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_mjmast_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_itmexp_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[PR_company_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_TransactionDetails_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_TransactionDetails_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[insupcbmast]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[insupcbmast]
GO
/****** Object:  StoredProcedure [dbo].[ledger_Delete]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[ledger_Delete]
GO
/****** Object:  StoredProcedure [dbo].[ledger_Insert]    Script Date: 01/11/2015 19:06:03 ******/
DROP PROCEDURE [dbo].[ledger_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_saltran_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_place_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_purtran_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_DeleteByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_SelectByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_SelectViewByPK]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[PR_ratemast_UpdateByPK]
GO
/****** Object:  StoredProcedure [dbo].[sp_journalmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[sp_journalmast_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_journalmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[sp_journalmast_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetails_Insert]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[TransactionDetails_Insert]
GO
/****** Object:  StoredProcedure [dbo].[temptable]    Script Date: 01/11/2015 19:06:04 ******/
DROP PROCEDURE [dbo].[temptable]
GO
/****** Object:  Table [dbo].[TransactionDetails]    Script Date: 01/11/2015 19:06:02 ******/
DROP TABLE [dbo].[TransactionDetails]
GO
/****** Object:  Table [dbo].[purtran]    Script Date: 01/11/2015 19:06:01 ******/
DROP TABLE [dbo].[purtran]
GO
/****** Object:  Table [dbo].[saltran]    Script Date: 01/11/2015 19:06:01 ******/
DROP TABLE [dbo].[saltran]
GO
/****** Object:  Table [dbo].[ratemast]    Script Date: 01/11/2015 19:06:01 ******/
DROP TABLE [dbo].[ratemast]
GO
/****** Object:  Table [dbo].[crdr]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[crdr]
GO
/****** Object:  Table [dbo].[createuser]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[createuser]
GO
/****** Object:  Table [dbo].[finyear]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[finyear]
GO
/****** Object:  Table [dbo].[cbtran]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[cbtran]
GO
/****** Object:  Table [dbo].[challanMast]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[challanMast]
GO
/****** Object:  Table [dbo].[challanTran]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[challanTran]
GO
/****** Object:  Table [dbo].[cbmast]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[cbmast]
GO
/****** Object:  Table [dbo].[journalmast]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[journalmast]
GO
/****** Object:  Table [dbo].[ledger]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[ledger]
GO
/****** Object:  Table [dbo].[grtype]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[grtype]
GO
/****** Object:  Table [dbo].[company]    Script Date: 01/11/2015 19:05:59 ******/
DROP TABLE [dbo].[company]
GO
/****** Object:  Table [dbo].[itmexp]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[itmexp]
GO
/****** Object:  Table [dbo].[ledrep]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[ledrep]
GO
/****** Object:  Table [dbo].[mapuser]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[mapuser]
GO
/****** Object:  Table [dbo].[mjmast]    Script Date: 01/11/2015 19:06:00 ******/
DROP TABLE [dbo].[mjmast]
GO
/****** Object:  Table [dbo].[place]    Script Date: 01/11/2015 19:06:01 ******/
DROP TABLE [dbo].[place]
GO
/****** Object:  Table [dbo].[place]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[place](
	[serno] [int] NOT NULL,
	[place] [nvarchar](20) NOT NULL,
	[atpost] [nvarchar](20) NULL,
	[tal] [nvarchar](20) NULL,
	[dist] [nvarchar](20) NULL,
 CONSTRAINT [PK__place__797309D9] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (6, N'Kopar Khairane', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (7, N'Nerul', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (8, N'PANVEL', N'Panvel', N'Panvel', N'RAIGAD')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (10, N'SHIVAJINAGAR', N'SHIVAJINAGAR', N'KHANADALA', N'SATARA')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (11, N'Turbhe', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (12, N'VASHI', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (14, N'Airoli', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (15, N'Belapur', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (16, N'Rabale', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (17, N'Jui Nagar', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (18, N'Khargar', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (19, N'Khandeshwar', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (20, N'Mansarovar', N'Navi Mumbai', N'Thane', N'Thane')
INSERT [dbo].[place] ([serno], [place], [atpost], [tal], [dist]) VALUES (21, N'CBD', N'Navi Mumbai', N'Thane', N'Thane')
/****** Object:  Table [dbo].[mjmast]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mjmast](
	[mjcd] [nvarchar](2) NOT NULL,
	[mjname] [nvarchar](50) NULL,
	[grcd] [nvarchar](2) NULL,
 CONSTRAINT [PK__mjmast__73BA3083] PRIMARY KEY CLUSTERED 
(
	[mjcd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A1', N'FIXED ASSETS A/C.', N'GN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A2', N'INVESTMENTS A/C.', N'GN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A3', N'Cash & Bank A/C', N'CB')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A4', N'SUNDRY CREDITORS A/C', N'DR')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A5', N'PROFIT & LOSS A/C.', N'GN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A6', N'SUNDRY SALLARY A/C', N'GN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'A7', N'SUSPENSE A/C', N'GN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'EE', N'EXPENCES A/C.', N'IE')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'II', N'INCOME ACCOUNTS', N'IE')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'L1', N'PARTNERS CAPITAL A/C.', N'PC')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'L2', N'LOAN & ADVANSES', N'LN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'L3', N'SECURED & UNSECURED LOAN A/C.', N'LN')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'L4', N'SUNDRAY PURCHASERS A/C.', N'PR')
INSERT [dbo].[mjmast] ([mjcd], [mjname], [grcd]) VALUES (N'L5', N'SUNDRAY TRANSPORTERS A/C.', N'TR')
/****** Object:  Table [dbo].[mapuser]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mapuser](
	[mapUserId] [bigint] NOT NULL,
	[userid] [bigint] NOT NULL,
	[compid] [bigint] NOT NULL,
	[role] [nvarchar](50) NULL,
 CONSTRAINT [PK_mapuser_1] PRIMARY KEY CLUSTERED 
(
	[mapUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ledrep]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ledrep](
	[cdate] [datetime] NULL,
	[crefno] [nvarchar](15) NULL,
	[camount] [decimal](18, 0) NULL,
	[ddate] [datetime] NULL,
	[drefno] [nvarchar](15) NULL,
	[damount] [decimal](18, 0) NULL,
	[acccd] [nvarchar](7) NULL,
	[acname] [nvarchar](50) NULL,
	[fdate] [datetime] NULL,
	[tdate] [datetime] NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ledrep] ([cdate], [crefno], [camount], [ddate], [drefno], [damount], [acccd], [acname], [fdate], [tdate]) VALUES (NULL, N'', CAST(0 AS Decimal(18, 0)), CAST(0x0000A25300000000 AS DateTime), N'To Bal.B/F.', CAST(5001 AS Decimal(18, 0)), N'0000078', N'KOMAL CHAUDHARY', CAST(0x0000A25300000000 AS DateTime), CAST(0x0000A3C000000000 AS DateTime))
INSERT [dbo].[ledrep] ([cdate], [crefno], [camount], [ddate], [drefno], [damount], [acccd], [acname], [fdate], [tdate]) VALUES (CAST(0x0000A3C000000000 AS DateTime), N'To Bal.C/F.', CAST(5001 AS Decimal(18, 0)), NULL, N'', CAST(0 AS Decimal(18, 0)), N'0000078', N'KOMAL CHAUDHARY', CAST(0x0000A25300000000 AS DateTime), CAST(0x0000A3C000000000 AS DateTime))
/****** Object:  Table [dbo].[itmexp]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[itmexp](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[date] [datetime] NULL,
	[itmcd] [int] NOT NULL,
	[comm] [decimal](13, 2) NULL,
	[hamali] [decimal](13, 2) NULL,
	[levy] [decimal](13, 2) NULL,
	[tolai] [decimal](13, 2) NULL,
	[vatav] [decimal](13, 2) NULL,
	[APMC] [decimal](13, 2) NULL,
	[maplevy] [decimal](13, 4) NULL,
 CONSTRAINT [PK__itmexp__40058253] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (2, 1, N'abc', CAST(0x0000A2FF00000000 AS DateTime), 1, CAST(1.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(3.00 AS Decimal(13, 2)), CAST(9.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(7.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (3, 1, N'abc', CAST(0x0000A2FF00000000 AS DateTime), 4, CAST(2.00 AS Decimal(13, 2)), CAST(3.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(6.00 AS Decimal(13, 2)), CAST(7.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (4, 1, N'abc', CAST(0x0000A2FF00000000 AS DateTime), 3, CAST(1.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(3.00 AS Decimal(13, 2)), CAST(7.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(6.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (8, 1, N'abc', CAST(0x0000A30600000000 AS DateTime), 10, CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (9, 1, N'abc', CAST(0x0000A2C800000000 AS DateTime), 1, CAST(1.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(1.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (10, 1, N'abc', CAST(0x0000A31A00000000 AS DateTime), 10, CAST(16.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(16.0000 AS Decimal(13, 4)))
INSERT [dbo].[itmexp] ([serno], [compid], [userid], [date], [itmcd], [comm], [hamali], [levy], [tolai], [vatav], [APMC], [maplevy]) VALUES (11, 1, N'abc', CAST(0x0000A32200000000 AS DateTime), 17, CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.0000 AS Decimal(13, 4)))
/****** Object:  Table [dbo].[company]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company](
	[coid] [int] NOT NULL,
	[cocd] [nvarchar](5) NULL,
	[coname] [nvarchar](50) NULL,
	[compadd] [nvarchar](50) NULL,
	[compphone] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[compwebsite] [nvarchar](50) NULL,
	[compfax] [nvarchar](50) NULL,
 CONSTRAINT [PK__company__09DE7BCC] PRIMARY KEY CLUSTERED 
(
	[coid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (1, N'JKT', N'JAY KISAN TRADERS', N'test', N'test', N'test', N'test', N'723782')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (2, N'TT', N'TEJAL TRADER', N'koper khairne', N'564654', N'tejal@gmail', N'co.in', N'544564')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (3, N'ST', N'SAVI TRADERS', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (4, N'DPK', N'DEEPAK', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (5, N'SA', N'SAAD_ENTERPRICES', N'MUMBRA_KAUSA', N'9876543210', N'SAADYER@GMAIL.COM', N'SAAD.co.in', N'4565454')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (6, N'A', N'HG', N'JHG', N'JHG', N'JHG', N'JH', N'GYJH')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (7, N'HG', N'JGJH', N'GHJ', N'GJH', N'G', N'JHG', N'JH')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (8, N'HJGJH', N'FDFGDFGDGFDFGD', N'DFGDFGD', N'FGD', N'FG', N'DFGD', N'FD')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (9, N'ZZZ', N'SAMEER', N'KK', N'29038293', N'SAM@LIVE.COM', N'WWW.SAM.COM', N'98756445494')
INSERT [dbo].[company] ([coid], [cocd], [coname], [compadd], [compphone], [email], [compwebsite], [compfax]) VALUES (10, N'AAA', N'JHJKH', N'JKHJKH', N'5946', N'JHH#DFF', N'MLM', N'564')
/****** Object:  Table [dbo].[grtype]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grtype](
	[grcd] [nvarchar](2) NOT NULL,
	[grtype] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[grcd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'CB', N'CASH & BANK A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'CR', N'SUNDRY CUSTOMER A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'GN', N'SUNDRY GENRAL A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'IE', N'INCOME & EXPENCES A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'LN', N'LOAN A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'PC', N'PARTNERS CAPITAL A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'PR', N'SUNDRY PURCHASER A/C.')
INSERT [dbo].[grtype] ([grcd], [grtype]) VALUES (N'TR', N'TRANSPORT')
/****** Object:  Table [dbo].[ledger]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ledger](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[tdate] [datetime] NULL,
	[rp] [nvarchar](50) NULL,
	[acccd1] [nvarchar](7) NULL,
	[sign12] [nvarchar](2) NULL,
	[acccd2] [nvarchar](7) NULL,
	[amount] [decimal](13, 2) NULL,
	[chqno] [int] NULL,
	[narrcd] [nvarchar](50) NULL,
	[lotno] [nvarchar](10) NULL,
	[posting] [nvarchar](10) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'PU', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(1028.00 AS Decimal(13, 2)), 0, N'', N'PU-1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'PU', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(1028.00 AS Decimal(13, 2)), 0, N'', N'PU-1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000083', N'CR', N'0000062', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000083', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(4566546.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(4566546.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(546456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(546456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000083', N'CR', N'0000062', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000083', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(456456.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000083', N'CR', N'0000062', CAST(465465.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000083', CAST(465465.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000083', N'CR', N'0000062', CAST(45564.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000083', CAST(45564.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(146546.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3EC00000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(146546.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3F300000000 AS DateTime), N'', N'0000083', N'CR', N'0000062', CAST(123.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3F300000000 AS DateTime), N'', N'0000062', N'DR', N'0000083', CAST(123.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3F300000000 AS DateTime), N'', N'0000077', N'CR', N'0000062', CAST(321.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'CB', CAST(0x0000A3F300000000 AS DateTime), N'', N'0000062', N'DR', N'0000077', CAST(321.00 AS Decimal(13, 2)), 0, N'', N'1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'PU', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000095', N'CR', N'0000062', CAST(62.00 AS Decimal(13, 2)), 0, N'', N'PU-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'PU', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000062', N'DR', N'0000095', CAST(62.00 AS Decimal(13, 2)), 0, N'', N'PU-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'SA', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000083', N'DR', N'0000063', CAST(232.00 AS Decimal(13, 2)), 0, N'', N'SA-1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (1, 6, N'sagar', N'SA', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000063', N'CR', N'0000083', CAST(232.00 AS Decimal(13, 2)), 0, N'', N'SA-1', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'SA', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000094', N'DR', N'0000063', CAST(110.00 AS Decimal(13, 2)), 0, N'', N'SA-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'SA', CAST(0x0000A3FA00000000 AS DateTime), N'', N'0000063', N'CR', N'0000094', CAST(110.00 AS Decimal(13, 2)), 0, N'', N'SA-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000003', N'DR', N'0000077', CAST(500.00 AS Decimal(13, 2)), 0, N'', N'JE-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (2, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000077', N'CR', N'0000003', CAST(500.00 AS Decimal(13, 2)), 0, N'', N'JE-2', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (4, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000003', N'CR', N'0000025', CAST(200.00 AS Decimal(13, 2)), 0, N'', N'JE-4', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (4, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000025', N'DR', N'0000003', CAST(200.00 AS Decimal(13, 2)), 0, N'', N'JE-4', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (5, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000003', N'DR', N'0000025', CAST(400.00 AS Decimal(13, 2)), 0, N'', N'JE-5', N'')
INSERT [dbo].[ledger] ([serno], [compid], [userid], [doccd], [tdate], [rp], [acccd1], [sign12], [acccd2], [amount], [chqno], [narrcd], [lotno], [posting]) VALUES (5, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'', N'0000025', N'CR', N'0000003', CAST(400.00 AS Decimal(13, 2)), 0, N'', N'JE-5', N'')
/****** Object:  Table [dbo].[journalmast]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[journalmast](
	[serno] [int] NOT NULL,
	[date] [datetime] NULL,
	[narr] [nvarchar](200) NULL,
 CONSTRAINT [PK_journalmast] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[journalmast] ([serno], [date], [narr]) VALUES (1, CAST(0x0000A3E500000000 AS DateTime), N'test')
INSERT [dbo].[journalmast] ([serno], [date], [narr]) VALUES (2, CAST(0x0000A41D00000000 AS DateTime), N'sdf')
INSERT [dbo].[journalmast] ([serno], [date], [narr]) VALUES (3, CAST(0x0000A41D00000000 AS DateTime), N'')
INSERT [dbo].[journalmast] ([serno], [date], [narr]) VALUES (4, CAST(0x0000A41D00000000 AS DateTime), N'ss')
INSERT [dbo].[journalmast] ([serno], [date], [narr]) VALUES (5, CAST(0x0000A41D00000000 AS DateTime), N'ss')
/****** Object:  Table [dbo].[cbmast]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbmast](
	[cbno] [int] NOT NULL,
	[date] [datetime] NULL,
	[doccode] [nchar](2) NULL,
	[trsnrp] [bit] NULL,
	[modecasbank] [bit] NULL,
	[BankCode] [nvarchar](7) NULL,
	[Bankname] [nvarchar](50) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=Receipt,1=payment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cbmast', @level2type=N'COLUMN',@level2name=N'trsnrp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=Bank,1=Cash' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'cbmast', @level2type=N'COLUMN',@level2name=N'modecasbank'
GO
INSERT [dbo].[cbmast] ([cbno], [date], [doccode], [trsnrp], [modecasbank], [BankCode], [Bankname]) VALUES (1, CAST(0x0000A3F300000000 AS DateTime), N'CB', 1, 1, N'0000097', N'Cash In Hand')
INSERT [dbo].[cbmast] ([cbno], [date], [doccode], [trsnrp], [modecasbank], [BankCode], [Bankname]) VALUES (2, CAST(0x0000A3FA00000000 AS DateTime), N'CB', 1, 1, N'0000097', N'Cash In Hand')
INSERT [dbo].[cbmast] ([cbno], [date], [doccode], [trsnrp], [modecasbank], [BankCode], [Bankname]) VALUES (3, CAST(0x0000A3FA00000000 AS DateTime), N'CB', 1, 1, N'0000097', N'Cash In Hand')
/****** Object:  Table [dbo].[challanTran]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[challanTran](
	[TransID] [int] IDENTITY(1,1) NOT NULL,
	[serno] [int] NOT NULL,
	[tserno] [int] NOT NULL,
	[itmcd] [int] NULL,
	[qty] [decimal](5, 0) NULL,
	[weight] [decimal](9, 3) NULL,
	[rate] [decimal](9, 2) NULL,
	[amount] [decimal](13, 2) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nchar](10) NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [nchar](10) NULL,
 CONSTRAINT [PK_ChallanTran] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[challanTran] ON
INSERT [dbo].[challanTran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, 3, 1, 3, CAST(5 AS Decimal(5, 0)), CAST(55.000 AS Decimal(9, 3)), CAST(50.00 AS Decimal(9, 2)), CAST(229.17 AS Decimal(13, 2)), NULL, NULL, NULL, NULL)
INSERT [dbo].[challanTran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [CreatedOn], [CreatedBy], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, 4, 1, 17, CAST(45 AS Decimal(5, 0)), CAST(54.000 AS Decimal(9, 3)), CAST(20.00 AS Decimal(9, 2)), CAST(108.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[challanTran] OFF
/****** Object:  Table [dbo].[challanMast]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[challanMast](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[challanNo] [int] NULL,
	[date] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
	[lotno] [nvarchar](10) NULL,
	[tamount] [decimal](13, 2) NULL,
	[grossamt] [decimal](13, 2) NULL,
	[roundoff] [decimal](13, 2) NULL,
	[netamt] [decimal](13, 2) NULL,
	[CreatedOn] [datetime] NULL,
	[YearId] [nchar](20) NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[challanMast] ([serno], [compid], [userid], [doccd], [challanNo], [date], [acccd], [lotno], [tamount], [grossamt], [roundoff], [netamt], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 6, N'sagar', N'CH', 1, CAST(0x0000A3E400000000 AS DateTime), N'0000083', N'CH-1', CAST(216.67 AS Decimal(13, 2)), CAST(216.67 AS Decimal(13, 2)), CAST(0.33 AS Decimal(13, 2)), CAST(217.00 AS Decimal(13, 2)), CAST(0x0000A3E400F79734 AS DateTime), N'2014-2015           ', CAST(0x0000A3E400F79D10 AS DateTime), N'sagar     ')
INSERT [dbo].[challanMast] ([serno], [compid], [userid], [doccd], [challanNo], [date], [acccd], [lotno], [tamount], [grossamt], [roundoff], [netamt], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, 6, N'sagar', N'CH', 2, CAST(0x0000A3E400000000 AS DateTime), N'0000083', N'CH-2', CAST(0.00 AS Decimal(13, 2)), NULL, NULL, NULL, CAST(0x0000A3E400F7C290 AS DateTime), N'2014-2015           ', NULL, NULL)
INSERT [dbo].[challanMast] ([serno], [compid], [userid], [doccd], [challanNo], [date], [acccd], [lotno], [tamount], [grossamt], [roundoff], [netamt], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (3, 6, N'sagar', N'CH', 3, CAST(0x0000A3FA00000000 AS DateTime), N'0000083', N'CH-3', CAST(229.17 AS Decimal(13, 2)), CAST(229.17 AS Decimal(13, 2)), CAST(0.83 AS Decimal(13, 2)), CAST(230.00 AS Decimal(13, 2)), CAST(0x0000A3FA00E962E0 AS DateTime), N'2014-2015           ', CAST(0x0000A3FA00E96664 AS DateTime), N'sagar     ')
INSERT [dbo].[challanMast] ([serno], [compid], [userid], [doccd], [challanNo], [date], [acccd], [lotno], [tamount], [grossamt], [roundoff], [netamt], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (4, 6, N'sagar', N'CH', 4, CAST(0x0000A3FA00000000 AS DateTime), N'0000094', N'CH-4', CAST(108.00 AS Decimal(13, 2)), CAST(108.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(108.00 AS Decimal(13, 2)), CAST(0x0000A3FA00E9802C AS DateTime), N'2014-2015           ', CAST(0x0000A3FA00E98284 AS DateTime), N'sagar     ')
/****** Object:  Table [dbo].[cbtran]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cbtran](
	[sr] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[cbno] [int] NULL,
	[acccd] [nvarchar](7) NULL,
	[branch] [nvarchar](10) NULL,
	[amount] [decimal](13, 2) NULL,
	[chqno] [int] NULL,
	[narr] [nvarchar](50) NULL,
	[chqdate] [datetime] NULL,
 CONSTRAINT [PK__cbtran__151B244E] PRIMARY KEY CLUSTERED 
(
	[sr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[cbtran] ([sr], [compid], [userid], [cbno], [acccd], [branch], [amount], [chqno], [narr], [chqdate]) VALUES (1, 6, N'sagar', 1, N'0000083', N'', CAST(123.00 AS Decimal(13, 2)), 0, N'jhg', NULL)
INSERT [dbo].[cbtran] ([sr], [compid], [userid], [cbno], [acccd], [branch], [amount], [chqno], [narr], [chqdate]) VALUES (2, 6, N'sagar', 1, N'0000077', N'', CAST(321.00 AS Decimal(13, 2)), 0, N'uhujkjl', NULL)
INSERT [dbo].[cbtran] ([sr], [compid], [userid], [cbno], [acccd], [branch], [amount], [chqno], [narr], [chqdate]) VALUES (3, 6, N'sagar', 3, N'0000026', N'', CAST(222.00 AS Decimal(13, 2)), 0, N'paid', NULL)
/****** Object:  Table [dbo].[finyear]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[finyear](
	[yearid] [int] IDENTITY(1,1) NOT NULL,
	[fdate] [datetime] NULL,
	[tdate] [datetime] NULL,
	[coyear] [nvarchar](9) NULL,
 CONSTRAINT [PK__finyear__1423F18D3BCADD1B] PRIMARY KEY CLUSTERED 
(
	[yearid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[finyear] ON
INSERT [dbo].[finyear] ([yearid], [fdate], [tdate], [coyear]) VALUES (1, CAST(0x00009EB800000000 AS DateTime), CAST(0x0000A02500000000 AS DateTime), N'2011-2012')
INSERT [dbo].[finyear] ([yearid], [fdate], [tdate], [coyear]) VALUES (2, CAST(0x0000A02600000000 AS DateTime), CAST(0x0000A19200000000 AS DateTime), N'2012-2013')
INSERT [dbo].[finyear] ([yearid], [fdate], [tdate], [coyear]) VALUES (3, CAST(0x0000A19300000000 AS DateTime), CAST(0x0000A2FF00000000 AS DateTime), N'2013-2014')
INSERT [dbo].[finyear] ([yearid], [fdate], [tdate], [coyear]) VALUES (10, CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46C00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[finyear] ([yearid], [fdate], [tdate], [coyear]) VALUES (13, CAST(0x0000A46D00000000 AS DateTime), CAST(0x0000A5DA00000000 AS DateTime), N'2015-2016')
SET IDENTITY_INSERT [dbo].[finyear] OFF
/****** Object:  Table [dbo].[createuser]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[createuser](
	[skey] [int] NOT NULL,
	[uname] [char](20) NULL,
	[sname] [char](20) NULL,
	[uid] [nvarchar](50) NULL,
	[pass] [nvarchar](20) NULL,
	[emailid] [nvarchar](50) NULL,
	[mob] [nvarchar](10) NULL,
	[authority] [nchar](20) NULL,
	[coid] [int] NULL,
 CONSTRAINT [PK__createuser__7F60ED59] PRIMARY KEY CLUSTERED 
(
	[skey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[createuser] ([skey], [uname], [sname], [uid], [pass], [emailid], [mob], [authority], [coid]) VALUES (2, N'abcd                ', N'dd                  ', N'abcd', N'123', N'uijhkjhkj', N'hhjkh', N'admin               ', 1)
INSERT [dbo].[createuser] ([skey], [uname], [sname], [uid], [pass], [emailid], [mob], [authority], [coid]) VALUES (7, N'sagar               ', N'abc                 ', N'sagar', N'123', N'sagar132@gmail.commmmm', N'454654645', N'SUPER-ADMIN         ', 2)
INSERT [dbo].[createuser] ([skey], [uname], [sname], [uid], [pass], [emailid], [mob], [authority], [coid]) VALUES (8, N'hjhh                ', N'hk                  ', N'www', N'123', N'1231', N'21333', N'admin               ', 5)
INSERT [dbo].[createuser] ([skey], [uname], [sname], [uid], [pass], [emailid], [mob], [authority], [coid]) VALUES (17, N'hjh                 ', N'jkkjhjkaaaa         ', N'5644657496', N'Exoj2B', N'h@ghyjh', N'hjjhlke', N'User                ', 2)
/****** Object:  Table [dbo].[crdr]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[crdr](
	[Initial] [nchar](10) NULL,
	[type] [nchar](10) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[crdr] ([Initial], [type]) VALUES (N'CR        ', N'Credit    ')
INSERT [dbo].[crdr] ([Initial], [type]) VALUES (N'DR        ', N'Debit     ')
/****** Object:  Table [dbo].[ratemast]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ratemast](
	[serno] [int] NOT NULL,
	[date] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
 CONSTRAINT [PK__ratemast__5E8A0973] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ratemast] ([serno], [date], [acccd]) VALUES (1, CAST(0x0000A38200000000 AS DateTime), N'0000083')
INSERT [dbo].[ratemast] ([serno], [date], [acccd]) VALUES (2, CAST(0x0000A38200000000 AS DateTime), N'0000094')
/****** Object:  Table [dbo].[saltran]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[saltran](
	[TransID] [int] IDENTITY(1,1) NOT NULL,
	[serno] [int] NOT NULL,
	[tserno] [int] NULL,
	[itmcd] [int] NULL,
	[qty] [decimal](5, 0) NULL,
	[weight] [decimal](9, 3) NULL,
	[rate] [decimal](9, 2) NULL,
	[amount] [decimal](13, 2) NULL,
	[lbharai] [decimal](13, 2) NULL,
	[lutrai] [decimal](13, 2) NULL,
	[lapmc] [decimal](13, 2) NULL,
	[lhamali] [decimal](13, 2) NULL,
	[ltolai] [decimal](13, 2) NULL,
 CONSTRAINT [PK_saltran] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[saltran] ON
INSERT [dbo].[saltran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai]) VALUES (1, 1, 1, 3, CAST(5 AS Decimal(5, 0)), CAST(55.000 AS Decimal(9, 3)), CAST(50.00 AS Decimal(9, 2)), CAST(229.17 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[saltran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai]) VALUES (2, 2, 1, 17, CAST(45 AS Decimal(5, 0)), CAST(54.000 AS Decimal(9, 3)), CAST(20.00 AS Decimal(9, 2)), CAST(108.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[saltran] OFF
/****** Object:  Table [dbo].[purtran]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[purtran](
	[TransID] [int] IDENTITY(1,1) NOT NULL,
	[serno] [int] NULL,
	[tserno] [int] NULL,
	[itmcd] [int] NULL,
	[qty] [decimal](13, 2) NULL,
	[weight] [decimal](13, 3) NULL,
	[rate] [decimal](13, 2) NULL,
	[amount] [decimal](13, 2) NULL,
	[lapmc] [decimal](13, 2) NULL,
	[lmaplevy] [decimal](13, 4) NULL,
	[lhamali] [decimal](13, 2) NULL,
 CONSTRAINT [PK_purtran_1] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[purtran] ON
INSERT [dbo].[purtran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [lapmc], [lmaplevy], [lhamali]) VALUES (1, 1, 1, 17, CAST(7.00 AS Decimal(13, 2)), CAST(74.000 AS Decimal(13, 3)), CAST(74.00 AS Decimal(13, 2)), CAST(547.60 AS Decimal(13, 2)), CAST(21.90 AS Decimal(13, 2)), CAST(296.0000 AS Decimal(13, 4)), CAST(28.00 AS Decimal(13, 2)))
INSERT [dbo].[purtran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [lapmc], [lmaplevy], [lhamali]) VALUES (2, 1, 2, 3, CAST(54.00 AS Decimal(13, 2)), CAST(4.000 AS Decimal(13, 3)), CAST(5.00 AS Decimal(13, 2)), CAST(1.67 AS Decimal(13, 2)), CAST(0.08 AS Decimal(13, 2)), CAST(24.0000 AS Decimal(13, 4)), CAST(108.00 AS Decimal(13, 2)))
INSERT [dbo].[purtran] ([TransID], [serno], [tserno], [itmcd], [qty], [weight], [rate], [amount], [lapmc], [lmaplevy], [lhamali]) VALUES (3, 2, 1, 17, CAST(4.00 AS Decimal(13, 2)), CAST(5.000 AS Decimal(13, 3)), CAST(50.00 AS Decimal(13, 2)), CAST(25.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(20.0000 AS Decimal(13, 4)), CAST(16.00 AS Decimal(13, 2)))
SET IDENTITY_INSERT [dbo].[purtran] OFF
/****** Object:  Table [dbo].[TransactionDetails]    Script Date: 01/11/2015 19:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionDetails](
	[serno] [int] NOT NULL,
	[TransactionMasterID] [int] NULL,
	[amount] [decimal](13, 2) NULL,
	[pmastid] [bigint] NOT NULL,
 CONSTRAINT [PK__paytran__DFD1E8D745DE573A] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[TransactionDetails] ([serno], [TransactionMasterID], [amount], [pmastid]) VALUES (1, 1, CAST(100.00 AS Decimal(13, 2)), 7)
INSERT [dbo].[TransactionDetails] ([serno], [TransactionMasterID], [amount], [pmastid]) VALUES (2, 2, CAST(50.00 AS Decimal(13, 2)), 7)
/****** Object:  StoredProcedure [dbo].[temptable]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[temptable]

AS

--if object_id('tempdb..#ledrep') is not null

--BEGIN


-- --drop table #ledrep
-- end

create table #ledrep
(
	sr int identity(1,1) ,
	name nvarchar(50),
	surname nvarchar(50),

)
declare @name as nvarchar(50)='Deepak'
declare @surname as nvarchar(50)='Patil'
begin

insert into #ledrep (name,surname)
select @name,@surname

END
------ Final select Satement
select * from #ledrep
GO
/****** Object:  StoredProcedure [dbo].[TransactionDetails_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_TransactionDetails_Insert] @serno = 1, @TransactionMasterID = 1, @InvoiceID = 1, @amount = 'amount'

CREATE PROCEDURE [dbo].[TransactionDetails_Insert]

		@serno               		int=0 OUTPUT,
		@TransactionMasterID 		int,

		@amount              		nchar (10),
		@pmastid					bigint

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[TransactionDetails]


INSERT [dbo].[TransactionDetails]
(
		[serno],
		[TransactionMasterID],

		[amount],
		[pmastid]
)
VALUES
(
		@serno,
		@TransactionMasterID,

		@amount,
		@pmastid
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[sp_journalmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	sp_journalmast_SelectAll
-- Author:	Mehdi Keramati
-- Create date:	10-05-2014 19:04:42
-- Description:	This stored procedure is intended for selecting all rows from journalmast table
-- ==========================================================================================
Create Procedure [dbo].[sp_journalmast_SelectAll]
As
Begin
	Select 
		[serno],
		[date],
		[narr]
	From journalmast
End
GO
/****** Object:  StoredProcedure [dbo].[sp_journalmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	sp_journalmast_Insert
-- Author:	Mehdi Keramati
-- Create date:	10-05-2014 19:04:42
-- Description:	This stored procedure is intended for inserting values to journalmast table
-- ==========================================================================================
Create Procedure [dbo].[sp_journalmast_Insert]
	@serno int,
	@date datetime,
	@narr nvarchar(200)
As
Begin
	Insert Into journalmast
		([serno],[date],[narr])
	Values
		(@serno,@date,@narr)

End
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_UpdateByPK] @serno = 1, @date = '2014-06-14', @acccd = 'acccd'

CREATE PROCEDURE [dbo].[PR_ratemast_UpdateByPK]

		@serno 		int,
		@date  		datetime,
		@acccd 		nvarchar (7)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[ratemast]
SET
		[date] = @date,
		[acccd] = @acccd
WHERE [dbo].[ratemast].[serno] = @serno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_SelectViewByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_ratemast_SelectViewByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratemast].[serno],
		[dbo].[ratemast].[date],
		[dbo].[ratemast].[acccd]
FROM  [dbo].[ratemast]

WHERE [dbo].[ratemast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_SelectByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_ratemast_SelectByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratemast].[serno],
		[dbo].[ratemast].[date],
		[dbo].[ratemast].[acccd]
FROM  [dbo].[ratemast]

WHERE [dbo].[ratemast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_SelectAll]

CREATE PROCEDURE [dbo].[PR_ratemast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratemast].[serno],
		[dbo].[ratemast].[date],
		[dbo].[ratemast].[acccd]
FROM  [dbo].[ratemast]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_Insert] @serno = 1, @date = '2014-06-14', @acccd = 'acccd'

CREATE PROCEDURE [dbo].[PR_ratemast_Insert]

		@serno 		int OUTPUT,
		@date  		datetime,
		@acccd 		nvarchar (7)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[ratemast]


INSERT [dbo].[ratemast]
(
		[serno],
		[date],
		[acccd]
)
VALUES
(
		@serno,
		@date,
		@acccd
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ratemast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratemast_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_ratemast_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[ratemast]
WHERE [dbo].[ratemast].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_UpdateByPK] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lapmc = 1, @lmaplevy = 1, @lhamali = 1

CREATE PROCEDURE [dbo].[PR_purtran_UpdateByPK]

		@TransID  		int,
		@serno    		int,
		@tserno   		int,
		@itmcd    		int,
		@qty      		decimal (13, 2),
		@weight   		decimal (13, 2),
		@rate     		decimal (13, 2),
		@amount   		decimal (13, 2),
		@lapmc    		decimal (13, 2),
		@lmaplevy 		decimal (13, 4),
		@lhamali  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[purtran]
SET
		[serno] = @serno,
		[tserno] = @tserno,
		[itmcd] = @itmcd,
		[qty] = @qty,
		[weight] = @weight,
		[rate] = @rate,
		[amount] = @amount,
		[lapmc] = @lapmc,
		[lmaplevy] = @lmaplevy,
		[lhamali] = @lhamali
WHERE [dbo].[purtran].[TransID] = @TransID

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_SelectViewByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_purtran_SelectViewByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran].[TransID],
		[dbo].[purtran].[serno],
		[dbo].[purtran].[tserno],
		[dbo].[purtran].[itmcd],
		[dbo].[purtran].[qty],
		[dbo].[purtran].[weight],
		[dbo].[purtran].[rate],
		[dbo].[purtran].[amount],
		[dbo].[purtran].[lapmc],
		[dbo].[purtran].[lmaplevy],
		[dbo].[purtran].[lhamali]
FROM  [dbo].[purtran]

WHERE [dbo].[purtran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_SelectByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_purtran_SelectByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran].[TransID],
		[dbo].[purtran].[serno],
		[dbo].[purtran].[tserno],
		[dbo].[purtran].[itmcd],
		[dbo].[purtran].[qty],
		[dbo].[purtran].[weight],
		[dbo].[purtran].[rate],
		[dbo].[purtran].[amount],
		[dbo].[purtran].[lapmc],
		[dbo].[purtran].[lmaplevy],
		[dbo].[purtran].[lhamali]
FROM  [dbo].[purtran]

WHERE [dbo].[purtran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_SelectAll]

CREATE PROCEDURE [dbo].[PR_purtran_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran].[TransID],
		[dbo].[purtran].[serno],
		[dbo].[purtran].[tserno],
		[dbo].[purtran].[itmcd],
		[dbo].[purtran].[qty],
		[dbo].[purtran].[weight],
		[dbo].[purtran].[rate],
		[dbo].[purtran].[amount],
		[dbo].[purtran].[lapmc],
		[dbo].[purtran].[lmaplevy],
		[dbo].[purtran].[lhamali]
FROM  [dbo].[purtran]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_Insert] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lapmc = 1, @lmaplevy = 1, @lhamali = 1

CREATE PROCEDURE [dbo].[PR_purtran_Insert]

		@TransID  		int OUTPUT,
		@serno    		int,
		@tserno   		int,
		@itmcd    		int,
		@qty      		decimal (13, 2),
		@weight   		decimal (13, 2),
		@rate     		decimal (13, 2),
		@amount   		decimal (13, 2),
		@lapmc    		decimal (13, 2),
		@lmaplevy 		decimal (13, 4),
		@lhamali  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[purtran]
(
		[serno],
		[tserno],
		[itmcd],
		[qty],
		[weight],
		[rate],
		[amount],
		[lapmc],
		[lmaplevy],
		[lhamali]
)
VALUES
(
		@serno,
		@tserno,
		@itmcd,
		@qty,
		@weight,
		@rate,
		@amount,
		@lapmc,
		@lmaplevy,
		@lhamali
)

SET @TransID = @@IDENTITY

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran_DeleteByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_purtran_DeleteByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[purtran]
WHERE [dbo].[purtran].[TransID] = @TransID


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_place_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_UpdateByPK] @serno = 1, @place = 'place', @atpost = 'atpost', @tal = 'tal', @dist = 'dist'

CREATE PROCEDURE [dbo].[PR_place_UpdateByPK]

		@serno  		int,
		@place  		nvarchar (20),
		@atpost 		nvarchar (20),
		@tal    		nvarchar (20),
		@dist   		nvarchar (20)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[place]
SET
		[place] = @place,
		[atpost] = @atpost,
		[tal] = @tal,
		[dist] = @dist
WHERE [dbo].[place].[serno] = @serno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_SelectViewByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_place_SelectViewByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[place].[serno],
		[dbo].[place].[place],
		[dbo].[place].[atpost],
		[dbo].[place].[tal],
		[dbo].[place].[dist]
FROM  [dbo].[place]

WHERE [dbo].[place].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_SelectByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_place_SelectByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[place].[serno],
		[dbo].[place].[place],
		[dbo].[place].[atpost],
		[dbo].[place].[tal],
		[dbo].[place].[dist]
FROM  [dbo].[place]

WHERE [dbo].[place].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_place_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_SelectAll]

CREATE PROCEDURE [dbo].[PR_place_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[place].[serno],
		[dbo].[place].[place],
		[dbo].[place].[atpost],
		[dbo].[place].[tal],
		[dbo].[place].[dist]
FROM  [dbo].[place]
GO
/****** Object:  StoredProcedure [dbo].[PR_place_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_Insert] @serno = 1, @place = 'place', @atpost = 'atpost', @tal = 'tal', @dist = 'dist'

CREATE PROCEDURE [dbo].[PR_place_Insert]

		@serno  		int OUTPUT,
		@place  		nvarchar (20),
		@atpost 		nvarchar (20),
		@tal    		nvarchar (20),
		@dist   		nvarchar (20)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[place]


INSERT [dbo].[place]
(
		[serno],
		[place],
		[atpost],
		[tal],
		[dist]
)
VALUES
(
		@serno,
		@place,
		@atpost,
		@tal,
		@dist
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_place_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_place_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_place_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[place]
WHERE [dbo].[place].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_UpdateByPK] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lbharai = 1, @lutrai = 1, @lapmc = 1, @lhamali = 1, @ltolai = 1

CREATE PROCEDURE [dbo].[PR_saltran_UpdateByPK]

		@TransID 		int,
		@serno   		int,
		@tserno  		int,
		@itmcd   		int,
		@qty     		decimal (5, 0),
		@weight  		decimal (9, 2),
		@rate    		decimal (9, 2),
		@amount  		decimal (13, 2),
		@lbharai 		decimal (13, 2),
		@lutrai  		decimal (13, 2),
		@lapmc   		decimal (13, 2),
		@lhamali 		decimal (13, 2),
		@ltolai  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[saltran]
SET
		[serno] = @serno,
		[tserno] = @tserno,
		[itmcd] = @itmcd,
		[qty] = @qty,
		[weight] = @weight,
		[rate] = @rate,
		[amount] = @amount,
		[lbharai] = @lbharai,
		[lutrai] = @lutrai,
		[lapmc] = @lapmc,
		[lhamali] = @lhamali,
		[ltolai] = @ltolai
WHERE [dbo].[saltran].[TransID] = @TransID

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_SelectViewByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_saltran_SelectViewByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[saltran].[TransID],
		[dbo].[saltran].[serno],
		[dbo].[saltran].[tserno],
		[dbo].[saltran].[itmcd],
		[dbo].[saltran].[qty],
		[dbo].[saltran].[weight],
		[dbo].[saltran].[rate],
		[dbo].[saltran].[amount],
		[dbo].[saltran].[lbharai],
		[dbo].[saltran].[lutrai],
		[dbo].[saltran].[lapmc],
		[dbo].[saltran].[lhamali],
		[dbo].[saltran].[ltolai]
FROM  [dbo].[saltran]

WHERE [dbo].[saltran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_SelectByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_saltran_SelectByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[saltran].[TransID],
		[dbo].[saltran].[serno],
		[dbo].[saltran].[tserno],
		[dbo].[saltran].[itmcd],
		[dbo].[saltran].[qty],
		[dbo].[saltran].[weight],
		[dbo].[saltran].[rate],
		[dbo].[saltran].[amount],
		[dbo].[saltran].[lbharai],
		[dbo].[saltran].[lutrai],
		[dbo].[saltran].[lapmc],
		[dbo].[saltran].[lhamali],
		[dbo].[saltran].[ltolai]
FROM  [dbo].[saltran]

WHERE [dbo].[saltran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_SelectAll]

CREATE PROCEDURE [dbo].[PR_saltran_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[saltran].[TransID],
		[dbo].[saltran].[serno],
		[dbo].[saltran].[tserno],
		[dbo].[saltran].[itmcd],
		[dbo].[saltran].[qty],
		[dbo].[saltran].[weight],
		[dbo].[saltran].[rate],
		[dbo].[saltran].[amount],
		[dbo].[saltran].[lbharai],
		[dbo].[saltran].[lutrai],
		[dbo].[saltran].[lapmc],
		[dbo].[saltran].[lhamali],
		[dbo].[saltran].[ltolai]
FROM  [dbo].[saltran]
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_Insert] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lbharai = 1, @lutrai = 1, @lapmc = 1, @lhamali = 1, @ltolai = 1

CREATE PROCEDURE [dbo].[PR_saltran_Insert]

		@TransID 		int OUTPUT,
		@serno   		int,
		@tserno  		int,
		@itmcd   		int,
		@qty     		decimal (5, 0),
		@weight  		decimal (9, 2),
		@rate    		decimal (9, 2),
		@amount  		decimal (13, 2),
		@lbharai 		decimal (13, 2),
		@lutrai  		decimal (13, 2),
		@lapmc   		decimal (13, 2),
		@lhamali 		decimal (13, 2),
		@ltolai  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[saltran]
(
		[serno],
		[tserno],
		[itmcd],
		[qty],
		[weight],
		[rate],
		[amount],
		[lbharai],
		[lutrai],
		[lapmc],
		[lhamali],
		[ltolai]
)
VALUES
(
		@serno,
		@tserno,
		@itmcd,
		@qty,
		@weight,
		@rate,
		@amount,
		@lbharai,
		@lutrai,
		@lapmc,
		@lhamali,
		@ltolai
)

SET @TransID = @@IDENTITY

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_saltran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_saltran_DeleteByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_saltran_DeleteByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[saltran]
WHERE [dbo].[saltran].[TransID] = @TransID


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[ledger_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ledger_Insert] @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @tdate = '2014-06-14', @rp = 'rp', @casbank = 'casbank', @acccd1 = 'acccd1', @sign12 = 'sign12', @acccd2 = 'acccd2', @amount = 1, @chqno = 1, @narrcd = 'narrcd', @lotno = 'lotno', @posting = 'posting'

CREATE PROCEDURE [dbo].[ledger_Insert]

		@serno   		int,
		@compid  		int,
		@userid  		nvarchar (20),
		@doccd   		nvarchar (2),
		@tdate   		datetime,
		@rp      		nvarchar (2),
		@acccd1  		nvarchar (7),
		@sign12  		nvarchar (2),
		@acccd2  		nvarchar (7),
		@amount  		decimal (13, 2),
		@chqno   		int,
		@narrcd  		nvarchar (3),
		@lotno   		nvarchar (10),
		@posting 		nvarchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[ledger]
(
		[serno],
		[compid],
		[userid],
		[doccd],
		[tdate],
		[rp],
		[acccd1],
		[sign12],
		[acccd2],
		[amount],
		[chqno],
		[narrcd],
		[lotno],
		[posting]
)
VALUES
(
		@serno,
		@compid,
		@userid,
		@doccd,
		@tdate,
		@rp,
		@acccd1,
		@sign12,
		@acccd2,
		@amount,
		@chqno,
		@narrcd,
		@lotno,
		@posting
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[ledger_Delete]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ledger_Insert] @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @tdate = '2014-06-14', @rp = 'rp', @casbank = 'casbank', @acccd1 = 'acccd1', @sign12 = 'sign12', @acccd2 = 'acccd2', @amount = 1, @chqno = 1, @narrcd = 'narrcd', @lotno = 'lotno', @posting = 'posting'

CREATE PROCEDURE [dbo].[ledger_Delete]

		@serno   		int,
		@compid  		int,
		@doccd   		nvarchar (2)
AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[ledger]
WHERE serno=@serno and compid=@compid and doccd=@doccd

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[insupcbmast]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insupcbmast] --100,'2012-10-10'
@cbno int,
@date datetime
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      SET NOCOUNT ON;
      IF EXISTS (SELECT cbno   FROM   cbmast    WHERE  cbno = @cbno)
        UPDATE cbmast
        set date = @date
        where cbno = @cbno
      ELSE
        INSERT INTO cbmast(cbno,  date)
        VALUES      (@cbno, @date)
  END
GO
/****** Object:  StoredProcedure [dbo].[PR_TransactionDetails_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_TransactionDetails_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_TransactionDetails_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[TransactionDetails]
WHERE [dbo].[TransactionDetails].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_company_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_UpdateByPK] @coid = 1, @cocd = 'cocd', @coname = 'coname'

CREATE PROCEDURE [dbo].[PR_company_UpdateByPK]

		@coid   		int,
		@cocd   		nvarchar (5),
		@coname 		nvarchar (50)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[company]
SET
		[cocd] = @cocd,
		[coname] = @coname
WHERE [dbo].[company].[coid] = @coid

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_SelectViewByPK] @coid = 1

CREATE PROCEDURE [dbo].[PR_company_SelectViewByPK]

		@coid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[company].[coid],
		[dbo].[company].[cocd],
		[dbo].[company].[coname]
FROM  [dbo].[company]

WHERE [dbo].[company].[coid] = @coid
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_SelectByPK] @coid = 1

CREATE PROCEDURE [dbo].[PR_company_SelectByPK]

		@coid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[company].[coid],
		[dbo].[company].[cocd],
		[dbo].[company].[coname]
FROM  [dbo].[company]

WHERE [dbo].[company].[coid] = @coid
GO
/****** Object:  StoredProcedure [dbo].[PR_company_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_SelectAll]

CREATE PROCEDURE [dbo].[PR_company_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[company].[coid],
		[dbo].[company].[cocd],
		[dbo].[company].[coname]
FROM  [dbo].[company]
GO
/****** Object:  StoredProcedure [dbo].[PR_company_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_Insert] @coid = 1, @cocd = 'cocd', @coname = 'coname'

CREATE PROCEDURE [dbo].[PR_company_Insert]

		@coid   		int OUTPUT,
		@cocd   		nvarchar (5),
		@coname 		nvarchar (50)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @coid = ISNULL(MAX([coid]),0) + 1 FROM [dbo].[company]


INSERT [dbo].[company]
(
		[coid],
		[cocd],
		[coname]
)
VALUES
(
		@coid,
		@cocd,
		@coname
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_company_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_company_DeleteByPK] @coid = 1

CREATE PROCEDURE [dbo].[PR_company_DeleteByPK]

		@coid 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[company]
WHERE [dbo].[company].[coid] = @coid


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_UpdateByPK] @serno = 1, @compid = 1, @userid = 'userid', @date = '2014-06-14', @itmcd = 1, @comm = 1, @hamali = 1, @levy = 1, @tolai = 1, @vatav = 1, @APMC = 1, @maplevy = 1

CREATE PROCEDURE [dbo].[PR_itmexp_UpdateByPK]

		@serno   		int,
		@compid  		int,
		@userid  		nvarchar (20),
		@date    		datetime,
		@itmcd   		int,
		@comm    		decimal (13, 2),
		@hamali  		decimal (13, 2),
		@levy    		decimal (13, 2),
		@tolai   		decimal (13, 2),
		@vatav   		decimal (13, 2),
		@APMC    		decimal (13, 2),
		@maplevy 		decimal (13, 4)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[itmexp]
SET
		[compid] = @compid,
		[userid] = @userid,
		[date] = @date,
		[itmcd] = @itmcd,
		[comm] = @comm,
		[hamali] = @hamali,
		[levy] = @levy,
		[tolai] = @tolai,
		[vatav] = @vatav,
		[APMC] = @APMC,
		[maplevy] = @maplevy
WHERE [dbo].[itmexp].[serno] = @serno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_SelectViewByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_itmexp_SelectViewByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[itmexp].[serno],
		[dbo].[itmexp].[compid],
		[dbo].[itmexp].[userid],
		[dbo].[itmexp].[date],
		[dbo].[itmexp].[itmcd],
		[dbo].[itmexp].[comm],
		[dbo].[itmexp].[hamali],
		[dbo].[itmexp].[levy],
		[dbo].[itmexp].[tolai],
		[dbo].[itmexp].[vatav],
		[dbo].[itmexp].[APMC],
		[dbo].[itmexp].[maplevy]
FROM  [dbo].[itmexp]

WHERE [dbo].[itmexp].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_SelectByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_itmexp_SelectByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[itmexp].[serno],
		[dbo].[itmexp].[compid],
		[dbo].[itmexp].[userid],
		[dbo].[itmexp].[date],
		[dbo].[itmexp].[itmcd],
		[dbo].[itmexp].[comm],
		[dbo].[itmexp].[hamali],
		[dbo].[itmexp].[levy],
		[dbo].[itmexp].[tolai],
		[dbo].[itmexp].[vatav],
		[dbo].[itmexp].[APMC],
		[dbo].[itmexp].[maplevy]
FROM  [dbo].[itmexp]

WHERE [dbo].[itmexp].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_SelectAll]

CREATE PROCEDURE [dbo].[PR_itmexp_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[itmexp].[serno],
		[dbo].[itmexp].[compid],
		[dbo].[itmexp].[userid],
		[dbo].[itmexp].[date],
		[dbo].[itmexp].[itmcd],
		[dbo].[itmexp].[comm],
		[dbo].[itmexp].[hamali],
		[dbo].[itmexp].[levy],
		[dbo].[itmexp].[tolai],
		[dbo].[itmexp].[vatav],
		[dbo].[itmexp].[APMC],
		[dbo].[itmexp].[maplevy]
FROM  [dbo].[itmexp]
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_Insert] @serno = 1, @compid = 1, @userid = 'userid', @date = '2014-06-14', @itmcd = 1, @comm = 1, @hamali = 1, @levy = 1, @tolai = 1, @vatav = 1, @APMC = 1, @maplevy = 1

CREATE PROCEDURE [dbo].[PR_itmexp_Insert]

		@serno   		int OUTPUT,
		@compid  		int,
		@userid  		nvarchar (20),
		@date    		datetime,
		@itmcd   		int,
		@comm    		decimal (13, 2),
		@hamali  		decimal (13, 2),
		@levy    		decimal (13, 2),
		@tolai   		decimal (13, 2),
		@vatav   		decimal (13, 2),
		@APMC    		decimal (13, 2),
		@maplevy 		decimal (13, 4)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[itmexp]


INSERT [dbo].[itmexp]
(
		[serno],
		[compid],
		[userid],
		[date],
		[itmcd],
		[comm],
		[hamali],
		[levy],
		[tolai],
		[vatav],
		[APMC],
		[maplevy]
)
VALUES
(
		@serno,
		@compid,
		@userid,
		@date,
		@itmcd,
		@comm,
		@hamali,
		@levy,
		@tolai,
		@vatav,
		@APMC,
		@maplevy
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_itmexp_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_itmexp_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_itmexp_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[itmexp]
WHERE [dbo].[itmexp].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_UpdateByPK] @mjcd = 'mjcd', @mjname = 'mjname', @grcd = 'grcd'

CREATE PROCEDURE [dbo].[PR_mjmast_UpdateByPK]

		@mjcd   		nvarchar (2),
		@mjname 		nvarchar (50),
		@grcd   		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[mjmast]
SET
		[mjname] = @mjname,
		[grcd] = @grcd
WHERE [dbo].[mjmast].[mjcd] = @mjcd

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_SelectViewByPK] @mjcd = 'mjcd'

CREATE PROCEDURE [dbo].[PR_mjmast_SelectViewByPK]

		@mjcd 		nvarchar (2)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[mjmast].[mjcd],
		[dbo].[mjmast].[mjname],
		[dbo].[mjmast].[grcd]
FROM  [dbo].[mjmast]

WHERE [dbo].[mjmast].[mjcd] = @mjcd
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_SelectByPK] @mjcd = 'mjcd'

CREATE PROCEDURE [dbo].[PR_mjmast_SelectByPK]

		@mjcd 		nvarchar (2)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[mjmast].[mjcd],
		[dbo].[mjmast].[mjname],
		[dbo].[mjmast].[grcd]
FROM  [dbo].[mjmast]

WHERE [dbo].[mjmast].[mjcd] = @mjcd
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_SelectAll]

CREATE PROCEDURE [dbo].[PR_mjmast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[mjmast].[mjcd],
		[dbo].[mjmast].[mjname],
		[dbo].[mjmast].[grcd]
FROM  [dbo].[mjmast]
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_Insert] @mjcd = 'mjcd', @mjname = 'mjname', @grcd = 'grcd'

CREATE PROCEDURE [dbo].[PR_mjmast_Insert]

		@mjcd   		nvarchar (2) OUTPUT,
		@mjname 		nvarchar (50),
		@grcd   		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[mjmast]
(
		[mjcd],
		[mjname],
		[grcd]
)
VALUES
(
		@mjcd,
		@mjname,
		@grcd
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_mjmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_mjmast_DeleteByPK] @mjcd = 'mjcd'

CREATE PROCEDURE [dbo].[PR_mjmast_DeleteByPK]

		@mjcd 		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[mjmast]
WHERE [dbo].[mjmast].[mjcd] = @mjcd


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ledrep_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ledrep_SelectAll]

CREATE PROCEDURE [dbo].[PR_ledrep_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ledrep].[cdate],
		[dbo].[ledrep].[crefno],
		[dbo].[ledrep].[camount],
		[dbo].[ledrep].[ddate],
		[dbo].[ledrep].[drefno],
		[dbo].[ledrep].[damount],
		[dbo].[ledrep].[acccd],
		[dbo].[ledrep].[acname],
		[dbo].[ledrep].[fdate],
		[dbo].[ledrep].[tdate]
FROM  [dbo].[ledrep]
GO
/****** Object:  StoredProcedure [dbo].[PR_ledrep_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ledrep_Insert] @cdate = '2014-06-14', @crefno = 'crefno', @camount = 1, @ddate = '2014-06-14', @drefno = 'drefno', @damount = 1, @acccd = 'acccd', @acname = 'acname', @fdate = '2014-06-14', @tdate = '2014-06-14'

CREATE PROCEDURE [dbo].[PR_ledrep_Insert]

		@cdate   		datetime,
		@crefno  		nvarchar (15),
		@camount 		decimal (18, 0),
		@ddate   		datetime,
		@drefno  		nvarchar (15),
		@damount 		decimal (18, 0),
		@acccd   		nvarchar (7),
		@acname  		nvarchar (50),
		@fdate   		datetime,
		@tdate   		datetime

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[ledrep]
(
		[cdate],
		[crefno],
		[camount],
		[ddate],
		[drefno],
		[damount],
		[acccd],
		[acname],
		[fdate],
		[tdate]
)
VALUES
(
		@cdate,
		@crefno,
		@camount,
		@ddate,
		@drefno,
		@damount,
		@acccd,
		@acname,
		@fdate,
		@tdate
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_UpdateByPK] @serno = 1, @date = '2014-06-14', @narr = 'narr'

CREATE PROCEDURE [dbo].[PR_journalmast_UpdateByPK]

		@serno 		int,
		@date  		datetime,
		@narr  		nvarchar (200)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[journalmast]
SET
		[date] = @date,
		[narr] = @narr
WHERE [dbo].[journalmast].[serno] = @serno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_SelectViewByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_journalmast_SelectViewByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journalmast].[serno],
		[dbo].[journalmast].[date],
		[dbo].[journalmast].[narr]
FROM  [dbo].[journalmast]

WHERE [dbo].[journalmast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_SelectByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_journalmast_SelectByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journalmast].[serno],
		[dbo].[journalmast].[date],
		[dbo].[journalmast].[narr]
FROM  [dbo].[journalmast]

WHERE [dbo].[journalmast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_SelectAll]

CREATE PROCEDURE [dbo].[PR_journalmast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journalmast].[serno],
		[dbo].[journalmast].[date],
		[dbo].[journalmast].[narr]
FROM  [dbo].[journalmast]
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_Insert] @serno = 1, @date = '2014-06-14', @narr = 'narr'

CREATE PROCEDURE [dbo].[PR_journalmast_Insert]

		@serno 		int OUTPUT,
		@date  		datetime,
		@narr  		nvarchar (200)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[journalmast]


INSERT [dbo].[journalmast]
(
		[serno],
		[date],
		[narr]
)
VALUES
(
		@serno,
		@date,
		@narr
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journalmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journalmast_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_journalmast_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[journalmast]
WHERE [dbo].[journalmast].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_UpdateByPK] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @CreatedOn = '2014-06-14', @CreatedBy = 'CreatedBy', @LastUpdatedOn = '2014-06-14', @LastUpdatedBy = 'LastUpdatedBy'

CREATE PROCEDURE [dbo].[PR_ChallanTran_UpdateByPK]

		@TransID       		int,
		@serno         		int,
		@tserno        		int,
		@itmcd         		int,
		@qty           		decimal (5, 0),
		@weight        		decimal (9, 2),
		@rate          		decimal (9, 2),
		@amount        		decimal (13, 2),
		@CreatedOn     		datetime,
		@CreatedBy     		nchar (10),
		@LastUpdatedOn 		datetime,
		@LastUpdatedBy 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[ChallanTran]
SET
		[serno] = @serno,
		[tserno] = @tserno,
		[itmcd] = @itmcd,
		[qty] = @qty,
		[weight] = @weight,
		[rate] = @rate,
		[amount] = @amount,
		[CreatedOn] = @CreatedOn,
		[CreatedBy] = @CreatedBy,
		[LastUpdatedOn] = @LastUpdatedOn,
		[LastUpdatedBy] = @LastUpdatedBy
WHERE [dbo].[ChallanTran].[TransID] = @TransID

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_SelectViewByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_ChallanTran_SelectViewByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ChallanTran].[TransID],
		[dbo].[ChallanTran].[serno],
		[dbo].[ChallanTran].[tserno],
		[dbo].[ChallanTran].[itmcd],
		[dbo].[ChallanTran].[qty],
		[dbo].[ChallanTran].[weight],
		[dbo].[ChallanTran].[rate],
		[dbo].[ChallanTran].[amount],
		[dbo].[ChallanTran].[CreatedOn],
		[dbo].[ChallanTran].[CreatedBy],
		[dbo].[ChallanTran].[LastUpdatedOn],
		[dbo].[ChallanTran].[LastUpdatedBy]
FROM  [dbo].[ChallanTran]

WHERE [dbo].[ChallanTran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_SelectByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_ChallanTran_SelectByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ChallanTran].[TransID],
		[dbo].[ChallanTran].[serno],
		[dbo].[ChallanTran].[tserno],
		[dbo].[ChallanTran].[itmcd],
		[dbo].[ChallanTran].[qty],
		[dbo].[ChallanTran].[weight],
		[dbo].[ChallanTran].[rate],
		[dbo].[ChallanTran].[amount],
		[dbo].[ChallanTran].[CreatedOn],
		[dbo].[ChallanTran].[CreatedBy],
		[dbo].[ChallanTran].[LastUpdatedOn],
		[dbo].[ChallanTran].[LastUpdatedBy]
FROM  [dbo].[ChallanTran]

WHERE [dbo].[ChallanTran].[TransID] = @TransID
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_SelectAll]

CREATE PROCEDURE [dbo].[PR_ChallanTran_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ChallanTran].[TransID],
		[dbo].[ChallanTran].[serno],
		[dbo].[ChallanTran].[tserno],
		[dbo].[ChallanTran].[itmcd],
		[dbo].[ChallanTran].[qty],
		[dbo].[ChallanTran].[weight],
		[dbo].[ChallanTran].[rate],
		[dbo].[ChallanTran].[amount],
		[dbo].[ChallanTran].[CreatedOn],
		[dbo].[ChallanTran].[CreatedBy],
		[dbo].[ChallanTran].[LastUpdatedOn],
		[dbo].[ChallanTran].[LastUpdatedBy]
FROM  [dbo].[ChallanTran]
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_Insert] @TransID = 1, @serno = 1, @tserno = 1, @itmcd = 1, @qty = 1, @weight = 1, @rate = 1, @amount = 1, @CreatedOn = '2014-06-14', @CreatedBy = 'CreatedBy', @LastUpdatedOn = '2014-06-14', @LastUpdatedBy = 'LastUpdatedBy'

CREATE PROCEDURE [dbo].[PR_ChallanTran_Insert]

		@TransID       		int OUTPUT,
		@serno         		int,
		@tserno        		int,
		@itmcd         		int,
		@qty           		decimal (5, 0),
		@weight        		decimal (9, 2),
		@rate          		decimal (9, 2),
		@amount        		decimal (13, 2),
		@CreatedOn     		datetime,
		@CreatedBy     		nchar (10),
		@LastUpdatedOn 		datetime,
		@LastUpdatedBy 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[ChallanTran]
(
		[serno],
		[tserno],
		[itmcd],
		[qty],
		[weight],
		[rate],
		[amount],
		[CreatedOn],
		[CreatedBy],
		[LastUpdatedOn],
		[LastUpdatedBy]
)
VALUES
(
		@serno,
		@tserno,
		@itmcd,
		@qty,
		@weight,
		@rate,
		@amount,
		@CreatedOn,
		@CreatedBy,
		@LastUpdatedOn,
		@LastUpdatedBy
)

SET @TransID = @@IDENTITY

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ChallanTran_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ChallanTran_DeleteByPK] @TransID = 1

CREATE PROCEDURE [dbo].[PR_ChallanTran_DeleteByPK]

		@TransID 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[ChallanTran]
WHERE [dbo].[ChallanTran].[TransID] = @TransID


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_UpdateByPK] @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @challanNo = 1, @date = '2014-06-14', @acccd = 'acccd', @lotno = 'lotno', @tamount = 1, @grossamt = 1, @roundoff = 1, @netamt = 1, @CreatedOn = '2014-06-14', @YearId = 'YearId', @LastUpdatedOn = '2014-06-14', @LastUpdatedBy = 'LastUpdatedBy'

CREATE PROCEDURE [dbo].[PR_challanMast_UpdateByPK]

		@serno         		int,
		@compid        		int,
		@userid        		nvarchar (20),
		@doccd         		nvarchar (2),
		@challanNo     		int,
		@date          		datetime,
		@acccd         		nvarchar (7),
		@lotno         		nvarchar (10),
		@tamount       		decimal (13, 2),
		@grossamt      		decimal (13, 2),
		@roundoff      		decimal (13, 2),
		@netamt        		decimal (13, 2),
		@CreatedOn     		datetime,
		@YearId        		nchar (20),
		@LastUpdatedOn 		datetime,
		@LastUpdatedBy 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[challanMast]
SET
		[compid] = @compid,
		[userid] = @userid,
		[doccd] = @doccd,
		[challanNo] = @challanNo,
		[date] = @date,
		[acccd] = @acccd,
		[lotno] = @lotno,
		[tamount] = @tamount,
		[grossamt] = @grossamt,
		[roundoff] = @roundoff,
		[netamt] = @netamt,
		[CreatedOn] = @CreatedOn,
		[YearId] = @YearId,
		[LastUpdatedOn] = @LastUpdatedOn,
		[LastUpdatedBy] = @LastUpdatedBy
WHERE [dbo].[challanMast].[serno] = @serno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_SelectViewByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_challanMast_SelectViewByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[challanMast].[serno],
		[dbo].[challanMast].[compid],
		[dbo].[challanMast].[userid],
		[dbo].[challanMast].[doccd],
		[dbo].[challanMast].[challanNo],
		[dbo].[challanMast].[date],
		[dbo].[challanMast].[acccd],
		[dbo].[challanMast].[lotno],
		[dbo].[challanMast].[tamount],
		[dbo].[challanMast].[grossamt],
		[dbo].[challanMast].[roundoff],
		[dbo].[challanMast].[netamt],
		[dbo].[challanMast].[CreatedOn],
		[dbo].[challanMast].[YearId],
		[dbo].[challanMast].[LastUpdatedOn],
		[dbo].[challanMast].[LastUpdatedBy]
FROM  [dbo].[challanMast]

WHERE [dbo].[challanMast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_SelectByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_challanMast_SelectByPK]

		@serno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[challanMast].[serno],
		[dbo].[challanMast].[compid],
		[dbo].[challanMast].[userid],
		[dbo].[challanMast].[doccd],
		[dbo].[challanMast].[challanNo],
		[dbo].[challanMast].[date],
		[dbo].[challanMast].[acccd],
		[dbo].[challanMast].[lotno],
		[dbo].[challanMast].[tamount],
		[dbo].[challanMast].[grossamt],
		[dbo].[challanMast].[roundoff],
		[dbo].[challanMast].[netamt],
		[dbo].[challanMast].[CreatedOn],
		[dbo].[challanMast].[YearId],
		[dbo].[challanMast].[LastUpdatedOn],
		[dbo].[challanMast].[LastUpdatedBy]
FROM  [dbo].[challanMast]

WHERE [dbo].[challanMast].[serno] = @serno
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_SelectAll]

CREATE PROCEDURE [dbo].[PR_challanMast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[challanMast].[serno],
		[dbo].[challanMast].[compid],
		[dbo].[challanMast].[userid],
		[dbo].[challanMast].[doccd],
		[dbo].[challanMast].[challanNo],
		[dbo].[challanMast].[date],
		[dbo].[challanMast].[acccd],
		[dbo].[challanMast].[lotno],
		[dbo].[challanMast].[tamount],
		[dbo].[challanMast].[grossamt],
		[dbo].[challanMast].[roundoff],
		[dbo].[challanMast].[netamt],
		[dbo].[challanMast].[CreatedOn],
		[dbo].[challanMast].[YearId],
		[dbo].[challanMast].[LastUpdatedOn],
		[dbo].[challanMast].[LastUpdatedBy]
FROM  [dbo].[challanMast]
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_Insert] @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @challanNo = 1, @date = '2014-06-14', @acccd = 'acccd', @lotno = 'lotno', @tamount = 1, @grossamt = 1, @roundoff = 1, @netamt = 1, @CreatedOn = '2014-06-14', @YearId = 'YearId', @LastUpdatedOn = '2014-06-14', @LastUpdatedBy = 'LastUpdatedBy'

CREATE PROCEDURE [dbo].[PR_challanMast_Insert]

		@serno         		int OUTPUT,
		@compid        		int,
		@userid        		nvarchar (20),
		@doccd         		nvarchar (2),
		@challanNo     		int,
		@date          		datetime,
		@acccd         		nvarchar (7),
		@lotno         		nvarchar (10),
		@tamount       		decimal (13, 2),
		@grossamt      		decimal (13, 2),
		@roundoff      		decimal (13, 2),
		@netamt        		decimal (13, 2),
		@CreatedOn     		datetime,
		@YearId        		nchar (20),
		@LastUpdatedOn 		datetime,
		@LastUpdatedBy 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @serno = ISNULL(MAX([serno]),0) + 1 FROM [dbo].[challanMast]


INSERT [dbo].[challanMast]
(
		[serno],
		[compid],
		[userid],
		[doccd],
		[challanNo],
		[date],
		[acccd],
		[lotno],
		[tamount],
		[grossamt],
		[roundoff],
		[netamt],
		[CreatedOn],
		[YearId],
		[LastUpdatedOn],
		[LastUpdatedBy]
)
VALUES
(
		@serno,
		@compid,
		@userid,
		@doccd,
		@challanNo,
		@date,
		@acccd,
		@lotno,
		@tamount,
		@grossamt,
		@roundoff,
		@netamt,
		@CreatedOn,
		@YearId,
		@LastUpdatedOn,
		@LastUpdatedBy
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_challanMast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_challanMast_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_challanMast_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[challanMast]
WHERE [dbo].[challanMast].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_cbtran_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_cbtran_DeleteByPK] @sr = 1

CREATE PROCEDURE [dbo].[PR_cbtran_DeleteByPK]

		@sr 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[cbtran]
WHERE [dbo].[cbtran].[sr] = @sr


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_cbmast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_cbmast_SelectAll]

CREATE PROCEDURE [dbo].[PR_cbmast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[cbmast].[cbno],
		[dbo].[cbmast].[date]
FROM  [dbo].[cbmast]
GO
/****** Object:  StoredProcedure [dbo].[PR_cbmast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_cbmast_Insert] @cbno = 1, @date = '2014-06-14'

CREATE PROCEDURE [dbo].[PR_cbmast_Insert]

		@cbno 		int,
		@date 		datetime

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[cbmast]
(
		[cbno],
		[date]
)
VALUES
(
		@cbno,
		@date
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_UpdateByPK] @grcd = 'grcd', @grtype = 'grtype'

CREATE PROCEDURE [dbo].[PR_grtype_UpdateByPK]

		@grcd   		nvarchar (2),
		@grtype 		nvarchar (30)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[grtype]
SET
		[grtype] = @grtype
WHERE [dbo].[grtype].[grcd] = @grcd

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_SelectViewByPK] @grcd = 'grcd'

CREATE PROCEDURE [dbo].[PR_grtype_SelectViewByPK]

		@grcd 		nvarchar (2)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[grtype].[grcd],
		[dbo].[grtype].[grtype]
FROM  [dbo].[grtype]

WHERE [dbo].[grtype].[grcd] = @grcd
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_SelectByPK] @grcd = 'grcd'

CREATE PROCEDURE [dbo].[PR_grtype_SelectByPK]

		@grcd 		nvarchar (2)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[grtype].[grcd],
		[dbo].[grtype].[grtype]
FROM  [dbo].[grtype]

WHERE [dbo].[grtype].[grcd] = @grcd
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_SelectAll]

CREATE PROCEDURE [dbo].[PR_grtype_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[grtype].[grcd],
		[dbo].[grtype].[grtype]
FROM  [dbo].[grtype]
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_Insert] @grcd = 'grcd', @grtype = 'grtype'

CREATE PROCEDURE [dbo].[PR_grtype_Insert]

		@grcd   		nvarchar (2) OUTPUT,
		@grtype 		nvarchar (30)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[grtype]
(
		[grcd],
		[grtype]
)
VALUES
(
		@grcd,
		@grtype
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_grtype_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_grtype_DeleteByPK] @grcd = 'grcd'

CREATE PROCEDURE [dbo].[PR_grtype_DeleteByPK]

		@grcd 		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[grtype]
WHERE [dbo].[grtype].[grcd] = @grcd


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_UpdateByPK] @yearid = 1, @fdate = '2014-06-14', @tdate = '2014-06-14', @coyear = 'coyear'

CREATE PROCEDURE [dbo].[PR_finyear_UpdateByPK]

		@yearid 		int,
		@fdate  		datetime,
		@tdate  		datetime,
		@coyear 		nvarchar (9)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[finyear]
SET
		[fdate] = @fdate,
		[tdate] = @tdate,
		[coyear] = @coyear
WHERE [dbo].[finyear].[yearid] = @yearid

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_SelectViewByPK] @yearid = 1

CREATE PROCEDURE [dbo].[PR_finyear_SelectViewByPK]

		@yearid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[finyear].[yearid],
		[dbo].[finyear].[fdate],
		[dbo].[finyear].[tdate],
		[dbo].[finyear].[coyear]
FROM  [dbo].[finyear]

WHERE [dbo].[finyear].[yearid] = @yearid
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_SelectByPK] @yearid = 1

CREATE PROCEDURE [dbo].[PR_finyear_SelectByPK]

		@yearid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[finyear].[yearid],
		[dbo].[finyear].[fdate],
		[dbo].[finyear].[tdate],
		[dbo].[finyear].[coyear]
FROM  [dbo].[finyear]

WHERE [dbo].[finyear].[yearid] = @yearid
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_SelectAll]

CREATE PROCEDURE [dbo].[PR_finyear_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[finyear].[yearid],
		[dbo].[finyear].[fdate],
		[dbo].[finyear].[tdate],
		[dbo].[finyear].[coyear]
FROM  [dbo].[finyear]
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_Insert] @yearid = 1, @fdate = '2014-06-14', @tdate = '2014-06-14', @coyear = 'coyear'

CREATE PROCEDURE [dbo].[PR_finyear_Insert]

		@yearid 		int OUTPUT,
		@fdate  		datetime,
		@tdate  		datetime,
		@coyear 		nvarchar (9)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @yearid = ISNULL(MAX([yearid]),0) + 1 FROM [dbo].[finyear]


INSERT [dbo].[finyear]
(
		[yearid],
		[fdate],
		[tdate],
		[coyear]
)
VALUES
(
		@yearid,
		@fdate,
		@tdate,
		@coyear
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_finyear_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_finyear_DeleteByPK] @yearid = 1

CREATE PROCEDURE [dbo].[PR_finyear_DeleteByPK]

		@yearid 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[finyear]
WHERE [dbo].[finyear].[yearid] = @yearid


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_UpdateByPK] @skey = 1, @uname = 'uname', @sname = 'sname', @uid = 'uid', @pass = 'pass', @emailid = 'emailid', @mob = 'mob', @authority = 'authority'

CREATE PROCEDURE [dbo].[PR_createuser_UpdateByPK]

		@skey      		int,
		@uname     		char (20),
		@sname     		char (20),
		@uid       		nvarchar (50),
		@pass      		nvarchar (20),
		@emailid   		nvarchar (50),
		@mob       		nvarchar (10),
		@authority 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[createuser]
SET
		[uname] = @uname,
		[sname] = @sname,
		[uid] = @uid,
		[pass] = @pass,
		[emailid] = @emailid,
		[mob] = @mob,
		[authority] = @authority
WHERE [dbo].[createuser].[skey] = @skey

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_SelectViewByPK] @skey = 1

CREATE PROCEDURE [dbo].[PR_createuser_SelectViewByPK]

		@skey 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[createuser].[skey],
		[dbo].[createuser].[uname],
		[dbo].[createuser].[sname],
		[dbo].[createuser].[uid],
		[dbo].[createuser].[pass],
		[dbo].[createuser].[emailid],
		[dbo].[createuser].[mob],
		[dbo].[createuser].[authority]
FROM  [dbo].[createuser]

WHERE [dbo].[createuser].[skey] = @skey
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_SelectByPK] @skey = 1

CREATE PROCEDURE [dbo].[PR_createuser_SelectByPK]

		@skey 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[createuser].[skey],
		[dbo].[createuser].[uname],
		[dbo].[createuser].[sname],
		[dbo].[createuser].[uid],
		[dbo].[createuser].[pass],
		[dbo].[createuser].[emailid],
		[dbo].[createuser].[mob],
		[dbo].[createuser].[authority]
FROM  [dbo].[createuser]

WHERE [dbo].[createuser].[skey] = @skey
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_SelectAll]

CREATE PROCEDURE [dbo].[PR_createuser_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[createuser].[skey],
		[dbo].[createuser].[uname],
		[dbo].[createuser].[sname],
		[dbo].[createuser].[uid],
		[dbo].[createuser].[pass],
		[dbo].[createuser].[emailid],
		[dbo].[createuser].[mob],
		[dbo].[createuser].[authority]
FROM  [dbo].[createuser]
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_Insert] @skey = 1, @uname = 'uname', @sname = 'sname', @uid = 'uid', @pass = 'pass', @emailid = 'emailid', @mob = 'mob', @authority = 'authority'

CREATE PROCEDURE [dbo].[PR_createuser_Insert]

		@skey      		int OUTPUT,
		@uname     		char (20),
		@sname     		char (20),
		@uid       		nvarchar (50),
		@pass      		nvarchar (20),
		@emailid   		nvarchar (50),
		@mob       		nvarchar (10),
		@authority 		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @skey = ISNULL(MAX([skey]),0) + 1 FROM [dbo].[createuser]


INSERT [dbo].[createuser]
(
		[skey],
		[uname],
		[sname],
		[uid],
		[pass],
		[emailid],
		[mob],
		[authority]
)
VALUES
(
		@skey,
		@uname,
		@sname,
		@uid,
		@pass,
		@emailid,
		@mob,
		@authority
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_createuser_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_createuser_DeleteByPK] @skey = 1

CREATE PROCEDURE [dbo].[PR_createuser_DeleteByPK]

		@skey 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[createuser]
WHERE [dbo].[createuser].[skey] = @skey


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_crdr_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_crdr_SelectAll]

CREATE PROCEDURE [dbo].[PR_crdr_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[crdr].[Initial],
		[dbo].[crdr].[type]
FROM  [dbo].[crdr]
GO
/****** Object:  StoredProcedure [dbo].[PR_crdr_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_crdr_Insert] @Initial = 'Initial', @type = 'type'

CREATE PROCEDURE [dbo].[PR_crdr_Insert]

		@Initial 		nchar (10),
		@type    		nchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[crdr]
(
		[Initial],
		[type]
)
VALUES
(
		@Initial,
		@type
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  Table [dbo].[compmast]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[compmast](
	[compid] [int] NOT NULL,
	[coid] [int] NULL,
	[yearid] [int] NULL,
	[cocd] [nvarchar](5) NULL,
	[coname] [nvarchar](50) NULL,
	[fdate] [datetime] NULL,
	[tdate] [datetime] NULL,
	[coyear] [nvarchar](9) NULL,
PRIMARY KEY CLUSTERED 
(
	[compid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (1, 1, 3, N'JKT', N'JAY KISAN TRADERS', CAST(0x0000A19300000000 AS DateTime), CAST(0x0000A2FF00000000 AS DateTime), N'2013-2014')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (2, 2, 3, N'TT', N'TEJAL TRADER', CAST(0x0000A19300000000 AS DateTime), CAST(0x0000A2FF00000000 AS DateTime), N'2013-2014')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (3, 3, 3, N'ST', N'SAVI TRADERS', CAST(0x0000A19300000000 AS DateTime), CAST(0x0000A2FF00000000 AS DateTime), N'2013-2014')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (5, 1, 10, N'JKT', N'JAY KISAN TRADERS', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (6, 2, 10, N'TT', N'TEJAL TRADER', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (7, 3, 10, N'ST', N'SAVI TRADERS', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (8, 4, 10, N'DPK', N'DEEPAK', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (9, 5, 10, N'SA', N'SAAD_ENTERPRICES', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (10, 5, 13, N'SA', N'SAAD_ENTERPRICES', CAST(0x0000A46D00000000 AS DateTime), CAST(0x0000A5DA00000000 AS DateTime), N'2015-2016')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (11, 2, 13, N'TT', N'TEJAL TRADER', CAST(0x0000A46D00000000 AS DateTime), CAST(0x0000A5DA00000000 AS DateTime), N'2015-2016')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (12, 9, 10, N'ZZZ', N'SAMEER', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46D00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (13, 9, 13, N'ZZZ', N'SAMEER', CAST(0x0000A46D00000000 AS DateTime), CAST(0x0000A5DA00000000 AS DateTime), N'2015-2016')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (14, 10, 10, N'AAA', N'JHJKH', CAST(0x0000A30000000000 AS DateTime), CAST(0x0000A46C00000000 AS DateTime), N'2014-2015')
INSERT [dbo].[compmast] ([compid], [coid], [yearid], [cocd], [coname], [fdate], [tdate], [coyear]) VALUES (15, 10, 13, N'AAA', N'JHJKH', CAST(0x0000A46D00000000 AS DateTime), CAST(0x0000A5DA00000000 AS DateTime), N'2015-2016')
/****** Object:  StoredProcedure [dbo].[cbmast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cbmast_UpdateByPK]

		@cbno 				int,
		@trsnrp			    nvarchar ,
		@modecasbank	    nvarchar ,
		@Bankcode			nvarchar ,
		@Bankname	   	    nvarchar ,
		@date			    datetime
		
AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[cbmast]
SET
      [trsnrp]=	@trsnrp,	    
	  [modecasbank]= @modecasbank,	    
	  [BankCode]= @Bankcode,		
		[Bankname]= @Bankname,   	    
		[date]= @date	
		
WHERE [dbo].[cbmast].[cbno] = @cbno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[AddNewFinancialYear]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--AddNewFinancialYear 2,10
CREATE PROCEDURE [dbo].[AddNewFinancialYear] 
	-- Add the parameters for the stored procedure here
	@coid int,
	@compid int,
	@IsCurrentYear bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--declare @coid int
--declare	@compid int

--set @coid =4
--set @compid =9

declare @futureYear varchar(20)
declare @NewCompId int
declare @YearId int

if @IsCurrentYear = 0
begin
	set @futureYear =  dbo.GetFinancialYear(dateadd(yy,1,getdate()))
end
else
begin
	set @futureYear =  dbo.GetFinancialYear(getdate())
	select @compid=MAX(compid)+1 from compmast
end

print @futureYear

if exists (select * from finyear where coyear=@futureYear)
	begin
		select @YearId=yearid from finyear where coyear=@futureYear
		
		if not exists ( select * from compmast where yearid =@YearId and coid=@coid )
		begin
			insert into compmast	
			select top 1 @compid,coid,@YearId,cocd,coname,f.fdate,f.tdate,f.coyear from company join finyear f on f.yearid=@YearId where coid=@coid
		end
		
	end
else	
	begin
			insert into finyear 
			select dbo.AddYear(@futureYear)+'-04-01 00:00:00.000', convert(nvarchar,CONVERT(int,dbo.AddYear(@futureYear))+1)+'-03-31 00:00:00.000',@futureYear	
			
			select @YearId=yearid from finyear where coyear=@futureYear
			if not exists ( select * from compmast where yearid =@YearId and coid=@coid)
			begin
				insert into compmast	
				select top 1 @compid,coid,@YearId,cocd,coname,f.fdate,f.tdate,f.coyear from company join finyear f on f.yearid=@YearId where coid=@coid
			end
	end

END
GO
/****** Object:  Table [dbo].[acmast]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acmast](
	[acccd] [nvarchar](50) NOT NULL,
	[compid] [int] NOT NULL,
	[userid] [nvarchar](20) NOT NULL,
	[acno] [int] NOT NULL,
	[grcd] [nvarchar](2) NOT NULL,
	[acname] [nvarchar](50) NOT NULL,
	[mjcd] [nvarchar](2) NOT NULL,
	[lf] [int] NULL,
	[cellno1] [bigint] NULL,
	[cellno2] [bigint] NULL,
	[srplace] [int] NULL,
 CONSTRAINT [PK_acmast] PRIMARY KEY CLUSTERED 
(
	[acno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000001', 1, N'abc', 1, N'CB', N'ABC Bank', N'A3', 2, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000002', 1, N'abc', 2, N'CB', N'STATE BANK OF INDIA', N'A3', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000003', 1, N'abc', 3, N'IE', N'COMMISSION A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000004', 1, N'abc', 4, N'GN', N'GROCARRY BOARD', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000005', 1, N'abc', 5, N'GN', N'INCOME TAX', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000006', 1, N'abc', 6, N'GN', N'MOTOR FREIGHT', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000007', 1, N'abc', 7, N'GN', N'SCALE', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000008', 1, N'abc', 8, N'GN', N'SUSPENCES A/C.', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000009', 1, N'abc', 9, N'GN', N'COMPUTER A/C.', N'A1', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000010', 1, N'abc', 10, N'GN', N'GODOWN', N'A1', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000011', 1, N'abc', 11, N'GN', N'MOTOR CYCLE', N'A1', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000012', 1, N'abc', 12, N'GN', N'HOUSE ( HOME )', N'A1', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000013', 1, N'abc', 13, N'IE', N'ROUND OFF.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000014', 1, N'abc', 14, N'GN', N'CAR A/C.', N'A1', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000015', 1, N'abc', 15, N'GN', N'VAT A/C.', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000016', 1, N'abc', 16, N'GN', N'APMC MARKET FEES & SUP. CHRGES', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000017', 1, N'abc', 17, N'GN', N'ADVANCE TAX', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000018', 1, N'abc', 18, N'GN', N'AIR CONDITIONER', N'L6', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000019', 1, N'abc', 19, N'IE', N'AUDIT FEES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000020', 1, N'abc', 20, N'IE', N'BANK DIVIDEND', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000021', 1, N'abc', 21, N'IE', N'BANK INTEREST', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000022', 1, N'abc', 22, N'IE', N'BONUS A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000023', 1, N'abc', 23, N'IE', N'CONVEYANANCE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000024', 1, N'abc', 24, N'IE', N'DEPRICATION A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000025', 1, N'abc', 25, N'IE', N'DIWALI EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000026', 1, N'abc', 26, N'IE', N'ELECTRICITY BILLS', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000027', 1, N'abc', 27, N'IE', N'INTEREST A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000028', 1, N'abc', 28, N'IE', N'KASAR EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000029', 1, N'abc', 29, N'IE', N'LEGAL EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000030', 1, N'abc', 30, N'IE', N'MOTOR CAR EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000031', 1, N'abc', 31, N'IE', N'NAVI MUMBAI PROPERTY TAX', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000032', 1, N'abc', 32, N'IE', N'PARTNERS CAPITAL INTEREST', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000033', 1, N'abc', 33, N'IE', N'PETROL & DISEL EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000034', 1, N'abc', 34, N'IE', N'PROFIT & LOSS A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000035', 1, N'abc', 35, N'IE', N'PROFESSIONAL FEES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000036', 1, N'abc', 36, N'IE', N'PRINTING & STATIONARY EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000037', 1, N'abc', 37, N'IE', N'POSTAGE & TELEGRAM', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000038', 1, N'abc', 38, N'IE', N'REPAIR & MAINTANANCE', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000039', 1, N'abc', 39, N'IE', N'SUTALI & BARDAN EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000040', 1, N'abc', 40, N'IE', N'APMC SERVICE CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000041', 1, N'abc', 41, N'IE', N'SCALE REPAIR & MAINTANANCE', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000042', 1, N'abc', 42, N'IE', N'SALARIES & WAGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000043', 1, N'abc', 43, N'IE', N'TEA & COFFEE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000044', 1, N'abc', 44, N'IE', N'TELEPHONE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000045', 1, N'abc', 45, N'IE', N'TRAVELLING & CONVEYNANACE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000046', 1, N'abc', 46, N'IE', N'VADAR A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000047', 1, N'abc', 47, N'IE', N'VATAV EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000048', 1, N'abc', 48, N'IE', N'MOTOR CAR INTEREST A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000049', 1, N'abc', 49, N'IE', N'WARFARE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000050', 1, N'abc', 50, N'IE', N'GODOWN EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000051', 1, N'abc', 51, N'IE', N'ACCOUNT CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000052', 1, N'abc', 52, N'IE', N'O.D. INTEREST A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000053', 1, N'abc', 53, N'IE', N'H.Y.L. INTEREST A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000054', 1, N'abc', 54, N'IE', N'A. C. SERVICE CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000055', 1, N'abc', 55, N'IE', N'INTEREST RECOVERY A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000056', 1, N'abc', 56, N'IE', N'F. D. INTEREST RECOVERY A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000057', 1, N'abc', 57, N'IE', N'KANDA BATATA ADAT SANGH', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000058', 1, N'abc', 58, N'IE', N'VYAPARI ASSOCIATON FEES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000059', 1, N'abc', 59, N'IE', N'VACHHAT EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000060', 1, N'abc', 60, N'IE', N'MOTOR CAR INSURANCE A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000061', 1, N'abc', 61, N'IE', N'PROFESSIONAL TAX', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000062', 1, N'abc', 62, N'IE', N'PURCHASE PROVISION A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000063', 1, N'abc', 63, N'IE', N'SALES PROVISION A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000064', 1, N'abc', 64, N'IE', N'TRANSPORT COMMISSION A/C.', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000065', 1, N'abc', 65, N'IE', N'BANK CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000066', 1, N'abc', 66, N'IE', N'BANK INSURANCE EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000067', 1, N'abc', 67, N'IE', N'GODOWN RENT', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000068', 1, N'abc', 68, N'IE', N'VAPSI COMMISSION', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000069', 1, N'abc', 69, N'IE', N'TRANSPORT CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000070', 1, N'abc', 70, N'IE', N'DONATION EXPENCES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000071', 1, N'abc', 71, N'IE', N'LABOUR CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000072', 1, N'abc', 72, N'IE', N'SOFTWARE MAINTANANCE CHARGES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000073', 1, N'abc', 73, N'IE', N'GUMASTA LICENACE FEES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000074', 1, N'abc', 74, N'IE', N'LICENACE FEES', N'EE', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000075', 1, N'abc', 75, N'IE', N'INCOME A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000076', 1, N'abc', 76, N'IE', N'DIFFERANCE A/C.', N'II', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000077', 1, N'ABC', 77, N'CR', N'DEEPAK PAWAR', N'L4', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000078', 1, N'abc', 78, N'CR', N'KOMAL CHAUDHARY', N'L4', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000079', 1, N'abc', 79, N'PR', N'PRAVIN GUNJAL', N'L4', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000080', 1, N'abc', 80, N'CB', N'OBC BANK', N'A3', 2, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000081', 1, N'abc', 81, N'CR', N'ZZZZZZZ', N'A3', 1, 12121, 4545646, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000082', 1, N'abc', 82, N'CB', N'KOMAL C', N'A3', 1, 98, 1231, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000083', 1, N'abc', 83, N'CR', N'SAMEER', N'A4', 1, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000084', 1, N'abc', 84, N'GN', N'SAM', N'A4', 1, 121, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000085', 1, N'abc', 85, N'CB', N'345345', N'A4', 3423, 34, 234, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000086', 1, N'abc', 86, N'CB', N'asdas', N'A2', 123, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000087', 2, N'abc', 87, N'CR', N'asdfa', N'A6', 123, 234, 234, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000088', 1, N'abc', 88, N'CB', N'xyz bank', N'A3', 1, 12121, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000089', 1, N'abc', 89, N'GN', N'Deepak', N'AA', 1, 12, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000090', 1, N'abc', 90, N'TR', N'XYZ Travel', N'AA', 1, 1212112, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000091', 1, N'abc', 91, N'CB', N'bank of baroda', N'A3', 2, 810826, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000092', 1, N'abc', 92, N'CB', N'obc bank', N'A3', 1, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000093', 1, N'abc', 93, N'CB', N'bank', N'A3', 1, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000094', 1, N'abc', 94, N'CR', N'Deepak Patil', N'A4', 1, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000095', 1, N'abc', 95, N'PR', N'sameer i', N'L4', 3, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000096', 1, N'abc', 96, N'CB', N'pqr bank', N'A3', 12, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000097', 1, N'abc', 97, N'CB', N'cash in Hand', N'A3', 0, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000098', 1, N'abc', 98, N'CB', N'NAVI MUMBAI BANK', N'', 1, 0, 0, 6)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000100', 1, N'ABC', 100, N'GN', N'UTRAI CREDIT A/C', N'L6', 0, 0, 0, 0)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000101', 1, N'ABC', 101, N'GN', N'TOLAI CREDIT A/C', N'L6', 0, 0, 0, 0)
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000102', 1, N'ABC', 102, N'IE', N'BHARAI EXPENSES', N'EE', 0, 0, 0, 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[acmast] ([acccd], [compid], [userid], [acno], [grcd], [acname], [mjcd], [lf], [cellno1], [cellno2], [srplace]) VALUES (N'0000103', 1, N'ABC', 103, N'IE', N'UTRAI EXPENSES', N'EE', 0, 0, 0, 0)
/****** Object:  Table [dbo].[journal]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[journal](
	[sr] [int] IDENTITY(1,1) NOT NULL,
	[serno] [int] NOT NULL,
	[tserno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[date] [datetime] NULL,
	[rp] [nvarchar](2) NULL,
	[casbank] [nvarchar](7) NULL,
	[acccd] [nvarchar](7) NULL,
	[crdr] [nvarchar](2) NULL,
	[cramt] [decimal](13, 2) NULL,
	[dramt] [decimal](13, 2) NULL,
	[narrcd] [nvarchar](200) NULL,
	[lotno] [nvarchar](10) NULL,
 CONSTRAINT [PK__journal__1AD3FDA4] PRIMARY KEY CLUSTERED 
(
	[sr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[journal] ON
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (1, 1, 1, 6, N'sagar', N'JE', CAST(0x0000A3E500000000 AS DateTime), N'DR', NULL, N'0000025', NULL, NULL, CAST(784.00 AS Decimal(13, 2)), N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (2, 1, 2, 6, N'sagar', N'JE', CAST(0x0000A3E500000000 AS DateTime), N'CR', NULL, N'0000056', NULL, CAST(784.00 AS Decimal(13, 2)), NULL, N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (3, 2, 1, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'DR', NULL, N'0000003', NULL, NULL, CAST(500.00 AS Decimal(13, 2)), N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (4, 2, 2, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'CR', NULL, N'0000077', NULL, CAST(500.00 AS Decimal(13, 2)), NULL, N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (5, 3, 1, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'CR', NULL, N'0000070', NULL, CAST(233.00 AS Decimal(13, 2)), NULL, N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (6, 4, 1, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'DR', NULL, N'0000025', NULL, NULL, CAST(200.00 AS Decimal(13, 2)), N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (7, 4, 2, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'CR', NULL, N'0000003', NULL, CAST(200.00 AS Decimal(13, 2)), NULL, N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (8, 5, 1, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'DR', NULL, N'0000003', NULL, NULL, CAST(400.00 AS Decimal(13, 2)), N'', NULL)
INSERT [dbo].[journal] ([sr], [serno], [tserno], [compid], [userid], [doccd], [date], [rp], [casbank], [acccd], [crdr], [cramt], [dramt], [narrcd], [lotno]) VALUES (9, 5, 2, 6, N'sagar', N'JE', CAST(0x0000A41D00000000 AS DateTime), N'CR', NULL, N'0000025', NULL, CAST(400.00 AS Decimal(13, 2)), NULL, N'', NULL)
SET IDENTITY_INSERT [dbo].[journal] OFF
/****** Object:  StoredProcedure [dbo].[GetCompInfo]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCompInfo]
	-- Add the parameters for the stored procedure here
	@compid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cm.coname,compadd,compphone,email,compwebsite,compfax from company c join compmast cm on c.coid=cm.coid
where compid=@compid
END
GO
/****** Object:  Table [dbo].[code]    Script Date: 01/11/2015 19:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[code](
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[cashcd] [nvarchar](7) NULL,
	[bankcd] [nvarchar](7) NULL,
	[motorfrt] [nvarchar](7) NULL,
	[pl] [nvarchar](7) NULL,
	[commission] [nvarchar](7) NULL,
	[income] [nvarchar](7) NULL,
	[purchase] [nvarchar](7) NULL,
	[sales] [nvarchar](7) NULL,
	[diff] [nvarchar](7) NULL,
	[roundoff] [nvarchar](7) NULL,
	[vadar] [nvarchar](7) NULL,
	[kasar] [nvarchar](7) NULL,
	[vatav] [nvarchar](7) NULL,
	[bankchgs] [nvarchar](7) NULL,
	[BHARAI] [nvarchar](7) NULL,
	[UTRAI] [nvarchar](7) NULL,
	[TOLAI] [nvarchar](7) NULL,
	[BHARAI_E] [nvarchar](7) NULL,
	[UTRAI_E] [nvarchar](7) NULL,
	[TOLAI_E] [nvarchar](7) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[code] ([compid], [userid], [cashcd], [bankcd], [motorfrt], [pl], [commission], [income], [purchase], [sales], [diff], [roundoff], [vadar], [kasar], [vatav], [bankchgs], [BHARAI], [UTRAI], [TOLAI], [BHARAI_E], [UTRAI_E], [TOLAI_E]) VALUES (1, N'abc', N'0000097', N'0000002', N'0000006', N'0000034', N'0000003', N'0000075', N'0000062', N'0000063', N'0000076', N'0000013', N'0000046', N'0000028', N'0000047', N'0000065', N'0000099', N'0000100', N'0000101', N'0000102', N'0000103', N'0000104')
/****** Object:  Table [dbo].[item]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[item](
	[itmcd] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[item] [nvarchar](20) NOT NULL,
	[unit] [int] NULL,
	[tare] [int] NULL,
 CONSTRAINT [PK_item] PRIMARY KEY CLUSTERED 
(
	[itmcd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (1, 1, N'abc', N'Ova', 201, 201)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (3, 1, N'abc', N'Orange', 12, 1)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (4, 1, N'abc', N'WATERMELEN', 4, 4)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (5, 1, N'abc', N'Apple', 12, 1)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (6, 1, N'abc', N'GRAPES', 7, 7)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (7, 1, N'abc', N'SANTRA', 12, 5)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (8, 1, N'abc', N'Sweet Lemon', 2, 2)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (9, 1, N'abc', N'Carrot', 1, 1)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (10, 1, N'abc', N'Mango', 10, 10)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (11, 1, N'abc', N'Papai', 10, 10)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (12, 1, N'abc', N'Strawberry', 12, 15)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (13, 1, N'abc', N'Berry', 1, 1)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (14, 1, N'abc', N'Cherry', 3, 4)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (15, 1, N'abc', N'Custurd Apple', 7, 4)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (16, 1, N'abc', N'Keyboard', 10, 10)
INSERT [dbo].[item] ([itmcd], [compid], [userid], [item], [unit], [tare]) VALUES (17, 1, N'abc', N'Mouse', 10, 10)
/****** Object:  Table [dbo].[openbal]    Script Date: 01/11/2015 19:06:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[openbal](
	[acccd] [nvarchar](7) NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[grcd] [nvarchar](2) NULL,
	[acname] [nvarchar](50) NULL,
	[mjcd] [nvarchar](3) NULL,
	[lf] [int] NULL,
	[opbal] [decimal](13, 2) NULL,
	[sign12] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[acccd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000001', 1, N'abc', N'CB', N'CASH IN HAND', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000002', 1, N'abc', N'CB', N'STATE BANK OF INDIA', N'A3', 0, CAST(50.00 AS Decimal(13, 2)), N'DR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000003', 1, N'abc', N'IE', N'COMMISSION A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000004', 1, N'abc', N'GN', N'GROCARRY BOARD', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000005', 1, N'abc', N'GN', N'INCOME TAX', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000006', 1, N'abc', N'GN', N'MOTOR FREIGHT', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000007', 1, N'abc', N'GN', N'SCALE', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000008', 1, N'abc', N'GN', N'SUSPENCES A/C.', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000009', 1, N'abc', N'GN', N'COMPUTER A/C.', N'A1', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000010', 1, N'abc', N'GN', N'GODOWN', N'A1', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000011', 1, N'abc', N'GN', N'MOTOR CYCLE', N'A1', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000012', 1, N'abc', N'GN', N'HOUSE ( HOME )', N'A1', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000013', 1, N'abc', N'IE', N'ROUND OFF.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000014', 1, N'abc', N'GN', N'CAR A/C.', N'A1', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000015', 1, N'abc', N'GN', N'VAT A/C.', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000016', 1, N'abc', N'GN', N'APMC MARKET FEES & SUP. CHRGES', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000017', 1, N'abc', N'GN', N'ADVANCE TAX', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000018', 1, N'abc', N'GN', N'AIR CONDITIONER', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000019', 1, N'abc', N'IE', N'AUDIT FEES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000020', 1, N'abc', N'IE', N'BANK DIVIDEND', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000021', 1, N'abc', N'IE', N'BANK INTEREST', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000022', 1, N'abc', N'IE', N'BONUS A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000023', 1, N'abc', N'IE', N'CONVEYANANCE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000024', 1, N'abc', N'IE', N'DEPRICATION A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000025', 1, N'abc', N'IE', N'DIWALI EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000026', 1, N'abc', N'IE', N'ELECTRICITY BILLS', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000027', 1, N'abc', N'IE', N'INTEREST A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000028', 1, N'abc', N'IE', N'KASAR EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000029', 1, N'abc', N'IE', N'LEGAL EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000030', 1, N'abc', N'IE', N'MOTOR CAR EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000031', 1, N'abc', N'IE', N'NAVI MUMBAI PROPERTY TAX', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000032', 1, N'abc', N'IE', N'PARTNERS CAPITAL INTEREST', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000033', 1, N'abc', N'IE', N'PETROL & DISEL EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000034', 1, N'abc', N'IE', N'PROFIT & LOSS A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000035', 1, N'abc', N'IE', N'PROFESSIONAL FEES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000036', 1, N'abc', N'IE', N'PRINTING & STATIONARY EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000037', 1, N'abc', N'IE', N'POSTAGE & TELEGRAM', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000038', 1, N'abc', N'IE', N'REPAIR & MAINTANANCE', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000039', 1, N'abc', N'IE', N'SUTALI & BARDAN EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000040', 1, N'abc', N'IE', N'APMC SERVICE CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000041', 1, N'abc', N'IE', N'SCALE REPAIR & MAINTANANCE', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000042', 1, N'abc', N'IE', N'SALARIES & WAGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000043', 1, N'abc', N'IE', N'TEA & COFFEE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000044', 1, N'abc', N'IE', N'TELEPHONE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000045', 1, N'abc', N'IE', N'TRAVELLING & CONVEYNANACE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000046', 1, N'abc', N'IE', N'VADAR A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000047', 1, N'abc', N'IE', N'VATAV EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000048', 1, N'abc', N'IE', N'MOTOR CAR INTEREST A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000049', 1, N'abc', N'IE', N'WARFARE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000050', 1, N'abc', N'IE', N'GODOWN EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000051', 1, N'abc', N'IE', N'ACCOUNT CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000052', 1, N'abc', N'IE', N'O.D. INTEREST A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000053', 1, N'abc', N'IE', N'H.Y.L. INTEREST A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000054', 1, N'abc', N'IE', N'A. C. SERVICE CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000055', 1, N'abc', N'IE', N'INTEREST RECOVERY A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000056', 1, N'abc', N'IE', N'F. D. INTEREST RECOVERY A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000057', 1, N'abc', N'IE', N'KANDA BATATA ADAT SANGH', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000058', 1, N'abc', N'IE', N'VYAPARI ASSOCIATON FEES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000059', 1, N'abc', N'IE', N'VACHHAT EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000060', 1, N'abc', N'IE', N'MOTOR CAR INSURANCE A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000061', 1, N'abc', N'IE', N'PROFESSIONAL TAX', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000062', 1, N'abc', N'IE', N'PURCHASE PROVISION A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000063', 1, N'abc', N'IE', N'SALES PROVISION A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000064', 1, N'abc', N'IE', N'TRANSPORT COMMISSION A/C.', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000065', 1, N'abc', N'IE', N'BANK CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000066', 1, N'abc', N'IE', N'BANK INSURANCE EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000067', 1, N'abc', N'IE', N'GODOWN RENT', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000068', 1, N'abc', N'IE', N'VAPSI COMMISSION', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000069', 1, N'abc', N'IE', N'TRANSPORT CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000070', 1, N'abc', N'IE', N'DONATION EXPENCES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000071', 1, N'abc', N'IE', N'LABOUR CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000072', 1, N'abc', N'IE', N'SOFTWARE MAINTANANCE CHARGES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000073', 1, N'abc', N'IE', N'GUMASTA LICENACE FEES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000074', 1, N'abc', N'IE', N'LICENACE FEES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000075', 1, N'abc', N'IE', N'INCOME A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000076', 1, N'abc', N'IE', N'DIFFERANCE A/C.', N'II', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000077', 1, N'abc', N'GN', N'DEEPAK PAWAR', N'L6', 0, CAST(77000.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000078', 1, N'abc', N'CR', N'KOMAL CHAUDHARY', N'A4', 0, CAST(5000.50 AS Decimal(13, 2)), N'DR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000079', 1, N'abc', N'PR', N'PRAVIN GUNJAL', N'L4', 0, CAST(10000.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000080', 1, N'abc', N'CB', N'OBC BANK', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000081', 1, N'abc', N'CR', N'HJHJHLHJ', N'', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000082', 1, N'abc', N'CB', N'KOMAL C', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000083', 1, N'abc', N'CR', N'SAMEER', N'A4', 0, CAST(10000.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000084', 1, N'abc', N'GN', N'SAM', N'A4', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000085', 1, N'abc', N'CB', N'345345', N'A4', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000086', 1, N'abc', N'CB', N'asdas', N'A2', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000087', 1, N'abc', N'CR', N'asdfa', N'A6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000088', 1, N'abc', N'CB', N'xyz bank', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000089', 1, N'abc', N'GN', N'Deepak', N'AA', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000090', 1, N'abc', N'TR', N'ABC Travel', N'AA', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000091', 1, N'abc', N'CB', N'bank of baroda', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000092', 1, N'abc', N'CB', N'obc bank', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000093', 1, N'abc', N'CB', N'bank', N'A3', 0, CAST(10000.00 AS Decimal(13, 2)), N'DR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000094', 1, N'abc', N'CR', N'Deepak Patil', N'A4', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000095', 1, N'abc', N'PR', N'sameer i', N'L4', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000096', 1, N'abc', N'CB', N'pqr bank', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000097', 1, N'abc', N'CB', N'cash in Hand', N'A3', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000098', 1, N'abc', N'CB', N'NAVI MUMBAI BANK', N'', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000099', 1, N'abc', N'GN', N'BHARAI CREDIT A/C', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000100', 1, N'abc', N'GN', N'UTRAI CREDIT A/C', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000101', 1, N'abc', N'GN', N'TOLAI CREDIT A/C', N'L6', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
GO
print 'Processed 100 total records'
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000102', 1, N'abc', N'IE', N'BHARAI EXPENSES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000103', 1, N'abc', N'IE', N'UTRAI EXPENSES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
INSERT [dbo].[openbal] ([acccd], [compid], [userid], [grcd], [acname], [mjcd], [lf], [opbal], [sign12]) VALUES (N'0000104', 1, N'abc', N'IE', N'TOLAI EXPENSES', N'EE', 0, CAST(0.00 AS Decimal(13, 2)), N'CR')
/****** Object:  StoredProcedure [dbo].[PR_compmast_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_UpdateByPK] @compid = 1, @coid = 1, @yearid = 1, @cocd = 'cocd', @coname = 'coname', @fdate = '2014-06-14', @tdate = '2014-06-14', @coyear = 'coyear'

CREATE PROCEDURE [dbo].[PR_compmast_UpdateByPK]

		@compid 		int,
		@coid   		int,
		@yearid 		int,
		@cocd   		nvarchar (5),
		@coname 		nvarchar (50),
		@fdate  		datetime,
		@tdate  		datetime,
		@coyear 		nvarchar (9)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[compmast]
SET
		[coid] = @coid,
		[yearid] = @yearid,
		[cocd] = @cocd,
		[coname] = @coname,
		[fdate] = @fdate,
		[tdate] = @tdate,
		[coyear] = @coyear
WHERE [dbo].[compmast].[compid] = @compid

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_SelectViewByPK] @compid = 1

CREATE PROCEDURE [dbo].[PR_compmast_SelectViewByPK]

		@compid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[compmast].[compid],
		[dbo].[compmast].[coid],
		[dbo].[compmast].[yearid],
		[dbo].[compmast].[cocd],
		[dbo].[compmast].[coname],
		[dbo].[compmast].[fdate],
		[dbo].[compmast].[tdate],
		[dbo].[compmast].[coyear]
FROM  [dbo].[compmast]

WHERE [dbo].[compmast].[compid] = @compid
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_SelectByPK] @compid = 1

CREATE PROCEDURE [dbo].[PR_compmast_SelectByPK]

		@compid 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[compmast].[compid],
		[dbo].[compmast].[coid],
		[dbo].[compmast].[yearid],
		[dbo].[compmast].[cocd],
		[dbo].[compmast].[coname],
		[dbo].[compmast].[fdate],
		[dbo].[compmast].[tdate],
		[dbo].[compmast].[coyear]
FROM  [dbo].[compmast]

WHERE [dbo].[compmast].[compid] = @compid
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_SelectAll]

CREATE PROCEDURE [dbo].[PR_compmast_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[compmast].[compid],
		[dbo].[compmast].[coid],
		[dbo].[compmast].[yearid],
		[dbo].[compmast].[cocd],
		[dbo].[compmast].[coname],
		[dbo].[compmast].[fdate],
		[dbo].[compmast].[tdate],
		[dbo].[compmast].[coyear]
FROM  [dbo].[compmast]
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_Insert] @compid = 1, @coid = 1, @yearid = 1, @cocd = 'cocd', @coname = 'coname', @fdate = '2014-06-14', @tdate = '2014-06-14', @coyear = 'coyear'

CREATE PROCEDURE [dbo].[PR_compmast_Insert]

		@compid 		int OUTPUT,
		@coid   		int,
		@yearid 		int,
		@cocd   		nvarchar (5),
		@coname 		nvarchar (50),
		@fdate  		datetime,
		@tdate  		datetime,
		@coyear 		nvarchar (9)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @compid = ISNULL(MAX([compid]),0) + 1 FROM [dbo].[compmast]


INSERT [dbo].[compmast]
(
		[compid],
		[coid],
		[yearid],
		[cocd],
		[coname],
		[fdate],
		[tdate],
		[coyear]
)
VALUES
(
		@compid,
		@coid,
		@yearid,
		@cocd,
		@coname,
		@fdate,
		@tdate,
		@coyear
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_compmast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_compmast_DeleteByPK] @compid = 1

CREATE PROCEDURE [dbo].[PR_compmast_DeleteByPK]

		@compid 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[compmast]
WHERE [dbo].[compmast].[compid] = @compid


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  Table [dbo].[ratetran]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ratetran](
	[itmserno] [int] NOT NULL,
	[tserno] [int] NOT NULL,
	[serno] [int] NOT NULL,
	[compid] [int] NOT NULL,
	[userid] [nvarchar](20) NOT NULL,
	[itmcd] [int] NOT NULL,
	[rate] [decimal](9, 2) NOT NULL,
 CONSTRAINT [PK__ratetran__6166761E] PRIMARY KEY CLUSTERED 
(
	[itmserno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ratetran] ([itmserno], [tserno], [serno], [compid], [userid], [itmcd], [rate]) VALUES (1, 1, 1, 1, N'abc', 3, CAST(50.00 AS Decimal(9, 2)))
INSERT [dbo].[ratetran] ([itmserno], [tserno], [serno], [compid], [userid], [itmcd], [rate]) VALUES (2, 1, 2, 1, N'abc', 17, CAST(20.00 AS Decimal(9, 2)))
/****** Object:  Table [dbo].[purtran.old]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[purtran.old](
	[PurTranId] [int] IDENTITY(1,1) NOT NULL,
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[date] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
	[lotno] [nvarchar](10) NULL,
	[sublotno] [nvarchar](10) NULL,
	[tserno] [int] NULL,
	[itmcd] [int] NULL,
	[item] [nvarchar](20) NULL,
	[qty] [decimal](5, 0) NULL,
	[weight] [decimal](9, 2) NULL,
	[rate] [decimal](9, 2) NULL,
	[amount] [decimal](13, 2) NULL,
	[lapmc] [decimal](13, 2) NULL,
	[lhamali] [decimal](13, 2) NULL,
	[lmaplevy] [decimal](13, 2) NULL,
 CONSTRAINT [PK_purtran] PRIMARY KEY CLUSTERED 
(
	[PurTranId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[purtran.old] ON
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (1, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Sweet Lemon', CAST(1 AS Decimal(5, 0)), CAST(5.00 AS Decimal(9, 2)), CAST(55.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (2, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, N'GRAPES', CAST(5 AS Decimal(5, 0)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (3, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Orange', CAST(5 AS Decimal(5, 0)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, N'Sweet Lemon', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (5, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'SANTRA', CAST(4 AS Decimal(5, 0)), CAST(4.00 AS Decimal(9, 2)), CAST(4.00 AS Decimal(9, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (6, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, N'8', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (7, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'SANTRA', CAST(5 AS Decimal(5, 0)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), CAST(5.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (8, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (9, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (10, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(2 AS Decimal(5, 0)), CAST(2.00 AS Decimal(9, 2)), CAST(2.00 AS Decimal(9, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), CAST(2.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (11, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(4 AS Decimal(5, 0)), CAST(4.00 AS Decimal(9, 2)), CAST(4.00 AS Decimal(9, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(4.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (12, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(8 AS Decimal(5, 0)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(9, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), CAST(8.00 AS Decimal(13, 2)), NULL)
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (14, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Orange', CAST(10 AS Decimal(5, 0)), CAST(650.00 AS Decimal(9, 2)), CAST(150.00 AS Decimal(9, 2)), CAST(97500.00 AS Decimal(13, 2)), CAST(780.00 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(7.15 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (15, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, N'WATERMELEN', CAST(10 AS Decimal(5, 0)), CAST(120.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), CAST(1200.00 AS Decimal(13, 2)), CAST(9.60 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(1.32 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (16, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Peru', CAST(10 AS Decimal(5, 0)), CAST(51.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(9, 2)), CAST(1275.00 AS Decimal(13, 2)), CAST(10.20 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(0.56 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (17, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(10 AS Decimal(5, 0)), CAST(21.00 AS Decimal(9, 2)), CAST(21.00 AS Decimal(9, 2)), CAST(441.00 AS Decimal(13, 2)), CAST(3.53 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(0.23 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (18, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'WATERMELEN', CAST(10 AS Decimal(5, 0)), CAST(21.00 AS Decimal(9, 2)), CAST(21.00 AS Decimal(9, 2)), CAST(441.00 AS Decimal(13, 2)), CAST(3.53 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(0.23 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (19, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Orange', CAST(1 AS Decimal(5, 0)), CAST(2.00 AS Decimal(9, 2)), CAST(2.00 AS Decimal(9, 2)), CAST(4.00 AS Decimal(13, 2)), CAST(0.03 AS Decimal(13, 2)), CAST(1.90 AS Decimal(13, 2)), CAST(0.02 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (20, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Peru', CAST(4 AS Decimal(5, 0)), CAST(5.00 AS Decimal(9, 2)), CAST(5.00 AS Decimal(9, 2)), CAST(25.00 AS Decimal(13, 2)), CAST(0.20 AS Decimal(13, 2)), CAST(7.60 AS Decimal(13, 2)), CAST(0.05 AS Decimal(13, 2)))
INSERT [dbo].[purtran.old] ([PurTranId], [serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lapmc], [lhamali], [lmaplevy]) VALUES (21, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, N'Peru', CAST(10 AS Decimal(5, 0)), CAST(10.00 AS Decimal(9, 2)), CAST(10.00 AS Decimal(9, 2)), CAST(50.00 AS Decimal(13, 2)), CAST(0.40 AS Decimal(13, 2)), CAST(19.00 AS Decimal(13, 2)), CAST(0.11 AS Decimal(13, 2)))
SET IDENTITY_INSERT [dbo].[purtran.old] OFF
/****** Object:  Table [dbo].[pmast]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pmast](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[pbillno] [int] NULL,
	[date] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
	[lotno] [nvarchar](10) NULL,
	[tamount] [decimal](13, 2) NULL,
	[tapmc] [decimal](13, 2) NULL,
	[thamali] [decimal](13, 2) NULL,
	[tmapLevy] [decimal](13, 4) NULL,
	[ttolai] [decimal](13, 2) NULL,
	[tbharai] [decimal](13, 2) NULL,
	[tutrai] [decimal](13, 2) NULL,
	[tmotorfrt] [decimal](13, 2) NULL,
	[grossamt] [decimal](13, 2) NULL,
	[roundoff] [decimal](13, 2) NULL,
	[netamt] [decimal](13, 2) NULL,
	[amtpaid_NA] [decimal](13, 2) NULL,
	[co] [nvarchar](2) NULL,
	[alpha] [nvarchar](5) NULL,
	[cmonth] [nvarchar](15) NULL,
	[trnscd] [nvarchar](7) NULL,
 CONSTRAINT [PK__pmast__2BFE89A6] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[pmast] ([serno], [compid], [userid], [doccd], [pbillno], [date], [acccd], [lotno], [tamount], [tapmc], [thamali], [tmapLevy], [ttolai], [tbharai], [tutrai], [tmotorfrt], [grossamt], [roundoff], [netamt], [amtpaid_NA], [co], [alpha], [cmonth], [trnscd]) VALUES (1, 6, N'sagar', N'PU', 7, CAST(0x0000A3EC00000000 AS DateTime), N'0000077', N'PU-1', CAST(549.27 AS Decimal(13, 2)), CAST(21.98 AS Decimal(13, 2)), CAST(136.00 AS Decimal(13, 2)), CAST(320.0000 AS Decimal(13, 4)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(1027.25 AS Decimal(13, 2)), CAST(0.75 AS Decimal(13, 2)), CAST(1028.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, N'')
INSERT [dbo].[pmast] ([serno], [compid], [userid], [doccd], [pbillno], [date], [acccd], [lotno], [tamount], [tapmc], [thamali], [tmapLevy], [ttolai], [tbharai], [tutrai], [tmotorfrt], [grossamt], [roundoff], [netamt], [amtpaid_NA], [co], [alpha], [cmonth], [trnscd]) VALUES (2, 6, N'sagar', N'PU', 123, CAST(0x0000A3FA00000000 AS DateTime), N'0000095', N'PU-2', CAST(25.00 AS Decimal(13, 2)), CAST(1.00 AS Decimal(13, 2)), CAST(16.00 AS Decimal(13, 2)), CAST(20.0000 AS Decimal(13, 4)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(62.00 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(62.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, N'')
/****** Object:  Table [dbo].[stock]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stock](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[date] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
	[lotno] [nvarchar](10) NULL,
	[sublotno] [nvarchar](10) NULL,
	[tserno] [int] NOT NULL,
	[itmcd] [int] NULL,
	[item] [nvarchar](20) NULL,
	[qty] [decimal](5, 0) NULL,
	[weight] [decimal](9, 2) NULL,
	[rate] [decimal](9, 2) NULL,
	[amount] [decimal](13, 2) NULL,
	[lbharai] [decimal](13, 2) NULL,
	[lutrai] [decimal](13, 2) NULL,
	[lapmc] [decimal](13, 2) NULL,
	[lhamali] [decimal](13, 2) NULL,
	[ltolai] [decimal](13, 2) NULL,
	[opqty] [decimal](5, 0) NULL,
	[salqty] [decimal](5, 0) NULL,
	[balqty] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (1, 1, N'abc', N'PU', CAST(0x0000A22F00000000 AS DateTime), N'0000077', N'PU-1', N'PU-1-1', 1, 1, N'ONION', CAST(10 AS Decimal(5, 0)), CAST(650.00 AS Decimal(9, 2)), CAST(250.00 AS Decimal(9, 2)), CAST(16250.00 AS Decimal(13, 2)), NULL, NULL, CAST(130.00 AS Decimal(13, 2)), CAST(7.15 AS Decimal(13, 2)), CAST(17.90 AS Decimal(13, 2)), CAST(10 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(10 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (2, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000079', N'PU-2', N'PU-2-1', 1, 1, N'ONION', CAST(5 AS Decimal(5, 0)), CAST(260.00 AS Decimal(9, 2)), CAST(480.00 AS Decimal(9, 2)), CAST(12480.00 AS Decimal(13, 2)), NULL, NULL, CAST(99.84 AS Decimal(13, 2)), CAST(2.86 AS Decimal(13, 2)), CAST(8.95 AS Decimal(13, 2)), CAST(5 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(5 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (2, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000079', N'PU-2', N'PU-2-2', 2, 2, N'POTATO', CAST(5 AS Decimal(5, 0)), CAST(245.00 AS Decimal(9, 2)), CAST(120.00 AS Decimal(9, 2)), CAST(1470.00 AS Decimal(13, 2)), NULL, NULL, CAST(11.76 AS Decimal(13, 2)), CAST(2.69 AS Decimal(13, 2)), CAST(8.95 AS Decimal(13, 2)), CAST(5 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(5 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (3, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000078', N'PU-3', N'PU-3-1', 1, 2, N'POTATO', CAST(20 AS Decimal(5, 0)), CAST(1250.00 AS Decimal(9, 2)), CAST(115.00 AS Decimal(9, 2)), CAST(7187.50 AS Decimal(13, 2)), NULL, NULL, CAST(57.50 AS Decimal(13, 2)), CAST(13.75 AS Decimal(13, 2)), CAST(35.80 AS Decimal(13, 2)), CAST(20 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(20 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (4, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000078', N'PU-4', N'PU-4-1', 1, 3, N'GARLIC', CAST(1 AS Decimal(5, 0)), CAST(65.00 AS Decimal(9, 2)), CAST(450.00 AS Decimal(9, 2)), CAST(2925.00 AS Decimal(13, 2)), NULL, NULL, CAST(23.40 AS Decimal(13, 2)), CAST(0.65 AS Decimal(13, 2)), CAST(0.00 AS Decimal(13, 2)), CAST(1 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(1 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (4, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000078', N'PU-4', N'PU-4-2', 2, 2, N'POTATO', CAST(5 AS Decimal(5, 0)), CAST(265.00 AS Decimal(9, 2)), CAST(124.00 AS Decimal(9, 2)), CAST(1643.00 AS Decimal(13, 2)), NULL, NULL, CAST(13.14 AS Decimal(13, 2)), CAST(2.92 AS Decimal(13, 2)), CAST(8.95 AS Decimal(13, 2)), CAST(5 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(5 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (5, 1, N'abc', N'PU', CAST(0x0000A23900000000 AS DateTime), N'0000077', N'PU-5', N'PU-5-1', 1, 1, N'ONION', CAST(3 AS Decimal(5, 0)), CAST(165.00 AS Decimal(9, 2)), CAST(400.00 AS Decimal(9, 2)), CAST(6600.00 AS Decimal(13, 2)), NULL, NULL, CAST(52.80 AS Decimal(13, 2)), CAST(1.81 AS Decimal(13, 2)), CAST(5.37 AS Decimal(13, 2)), CAST(3 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(3 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (6, 1, N'abc', N'PU', CAST(0x0000A23F00000000 AS DateTime), N'0000079', N'PU-6', N'PU-6-1', 1, 1, N'ONION', CAST(1 AS Decimal(5, 0)), CAST(60.00 AS Decimal(9, 2)), CAST(440.00 AS Decimal(9, 2)), CAST(2640.00 AS Decimal(13, 2)), NULL, NULL, CAST(21.12 AS Decimal(13, 2)), CAST(1.79 AS Decimal(13, 2)), CAST(0.66 AS Decimal(13, 2)), CAST(1 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(1 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (6, 1, N'abc', N'PU', CAST(0x0000A23F00000000 AS DateTime), N'0000079', N'PU-6', N'PU-6-2', 2, 1, N'ONION', CAST(2 AS Decimal(5, 0)), CAST(120.00 AS Decimal(9, 2)), CAST(450.00 AS Decimal(9, 2)), CAST(5400.00 AS Decimal(13, 2)), NULL, NULL, CAST(43.20 AS Decimal(13, 2)), CAST(3.58 AS Decimal(13, 2)), CAST(1.32 AS Decimal(13, 2)), CAST(2 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(2 AS Decimal(5, 0)))
INSERT [dbo].[stock] ([serno], [compid], [userid], [doccd], [date], [acccd], [lotno], [sublotno], [tserno], [itmcd], [item], [qty], [weight], [rate], [amount], [lbharai], [lutrai], [lapmc], [lhamali], [ltolai], [opqty], [salqty], [balqty]) VALUES (7, 2, N'abc', N'PU', CAST(0x0000A2F400000000 AS DateTime), N'0000077', N'PU-7', N'PU-7-1', 1, 1, N'ONION', CAST(23 AS Decimal(5, 0)), CAST(20.00 AS Decimal(9, 2)), CAST(2220.00 AS Decimal(9, 2)), CAST(22.00 AS Decimal(13, 2)), NULL, NULL, CAST(212.00 AS Decimal(13, 2)), CAST(11.00 AS Decimal(13, 2)), CAST(11.00 AS Decimal(13, 2)), CAST(23 AS Decimal(5, 0)), CAST(0 AS Decimal(5, 0)), CAST(23 AS Decimal(5, 0)))
/****** Object:  Table [dbo].[TransactionMaster]    Script Date: 01/11/2015 19:06:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionMaster](
	[TransactionMasterID] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[date] [datetime] NULL,
	[rp] [bit] NULL,
	[casbankCode] [nvarchar](7) NULL,
	[TransType] [bit] NULL,
	[AcCode] [nvarchar](7) NULL,
	[chqno] [int] NULL,
	[narrcd] [nvarchar](3) NULL,
	[createdOn] [datetime] NULL,
	[BankName] [nvarchar](50) NULL,
	[BankBranch] [nvarchar](50) NULL,
	[BankChqNo] [int] NULL,
	[BankChqDate] [datetime] NULL,
 CONSTRAINT [PK__payment__DFD1E8D737703C52] PRIMARY KEY CLUSTERED 
(
	[TransactionMasterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=receipt, 1=payment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionMaster', @level2type=N'COLUMN',@level2name=N'rp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=Bank, 1= Cash' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionMaster', @level2type=N'COLUMN',@level2name=N'TransType'
GO
INSERT [dbo].[TransactionMaster] ([TransactionMasterID], [compid], [userid], [doccd], [date], [rp], [casbankCode], [TransType], [AcCode], [chqno], [narrcd], [createdOn], [BankName], [BankBranch], [BankChqNo], [BankChqDate]) VALUES (1, 6, N'sagar', N'PP', CAST(0x0000A3C200000000 AS DateTime), 1, N'0000097', 1, N'0000077', 0, N'', CAST(0x0000A3C201247086 AS DateTime), N'', N'', 0, CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[TransactionMaster] ([TransactionMasterID], [compid], [userid], [doccd], [date], [rp], [casbankCode], [TransType], [AcCode], [chqno], [narrcd], [createdOn], [BankName], [BankBranch], [BankChqNo], [BankChqDate]) VALUES (2, 6, N'sagar', N'PP', CAST(0x0000A3C200000000 AS DateTime), 1, N'0000097', 1, N'0000077', 0, N'', CAST(0x0000A3C20129DDC0 AS DateTime), N'', N'', 0, CAST(0x0000000000000000 AS DateTime))
/****** Object:  Table [dbo].[smast]    Script Date: 01/11/2015 19:06:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[smast](
	[serno] [int] NOT NULL,
	[compid] [int] NULL,
	[userid] [nvarchar](20) NULL,
	[doccd] [nvarchar](2) NULL,
	[challanNo] [int] NULL,
	[ChallanDate] [datetime] NULL,
	[InvoiceDate] [datetime] NULL,
	[acccd] [nvarchar](7) NULL,
	[lotno] [nvarchar](10) NULL,
	[tamount] [decimal](13, 2) NULL,
	[hamali] [decimal](13, 2) NULL,
	[tolai] [decimal](13, 2) NULL,
	[bharai] [decimal](13, 2) NULL,
	[utrai] [decimal](13, 2) NULL,
	[motorfrt] [decimal](13, 2) NULL,
	[grossamt] [decimal](13, 2) NULL,
	[apmc] [decimal](13, 2) NULL,
	[roundoff] [decimal](13, 2) NULL,
	[netamt] [decimal](13, 2) NULL,
	[amtrecd] [decimal](13, 2) NULL,
	[co] [nvarchar](2) NULL,
	[aplha] [nvarchar](5) NULL,
	[cmonth] [nvarchar](15) NULL,
	[CreatedOn] [datetime] NULL,
	[YearId] [nchar](20) NULL,
	[LastUpdatedOn] [datetime] NULL,
	[LastUpdatedBy] [nchar](10) NULL,
 CONSTRAINT [PK__smast__DFD1E8D758D1301D] PRIMARY KEY CLUSTERED 
(
	[serno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[smast] ([serno], [compid], [userid], [doccd], [challanNo], [ChallanDate], [InvoiceDate], [acccd], [lotno], [tamount], [hamali], [tolai], [bharai], [utrai], [motorfrt], [grossamt], [apmc], [roundoff], [netamt], [amtrecd], [co], [aplha], [cmonth], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (1, 6, N'sagar', N'SA', 3, CAST(0x0000A3FA00000000 AS DateTime), CAST(0x0000A3FA00000000 AS DateTime), N'0000083', N'SA-1', CAST(229.17 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, NULL, CAST(231.46 AS Decimal(13, 2)), CAST(2.29 AS Decimal(13, 2)), CAST(0.54 AS Decimal(13, 2)), CAST(232.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, CAST(0x0000A3FA00E99C4C AS DateTime), N'2014-2015           ', CAST(0x0000A3FA00E99C4C AS DateTime), N'sagar     ')
INSERT [dbo].[smast] ([serno], [compid], [userid], [doccd], [challanNo], [ChallanDate], [InvoiceDate], [acccd], [lotno], [tamount], [hamali], [tolai], [bharai], [utrai], [motorfrt], [grossamt], [apmc], [roundoff], [netamt], [amtrecd], [co], [aplha], [cmonth], [CreatedOn], [YearId], [LastUpdatedOn], [LastUpdatedBy]) VALUES (2, 6, N'sagar', N'SA', 4, CAST(0x0000A3FA00000000 AS DateTime), CAST(0x0000A3FA00000000 AS DateTime), N'0000094', N'SA-2', CAST(108.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, NULL, CAST(109.08 AS Decimal(13, 2)), CAST(1.08 AS Decimal(13, 2)), CAST(0.92 AS Decimal(13, 2)), CAST(110.00 AS Decimal(13, 2)), NULL, NULL, NULL, NULL, CAST(0x0000A3FA00E9AB88 AS DateTime), N'2014-2015           ', CAST(0x0000A3FA00E9AB88 AS DateTime), N'sagar     ')
/****** Object:  StoredProcedure [dbo].[TransactionMaster_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionMaster_UpdateByPK]

		@TransactionMasterID 		int,
		@compid              		int,
		@userid              		nvarchar (20),
		@date                		datetime,
		@rp                  		bit,
		@casbankCode             		nvarchar (7),
		@TransType           		bit,
		@AcCode              		nvarchar (7),
		@chqno               		int=0,
		@narrcd              		nvarchar (3)='',
		@createdOn           		datetime,
		@BankName					nvarchar (50)='',
		@BankBranch					nvarchar(50)='',
		@BankChqNo					int =0,
		@BankChqDate				datetime=''
AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[TransactionMaster]
SET
		[compid] = @compid,
		[userid] = @userid,
		[date] = @date,
		[rp] = @rp,
		casbankCode = @casbankCode,
		[TransType] = @TransType,
		[AcCode] = @AcCode,
		[chqno] = @chqno,
		[narrcd] = @narrcd,
		[createdOn] = @createdOn,
		[BankName]=@BankName,
		[BankBranch]=@BankBranch,
		[BankChqNo]=@BankChqNo,
		[BankChqDate]=@BankChqDate
WHERE [dbo].[TransactionMaster].[TransactionMasterID] = @TransactionMasterID

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[TransactionMaster_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_TransactionMaster_Insert] @TransactionMasterID = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @rp = 1, @casbank = 'casbank', @TransType = 1, @AcCode = 'AcCode', @amount = 1, @chqno = 1, @narrcd = 'narrcd', @createdOn = '2014-06-14'

CREATE PROCEDURE [dbo].[TransactionMaster_Insert]

		@TransactionMasterID 		int=0 OUTPUT,
		@compid              		int,
		@userid              		nvarchar (20),
		@doccd               		nvarchar (2)='',
		@date                		datetime,
		@rp                  		bit,
		@casbankCode           		nvarchar (7)='',
		@TransType           		bit,
		@AcCode              		nvarchar (7),
		@chqno               		int=0,
		@narrcd              		nvarchar (3)='',
		@createdOn           		datetime,
		@BankName					nvarchar (50)='',
		@BankBranch					nvarchar(50)='',
		@BankChqNo					int =0,
		@BankChqDate				datetime=''

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @TransactionMasterID = ISNULL(MAX([TransactionMasterID]),0) + 1 FROM [dbo].[TransactionMaster]
if @rp=0
begin
select @doccd= 'RC'
end
else
begin
select @doccd= 'PP'
end

INSERT [dbo].[TransactionMaster]
(
		[TransactionMasterID],
		[compid],
		[userid],
		[doccd],
		[date],
		[rp],
		[casbankCode],
		[TransType],
		[AcCode],
		[chqno],
		[narrcd],
		[createdOn],
		[BankName],
		[BankBranch],
		[BankChqNo],
		[BankChqDate]
)
VALUES
(
		@TransactionMasterID,
		@compid,
		@userid,
		@doccd,
		@date,
		@rp,
		@casbankCode,
		@TransType,
		@AcCode,
		@chqno,
		@narrcd,
		@createdOn,
		@BankName,
		@BankBranch,
		@BankChqNo,
		@BankChqDate
)
SELECT @TransactionMasterID
COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_TransactionMaster_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_TransactionMaster_DeleteByPK] @TransactionMasterID = 1

CREATE PROCEDURE [dbo].[PR_TransactionMaster_DeleteByPK]

		@TransactionMasterID 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[TransactionMaster]
WHERE [dbo].[TransactionMaster].[TransactionMasterID] = @TransactionMasterID


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_stock_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_stock_SelectAll]

CREATE PROCEDURE [dbo].[PR_stock_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[stock].[serno],
		[dbo].[stock].[compid],
		[dbo].[stock].[userid],
		[dbo].[stock].[doccd],
		[dbo].[stock].[date],
		[dbo].[stock].[acccd],
		[dbo].[stock].[lotno],
		[dbo].[stock].[sublotno],
		[dbo].[stock].[tserno],
		[dbo].[stock].[itmcd],
		[dbo].[stock].[item],
		[dbo].[stock].[qty],
		[dbo].[stock].[weight],
		[dbo].[stock].[rate],
		[dbo].[stock].[amount],
		[dbo].[stock].[lbharai],
		[dbo].[stock].[lutrai],
		[dbo].[stock].[lapmc],
		[dbo].[stock].[lhamali],
		[dbo].[stock].[ltolai],
		[dbo].[stock].[opqty],
		[dbo].[stock].[salqty],
		[dbo].[stock].[balqty]
FROM  [dbo].[stock]
GO
/****** Object:  StoredProcedure [dbo].[PR_stock_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_stock_Insert] @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @acccd = 'acccd', @lotno = 'lotno', @sublotno = 'sublotno', @tserno = 1, @itmcd = 1, @item = 'item', @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lbharai = 1, @lutrai = 1, @lapmc = 1, @lhamali = 1, @ltolai = 1, @opqty = 1, @salqty = 1, @balqty = 1

CREATE PROCEDURE [dbo].[PR_stock_Insert]

		@serno    		int,
		@compid   		int,
		@userid   		nvarchar (20),
		@doccd    		nvarchar (2),
		@date     		datetime,
		@acccd    		nvarchar (7),
		@lotno    		nvarchar (10),
		@sublotno 		nvarchar (10),
		@tserno   		int,
		@itmcd    		int,
		@item     		nvarchar (20),
		@qty      		decimal (5, 0),
		@weight   		decimal (9, 2),
		@rate     		decimal (9, 2),
		@amount   		decimal (13, 2),
		@lbharai  		decimal (13, 2),
		@lutrai   		decimal (13, 2),
		@lapmc    		decimal (13, 2),
		@lhamali  		decimal (13, 2),
		@ltolai   		decimal (13, 2),
		@opqty    		decimal (5, 0),
		@salqty   		decimal (5, 0),
		@balqty   		decimal (5, 0)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[stock]
(
		[serno],
		[compid],
		[userid],
		[doccd],
		[date],
		[acccd],
		[lotno],
		[sublotno],
		[tserno],
		[itmcd],
		[item],
		[qty],
		[weight],
		[rate],
		[amount],
		[lbharai],
		[lutrai],
		[lapmc],
		[lhamali],
		[ltolai],
		[opqty],
		[salqty],
		[balqty]
)
VALUES
(
		@serno,
		@compid,
		@userid,
		@doccd,
		@date,
		@acccd,
		@lotno,
		@sublotno,
		@tserno,
		@itmcd,
		@item,
		@qty,
		@weight,
		@rate,
		@amount,
		@lbharai,
		@lutrai,
		@lapmc,
		@lhamali,
		@ltolai,
		@opqty,
		@salqty,
		@balqty
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[InsertJournal]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertJournal]
@serno int,
@tserno int,
@compid int,
@userid nvarchar(20),
@doccd nvarchar(2)='JE',
@date datetime,
@rp nvarchar(2),
@acccd nvarchar(7),
@crdr nvarchar(2)=null,
@cramt decimal(13, 2)=null,
@dramt decimal(13, 2)=null,
@narrcd nvarchar(200),
@lotno nvarchar(10)=null

AS
BEGIN
	SET NOCOUNT ON;
IF EXISTS (select tserno from journal where tserno= @tserno and serno=@serno)
        UPDATE journal
        set 
        serno=@serno,
        compid=@compid,
        userid=@userid,
        doccd=@doccd,
		date=@date,
		rp=@rp,
		acccd=@acccd,
		crdr=@crdr,
		cramt=@cramt,
		dramt=@dramt,
		narrcd=@narrcd,
		lotno=@lotno
		where tserno = @tserno
		and serno=@serno
      ELSE
        INSERT INTO journal(serno,tserno,compid,userid,doccd,date,rp,acccd,crdr,cramt,dramt,narrcd,lotno)
        VALUES(@serno,@tserno,@compid,@userid,@doccd,@date,@rp,@acccd,@crdr,@cramt,@dramt,@narrcd,@lotno)
END
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_UpdateByPK] @itmserno = 1, @tserno = 1, @serno = 1, @compid = 1, @userid = 'userid', @itmcd = 1, @rate = 1

CREATE PROCEDURE [dbo].[PR_ratetran_UpdateByPK]

		@itmserno 		int,
		@tserno   		int,
		@serno    		int,
		@compid   		int,
		@userid   		nvarchar (20),
		@itmcd    		int,
		@rate     		decimal (9, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[ratetran]
SET
		[tserno] = @tserno,
		[serno] = @serno,
		[compid] = @compid,
		[userid] = @userid,
		[itmcd] = @itmcd,
		[rate] = @rate
WHERE [dbo].[ratetran].[itmserno] = @itmserno

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_SelectViewByPK] @itmserno = 1

CREATE PROCEDURE [dbo].[PR_ratetran_SelectViewByPK]

		@itmserno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratetran].[itmserno],
		[dbo].[ratetran].[tserno],
		[dbo].[ratetran].[serno],
		[dbo].[ratetran].[compid],
		[dbo].[ratetran].[userid],
		[dbo].[ratetran].[itmcd],
		[dbo].[ratetran].[rate]
FROM  [dbo].[ratetran]

WHERE [dbo].[ratetran].[itmserno] = @itmserno
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_SelectByPK] @itmserno = 1

CREATE PROCEDURE [dbo].[PR_ratetran_SelectByPK]

		@itmserno 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratetran].[itmserno],
		[dbo].[ratetran].[tserno],
		[dbo].[ratetran].[serno],
		[dbo].[ratetran].[compid],
		[dbo].[ratetran].[userid],
		[dbo].[ratetran].[itmcd],
		[dbo].[ratetran].[rate]
FROM  [dbo].[ratetran]

WHERE [dbo].[ratetran].[itmserno] = @itmserno
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_SelectAll]

CREATE PROCEDURE [dbo].[PR_ratetran_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[ratetran].[itmserno],
		[dbo].[ratetran].[tserno],
		[dbo].[ratetran].[serno],
		[dbo].[ratetran].[compid],
		[dbo].[ratetran].[userid],
		[dbo].[ratetran].[itmcd],
		[dbo].[ratetran].[rate]
FROM  [dbo].[ratetran]
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_Insert] @itmserno = 1, @tserno = 1, @serno = 1, @compid = 1, @userid = 'userid', @itmcd = 1, @rate = 1

CREATE PROCEDURE [dbo].[PR_ratetran_Insert]

		@itmserno 		int OUTPUT,
		@tserno   		int,
		@serno    		int,
		@compid   		int,
		@userid   		nvarchar (20),
		@itmcd    		int,
		@rate     		decimal (9, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @itmserno = ISNULL(MAX([itmserno]),0) + 1 FROM [dbo].[ratetran]


INSERT [dbo].[ratetran]
(
		[itmserno],
		[tserno],
		[serno],
		[compid],
		[userid],
		[itmcd],
		[rate]
)
VALUES
(
		@itmserno,
		@tserno,
		@serno,
		@compid,
		@userid,
		@itmcd,
		@rate
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_ratetran_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_ratetran_DeleteByPK] @itmserno = 1

CREATE PROCEDURE [dbo].[PR_ratetran_DeleteByPK]

		@itmserno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[ratetran]
WHERE [dbo].[ratetran].[itmserno] = @itmserno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_UpdateByPK] @acccd = 'acccd', @compid = 1, @userid = 'userid', @grcd = 'grcd', @acname = 'acname', @mjcd = 'mjcd', @lf = 1, @opbal = 1, @sign12 = 'sign12'

CREATE PROCEDURE [dbo].[PR_openbal_UpdateByPK]

		@acccd  		nvarchar (7),
		@compid 		int,
		@userid 		nvarchar (20),
		@grcd   		nvarchar (2),
		@acname 		nvarchar (50),
		@mjcd   		nvarchar (3),
		@lf     		int,
		@opbal  		decimal (13, 2),
		@sign12 		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[openbal]
SET
		[compid] = @compid,
		[userid] = @userid,
		[grcd] = @grcd,
		[acname] = @acname,
		[mjcd] = @mjcd,
		[lf] = @lf,
		[opbal] = @opbal,
		[sign12] = @sign12
WHERE [dbo].[openbal].[acccd] = @acccd

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_SelectViewByPK] @acccd = 'acccd'

CREATE PROCEDURE [dbo].[PR_openbal_SelectViewByPK]

		@acccd 		nvarchar (7)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[openbal].[acccd],
		[dbo].[openbal].[compid],
		[dbo].[openbal].[userid],
		[dbo].[openbal].[grcd],
		[dbo].[openbal].[acname],
		[dbo].[openbal].[mjcd],
		[dbo].[openbal].[lf],
		[dbo].[openbal].[opbal],
		[dbo].[openbal].[sign12]
FROM  [dbo].[openbal]

WHERE [dbo].[openbal].[acccd] = @acccd
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_SelectByPK] @acccd = 'acccd'

CREATE PROCEDURE [dbo].[PR_openbal_SelectByPK]

		@acccd 		nvarchar (7)

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[openbal].[acccd],
		[dbo].[openbal].[compid],
		[dbo].[openbal].[userid],
		[dbo].[openbal].[grcd],
		[dbo].[openbal].[acname],
		[dbo].[openbal].[mjcd],
		[dbo].[openbal].[lf],
		[dbo].[openbal].[opbal],
		[dbo].[openbal].[sign12]
FROM  [dbo].[openbal]

WHERE [dbo].[openbal].[acccd] = @acccd
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_SelectAll]

CREATE PROCEDURE [dbo].[PR_openbal_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[openbal].[acccd],
		[dbo].[openbal].[compid],
		[dbo].[openbal].[userid],
		[dbo].[openbal].[grcd],
		[dbo].[openbal].[acname],
		[dbo].[openbal].[mjcd],
		[dbo].[openbal].[lf],
		[dbo].[openbal].[opbal],
		[dbo].[openbal].[sign12]
FROM  [dbo].[openbal]
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_Insert] @acccd = 'acccd', @compid = 1, @userid = 'userid', @grcd = 'grcd', @acname = 'acname', @mjcd = 'mjcd', @lf = 1, @opbal = 1, @sign12 = 'sign12'

CREATE PROCEDURE [dbo].[PR_openbal_Insert]

		@acccd  		nvarchar (7) OUTPUT,
		@compid 		int,
		@userid 		nvarchar (20),
		@grcd   		nvarchar (2),
		@acname 		nvarchar (50),
		@mjcd   		nvarchar (3),
		@lf     		int,
		@opbal  		decimal (13, 2),
		@sign12 		nvarchar (2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[openbal]
(
		[acccd],
		[compid],
		[userid],
		[grcd],
		[acname],
		[mjcd],
		[lf],
		[opbal],
		[sign12]
)
VALUES
(
		@acccd,
		@compid,
		@userid,
		@grcd,
		@acname,
		@mjcd,
		@lf,
		@opbal,
		@sign12
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_openbal_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_openbal_DeleteByPK] @acccd = 'acccd'

CREATE PROCEDURE [dbo].[PR_openbal_DeleteByPK]

		@acccd 		nvarchar (7)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[openbal]
WHERE [dbo].[openbal].[acccd] = @acccd


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_UpdateByPK] @PurTranId = 1, @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @acccd = 'acccd', @lotno = 'lotno', @sublotno = 'sublotno', @tserno = 1, @itmcd = 1, @item = 'item', @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lapmc = 1, @lhamali = 1, @lmaplevy = 1

CREATE PROCEDURE [dbo].[PR_purtran.old_UpdateByPK]

		@PurTranId 		int,
		@serno     		int,
		@compid    		int,
		@userid    		nvarchar (20),
		@doccd     		nvarchar (2),
		@date      		datetime,
		@acccd     		nvarchar (7),
		@lotno     		nvarchar (10),
		@sublotno  		nvarchar (10),
		@tserno    		int,
		@itmcd     		int,
		@item      		nvarchar (20),
		@qty       		decimal (5, 0),
		@weight    		decimal (9, 2),
		@rate      		decimal (9, 2),
		@amount    		decimal (13, 2),
		@lapmc     		decimal (13, 2),
		@lhamali   		decimal (13, 2),
		@lmaplevy  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[purtran.old]
SET
		[serno] = @serno,
		[compid] = @compid,
		[userid] = @userid,
		[doccd] = @doccd,
		[date] = @date,
		[acccd] = @acccd,
		[lotno] = @lotno,
		[sublotno] = @sublotno,
		[tserno] = @tserno,
		[itmcd] = @itmcd,
		[item] = @item,
		[qty] = @qty,
		[weight] = @weight,
		[rate] = @rate,
		[amount] = @amount,
		[lapmc] = @lapmc,
		[lhamali] = @lhamali,
		[lmaplevy] = @lmaplevy
WHERE [dbo].[purtran.old].[PurTranId] = @PurTranId

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_SelectViewByPK] @PurTranId = 1

CREATE PROCEDURE [dbo].[PR_purtran.old_SelectViewByPK]

		@PurTranId 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran.old].[PurTranId],
		[dbo].[purtran.old].[serno],
		[dbo].[purtran.old].[compid],
		[dbo].[purtran.old].[userid],
		[dbo].[purtran.old].[doccd],
		[dbo].[purtran.old].[date],
		[dbo].[purtran.old].[acccd],
		[dbo].[purtran.old].[lotno],
		[dbo].[purtran.old].[sublotno],
		[dbo].[purtran.old].[tserno],
		[dbo].[purtran.old].[itmcd],
		[dbo].[purtran.old].[item],
		[dbo].[purtran.old].[qty],
		[dbo].[purtran.old].[weight],
		[dbo].[purtran.old].[rate],
		[dbo].[purtran.old].[amount],
		[dbo].[purtran.old].[lapmc],
		[dbo].[purtran.old].[lhamali],
		[dbo].[purtran.old].[lmaplevy]
FROM  [dbo].[purtran.old]

WHERE [dbo].[purtran.old].[PurTranId] = @PurTranId
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_SelectByPK] @PurTranId = 1

CREATE PROCEDURE [dbo].[PR_purtran.old_SelectByPK]

		@PurTranId 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran.old].[PurTranId],
		[dbo].[purtran.old].[serno],
		[dbo].[purtran.old].[compid],
		[dbo].[purtran.old].[userid],
		[dbo].[purtran.old].[doccd],
		[dbo].[purtran.old].[date],
		[dbo].[purtran.old].[acccd],
		[dbo].[purtran.old].[lotno],
		[dbo].[purtran.old].[sublotno],
		[dbo].[purtran.old].[tserno],
		[dbo].[purtran.old].[itmcd],
		[dbo].[purtran.old].[item],
		[dbo].[purtran.old].[qty],
		[dbo].[purtran.old].[weight],
		[dbo].[purtran.old].[rate],
		[dbo].[purtran.old].[amount],
		[dbo].[purtran.old].[lapmc],
		[dbo].[purtran.old].[lhamali],
		[dbo].[purtran.old].[lmaplevy]
FROM  [dbo].[purtran.old]

WHERE [dbo].[purtran.old].[PurTranId] = @PurTranId
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_SelectAll]

CREATE PROCEDURE [dbo].[PR_purtran.old_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[purtran.old].[PurTranId],
		[dbo].[purtran.old].[serno],
		[dbo].[purtran.old].[compid],
		[dbo].[purtran.old].[userid],
		[dbo].[purtran.old].[doccd],
		[dbo].[purtran.old].[date],
		[dbo].[purtran.old].[acccd],
		[dbo].[purtran.old].[lotno],
		[dbo].[purtran.old].[sublotno],
		[dbo].[purtran.old].[tserno],
		[dbo].[purtran.old].[itmcd],
		[dbo].[purtran.old].[item],
		[dbo].[purtran.old].[qty],
		[dbo].[purtran.old].[weight],
		[dbo].[purtran.old].[rate],
		[dbo].[purtran.old].[amount],
		[dbo].[purtran.old].[lapmc],
		[dbo].[purtran.old].[lhamali],
		[dbo].[purtran.old].[lmaplevy]
FROM  [dbo].[purtran.old]
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_Insert]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_Insert] @PurTranId = 1, @serno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @acccd = 'acccd', @lotno = 'lotno', @sublotno = 'sublotno', @tserno = 1, @itmcd = 1, @item = 'item', @qty = 1, @weight = 1, @rate = 1, @amount = 1, @lapmc = 1, @lhamali = 1, @lmaplevy = 1

CREATE PROCEDURE [dbo].[PR_purtran.old_Insert]

		@PurTranId 		int OUTPUT,
		@serno     		int,
		@compid    		int,
		@userid    		nvarchar (20),
		@doccd     		nvarchar (2),
		@date      		datetime,
		@acccd     		nvarchar (7),
		@lotno     		nvarchar (10),
		@sublotno  		nvarchar (10),
		@tserno    		int,
		@itmcd     		int,
		@item      		nvarchar (20),
		@qty       		decimal (5, 0),
		@weight    		decimal (9, 2),
		@rate      		decimal (9, 2),
		@amount    		decimal (13, 2),
		@lapmc     		decimal (13, 2),
		@lhamali   		decimal (13, 2),
		@lmaplevy  		decimal (13, 2)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @PurTranId = ISNULL(MAX([PurTranId]),0) + 1 FROM [dbo].[purtran.old]


INSERT [dbo].[purtran.old]
(
		[PurTranId],
		[serno],
		[compid],
		[userid],
		[doccd],
		[date],
		[acccd],
		[lotno],
		[sublotno],
		[tserno],
		[itmcd],
		[item],
		[qty],
		[weight],
		[rate],
		[amount],
		[lapmc],
		[lhamali],
		[lmaplevy]
)
VALUES
(
		@PurTranId,
		@serno,
		@compid,
		@userid,
		@doccd,
		@date,
		@acccd,
		@lotno,
		@sublotno,
		@tserno,
		@itmcd,
		@item,
		@qty,
		@weight,
		@rate,
		@amount,
		@lapmc,
		@lhamali,
		@lmaplevy
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_purtran.old_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_purtran.old_DeleteByPK] @PurTranId = 1

CREATE PROCEDURE [dbo].[PR_purtran.old_DeleteByPK]

		@PurTranId 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[purtran.old]
WHERE [dbo].[purtran.old].[PurTranId] = @PurTranId


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_pmast_DeleteByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_pmast_DeleteByPK] @serno = 1

CREATE PROCEDURE [dbo].[PR_pmast_DeleteByPK]

		@serno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[pmast]
WHERE [dbo].[pmast].[serno] = @serno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_UpdateByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_UpdateByPK] @sr = 1, @serno = 1, @tserno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @rp = 'rp', @casbank = 'casbank', @acccd = 'acccd', @crdr = 'crdr', @cramt = 1, @dramt = 1, @narrcd = 'narrcd', @lotno = 'lotno'

CREATE PROCEDURE [dbo].[PR_journal_UpdateByPK]

		@sr      		int,
		@serno   		int,
		@tserno  		int,
		@compid  		int,
		@userid  		nvarchar (20),
		@doccd   		nvarchar (2),
		@date    		datetime,
		@rp      		nvarchar (2),
		@casbank 		nvarchar (7),
		@acccd   		nvarchar (7),
		@crdr    		nvarchar (2),
		@cramt   		decimal (13, 2),
		@dramt   		decimal (13, 2),
		@narrcd  		nvarchar (200),
		@lotno   		nvarchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[journal]
SET
		[serno] = @serno,
		[tserno] = @tserno,
		[compid] = @compid,
		[userid] = @userid,
		[doccd] = @doccd,
		[date] = @date,
		[rp] = @rp,
		[casbank] = @casbank,
		[acccd] = @acccd,
		[crdr] = @crdr,
		[cramt] = @cramt,
		[dramt] = @dramt,
		[narrcd] = @narrcd,
		[lotno] = @lotno
WHERE [dbo].[journal].[sr] = @sr

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectViewByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_SelectViewByPK] @sr = 1

CREATE PROCEDURE [dbo].[PR_journal_SelectViewByPK]

		@sr 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journal].[sr],
		[dbo].[journal].[serno],
		[dbo].[journal].[tserno],
		[dbo].[journal].[compid],
		[dbo].[journal].[userid],
		[dbo].[journal].[doccd],
		[dbo].[journal].[date],
		[dbo].[journal].[rp],
		[dbo].[journal].[casbank],
		[dbo].[journal].[acccd],
		[dbo].[journal].[crdr],
		[dbo].[journal].[cramt],
		[dbo].[journal].[dramt],
		[dbo].[journal].[narrcd],
		[dbo].[journal].[lotno]
FROM  [dbo].[journal]

WHERE [dbo].[journal].[sr] = @sr
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectByPK]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_SelectByPK] @sr = 1

CREATE PROCEDURE [dbo].[PR_journal_SelectByPK]

		@sr 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journal].[sr],
		[dbo].[journal].[serno],
		[dbo].[journal].[tserno],
		[dbo].[journal].[compid],
		[dbo].[journal].[userid],
		[dbo].[journal].[doccd],
		[dbo].[journal].[date],
		[dbo].[journal].[rp],
		[dbo].[journal].[casbank],
		[dbo].[journal].[acccd],
		[dbo].[journal].[crdr],
		[dbo].[journal].[cramt],
		[dbo].[journal].[dramt],
		[dbo].[journal].[narrcd],
		[dbo].[journal].[lotno]
FROM  [dbo].[journal]

WHERE [dbo].[journal].[sr] = @sr
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_SelectAll]    Script Date: 01/11/2015 19:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_SelectAll]

CREATE PROCEDURE [dbo].[PR_journal_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[journal].[sr],
		[dbo].[journal].[serno],
		[dbo].[journal].[tserno],
		[dbo].[journal].[compid],
		[dbo].[journal].[userid],
		[dbo].[journal].[doccd],
		[dbo].[journal].[date],
		[dbo].[journal].[rp],
		[dbo].[journal].[casbank],
		[dbo].[journal].[acccd],
		[dbo].[journal].[crdr],
		[dbo].[journal].[cramt],
		[dbo].[journal].[dramt],
		[dbo].[journal].[narrcd],
		[dbo].[journal].[lotno]
FROM  [dbo].[journal]
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_Insert] @sr = 1, @serno = 1, @tserno = 1, @compid = 1, @userid = 'userid', @doccd = 'doccd', @date = '2014-06-14', @rp = 'rp', @casbank = 'casbank', @acccd = 'acccd', @crdr = 'crdr', @cramt = 1, @dramt = 1, @narrcd = 'narrcd', @lotno = 'lotno'

CREATE PROCEDURE [dbo].[PR_journal_Insert]

		@sr      		int OUTPUT,
		@serno   		int,
		@tserno  		int,
		@compid  		int,
		@userid  		nvarchar (20),
		@doccd   		nvarchar (2),
		@date    		datetime,
		@rp      		nvarchar (2),
		@casbank 		nvarchar (7),
		@acccd   		nvarchar (7),
		@crdr    		nvarchar (2),
		@cramt   		decimal (13, 2),
		@dramt   		decimal (13, 2),
		@narrcd  		nvarchar (200),
		@lotno   		nvarchar (10)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[journal]
(
		[serno],
		[tserno],
		[compid],
		[userid],
		[doccd],
		[date],
		[rp],
		[casbank],
		[acccd],
		[crdr],
		[cramt],
		[dramt],
		[narrcd],
		[lotno]
)
VALUES
(
		@serno,
		@tserno,
		@compid,
		@userid,
		@doccd,
		@date,
		@rp,
		@casbank,
		@acccd,
		@crdr,
		@cramt,
		@dramt,
		@narrcd,
		@lotno
)

SET @sr = @@IDENTITY

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_journal_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_journal_DeleteByPK] @sr = 1

CREATE PROCEDURE [dbo].[PR_journal_DeleteByPK]

		@sr 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[journal]
WHERE [dbo].[journal].[sr] = @sr


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_item_UpdateByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_UpdateByPK] @itmcd = 1, @compid = 1, @userid = 'userid', @item = 'item', @unit = 1, @tare = 1

CREATE PROCEDURE [dbo].[PR_item_UpdateByPK]

		@itmcd  		int,
		@compid 		int,
		@userid 		nvarchar (20),
		@item   		nvarchar (20),
		@unit   		int,
		@tare   		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

UPDATE [dbo].[item]
SET
		[compid] = @compid,
		[userid] = @userid,
		[item] = @item,
		[unit] = @unit,
		[tare] = @tare
WHERE [dbo].[item].[itmcd] = @itmcd

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectViewByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_SelectViewByPK] @itmcd = 1

CREATE PROCEDURE [dbo].[PR_item_SelectViewByPK]

		@itmcd 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[item].[itmcd],
		[dbo].[item].[compid],
		[dbo].[item].[userid],
		[dbo].[item].[item],
		[dbo].[item].[unit],
		[dbo].[item].[tare]
FROM  [dbo].[item]

WHERE [dbo].[item].[itmcd] = @itmcd
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_SelectByPK] @itmcd = 1

CREATE PROCEDURE [dbo].[PR_item_SelectByPK]

		@itmcd 		int

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[item].[itmcd],
		[dbo].[item].[compid],
		[dbo].[item].[userid],
		[dbo].[item].[item],
		[dbo].[item].[unit],
		[dbo].[item].[tare]
FROM  [dbo].[item]

WHERE [dbo].[item].[itmcd] = @itmcd
GO
/****** Object:  StoredProcedure [dbo].[PR_item_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_SelectAll]

CREATE PROCEDURE [dbo].[PR_item_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[item].[itmcd],
		[dbo].[item].[compid],
		[dbo].[item].[userid],
		[dbo].[item].[item],
		[dbo].[item].[unit],
		[dbo].[item].[tare]
FROM  [dbo].[item]
GO
/****** Object:  StoredProcedure [dbo].[PR_item_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_Insert] @itmcd = 1, @compid = 1, @userid = 'userid', @item = 'item', @unit = 1, @tare = 1

CREATE PROCEDURE [dbo].[PR_item_Insert]

		@itmcd  		int OUTPUT,
		@compid 		int,
		@userid 		nvarchar (20),
		@item   		nvarchar (20),
		@unit   		int,
		@tare   		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

SELECT @itmcd = ISNULL(MAX([itmcd]),0) + 1 FROM [dbo].[item]


INSERT [dbo].[item]
(
		[itmcd],
		[compid],
		[userid],
		[item],
		[unit],
		[tare]
)
VALUES
(
		@itmcd,
		@compid,
		@userid,
		@item,
		@unit,
		@tare
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_item_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_item_DeleteByPK] @itmcd = 1

CREATE PROCEDURE [dbo].[PR_item_DeleteByPK]

		@itmcd 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[item]
WHERE [dbo].[item].[itmcd] = @itmcd


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_code_SelectAll]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_code_SelectAll]

CREATE PROCEDURE [dbo].[PR_code_SelectAll]

AS

SET NOCOUNT ON;

SELECT  
		[dbo].[code].[compid],
		[dbo].[code].[userid],
		[dbo].[code].[cashcd],
		[dbo].[code].[bankcd],
		[dbo].[code].[motorfrt],
		[dbo].[code].[pl],
		[dbo].[code].[commission],
		[dbo].[code].[income],
		[dbo].[code].[purchase],
		[dbo].[code].[sales],
		[dbo].[code].[diff],
		[dbo].[code].[roundoff],
		[dbo].[code].[vadar],
		[dbo].[code].[kasar],
		[dbo].[code].[vatav],
		[dbo].[code].[bankchgs]
FROM  [dbo].[code]
GO
/****** Object:  StoredProcedure [dbo].[PR_code_Insert]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_code_Insert] @compid = 1, @userid = 'userid', @cashcd = 'cashcd', @bankcd = 'bankcd', @motorfrt = 'motorfrt', @pl = 'pl', @commission = 'commission', @income = 'income', @purchase = 'purchase', @sales = 'sales', @diff = 'diff', @roundoff = 'roundoff', @vadar = 'vadar', @kasar = 'kasar', @vatav = 'vatav', @bankchgs = 'bankchgs'

CREATE PROCEDURE [dbo].[PR_code_Insert]

		@compid     		int,
		@userid     		nvarchar (20),
		@cashcd     		nvarchar (7),
		@bankcd     		nvarchar (7),
		@motorfrt   		nvarchar (7),
		@pl         		nvarchar (7),
		@commission 		nvarchar (7),
		@income     		nvarchar (7),
		@purchase   		nvarchar (7),
		@sales      		nvarchar (7),
		@diff       		nvarchar (7),
		@roundoff   		nvarchar (7),
		@vadar      		nvarchar (7),
		@kasar      		nvarchar (7),
		@vatav      		nvarchar (7),
		@bankchgs   		nvarchar (7)

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

INSERT [dbo].[code]
(
		[compid],
		[userid],
		[cashcd],
		[bankcd],
		[motorfrt],
		[pl],
		[commission],
		[income],
		[purchase],
		[sales],
		[diff],
		[roundoff],
		[vadar],
		[kasar],
		[vatav],
		[bankchgs]
)
VALUES
(
		@compid,
		@userid,
		@cashcd,
		@bankcd,
		@motorfrt,
		@pl,
		@commission,
		@income,
		@purchase,
		@sales,
		@diff,
		@roundoff,
		@vadar,
		@kasar,
		@vatav,
		@bankchgs
)

COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PR_acmast_DeleteByPK]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
==================================
Author      : <Author,,Name>
Create date : <Create Date,,>
Description : <Description,,>
==================================
*/

--   [dbo].[PR_acmast_DeleteByPK] @acno = 1

CREATE PROCEDURE [dbo].[PR_acmast_DeleteByPK]

		@acno 		int

AS

SET NOCOUNT ON;

BEGIN TRY
BEGIN TRAN

DELETE FROM [dbo].[acmast]
WHERE [dbo].[acmast].[acno] = @acno


COMMIT TRAN
END TRY

BEGIN CATCH
ROLLBACK TRAN

DECLARE @ErrorNumber_INT INT;
DECLARE @ErrorSeverity_INT INT;
DECLARE @ErrorProcedure_VC VARCHAR(200);
DECLARE @ErrorLine_INT INT;
DECLARE @ErrorMessage_NVC NVARCHAR(4000);

SELECT
		@ErrorMessage_NVC = ERROR_MESSAGE(),
		@ErrorSeverity_INT = ERROR_SEVERITY(),
		@ErrorNumber_INT = ERROR_NUMBER(),
		@ErrorProcedure_VC = ERROR_PROCEDURE(),
		@ErrorLine_INT = ERROR_LINE()

RAISERROR(@ErrorMessage_NVC,@ErrorSeverity_INT,1);

END CATCH
GO
/****** Object:  StoredProcedure [dbo].[PandLreport]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PandLreport]
as
 if object_id('tempdb..#CRPandL') is not null
 begin
 drop table #CRPandL
 end
  if object_id('tempdb..#DRPandL') is not null
 begin
 drop table #DRPandL
 end


create table #DRPandL
(
	sr int identity(1,1) ,
	dramount decimal,
	DrParticulars nvarchar(50)
)
 
create table #CRPandL
(
    sr int identity(1,1) ,
	cramount decimal,
	CrParticulars nvarchar(50)
	
)
Declare @salesamt Decimal(15,2)
Declare @purchaseamt Decimal(15,2)
Declare @sign12 nvarchar(2)
Declare @acname nvarchar(50)
Declare @cramt Decimal(15,2)=0
Declare @dramt Decimal(15,2)=0
Declare @crclosingamt Decimal(15,2)=0
Declare @drclosingamt Decimal(15,2)=0
Declare @grossprofit Decimal(15,2)=0
Declare @grossloss Decimal(15,2)=0
Declare @totalcrclosingamt Decimal(15,2)=0
Declare @totaldrclosingamt Decimal(15,2)=0



-- Insert Puchase & sales amounts
set @purchaseamt=(select sum(amount) as amount from ledger where doccd='PU' and sign12='DR')
set @salesamt=(select sum(amount) as amount from ledger where doccd='SA' and sign12='CR')

insert into #DRPandL (dramount,DrParticulars)
select isnull(@purchaseamt,0),'PURCHASE'

insert into #CRPandL (cramount,CrParticulars)
select isnull(@salesamt,0),'SALE'


 --Insert Gross Loss/ Gross Profit

if isnull(@purchaseamt,0)>isnull(@salesamt,0)
begin
set @dramt=isnull(@purchaseamt,0)-isnull(@salesamt,0)
end
else if isnull(@salesamt,0)>isnull(@purchaseamt,0)
begin
set @cramt=isnull(@salesamt,0)-isnull(@purchaseamt,0)
end

 if isnull(@purchaseamt,0)>isnull(@salesamt,0)
Begin
   insert into #CRPandL (cramount,CrParticulars)
select isnull(@dramt,0),'Gross Loss'
insert into #DRPandL (dramount,DrParticulars)
select isnull(@cramt,0),'Gross Profit'
End
else if isnull(@salesamt,0)>isnull(@purchaseamt,0)
Begin
	insert into #DRPandL (dramount,DrParticulars)
select isnull(@cramt,0),'Gross Profit'
insert into #CRPandL (cramount,CrParticulars)
select isnull(@dramt,0),'Gross Loss'
End

 --Insert Trading Total amount 
if isnull(@purchaseamt,0)>isnull(@salesamt,0)
Begin
   insert into #CRPandL (cramount,CrParticulars)
select isnull(@purchaseamt,0),'Total'
   insert into #DRPandL (dramount,DrParticulars)
select isnull(@purchaseamt,0),'Total'   
End
else if isnull(@salesamt,0)>isnull(@purchaseamt,0)
Begin
   insert into #CRPandL (cramount,CrParticulars)
select isnull(@salesamt,0),'Total'
	insert into #DRPandL (dramount,DrParticulars)
select isnull(@salesamt,0),'Total'
End

-- Insert Gross Loss/ Gross Profit at apposite positions
if isnull(@purchaseamt,0)>isnull(@salesamt,0)
Begin
   insert into #DRPandL (dramount,DrParticulars)
select isnull(@dramt,0),'Gross Loss'
insert into #CRPandL (cramount,CrParticulars)
select isnull(@cramt,0),'Gross Profit'
End
else if isnull(@salesamt,0)>isnull(@purchaseamt,0)
Begin
	insert into #CRPandL (cramount,CrParticulars)
select isnull(@cramt,0),'Gross Profit'
insert into #DRPandL (dramount,DrParticulars)
select isnull(@dramt,0),'Gross Loss'
End

-- Insert all Dr. Closing balances Dr.- Cr Or Cr. - Dr.
set @drclosingamt=(select sum(amount)as amount  from ledger where acccd1 in  
(select acccd from acmast where grcd='ie' and acccd not in ('0000063','0000062')))
 insert into #DRPandL (dramount,DrParticulars)
select isnull(@drclosingamt,0),'XYZ'


---- Insert all Cr. Closing balances Dr.- Cr Or Cr. - Dr.
set @crclosingamt=(select sum(amount)as amount from ledger where sign12= 'CR' and acccd1 in  (select acccd from acmast where grcd='ie' and acccd not in ('0000063','0000062')))

set @drclosingamt=(select sum(amount)as amount from ledger where sign12= 'DR' and acccd1 in  (select acccd from acmast where grcd='ie' and acccd not in ('0000063','0000062')))


if isnull(@crclosingamt,0)>isnull(@drclosingamt,0)
set @totalcrclosingamt=@crclosingamt-@drclosingamt

Begin
   insert into #CRPandL (cramount,CrParticulars)
select isnull(@totalcrclosingamt,0),'Commision Received'
insert into #DRPandL (dramount,DrParticulars)
select isnull(0,0),'0'
End

if isnull(@drclosingamt,0)>isnull(@crclosingamt,0)
set @totaldrclosingamt=@drclosingamt-@crclosingamt

Begin
   insert into #DRPandL (dramount,DrParticulars)
select isnull(@totaldrclosingamt,0),'Commision Received'
insert into #CRPandL (cramount,CrParticulars)
select isnull(0,0),'0'
End

--set @crclosingamt=(select * from ledger where acccd1 in  (select acccd from acmast where grcd='ie' and acccd not in ('0000063','0000062')))
--insert into #CRPandL (cramount,CrParticulars)
--select isnull(@crclosingamt,0),'XYZ'


--select #DRPandL.sr ,#DRPandL.DrParticulars Particulars,#DRPandL.dramount Amount,#CRPandL.CrParticulars Particulars,#CRPandL.cramount Amount from #DRPandL,#CRPandL

select dr.dramount,dr.DrParticulars,cr.cramount,cr.CrParticulars from #DRPandL dr join #CRPandL cr on dr.sr=cr.sr

-- Insert Net Loss/ Net Profit


-- Insert PnL Total amount 

	


   -- Closing Balance
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport_temp]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[ledgerreport_temp] '2014-04-01','2015-03-31','0000095'
CREATE PROCEDURE [dbo].[ledgerreport_temp](@fdate datetime,@tdate datetime,@acccd nvarchar(7))
AS

--declare @fdate datetime = '2010-10-10'
--declare @tdate datetime = '2014-10-10'
--declare @acccd nvarchar(7)= '0000077'

 if object_id('tempdb..#ledrep') is not null
 begin
 drop table #ledrep
 end

create table #ledrep
(
	sr int identity(1,1) ,
	cdate datetime,
	crefno nvarchar(20),
	camount decimal,
	ddate datetime,
	drefno nvarchar(20),
	damount decimal
)

Declare @bal Decimal(15,2)
Declare @sign12 nvarchar(2)
Declare @acname nvarchar(50)
Declare @clbal Decimal(15,2)=0
Declare @cramt Decimal(15,2)=0
Declare @sumcr Decimal(15,2)=0
Declare @sumdr Decimal(15,2)=0
Declare @dramt Decimal(15,2)=0


delete from #ledrep
set @bal=(select opbal from openbal where acccd=@acccd)
set @sign12=(select sign12 from openbal where acccd=@acccd)
set @acname=(select acname from openbal where acccd=@acccd)


------ Opening Balance
if @sign12='CR' 
Begin
   insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
   select @fdate,'To Bal.B/F.',@bal,NULL,'',0
End
else if @sign12='DR' 
Begin
	insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
	select NULL,'',0,@fdate,'To Bal.B/F.',@bal
End




--All Dr Entries
insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select Null,'',0,tdate,lotno,amount from ledger where sign12='DR' and acccd1=@acccd


--All Cr Entries
insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select tdate,lotno,amount,Null,'',0 from ledger where sign12='CR' and acccd1=@acccd




------ Closing Balance

set @sumcr=(select sum(amount) from ledger where sign12='CR' and acccd1=@acccd)
print '@sumcr'+ convert(nvarchar, @sumcr)

set @sumdr=(select sum(amount) from ledger where sign12='DR' and acccd1=@acccd)
print '@sumdr'+ convert(nvarchar, @sumdr)

if @sign12='CR' and @bal<> 0
begin
set @cramt=@bal+@sumcr
set @dramt=@sumdr
print '@cramt'+ convert(nvarchar, @cramt)
end
else
Begin
set @dramt=@bal+@sumdr
set @cramt=@sumcr
print '@dramt'+ convert(nvarchar, @dramt)
end



if @cramt>@dramt
set @clbal=@cramt-@dramt
else
set @clbal=@dramt-@cramt


print 'clbal: ' + convert(nvarchar,  @clbal)

if @sign12='CR' 
Begin
   insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select NULL,'',0,@tdate,'To Bal.C/F.',@clbal

End
else if @sign12='DR' 
Begin
	insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
	   select @tdate,'To Bal.C/F.',@clbal,NULL,'',0
End



------ Final select Satement
select convert(nvarchar,cdate,103),crefno,sum(camount),convert(nvarchar,ddate,103),drefno,sum(damount) from #ledrep
where cdate is not null or ddate is not null
group by cdate,crefno,ddate,drefno,sr order by sr
return
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport_old]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ledgerreport_old](@fdate datetime,@tdate datetime,@acccd nvarchar(7))
AS
delete from ledrep
Declare @bal Decimal(15,2)
Declare @sign12 nvarchar(2)
Declare @acname nvarchar(50)
    set @bal=(select opbal from openbal where acccd=@acccd)
 set @sign12=(select sign12 from openbal where acccd=@acccd)
set @acname=(select acname from acmast where acccd=@acccd)

if @sign12='CR' and @bal<> 0
Begin
   insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
   select @fdate,'To Bal.B/F.',@bal,NULL,'',0,@acccd,@acname,@fdate,@tdate
End
else if @sign12='DR' and @bal<> 0
Begin
	insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
	select NULL,'',0,@fdate,'To Bal.B/F.',@bal,@acccd,@acname,@fdate,@tdate
End
else
Begin
	insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
	select NULL,'',0,NULL,'',0,@acccd,@acname,@fdate,@tdate
End

Declare @clbal Decimal(15,2)
Declare @cramt Decimal(15,2)
Declare @sumcr Decimal(15,2)
Declare @sumdr Decimal(15,2)
Declare @dramt Decimal(15,2)

if @sign12='CR' and @bal<> 0
begin
set @sumcr=(select sum(camount) from ledrep)
set @cramt=@bal+@sumcr
end
else
Begin
set @sumdr=(select sum(damount) from ledrep)
set @dramt=@bal+@sumdr
end


if @cramt>@dramt
set @clbal=@cramt-@dramt
else
set @clbal=@dramt-@cramt

if @sign12='CR' and @bal<> 0
Begin
   insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
select NULL,'',0,@tdate,'To Bal.C/F.',@sumcr,@acccd,@acname,@fdate,@tdate

End
else if @sign12='DR' and @bal<> 0
Begin
	insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
	   select @tdate,'To Bal.C/F.',@sumdr,NULL,'',0,@acccd,@acname,@fdate,@tdate
End
else
Begin
	insert into ledrep(cdate,crefno,camount,ddate,drefno,damount,acccd,acname,fdate,tdate)
	select NULL,'',0,NULL,'',0,@acccd,@acname,@fdate,@tdate
End

select * from ledrep
return
GO
/****** Object:  StoredProcedure [dbo].[ledgerreport]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[ledgerreport] '2014-04-01','2015-03-31','0000005'
CREATE PROCEDURE [dbo].[ledgerreport](@fdate datetime,@tdate datetime,@acccd nvarchar(7))
AS

--declare @fdate datetime = '2010-10-10'
--declare @tdate datetime = '2014-10-10'
--declare @acccd nvarchar(7)= '0000077'

 if object_id('tempdb..#ledrep') is not null
 begin
 drop table #ledrep
 end

create table #ledrep
(
	sr int identity(1,1) ,
	cdate datetime,
	crefno nvarchar(20),
	camount decimal,
	ddate datetime,
	drefno nvarchar(20),
	damount decimal
)

Declare @bal Decimal(15,2)
Declare @sign12 nvarchar(2)
Declare @acname nvarchar(50)
Declare @clbal Decimal(15,2)=0
Declare @cramt Decimal(15,2)=0
Declare @sumcr Decimal(15,2)=0
Declare @sumdr Decimal(15,2)=0
Declare @dramt Decimal(15,2)=0


delete from #ledrep
set @bal=(select opbal from openbal where acccd=@acccd)
set @sign12=(select sign12 from openbal where acccd=@acccd)
set @acname=(select acname from openbal where acccd=@acccd)


------ Opening Balance
if @sign12='CR' 
Begin
   insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
   select @fdate,'To Bal.B/F.',@bal,NULL,'',0
End
else if @sign12='DR' 
Begin
	insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
	select NULL,'',0,@fdate,'To Bal.B/F.',@bal
End




--All Dr Entries
insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select Null,'',0,tdate,lotno,amount from ledger where sign12='DR' and acccd1=@acccd


--All Cr Entries
insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select tdate,lotno,amount,Null,'',0 from ledger where sign12='CR' and acccd1=@acccd




------ Closing Balance

set @sumcr=(select sum(amount) from ledger where sign12='CR' and acccd1=@acccd)
print '@sumcr'+ convert(nvarchar, @sumcr)

set @sumdr=(select sum(amount) from ledger where sign12='DR' and acccd1=@acccd)
print '@sumdr'+ convert(nvarchar, @sumdr)

if @sign12='CR' 
begin
set @cramt=@bal+@sumcr
set @dramt=@sumdr
print '@cramt'+ convert(nvarchar, @cramt)
end
else
Begin
set @dramt=@bal+@sumdr
set @cramt=@sumcr
print '@dramt'+ convert(nvarchar, @dramt)
end
print '@dramt'+ convert(nvarchar, @dramt)


if isnull(@cramt,0)>isnull(@dramt,0)
begin
set @clbal=isnull(@cramt,0)-isnull(@dramt,0)
end
else
begin
set @clbal=isnull(@dramt,0)-isnull(@cramt,0)
end


print 'clbal: ' + convert(nvarchar,  @clbal)

if isnull(@cramt,0)>isnull(@dramt,0)
Begin
   insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
select NULL,'',0,@tdate,'To Bal.C/F.',@clbal

End
else if isnull(@cramt,0)<isnull(@dramt,0)
Begin
	insert into #ledrep(cdate,crefno,camount,ddate,drefno,damount)
	   select @tdate,'To Bal.C/F.',@clbal,NULL,'',0
End




------ Final select Satement
select convert(nvarchar,cdate,103),crefno,isnull(sum(camount),0),convert(nvarchar,ddate,103),drefno,isnull(sum(damount),0) from #ledrep
where cdate is not null or ddate is not null
group by cdate,crefno,ddate,drefno,sr order by sr
return
GO
/****** Object:  StoredProcedure [dbo].[CashBankReport]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CashBankReport](@fdate datetime,@tdate datetime,@acccd nvarchar(7),@mode bit) 
AS
--declare @fdate datetime = '2010-10-10'
--declare @tdate datetime = '2014-10-10'
--declare @acccd nvarchar(7)= '0000077'
--declare @mode bit=1
if object_id('tempdb..#CBrep') is not null
 begin
 drop table #CBrep
 end
 
create table #CBrep
(
	 pdate datetime,
	 DocCd nvarchar(10),
	 acname nvarchar(20),
	 cramt decimal,
	 dramt decimal,
	 )
Declare @bal Decimal(15,2)	 
Declare @sign12 nvarchar(2)
Declare @acname nvarchar(50)
Declare @cramt Decimal(15,2)=0
Declare @dramt Decimal(15,2)=0
Declare @sumcr Decimal(15,2)=0
Declare @sumdr Decimal(15,2)=0
Declare @clbal Decimal(15,2)=0


delete from #CBrep
set @bal=(select opbal from openbal where acccd=@acccd)
set @sign12=(select sign12 from openbal where acccd=@acccd)
set @acname=(select acname from openbal where acccd=@acccd)

------ Opening Balance
if @sign12='CR' 
Begin
   insert into #CBrep(pdate,DocCd,acname,cramt,dramt)
      select @fdate,'','To Bal.B/F.',@bal,0
    End
else if @sign12='DR' 
Begin
	insert into #CBrep(pdate,DocCd,acname,cramt,dramt)
      select @fdate,'','To Bal.B/F.',0,@bal
End      


--All Dr Entries
insert into  #CBrep(pdate,DocCd,acname,cramt,dramt)
select cm.date,cm.doccode,am.acname,0,ct.amount from cbmast cm join cbtran ct on cm.cbno=ct.cbno 
join acmast am on ct.acccd=am.acccd
where cm.trsnrp=1 and cm.modecasbank=@mode
insert into  #CBrep(pdate,DocCd,acname,cramt,dramt)
select tm.date,tm.doccd,am.acname,0,td.amount from TransactionMaster tm join TransactionDetails td on tm.TransactionMasterID=td.TransactionMasterID 
join acmast am on tm.AcCode=am.acccd
where tm.rp=1 and tm.TransType=@mode 




--All Cr Entries
insert into  #CBrep(pdate,DocCd,acname,cramt,dramt)
select cm.date,cm.doccode,am.acname,ct.amount ,0 from cbmast cm join cbtran ct on cm.cbno=ct.cbno 
join acmast am on ct.acccd=am.acccd
where cm.trsnrp=0 and cm.modecasbank=@mode
insert into  #CBrep(pdate,DocCd,acname,cramt,dramt)
select tm.date,tm.doccd,am.acname,td.amount ,0 from TransactionMaster tm join TransactionDetails td on tm.TransactionMasterID=td.TransactionMasterID 
join acmast am on tm.AcCode=am.acccd
where tm.rp=0 and tm.TransType=@mode



------ Closing Balance

set @sumcr=isnull((select sum(ct.amount) from cbtran ct join cbmast cm on cm.cbno=ct.cbno where cm.trsnrp='0' and cm.modecasbank=@mode),0)
print '@sumcr'+ convert(nvarchar,isnull( @sumcr,0))
set @sumcr=@sumcr+isnull((select SUM(td.amount) from TransactionDetails td join TransactionMaster tm on td.TransactionMasterID=tm.TransactionMasterID where tm.rp=0 and tm.TransType=@mode),0)
print '@sumcr'+ convert(nvarchar,isnull( @sumcr,0))

set @sumdr=isnull((select sum(ct.amount) from cbtran ct join cbmast cm on cm.cbno=ct.cbno where cm.trsnrp='1' and cm.modecasbank=@mode),0)
print '@sumdr'+ convert(nvarchar,isnull( @sumcr,0))
set @sumdr=@sumdr+isnull((select SUM(td.amount) from TransactionDetails td join TransactionMaster tm on td.TransactionMasterID=tm.TransactionMasterID where tm.rp=1 and tm.TransType=@mode),0)
print '@sumdr'+ convert(nvarchar,isnull( @sumcr,0))


if @sign12='CR' 
begin
set @cramt=@bal+@sumcr
set @dramt=@sumdr
print '@cramt'+ convert(nvarchar, @cramt)
end
else
Begin
set @dramt=@bal+@sumdr
set @cramt=@sumcr
print '@dramt'+ convert(nvarchar, @dramt)
end
print '@dramt'+ convert(nvarchar, @dramt)



if isnull(@cramt,0)>isnull(@dramt,0)
begin
set @clbal=isnull(@cramt,0)-isnull(@dramt,0)
end
else
begin
set @clbal=isnull(@dramt,0)-isnull(@cramt,0)
end

print 'clbal: ' + convert(nvarchar,  @clbal)

if isnull(@cramt,0)>isnull(@dramt,0)
Begin
   insert into #CBrep(pdate,DocCd,acname,cramt,dramt)
select '2014-10-10','','To Bal.C/F.',0,@clbal
End
else if isnull(@cramt,0)<isnull(@dramt,0)
Begin
   insert into #CBrep(pdate,DocCd,acname,cramt,dramt)
select '2014-10-10','','To Bal.C/F.',@clbal,0

End


select * from #CBrep
GO
/****** Object:  StoredProcedure [dbo].[billnowise]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[billnowise](@fbillno int,@tbillno int)
AS
select * from pmast where serno between @fbillno and @tbillno
select * from purtran where  serno between @fbillno and @tbillno
   
return
GO
/****** Object:  StoredProcedure [dbo].[abcd]    Script Date: 01/11/2015 19:06:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[abcd]
AS
BEGIN
select doccode,modecasbank,trsnrp,isnull(SUM(cbtran.amount),0)amount from cbmast left join cbtran on cbmast.cbno=cbtran.cbno where modecasbank=1 group by cbmast.cbno,doccode,modecasbank,trsnrp
union all
select doccd,TransType,rp,(select SUM(amount) from TransactionDetails where TransactionMasterID=1)amount from TransactionMaster where TransType=1
end

--SELECT DISTINCT COALESCE(cbmast.modecasbank, TransactionMaster.TransType) AS item
--FROM cbmast
--  FULL OUTER JOIN TransactionMaster
--  ON cbmast.modecasbank = TransactionMaster.TransType



--select * from cbmast
--select * from TransactionMaster


--	select cm.coname,compadd,compphone,email,compwebsite,compfax from company c join compmast cm on c.coid=cm.coid
--where compid=6


--select * from company
GO
/****** Object:  ForeignKey [FK__acmast__compid__43D61337]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[acmast]  WITH CHECK ADD  CONSTRAINT [FK__acmast__compid__43D61337] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[acmast] CHECK CONSTRAINT [FK__acmast__compid__43D61337]
GO
/****** Object:  ForeignKey [FK__code__compid__5812E165]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[code]  WITH CHECK ADD FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
/****** Object:  ForeignKey [FK__compmast__coid__0EA330E9]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[compmast]  WITH CHECK ADD  CONSTRAINT [FK__compmast__coid__0EA330E9] FOREIGN KEY([coid])
REFERENCES [dbo].[company] ([coid])
GO
ALTER TABLE [dbo].[compmast] CHECK CONSTRAINT [FK__compmast__coid__0EA330E9]
GO
/****** Object:  ForeignKey [FK__compmast__yearid__76B698BF]    Script Date: 01/11/2015 19:05:59 ******/
ALTER TABLE [dbo].[compmast]  WITH CHECK ADD  CONSTRAINT [FK__compmast__yearid__76B698BF] FOREIGN KEY([yearid])
REFERENCES [dbo].[finyear] ([yearid])
GO
ALTER TABLE [dbo].[compmast] CHECK CONSTRAINT [FK__compmast__yearid__76B698BF]
GO
/****** Object:  ForeignKey [FK__item__compid__02FC7413]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[item]  WITH CHECK ADD  CONSTRAINT [FK__item__compid__02FC7413] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[item] CHECK CONSTRAINT [FK__item__compid__02FC7413]
GO
/****** Object:  ForeignKey [FK__journal__compid__1BC821DD]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[journal]  WITH CHECK ADD  CONSTRAINT [FK__journal__compid__1BC821DD] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[journal] CHECK CONSTRAINT [FK__journal__compid__1BC821DD]
GO
/****** Object:  ForeignKey [FK__openbal__compid__5BE37249]    Script Date: 01/11/2015 19:06:00 ******/
ALTER TABLE [dbo].[openbal]  WITH CHECK ADD FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
/****** Object:  ForeignKey [FK__pmast__compid__2CF2ADDF]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[pmast]  WITH CHECK ADD  CONSTRAINT [FK__pmast__compid__2CF2ADDF] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[pmast] CHECK CONSTRAINT [FK__pmast__compid__2CF2ADDF]
GO
/****** Object:  ForeignKey [FK__purtran__compid__245D67DE]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[purtran.old]  WITH CHECK ADD  CONSTRAINT [FK__purtran__compid__245D67DE] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[purtran.old] CHECK CONSTRAINT [FK__purtran__compid__245D67DE]
GO
/****** Object:  ForeignKey [FK__ratetran__compid__625A9A57]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[ratetran]  WITH CHECK ADD  CONSTRAINT [FK__ratetran__compid__625A9A57] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[ratetran] CHECK CONSTRAINT [FK__ratetran__compid__625A9A57]
GO
/****** Object:  ForeignKey [FK__smast__compid__5AB9788F]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[smast]  WITH CHECK ADD  CONSTRAINT [FK__smast__compid__5AB9788F] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[smast] CHECK CONSTRAINT [FK__smast__compid__5AB9788F]
GO
/****** Object:  ForeignKey [FK__stock__compid__2739D489]    Script Date: 01/11/2015 19:06:01 ******/
ALTER TABLE [dbo].[stock]  WITH CHECK ADD  CONSTRAINT [FK__stock__compid__2739D489] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[stock] CHECK CONSTRAINT [FK__stock__compid__2739D489]
GO
/****** Object:  ForeignKey [FK__payment__compid__4B7734FF]    Script Date: 01/11/2015 19:06:02 ******/
ALTER TABLE [dbo].[TransactionMaster]  WITH CHECK ADD  CONSTRAINT [FK__payment__compid__4B7734FF] FOREIGN KEY([compid])
REFERENCES [dbo].[compmast] ([compid])
GO
ALTER TABLE [dbo].[TransactionMaster] CHECK CONSTRAINT [FK__payment__compid__4B7734FF]
GO
