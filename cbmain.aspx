﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cbmain.aspx.vb" Inherits="cbmain" MasterPageFile="~/MasterPage.master" %>

<%@ Register assembly="BasicFrame.WebControls.BasicDatePicker" namespace="BasicFrame.WebControls" tagprefix="BDP" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <%-- <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>--%>
    <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">
         $(document).ready(function () {
             $("#<%=Txt_Bname.ClientID %>").autocomplete({
                 autoFocus: true,
                 source: function (request, response) {
                     $.ajax({
                         url: '<%=ResolveUrl("~/Service.asmx/GetbankName") %>',
                         data: "{ 'prefix': '" + request.term + "'}",
                         dataType: "json",
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         success: function (data) {
                             response($.map(data.d, function (item) {
                                 return {
                                     label: item.split('-')[0],
                                     val: item.split('-')[1]
                                 }
                             }))
                         },
                         error: function (response) {
                             alert(response.responseText);
                         },
                         failure: function (response) {
                             alert(response.responseText);
                         }
                     });
                 },
                 select: function (e, i) {
                     $("#<%=Txt_BCode.ClientID %>").val(i.item.val);
                     $("#<%=Txt_Bname.ClientID %>").val(i.item.label);


                 },
                 minLength: 1

             });
         }); 
    </script>
         <script type="text/javascript">
             $(document).ready(function () {
                 $("#<%=Txt_Account.ClientID %>").autocomplete({
                     autoFocus: true,
                     source: function (request, response) {
                         $.ajax({
                             url: '<%=ResolveUrl("~/Service.asmx/GetAcNameForCashBook") %>',
                             data: "{ 'prefix': '" + request.term + "'}",
                             dataType: "json",
                             type: "POST",
                             contentType: "application/json; charset=utf-8",
                             success: function (data) {
                                 response($.map(data.d, function (item) {
                                     return {
                                         label: item.split('-')[0],
                                         val: item.split('-')[1]
                                     }
                                 }))
                             },
                             error: function (response) {
                                 alert(response.responseText);
                             },
                             failure: function (response) {
                                 alert(response.responseText);
                             }
                         });
                     },
                     select: function (e, i) {
                         $("#<%=Txt_AcCode.ClientID %>").val(i.item.val);
                         $("#<%=Txt_Account.ClientID %>").val(i.item.label);


                     },
                     minLength: 1

                 });
             }); 
    </script>
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>

    
    <table width="100%">
        <tr>
            <td colspan="7" align="right">
                    <asp:TextBox ID="Txtcompid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                    <asp:TextBox ID="Txtuserid" runat="server" Enabled="False" Visible="False" Width="59px"></asp:TextBox>
                    <asp:TextBox ID="Txtdoccd" runat="server" Enabled="False" Visible="False" 
                        Width="49px" ReadOnly="True">CB</asp:TextBox>
                <asp:Button ID="btn_back" runat="server" Text="Back" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="Lbl_CBNo" runat="server" Text="CashBook No."></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="Txt_CBNo" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_PayRec" runat="server" Text="Payment/Receipt"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="Drl_PayRec" runat="server" Height="16px">
                    <asp:ListItem Selected="True" Value="True">Payment</asp:ListItem>
                    <asp:ListItem Value="False">Receipt</asp:ListItem>
                </asp:DropDownList>
            </td>
                        <td>
                            <asp:RadioButton ID="Cash" AutoPostBack="true" runat="server" AccessKey="C" GroupName="CashBank" 
                                Text="Cash" />
                            <asp:RadioButton ID="Bank" AutoPostBack="true" runat="server" AccessKey="B" Checked="True" 
                                GroupName="CashBank" Text="Bank" />
            </td>
            <td>
                <asp:TextBox ID="Txt_Bname" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="Txt_Bname" ErrorMessage="*" ValidationGroup="2"></asp:RequiredFieldValidator>
                <asp:TextBox ID="Txt_BCode" runat="server" Width="50px"></asp:TextBox>
            </td>
                        <td align="right">
                <asp:Label ID="Lbl_Date" runat="server" Text="Date"></asp:Label>
            </td>
            <td>
                <BDP:BasicDatePicker ID="Txt_Date" runat="server" DisplayType="TextBox"></BDP:BasicDatePicker>
            &nbsp;
                <asp:Button ID="btnsave" runat="server" Text="Save" ValidationGroup="2" />
            </td>
        </tr>
    </table>
    <hr />
    <table id="Tbl_Details1">
        <tr>
            <td align="right">
                <asp:Label ID="Lbl_Account" runat="server" Text="Account" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Account" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="Txt_Account" ErrorMessage="*" ValidationGroup="1"></asp:RequiredFieldValidator>
                <asp:TextBox ID="Txt_AcCode" runat="server" Width="50px"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_Amount" runat="server" Text="Amount" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Amount" runat="server" Width="100px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="Txt_Amount" ErrorMessage="*" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_Narration" runat="server" Text="Narration" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_Narration" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:HiddenField ID="hf_serno" runat="server" />
            </td>
        <td>
                <asp:HiddenField ID="hf_savemode" runat="server" Value="0" />
            </td>
        <td>
                <asp:Button ID="btn_Save" runat="server" Text="Add" ValidationGroup="1" />
            </td>
        </tr>
        <tr id="tablerow" runat="server">
            <td align="right">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="right">
                <asp:Label ID="Lbl_BBranch" runat="server" Text="Branch" Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_BBranch" runat="server" Width="100px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="Txt_BBranch" ErrorMessage="*" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td align="right">
                <asp:Label ID="Lbl_ChqNo" runat="server" Text="Cheque No." Width="100px"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="Txt_ChqNo" runat="server" Width="100px" MaxLength="6"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="Txt_ChqNo" ErrorMessage="*" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="Lbl_ChqDate" runat="server" Text="Cheque Date" Width="100px" 
                    Height="19px"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="Txt_ChqDate" runat="server" DisplayType="TextBox" 
                    Width="100px"></BDP:BasicDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="Txt_ChqDate" ErrorMessage="*" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                        <asp:HiddenField ID="hfAccode" runat="server" />
                    </td>
        </tr>
        <tr>
        <td colspan="9">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
                        <asp:HiddenField ID="hfChallanTranID" runat="server" Value="0" />
            
        </td>
        </tr>
        <tr>
        <td colspan="9" align="right">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
                    Width="100%" AutoGenerateColumns="False" DataKeyNames="cbno" HorizontalAlign="Center"
                    EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
                    PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
                    <Columns>
           <%--             <asp:BoundField DataField="sr" HeaderText="Sr. No." ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>--%>
                          <asp:BoundField DataField="acccd" HeaderText="AcCode" ReadOnly="True" SortExpression="acccd" />
                       <asp:BoundField DataField="acname" HeaderText="AcName" ReadOnly="True" SortExpression="acname" />
                        <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" SortExpression="amount" />
                          <asp:BoundField DataField="narr" HeaderText="Narration" ReadOnly="True" SortExpression="narr" />
                          <%-- <asp:BoundField DataField="modecasbank" HeaderText="Mode" ReadOnly="True" SortExpression="modecasbank" />
--%>
                        <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                               
                                <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                    OnClientClick="" CommandArgument='<%#Eval("sr")%>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BorderStyle="None" HorizontalAlign="Center" />
                    <EmptyDataRowStyle BorderStyle="None" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
        </table>
    
 
    <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
        </p>
 </asp:Content>

