﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="purchase.aspx.vb" Inherits="purchase" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <%--<link rel="stylesheet" href="/resources/demos/style.css"/>--%>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, Verdana, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .modalBackground
        {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Purchase Master" Font-Bold="True" Font-Size="Large"
                ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
            </td>
        </tr>
    </table>
    <div>
        <%--<p align="center">
            <asp:Label ID="Label2" runat="server" Text="Purchase Master" Font-Bold="True" Font-Size="Large"
                ForeColor="#6600CC"></asp:Label>
        </p>
        <p align="center">
            &nbsp;</p>
        <hr />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
        <br />
        <br />--%>
        <asp:Label ID="Label3" runat="server" Text="Purchase Bill no."></asp:Label>
        &nbsp;
        <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
        &nbsp;
        <asp:TextBox ID="txtSearchAcName" runat="server"></asp:TextBox>
        &nbsp; &nbsp; &nbsp;
        <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
        &nbsp;&nbsp;
        <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
            DisplayType="TextBox" />
        &nbsp;
        <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
        &nbsp;&nbsp;
        <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
            DisplayType="TextBox" />
        &nbsp;
        <asp:Button ID="btnSearch" runat="server" Text="Search" />
        <br />
        <br />
        <hr />
    </div>
    <asp:Panel ID="panel2" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="serno"
            HorizontalAlign="Center" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC"
            BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%"
            EmptyDataText="Data not Found!!!" EmptyDataRowStyle-HorizontalAlign="Center">
            <Columns>
                <asp:BoundField DataField="serno" HeaderText="Sr No." ReadOnly="true" SortExpression="serno">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="pbillno" HeaderText="Bill No." ReadOnly="true" SortExpression="pbillno"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="date" HeaderText="Date" ReadOnly="true" SortExpression="date"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="acccd" HeaderText="A/c Code" ReadOnly="true" SortExpression="acccd"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="acname" HeaderText="A/c Name" ReadOnly="true" SortExpression="acname" />
                <asp:BoundField DataField="lotno" HeaderText="Lot No." ReadOnly="true" SortExpression="lotno"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="tamount" HeaderText="Amount" ReadOnly="true" SortExpression="tamount">
                    <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                </asp:BoundField>
                <asp:BoundField DataField="netamt" HeaderText="Net Amount" ReadOnly="true" SortExpression="netamt">
                    <ItemStyle HorizontalAlign="Right" CssClass="hs" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgbtnedit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                            CommandArgument='<%#Eval("serno")%>' />
                        <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                            CommandArgument='<%#Eval("serno")%>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <%-- /****** Script for SelectTopNRows command from SSMS  ******/
SELECT [serno] ,    
      [pbillno]
      ,[date]
      ,a.[acccd],
      a.acname
      ,[lotno]
      ,[tamount]    
      ,[netamt]
  FROM [komal].[dbo].[pmast] p  join dbo.acmast a on  p.acccd=a.acccd--%>
    </asp:Panel>
</asp:Content>
