﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JournalEntry.aspx.vb" Inherits="JournalEntry"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <link href="Scripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
<%--    <script type="text/javascript">
        function goodbye(e) {
            if (!e) e = window.event;
            //e.cancelBubble is supported by IE - this will kill the bubbling process.
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog

            //e.stopPropagation works in Firefox.
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        window.onbeforeunload = goodbye;
    </script>--%>
   <%-- <script type="text/javascript">
        $(document).ready(function () {

            $('a').on('mousedown', stopNavigate);

            $('a').on('mouseleave', function () {
                $(window).on('beforeunload', function () {
                    return 'Are you sure you want to leave?';
                });
            });
        });

        function stopNavigate() {
            $(window).off('beforeunload');
        }
    </script>
    <script type="text/javascript">
        $(window).on('beforeunload', function () {
            return 'Are you sure you want to leave?';
        });

        $(window).on('unload', function () {

            logout();

        });
    </script>--%>
<%--    <script type"text/javascript">
        window.onbeforeunload = function () {
            return "You're about to end your session, are you sure?";
        }
    </script>--%>
    <%--<script type="text/javascript">
        document.onkeydown = getKeyCode;
        var keyCode = 0;
        window.onbeforeunload = confirmExit;
        function confirmExit() {
            // F5 is 116.
            if (keyCode == 116) {
                keyCode = 0;
                // For F5
                return "If you have made any changes to the fields without clicking the Save button, " +
        "Your changes will be lost. Are you sure you want to exit this page?";
            }
            else {
                // For close browser
                return "You have attempted to leave this page." +
        "Are you sure you want to exit this page?";
            }
        }
        function getKeyCode(e) {
            if (window.event) {
                e = window.event;
                keyCode = e.keyCode;
            }
            else {
                keyCode = e.which;
            }
        }
        $buttonpressed = true;
        $j("$btnsave.click").click(function () {
            $buttonpressed = false;
        });
</script>--%> 
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtacname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetAcNameforjournal") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#6600CC"
                    Text="Journal Entry"></asp:Label>
                &nbsp;
            </td>
            <td colspan="3">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td colspan="2">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td align="right">
                <asp:Button ID="btn_back" runat="server" Text="Back" />
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td colspan="14">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfacccd" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfacname" runat="server" />
            </td>
            <td colspan="2">
                <asp:HiddenField ID="hfmaster" runat="server" Value="0" />
            </td>
            <td>
                <asp:TextBox ID="Txtuserid" runat="server" Width="50px" Visible="False"></asp:TextBox>
                <asp:TextBox ID="Txtcompid" runat="server" Width="50px" Visible="False"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td colspan="2">
            </td>
            <td>
            </td>
            <td colspan="3">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <asp:Label ID="Label4" runat="server" Text="Journal Entry No."></asp:Label>
            </td>
            <td align="left" colspan="3">
                <asp:TextBox ID="txtjeno" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td>
            </td>
            <td align="right" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="Date"></asp:Label>
            </td>
            <td align="left" colspan="5">
                <BDP:BasicDatePicker ID="txtDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                    runat="server" ControlToValidate="txtDate"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <asp:HiddenField ID="hfserno" runat="server" />
                <asp:Label ID="Label6" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left" colspan="6">
                <asp:TextBox ID="txtacname" runat="server" Width="300px" TabIndex="1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                    runat="server" ControlToValidate="txtacname"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Credit/Debit"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drlrecpay" runat="server" Width="105px" TabIndex="2">
                    <asp:ListItem Selected="True" Value="DR">Debit</asp:ListItem>
                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td align="left">
                &nbsp;
                <asp:Label ID="Label8" runat="server" Text="Amount"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtamount" runat="server" TabIndex="4"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                    runat="server" ControlToValidate="txtamount"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                <asp:Button ID="btnsave" runat="server" Text="Save" TabIndex="6" 
                    ValidationGroup="Save"/>
                    <form action="btnsave" onsubmit="return cancelEvent('onbeforeunload')">
                <asp:HiddenField ID="hfupdate" runat="server" Value="0" />
            </td>
        </tr>
    </table>
    <hr />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="tserno"
        HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
        Width="100%" EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
        PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" BorderColor="Black"
        ShowFooter="True">
        <Columns>
            <asp:BoundField DataField="tserno" HeaderText="Serial No." Visible="false" ReadOnly="True" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="right" Width="15%" CssClass="hs" />
            </asp:BoundField>
              <asp:TemplateField HeaderText="Account" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
            <asp:label text='<%#Eval("acname")%>' runat="server" />
            <asp:HiddenField runat="server" id="hf_accCode" Value='<%#Eval("acccd")%>'/>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="rp" HeaderText="Credit/Debit" ReadOnly="True" />
            <asp:BoundField DataField="dramt" HeaderText="Debit Amount" ReadOnly="True" />
            <asp:BoundField DataField="cramt" HeaderText="Credit Amount" ReadOnly="True" />
            <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                        CommandArgument='<%#Eval("tserno")%>' CssClass="hs" />
                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                        CommandArgument='<%#Eval("tserno")%>' OnClientClick="return confirm('Are you sure want to Delete?');" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="15%" CssClass="hs" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
        <FooterStyle BackColor="#CC99FF" BorderColor="#6600CC" />
        <HeaderStyle BackColor="Black" ForeColor="White"></HeaderStyle>
        <PagerStyle HorizontalAlign="Right"></PagerStyle>
    </asp:GridView>
    <table>
        <tr>
            <td align="right" colspan="3">
                <asp:Label ID="Label9" runat="server" Text="Narration" Visible="False"></asp:Label>
            </td>
            <td align="left" colspan="11">
                <asp:TextBox ID="txtnarration" runat="server" Width="442px" TabIndex="1" 
                    Visible="False"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="saveconfirm" runat="server" Text="Save" TabIndex="2" 
                    Visible="False" />
            </td>
        </tr>
    </table>
<%--    <a href="http://localhost:4475/komal1/JournalEntry.aspx" id="navigate"> click here </a>
--%></asp:Content>
