﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ledger_Report.aspx.vb" Inherits="Ledger_Report"  MasterPageFile="~/MasterPage.master"%>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
     <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtacname.ClientID %>").autocomplete({
                autoFocus: true,
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Service.asmx/GetLedgeracname") %>',
                        data: "{ 'prefix': '" + request.term + "','acccd' : '" + $("#<%=txtacccd.ClientID %>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (acname) {
                                return {
                                    label: acname.split('-')[0],
                                    val: acname.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=txtacname.ClientID %>").val(i.item.val);
                    $("#<%=txtacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacccd.ClientID %>").val(i.item.val);
                    $("#<%=hfacname.ClientID %>").val(i.item.label);


                },
                minLength: 1

            });
        }); 
    </script>
    <table width="100%">
        <tr>
            <td colspan="5" align="left">
                <asp:Label ID="Label2" runat="server" Text="Sales Outstandings" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right" colspan="5">
                <asp:LinkButton ID="Btn_Print" runat="server" Text="Print" />
            </td>
        </tr>
    </table>
    <hr />
    <table width="100%">
        <tr>
            <%--<td align="left">
                <asp:Label ID="Label3" runat="server" Text="Sales Invoice no."></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
            </td>--%>
            <td align="left">
                <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtacname" runat="server"></asp:TextBox>
            </td>
            <td align="left">
                <asp:TextBox ID="txtacccd" runat="server" Enabled="false"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left">
                <bdp:basicdatepicker ID="txtfromdate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
            </td>
            <td align="left">
                <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left">
                <bdp:basicdatepicker ID="txttodate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
            </td>
            <td align="left">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" />
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                    <asp:HiddenField ID="hfacname" runat="server" />
            </td>
            <td align="left">
                    <asp:HiddenField ID="hfacccd" runat="server"/>
            </td>
            <td align="left">
                    &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
        </tr>
    </table>
    <div>
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
    </div>
</asp:Content>