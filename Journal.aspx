﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Journal.aspx.vb" Inherits="Journal"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <link href="Scripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table width="100%">
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#6600CC"
                    Text="Journal"></asp:Label>
            </td>
            <td colspan="3">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td colspan="2">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" EnableTheming="True" />
            </td>
        </tr>
        <tr>
         <td align="right" class="style8">
                <asp:Label ID="lbl_SearchJENo" runat="server" Text="Journal Entry No."></asp:Label>
            </td>
            <td align="left" class="style8" colspan="2">
                <asp:TextBox ID="txt_SearchJENo" runat="server"></asp:TextBox>
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="left" class="style9">
                <asp:Button ID="Btn_Search" runat="server" Text="Search" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="serno" 
        HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
        Width="100%" EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
        PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" BorderColor="Black"
        ShowFooter="True" ShowHeaderWhenEmpty="True">
        <Columns>
            <asp:BoundField DataField="serno" HeaderText="Serial No." ReadOnly="True" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="right" Width="15%" CssClass="hs" />
            </asp:BoundField>
            <asp:BoundField DataField="date" HeaderText="Date" ReadOnly="True" />
            <asp:BoundField DataField="narr" HeaderText="Narration" ReadOnly="True" />
            <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                        CommandArgument='<%#Eval("serno")%>' CssClass="hs" />
                    <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                        CommandArgument='<%#Eval("serno")%>' OnClientClick="" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="15%" CssClass="hs" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
        <FooterStyle BackColor="#CC99FF" BorderColor="#6600CC" />
        <HeaderStyle BackColor="Black" ForeColor="White"></HeaderStyle>
        <PagerStyle HorizontalAlign="Right"></PagerStyle>
    </asp:GridView>
<%--    <asp:Panel ID="panel2" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" 
            OnInit="GridView1_Init" ShowHeaderWhenEmpty="True">
        <Columns>
        <asp:TemplateField HeaderText="Column1">
        <ItemTemplate>
        <asp:TextBox ID="c1" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Column2">
        <ItemTemplate>
        <asp:TextBox ID="c2" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Column3">
        <ItemTemplate>
        <asp:TextBox ID="c3" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Column4">
        <ItemTemplate>
        <asp:TextBox ID="c4" runat="server"></asp:TextBox>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
</asp:GridView>
</asp:Panel>--%>
</asp:Content>
