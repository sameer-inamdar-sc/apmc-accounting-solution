﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Partial Class ratemaster
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Protected Sub Btn_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Response.Redirect("rateadd.aspx")
    End Sub
    Protected Sub bindgrid()
        con.Open()
        Dim filter As String = ""
        If txtacname.Text <> "" Then
            filter = "and acmast.acname like '" & txtacname.Text & "%'"
        End If
        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If
        Dim cmd As New SqlCommand("select serno,CONVERT(VARCHAR(10),date, 105) AS date,ratemast.acccd,upper(acmast.acname) acname from ratemast join acmast on ratemast.acccd=acmast.acccd " & filter & "ORDER BY acmast.acname DESC", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        gridmaster.DataSource = dt
        gridmaster.DataBind()
        con.Close()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindgrid()
        End If
    End Sub
    Protected Sub gridmaster_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridmaster.RowCommand
        If e.CommandName = "e" Then
            Dim srno As String = Convert.ToInt32(e.CommandArgument)
            Response.Redirect("~/rateadd.aspx?id=" & srno)
        End If
        If e.CommandName = "d" Then
con.Open()
            Dim srno As Integer = e.CommandArgument
            Dim cmd8 As New SqlCommand("select COUNT(tserno) from ratetran where serno=" + srno.ToString() + "", con)
            Dim a As String
            a = cmd8.ExecuteScalar()
            If a = 0 Then
                Dim cmd7 As New SqlCommand("delete from ratemast where serno=" + srno.ToString() + "", con)
                cmd7.ExecuteNonQuery()
                MsgBox("Your Record Has Been Deleted", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record Deleted")
            Else
                MsgBox("Your Record can not delete", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Information + MsgBoxStyle.SystemModal, "Record can not Delete")
            End If
            con.Close()
            bindgrid()
        End If

    End Sub

    Protected Sub BtnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnShow.Click
        bindgrid()
    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gridmaster.PageIndexChanging
        gridmaster.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub
End Class
