﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesRegister.aspx.vb" Inherits="SalesRegister"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td colspan="5" align="left">
                <asp:Label ID="Label2" runat="server" Text="Sales Register" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right" colspan="5">
                <asp:LinkButton ID="Btn_Print" runat="server" Text="Print" 
                    ValidationGroup="y" />
            </td>
        </tr>
    </table>
    <hr />
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label3" runat="server" Text="Sales Invoice no."></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchAcName" runat="server"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtSearchFromDate" ErrorMessage="*" ValidationGroup="y"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtSearchToDate" ErrorMessage="*" ValidationGroup="y"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" 
                    ValidationGroup="y" />
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
    </table>
    <%--<asp:Panel ID="panel1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="serno"
            HorizontalAlign="Center" AllowPaging="True" BackColor="White" BorderColor="#CCCCCC"
            BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%"
            EmptyDataText="Data not Found!!!" EmptyDataRowStyle-HorizontalAlign="Center">
            <Columns>
                <asp:BoundField DataField="serno" HeaderText="Invoice No." ReadOnly="true" SortExpression="serno"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="challanNo" HeaderText="Challan No." ReadOnly="true" SortExpression="pbillno"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="date" HeaderText="Date" ReadOnly="true" SortExpression="date"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="acccd" HeaderText="A/c Code" ReadOnly="true" SortExpression="acccd"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="acname" HeaderText="A/c Name" ReadOnly="true" SortExpression="acname" />
                <asp:BoundField DataField="lotno" HeaderText="Lot No." ReadOnly="true" SortExpression="lotno"
                    ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="tamount" HeaderText="Amount" ReadOnly="true" SortExpression="tamount"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:BoundField DataField="netamt" HeaderText="Net Amount" ReadOnly="true" SortExpression="netamt"
                    ItemStyle-HorizontalAlign="Right" />
                <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgbtnedit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                            CommandArgument='<%#Eval("serno")%>' />
                        <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                            CommandArgument='<%#Eval("serno")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="False" ForeColor="White" Font-Italic="False" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
    </asp:Panel>--%>
    <div>
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
    </div>
    <%--<asp:Panel ID="panel2" runat="server">
        <table width='100%'>
            <tr>
                <td width='3%'>
                    Sr No.
                </td>
                <td width='5%'>
                    Challan No
                </td>
                <td width='8%'>
                    Invoice date
                </td>
                <td width='7%'>
                    Account Code
                </td>
                <td width='20%'>
                    Account Name
                </td>
                <td width='15%'>
                    Item Name
                </td>
                <td width='7%'>
                    Quantity
                </td>
                <td width='10%'>
                    Weight
                </td>
                <td width='10%'>
                    Rate
                </td>
                <td width='15%'>
                    Amount
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
</asp:Content>
