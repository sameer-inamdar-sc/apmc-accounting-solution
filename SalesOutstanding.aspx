﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesOutstanding.aspx.vb"
    Inherits="SalesOutstanding" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
     <link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    $(document).ready(function () {
        $("#<%=txtSearchAcName.ClientID %>").autocomplete({
            autoFocus: true,
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("~/Service.asmx/GetAcNameSaleOut") %>',
                    data: "{ 'prefix': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (acname) {
                            return {
                                label: acname.split('-')[0],
                                val: acname.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("#<%=hfacccd.ClientID %>").val(i.item.val);
                $("#<%=hfSearchAcName.ClientID %>").val(i.item.label);


            },
            minLength: 1

        });
    }); 
    </script>
    <table width="100%">
        <tr>
            <td colspan="5" align="left">
                <asp:Label ID="Label2" runat="server" Text="Sales Outstandings" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right" colspan="5">
                <asp:LinkButton ID="Btn_Print" runat="server" Text="Print" 
                    ValidationGroup="a" />
            </td>
        </tr>
    </table>
    <hr />
    <table width="100%">
        <tr>
            <%--<td align="left">
                <asp:Label ID="Label3" runat="server" Text="Sales Invoice no."></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
            </td>--%>
            <td align="left">
                <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtSearchAcName" runat="server"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtSearchFromDate" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left">
                <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
                    DisplayType="TextBox" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtSearchToDate" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>
            </td>
            <td align="left">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generate Report" 
                    ValidationGroup="a" />
            </td>
            <td align="left">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                    <asp:HiddenField ID="hfSearchAcName" runat="server" />
            </td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                    <asp:HiddenField ID="hfacccd" runat="server" />
            </td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
        </tr>
    </table>
    <div>
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
    </div>
</asp:Content>
