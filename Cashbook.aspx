﻿
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cashbook.aspx.vb" Inherits="Cashbook"
    MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.js" type="text/javascript"></script>
    <link href="Scripts/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript">
        $(function () {
            $("#txtdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#TextBox1").datepicker();
        });
    </script>
    <script type="text/javascript">
    </script>
    <style type="text/css">
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#6600CC"
                    Text="Cash Book"></asp:Label>
            </td>
            <td colspan="3">
                <asp:HiddenField ID="hfaccd" runat="server" />
            </td>
            <td colspan="2">
                <asp:HiddenField ID="hfacname" runat="server" />
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" EnableTheming="True" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_searchcode" runat="server" Text="Cash Book No."></asp:Label>
            </td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txt_Searchcashbook" runat="server"></asp:TextBox>
            </td>
            <td align="right">
                &nbsp;
            </td>
            <td align="left" colspan="2">
                &nbsp;
            </td>
            <td align="right" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_fromDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="right" class="style9">
                <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
            </td>
            <td align="left" class="style9">
                <BDP:BasicDatePicker ID="txt_toDate" runat="server" DisplayType="TextBox" DateFormat="dd-MM-yyyy" />
            </td>
            <td align="left">
                <asp:Button ID="btn_search" runat="server" Text="Search" />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="11">
                <hr />
            </td>
        </tr>
        <tr>
            <td align="right" colspan="11">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
                    Width="100%" AutoGenerateColumns="False" DataKeyNames="cbno" HorizontalAlign="Center"
                    EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
                    PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
                    <Columns>
                        <asp:BoundField DataField="CBNo" HeaderText="CashBook No" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="date" HeaderText="Date" ReadOnly="True" SortExpression="date" />
                        <asp:BoundField DataField="Type" HeaderText="Transaction Type" ReadOnly="True" SortExpression="Dr" />
                        <asp:BoundField DataField="mode" HeaderText="Mode" ReadOnly="True" SortExpression="Cr" />
                        <asp:BoundField DataField="Bankname" HeaderText="Bankname" ReadOnly="True" />
                       <asp:BoundField DataField="Total" HeaderText="Total Amount" ReadOnly="true" SortExpression="Total"/>
                        <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                                    CommandArgument='<%#Eval("cbno")%>' />
                                <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                                    OnClientClick="" CommandArgument='<%#Eval("cbno")%>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BorderStyle="None" HorizontalAlign="Center" />
                    <EmptyDataRowStyle BorderStyle="None" HorizontalAlign="Center" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
