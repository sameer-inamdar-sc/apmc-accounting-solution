﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.DataSet
Imports System.Globalization
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Partial Class Journal
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Public query As String, constr As String
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        Bindgrid()
    End Sub

    Protected Sub Btn_Add_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Btn_Add.Click
        Response.Redirect("JournalEntry.aspx")

    End Sub
    Protected Sub Bindgrid()
        Dim filter As String = ""
        If txt_SearchJENo.Text <> "" Then
            filter = "and serno= " & txt_SearchJENo.Text & ""
        End If

        If (txt_fromDate.Text <> "" Or txt_fromDate.SelectedDateFormatted.ToString() <> "") And (txt_toDate.Text <> "" Or txt_toDate.SelectedDateFormatted.ToString() <> "") Then
            filter = filter & "and date BETWEEN '" & txt_fromDate.SelectedDate & "' AND '" & txt_toDate.SelectedDate & "'"
        End If
        If filter <> "" Then
            filter = Replace(filter, "and", "where", 1, 1)
        End If
        con.Open()
        Dim cmd As New SqlCommand("select serno,CONVERT(VARCHAR(10), date, 105) AS date,narr from journalmast " & filter & " ORDER BY date desc", con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Bindgrid()
        End If
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        If e.CommandName = "e" Then
            Dim srno As String = Convert.ToInt32(e.CommandArgument)

            Response.Redirect("~/JournalEntry.aspx?id=" & srno)
        End If

        If e.CommandName = "d" Then
            con.Open()
            Dim intResponse As Integer
            intResponse = MsgBox("You want to delete whole data", MsgBoxStyle.MsgBoxSetForeground + MsgBoxStyle.Critical + MsgBoxStyle.SystemModal + MsgBoxStyle.YesNoCancel, "Delete")
            Dim srno As Integer = e.CommandArgument
            If intResponse = MsgBoxResult.Yes Then
                Dim Cmd As New SqlCommand("delete from journal where serno =" & srno.ToString() & "  delete from journalmast where serno =" & srno.ToString(), con)
                Cmd.ExecuteNonQuery()
                query = "ledger_Delete"
                Dim com As New SqlCommand(query, con)
                com.CommandType = CommandType.StoredProcedure
                com.Parameters.AddWithValue("@serno", srno)
                com.Parameters.AddWithValue("@compid", Session("compid"))
                com.Parameters.AddWithValue("@doccd", "JE")
                com.ExecuteNonQuery()

                MsgBox("Entry Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")

                con.Close()

                Bindgrid()
            End If
        End If
    End Sub
End Class
