﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic

''deepak

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
'<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class Service
    Inherits System.Web.Services.WebService


    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetItems(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select item,itmcd,unit from item where " & "item like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}-{2}", sdr("item"), sdr("itmcd"), sdr("unit")))
                    End While
                End Using
                conn.Close()
                If customers.Count = 0 Then

                End If
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcName(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where mjcd='a4' and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcNameforSales(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "SELECT DISTINCT c.acccd, a.acname from acmast a join challanMast c on a.acccd=c.acccd and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcNameforjournal(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where mjcd<>'A3' and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetbankName(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select a.acccd, a.acname, p.place from acmast a join place p on a.srplace=p.serno where a.mjcd='a3' and a.acname not like '%cash%' and " & "a.acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}-{2}", sdr("acname"), sdr("acccd"), sdr("place")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetallAcName(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetPurchaser(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where mjcd='L4' and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcc(ByVal prefix As String, ByVal prefix1 As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                If prefix1 = 1 Then
                    cmd.CommandText = "select acccd, acname from acmast where mjcd='L4' and " & "acname like @SearchText + '%'"
                ElseIf prefix1 = 0 Then
                    cmd.CommandText = "select acccd, acname from acmast where mjcd='A4' and " & "acname like @SearchText + '%'"
                End If
                'cmd.CommandText = "select acccd, acname from acmast where mjcd='L4' and " & "acname like @SearchText + '%' and " & "rp = @pagetype"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Parameters.AddWithValue("@pagetype", prefix1)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetPlace(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select distinct serno,Place from Place where " & "Place like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("Place"), sdr("serno")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
  <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetDetailsbyatpost(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select distinct atpost+' | '+tal+' | '+dist Place from Place where " & "atpost like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}", sdr("Place")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetDetailsbytal(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select distinct tal+' | '+dist Place from Place where " & "tal like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}", sdr("Place")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetDetailsbydist(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select distinct dist from Place where " & "dist like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}", sdr("dist")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetTransDtls(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where grcd='tr' and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetChallanAcName(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select distinct a.acccd, a.acname from acmast a left join ratemast r on a.acccd=r.acccd where a.acccd=r.acccd and " & "a.acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetItemsForChallan(ByVal prefix As String, ByVal acccd As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select i.item,rt.itmcd,unit,rt.rate from item i join ratetran rt on rt.itmcd=i.itmcd join ratemast rm on rt.serno=rm.serno where item like @SearchText + '%' and acccd= @acccd + ''"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}-{2}-{3}", sdr("item"), sdr("itmcd"), sdr("unit"), sdr("rate")))
                    End While
                End Using
                conn.Close()
                If customers.Count = 0 Then

                End If
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
   <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetItemsForPurchase(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select item,item.itmcd,unit,date,hamali,tolai,APMC,maplevy from item inner join itmexp on item.itmcd=itmexp.itmcd where " & "item like @SearchText + '%' " & " and date=(select max(date) from itmexp inner join item on item.itmcd=itmexp.itmcd and " & "item like @SearchText + '%') "
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}", sdr("item"), sdr("itmcd"), sdr("unit"), sdr("date"), sdr("hamali"), sdr("tolai"), sdr("APMC"), sdr("maplevy")))
                    End While
                End Using
                conn.Close()
                If customers.Count = 0 Then

                End If
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetChallanNoForSales(ByVal prefix As String, ByVal acccd As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select challanNo from challanMast where " & "challanNo like @SearchText + '%' and acccd = @acccd + '' and challanNo not in (select challanNo from smast)"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}", sdr("challanNo")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
 <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetLedgeracname(ByVal prefix As String, ByVal acccd As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd, acname from acmast where acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Parameters.AddWithValue("@acccd", acccd)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcNameSaleOut(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acname from acmast where acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}", sdr("acname")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
    <WebMethod()> _
   <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAcNameForCashBook(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Using conn As New SqlConnection()
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString
            Using cmd As New SqlCommand()
                cmd.CommandText = "select acccd,acname from acmast where (mjcd='A4' or mjcd='L4' or mjcd='L5') and " & "acname like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("acname"), sdr("acccd")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function
End Class