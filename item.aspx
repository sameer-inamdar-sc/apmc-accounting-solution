﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="item.aspx.vb" Inherits="item" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <title>jQuery UI Dialog - Default functionality</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script language="Javascript">
        $(function () {


            $('.decimal').bind('paste', function () {
                var self = this;
                setTimeout(function () {
                    if (!/^\d*(\.\d{1,2})+$/.test($(self).val())) $(self).val('');
                }, 0);
            });

            $('.decimal').keypress(function (e) {
                var character = String.fromCharCode(e.keyCode)
                var newValue = this.value + character;
                if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
                    e.preventDefault();
                    return false;
                }
            });

            function hasDecimalPlace(value, x) {
                var pointIndex = value.indexOf('.');
                return pointIndex >= 0 && pointIndex < value.length - x;
            }
        });
    </script>
    <style type="text/css">
        .hs
        {
            padding-right: 10px;
        }
        .AutoExtender
        {
            font-family: arial, cambria, Helvetica, sans-serif;
            font-size: 10pt;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 0px;
        }
        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Blue;
        }
        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }
        
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }
    </style>
    <table class="style4" width="100%">
        <tr>
            <td colspan="2" style="padding-left: 5px;" align="left">
                <asp:Label ID="Label2" runat="server" Text="Item Master" Font-Bold="True" Font-Size="Large"
                    ForeColor="#6600CC"></asp:Label>
            </td>
            <td colspan="3" style="padding-left: 5px;" align="left">
                <asp:HiddenField ID="hfItemId" runat="server" />
            </td>
            <td colspan="2" style="padding-left: 5px;" align="left">
                <asp:HiddenField ID="hfItemname" runat="server" />
            </td>
            <td colspan="5" align="right">
                <asp:ImageButton ID="Btn_Add" runat="server" ImageUrl="~/img/add.png" />
            </td>
        </tr>
        <tr>
            <td align="right" class="style8">
                <asp:Label ID="lbl_SearchItmID" runat="server" Text="Item Code"></asp:Label>
            </td>
            <td align="left" class="style8" colspan="2">
                <asp:TextBox ID="txt_SearchCode" runat="server" Class="decimal">
                </asp:TextBox>
            </td>
            <td align="right" class="style8">
                <asp:Label ID="lbl_SearchItmName" runat="server" Text="Item Name"></asp:Label>
            </td>
            <td align="left" class="style8" colspan="2">
                <asp:TextBox ID="txt_SearchDescription" runat="server">
                </asp:TextBox>
            </td>
            <td align="right" colspan="2">
                <asp:Button ID="Btn_Search" runat="server" Text="Search" />
            </td>
            <td align="left" class="style9">
                &nbsp;
            </td>
            <td align="right" class="style9">
                &nbsp;
            </td>
            <td align="left" class="style9">
                &nbsp;
            </td>
            <td align="left" class="style9">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" class="style8" colspan="12">
                <hr />
            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server" Height="397px">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="itmcd"
            HorizontalAlign="Center" AllowPaging="True" PagerStyle-HorizontalAlign="Right"
            Width="100%" EmptyDataText="Data not found !!!" EmptyDataRowStyle-HorizontalAlign="Center"
            PageSize="15" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
            <Columns>
                <asp:BoundField DataField="itmcd" HeaderText="Item Code" ReadOnly="True" SortExpression="itmcd"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="right" Width="15%" CssClass="hs" />
                </asp:BoundField>
                <asp:BoundField DataField="item" HeaderText="Item Name" ReadOnly="True" SortExpression="item" />
                <asp:BoundField DataField="unit" HeaderText="Unit" ReadOnly="True" SortExpression="unit">
                    <ItemStyle HorizontalAlign="right" Width="15%" CssClass="hs" />
                </asp:BoundField>
                <asp:BoundField DataField="tare" HeaderText="Tare" ReadOnly="True" SortExpression="tare"
                    ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="right" Width="15%" CssClass="hs" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton ID="edit" runat="server" ImageUrl="~/img/Edit.png" CommandName="e"
                            CommandArgument='<%#Eval("itmcd")%>' CssClass="hs" />
                        <asp:ImageButton ID="delete" runat="server" ImageUrl="~/img/Delete.png" CommandName="d"
                            CommandArgument='<%#Eval("itmcd")%>' OnClientClick="return confirm('Are you sure want to Delete?');" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="15%" CssClass="hs" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle HorizontalAlign="Center" />
            <RowStyle Height="7px" />
        </asp:GridView>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel2" runat="server" BackColor="White" Width="302px" ForeColor="Black"
        Font-Bold="true" Font-Size="X-Large" Font-Names="Book Antiqua" Height="237px"
        BorderColor="White" BorderStyle="Outset" BorderWidth="6px">
        <asp:Button ID="zhol" runat="server" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="zhol"
            PopupControlID="Panel2" CancelControlID="Btn_Cancle" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <div id="dialog" title="Add Item Expenses" class="dialog" style="font-family: Arial, Helvetica, sans-serif;
            font-size: smaller">
            <table align="center" height="200px" width="300px" cellspacing="0">
                <tr>
                    <td>
                        <table width="300px" height="200px" align="center">
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Label ID="lbl_popuphead" runat="server" Font-Bold="True" Font-Italic="True"
                                        Font-Names="Bookman Old Style" Font-Overline="True" Font-Size="X-Large" Font-Underline="True"
                                        Text="Item Details"></asp:Label>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="Txtcompid" runat="server" Visible="False" Width="40px">
                                    </asp:TextBox>
                                    <asp:TextBox ID="Txtuserid" runat="server" Visible="False" Width="40px">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:ToolkitScriptManager ID="toolscriptmanager1" runat="server">
                                    </asp:ToolkitScriptManager>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label3" runat="server" Text="Item Code"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtitmid" runat="server" Enabled="False">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label1" runat="server" Text="Item Name" Width="100px"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtitem" runat="server" onfocus="disableautocompletion(this.id)"
                                        TabIndex="2">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtitem"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label4" runat="server" Text="Unit"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtunit" runat="server" onfocus="disableautocompletion(this.id)"
                                        TabIndex="3" Class="decimal" ValidationGroup="Save">0</asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtunit"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="Label5" runat="server" Text="Tare"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txttare" runat="server" onfocus="disableautocompletion(this.id)"
                                        TabIndex="4" Class="decimal" ValidationGroup="Save">0</asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttare"
                                        ErrorMessage="*" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            <tr height="45px">
                                <td colspan="2" align="center">
                                    <asp:Button ID="btn_save" runat="server" Text="Save" ValidationGroup="Save" />
                                    <asp:Button ID="Btn_Cancle" runat="server" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
