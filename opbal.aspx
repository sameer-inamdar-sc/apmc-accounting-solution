﻿
<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="opbal.aspx.vb" Inherits="opbal" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <table align="center" height="50px">
      <tr>
      <td>
       <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large" 
            ForeColor="#6600CC" Text="Opening Balance "></asp:Label>
      </td>
      </tr>
      </table>
       
       <center><table height="50px">
       <tr>
       <td>
       <asp:Label ID="Label4" runat="server" Text="Major Code"></asp:Label>
       </td>
       <td>
       <asp:DropDownList ID="drlmjname" AutoPostBack="true" runat="server" Height="20px" Width="343px">
       </asp:DropDownList>
       <asp:TextBox ID="txtmjcd" runat="server" Height="17px" Width="37px" Enabled="false"></asp:TextBox>
       <asp:TextBox ID="Txtcompid" runat="server" Width="60px" Visible="False"></asp:TextBox>
       <asp:TextBox ID="Txtuserid" runat="server" Width="59px" Visible="False"></asp:TextBox>
       </td>
       </tr>
       </table></center>
       
       <center><table>
       <tr>
       <td>
         <asp:Panel ID="Panel1" runat="server" Visible="false">
          <table border="1" cellpadding="0" cellspacing="0">
          <tr>
          <td>
          <asp:GridView ID="GridView1" runat="server" 
                             AllowPaging="True" 
                             DataSourceID="SqlDataSource1" 
                             AutoGenerateColumns="False">
        <Columns>
       <asp:TemplateField HeaderText="Select">
        <ItemTemplate>
          <asp:CheckBox ID="chkSelect" runat="server" 
                  AutoPostBack="true" Enabled="false"
             OnCheckedChanged="chkSelect_CheckedChanged"/>
        </ItemTemplate>
    
    </asp:TemplateField>
    <asp:BoundField DataField="acccd" HeaderText="Account Code" 
                                   SortExpression="acccd"/>
     <asp:BoundField DataField="acname" HeaderText="Account name" 
                                   SortExpression="acname"/>
    <asp:BoundField DataField="mjcd" HeaderText="Major Code" 
                                   SortExpression="mjcd"/>
     <asp:BoundField DataField="grcd" HeaderText="Group Code" 
                                   SortExpression="grcd"/> 
                                                                                         
    <asp:TemplateField HeaderText="Opening Balance" 
                       SortExpression="opbal">
    <ItemTemplate>
    <asp:TextBox ID="txtopbal" runat="server" 
                 Text='<%# Bind("opbal") %>' ReadOnly="true" 
                 ForeColor="Blue" BorderStyle="none" style="text-align:right"
                 BorderWidth="0px" >
    </asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    
     <asp:TemplateField HeaderText="Sign" 
                       SortExpression="sign12">
    <ItemTemplate>
       
    <asp:DropDownList ID="drlsign12" runat="server" Text='<%# Bind("sign12") %>'  ReadOnly="true" 
                 ForeColor="Blue" BorderStyle="none"  BorderWidth="0px">
                 <asp:ListItem>CR</asp:ListItem>
                 <asp:ListItem>DR</asp:ListItem>
         </asp:DropDownList>
               
    </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:komalConnectionString %>"
            SelectCommand="SELECT [acccd], [acname], [mjcd], [grcd], [opbal], [sign12] FROM [openbal] WHERE ([mjcd] = @mjcd)">
         <SelectParameters>
        <asp:ControlParameter ControlID="txtmjcd" Name="mjcd" PropertyName="Text" 
            Type="String" />
    </SelectParameters>
        </asp:SqlDataSource>
        <center> <asp:Button ID="btnupdate" runat="server" Text="Update" /></center>
          </td>
          </tr>
          </table>
      </asp:Panel>
       </td>
       </tr>
       </table>
       </center>
   
</asp:Content>

