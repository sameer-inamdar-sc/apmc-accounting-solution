﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PurchaseOutstanding.aspx.vb"
    Inherits="PurchaseOutstanding" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls"
    TagPrefix="BDP" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" runat="server" Text="Purchase Outstanding" Font-Bold="True"
                    Font-Size="Large" ForeColor="#6600CC"></asp:Label>
            </td>
            <td align="right">
                <asp:LinkButton ID="BtnPrint" runat="server" Text="Print" />
            </td>
        </tr>
    </table>
    <hr />
    <div>
        <table width="100%">
            <tr>
                <%--<td align="left">
                    <asp:Label ID="Label3" runat="server" Text="Purchase Bill no."></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtSearchInvoice" runat="server"></asp:TextBox>
                </td>--%>
                <td align="left">
                    <asp:Label ID="Label4" runat="server" Text="Account Name"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtSearchAcName" runat="server"></asp:TextBox>
                </td>
                <td align="left">
                    <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
                </td>
                <td align="left">
                    <BDP:BasicDatePicker ID="txtSearchFromDate" runat="server" DateFormat="dd-MM-yyyy"
                        DisplayType="TextBox" />
                </td>
                <td align="left">
                    <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
                </td>
                <td align="left">
                    <BDP:BasicDatePicker ID="txtSearchToDate" runat="server" DateFormat="dd-MM-yyyy"
                        DisplayType="TextBox" />
                </td>
                <td align="left">
                    <asp:Button ID="GenerateReport" runat="server" Text="Generate Report" />
                </td>
                <td align="left">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div>
        <asp:Label ID="lblPageString" runat="server" EnableViewState="False" Width="100%"></asp:Label>
        <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
    </div>
</asp:Content>
