﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Globalization

Partial Class paytran
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("komalConnectionString").ConnectionString)
    Public query1 As String

    Public Sub InsertInLedgerDr()
        con.Open()
        query1 = "ledger_Insert"
        Dim com As New SqlCommand(query1, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtserno.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", IIf(Page.Request.QueryString("type") = "0", "RC", "PP"))
        com.Parameters.AddWithValue("@tdate", txt_Date.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", IIf(Page.Request.QueryString("type") = "0", "R", "P"))
        com.Parameters.AddWithValue("@acccd1", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", IIf(Page.Request.QueryString("type") = "0", "CR", "DR"))
        com.Parameters.AddWithValue("@acccd2", hfacccd1.Value)
        com.Parameters.AddWithValue("@amount", paidamt)
        com.Parameters.AddWithValue("@chqno", txtchqno0.Text.ToString())
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", IIf(Page.Request.QueryString("type") = "0", "RC", "PP") & "-" & txtserno.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Public Sub InsertInLedgerCr()
        con.Open()
        query1 = "ledger_Insert"
        Dim com As New SqlCommand(query1, con)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.AddWithValue("@serno", txtserno.Text.ToString())
        com.Parameters.AddWithValue("@compid", Txtcompid.Text.ToString())
        com.Parameters.AddWithValue("@userid", Txtuserid.Text.ToString())
        com.Parameters.AddWithValue("@doccd", IIf(Page.Request.QueryString("type") = "0", "RC", "PP"))
        com.Parameters.AddWithValue("@tdate", txt_Date.SelectedDate.ToString())
        com.Parameters.AddWithValue("@rp", IIf(Page.Request.QueryString("type") = "0", "R", "P"))
        com.Parameters.AddWithValue("@acccd2", txtAcCode.Text.ToString())
        com.Parameters.AddWithValue("@sign12", IIf(Page.Request.QueryString("type") = "0", "DR", "CR"))
        com.Parameters.AddWithValue("@acccd1", hfacccd1.Value)
        com.Parameters.AddWithValue("@amount", paidamt)
        com.Parameters.AddWithValue("@chqno", txtchqno0.Text.ToString())
        com.Parameters.AddWithValue("@narrcd", "")
        com.Parameters.AddWithValue("@lotno", IIf(Page.Request.QueryString("type") = "0", "RC", "PP") & "-" & txtserno.Text.ToString())
        com.Parameters.AddWithValue("@posting", "")
        com.ExecuteNonQuery()
        con.Close()
    End Sub
    Protected Sub bindgrid()
        con.Open()
        Dim s, q As String
        
        If Page.Request.QueryString("type") = "1" Then
            s = "select ROW_NUMBER() OVER (ORDER BY p.serno aSC)'Sr. No.',p.serno pmastid, p.pbillno 'Invoice No.',CONVERT(VARCHAR(10), p.date, 105) AS date,p.netamt 'Amount',sum(isnull(d.amount,0.00)) 'Paid Amount' from TransactionDetails d right join TransactionMaster m on d.TransactionMasterID=m.TransactionMasterID right join pmast p on d.pmastid=p.serno where p.acccd= " & hfAccode.Value & " and p.netamt <>0 group by p.pbillno,p.date,p.netamt ,p.serno having p.netamt<>sum(isnull(d.amount,0.00)) order by date"
        ElseIf Page.Request.QueryString("type") = "0" Then
            s = "select ROW_NUMBER() OVER (ORDER BY p.serno aSC)'Sr. No.',p.serno pmastid, p.challanNo 'Invoice No.',CONVERT(VARCHAR(10), p.InvoiceDate, 105) AS InvoiceDate,p.netamt 'Amount',sum(isnull(d.amount,0.00)) 'Paid Amount' from TransactionDetails d right join TransactionMaster m on d.TransactionMasterID=m.TransactionMasterID right join smast p on d.pmastid=p.serno where p.acccd= " & hfAccode.Value & " and p.netamt <>0 group by p.challanNo,p.InvoiceDate,p.netamt ,p.serno having p.netamt<>sum(isnull(d.amount,0.00)) order by InvoiceDate"
        End If
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        con.Close()
        txtAcCode.Text = hfAccode.Value
        hftype.Value = Page.Request.QueryString("type")
    End Sub
    Protected Sub bindgrid2()
        con.Open()
        Dim s, q As String
        
        If Page.Request.QueryString("type") = 1 Then
            s = "select ROW_NUMBER() OVER (ORDER BY d.serno aSC)'Sr. No.',d.serno, p.serno pmastid, p.pbillno 'Invoice',CONVERT(VARCHAR(10), p.date, 105) AS date,p.netamt 'Amount',sum(isnull(d.amount,0.00)) 'Paid Amount' from TransactionDetails d right join TransactionMaster m on d.TransactionMasterID=m.TransactionMasterID right join pmast p on d.pmastid=p.serno where d.amount<>0 and p.acccd= '" & txtAcCode.Text & "' and d.amount <>0 and m.TransactionMasterID= " & hftransactionmasterid.Value & " group by p.pbillno,p.date,p.netamt,d.serno,p.serno"
        ElseIf hftype.Value = 0 Then
            s = "select ROW_NUMBER() OVER (ORDER BY d.serno aSC)'Sr. No.',d.serno, p.serno 'Invoice',CONVERT(VARCHAR(10), p.InvoiceDate, 105) AS date,p.netamt 'Amount',sum(isnull(d.amount,0.00)) 'Paid Amount' from TransactionDetails d right join TransactionMaster m on d.TransactionMasterID=m.TransactionMasterID right join smast p on d.pmastid=p.serno where d.amount<>0 and p.acccd=  '" & txtAcCode.Text & "'  and d.amount <>0 and m.TransactionMasterID= " & hftransactionmasterid.Value & " group by p.serno,p.InvoiceDate,p.netamt,d.serno"
        End If
        Dim cmd As New SqlCommand(s, con)
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable
        da.Fill(dt)
        gridview2.DataSource = dt
        gridview2.DataBind()
        con.Close()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hftype.Value = Page.Request.QueryString("type")
        If Page.Request.QueryString("type") = "0" Then
            Label1.Text = "Receipt"
            Label18.Visible = True
            txtbankBranch.Visible = True

        ElseIf Page.Request.QueryString("type") = "1" Then
            Label1.Text = "Payment"


        End If
        Txtuserid.Text = Session("userid")
        Txtcompid.Text = Session("compid")
        If Not Page.IsPostBack Then
            
            Dim type As String = Page.Request.QueryString("type")
            Dim id As String = Page.Request.QueryString("id")
            If Not id = String.Empty Then

                con.Open()
                Dim cmd3 As New SqlCommand("select t.*,a.acname from TransactionMaster t left join acmast a on a.acccd=t.AcCode where TransactionMasterID=" & id & " order by date", con)
                Dim da As New SqlDataAdapter(cmd3)
                Dim dt As New DataTable
                da.Fill(dt)

                txtAcCode.Text = dt.Rows(0).Item("accode")
                hfAccode.Value = dt.Rows(0).Item("accode")
                txtAcName.Text = dt.Rows(0).Item("acname")
                hftransactionmasterid.Value = id
                txtserno.Text = id
                con.Close()
                bindgrid()
                bindgrid2()
                Lbl_unpaidAmt.Visible = True
                Lbl_unpaid.Visible = True
            Else
                bindgrid()
                'bindgrid2()
            End If

        End If
        txt_Date.SelectedDate = Today
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("Payment.aspx?type=" & Page.Request.QueryString("type"))

    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        If e.Row.RowType = DataControlRowType.DataRow Then
            'total += Convert.ToInt32(e.Row.Cells("Amount"))
            total += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Amount"))
            'paidamt += Convert.ToDecimal(e.Row.Cells(4))
            paidamt += Convert.ToDecimal(e.Row.Cells(5).Text.Trim())

            e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
        End If

        If e.Row.RowType = DataControlRowType.Footer Then
            'Dim lblamount As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            'lblamount.Text = total.ToString()
            e.Row.Cells(4).Text = total.ToString("#0.00")
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(5).Text = paidamt.ToString("#0.00")
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            Lbl_unpaidAmt.Text = total - paidamt
        End If
    End Sub
    Dim total1 As Decimal = 0.0
    Dim paidamt1 As Decimal = 0.0
    Protected Sub GridView2_RowDataBound(sender As Object, e As GridViewRowEventArgs)


        If e.Row.RowType = DataControlRowType.DataRow Then
            'total += Convert.ToInt32(e.Row.Cells("Amount"))
            total1 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Amount"))
            'paidamt += Convert.ToDecimal(e.Row.Cells(4))
            paidamt1 += Convert.ToDecimal(e.Row.Cells(5).Text.Trim())

            e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
        End If


        If e.Row.RowType = DataControlRowType.Footer Then
            'Dim lblamount As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            'lblamount.Text = total.ToString()
            e.Row.Cells(5).Text = total1.ToString("#0.00")
            e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(6).Text = paidamt1.ToString("#0.00")
            e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Right
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Visible = False
            e.Row.Cells(2).Visible = False
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Visible = False
            e.Row.Cells(1).Visible = False
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(2).Visible = False
            e.Row.Cells(1).Visible = False
        End If
    End Sub

    Protected Sub Btn_GetBills_Click(sender As Object, e As System.EventArgs) Handles Btn_GetBills.Click
        bindgrid()
        txtAcName.Enabled = False
        drlcb.Enabled = True
        Txt_Amt.Enabled = True
        Btn_GetBills.Enabled = False
        Txt_Amt.Focus()
        BtnSave.Visible = True
        Lbl_unpaidAmt.Visible = True
        Lbl_unpaid.Visible = True

    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'If GridView1.FooterRow.Cells(6).Text = Txt_Amt.Text Then


        savetransactionmaster()
        For Each tr As GridViewRow In GridView1.Rows
            Dim txt As TextBox = DirectCast(tr.Cells(5).FindControl("TextBox1"), TextBox)
            Dim pmastid As Integer = tr.Cells(1).Text
            If txt.Text <> "0" Then
                savetransactiondetails(hftransactionmasterid.Value, txt.Text, pmastid)
            End If
        Next
        bindgrid()
        txtserno.Text = hftransactionmasterid.Value

        bindgrid2()
        BtnSave.Enabled = False
        Txt_Amt.Enabled = False
        GridView1.Enabled = False
        Lbl_unpaidAmt.Text = total - paidamt
        InsertInLedgerCr()
        InsertInLedgerDr()
        'Else
        'MsgBox("Total and Amount Mismatch", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Mismatch")

        'End If
    End Sub
    Public query As String, constr As String
    Public Sub savetransactiondetails(ByVal TransactionMasterID As Integer, ByVal amount As Integer, ByVal pmastid As Integer)
        Dim com As New SqlCommand()

        query = "TransactionDetails_Insert"

        com.Parameters.AddWithValue("@TransactionMasterID ", TransactionMasterID)
        'com.Parameters.AddWithValue("@InvoiceID ", InvoiceID)
        com.Parameters.AddWithValue("@amount ", amount)
        com.Parameters.AddWithValue("@pmastid ", pmastid)

        com.CommandText = query
        com.Connection = con
        con.Open()

        com.CommandType = CommandType.StoredProcedure
        com.ExecuteNonQuery()
        con.Close()

    End Sub
    Public Sub savetransactionmaster()
        Dim com As New SqlCommand()
        If hftransactionmasterid.Value = 0 Then
            query = "TransactionMaster_Insert"
        Else

            query = "TransactionMaster_UpdateByPK"
            com.Parameters.AddWithValue("@TransactionMasterID ", hftransactionmasterid.Value)

        End If
        com.CommandText = query
        com.Connection = con
        con.Open()

        com.CommandType = CommandType.StoredProcedure
        Dim d As String
        Dim thisDate As Date = txt_Date.SelectedDate
        Dim culture As New CultureInfo("pt-BR")
        d = thisDate.ToString("MM/dd/yyyy", culture)

        Dim e As String
        Dim thisDate1 As Date = txtdate0.SelectedDate
        Dim culture1 As New CultureInfo("pt-BR")
        e = thisDate1.ToString("MM/dd/yyyy", culture1)

        com.Parameters.AddWithValue("@date ", d)
        com.Parameters.AddWithValue("@AcCode ", hfAccode.Value)
        com.Parameters.AddWithValue("@TransType ", IIf(drlcb.SelectedValue = "Bn", False, True))
        com.Parameters.AddWithValue("@compid ", Txtcompid.Text)
        com.Parameters.AddWithValue("@userid ", Txtuserid.Text)
        com.Parameters.AddWithValue("@rp ", Page.Request.QueryString("type"))
        com.Parameters.AddWithValue("@createdOn ", DateTime.Now)


        If drlcb.SelectedValue = "Bn" Then
            com.Parameters.AddWithValue("@chqno ", txtchqno0.Text)
            com.Parameters.AddWithValue("@BankChqDate ", e)
            com.Parameters.AddWithValue("@BankBranch ", txtbankBranch.Text)
            If Page.Request.QueryString("type") = "0" Then
                com.Parameters.AddWithValue("@BankName ", txtbankname.Text)
            ElseIf Page.Request.QueryString("type") = "1" Then
                com.Parameters.AddWithValue("@casbankCode ", hfacccd1.Value)
            End If
        ElseIf drlcb.SelectedValue = "Cs" Then
            com.Parameters.AddWithValue("@casbankCode ", "0000097")
        End If
 





        hftransactionmasterid.Value = com.ExecuteScalar()
        con.Close()
        'bindgrid()
        'bindgrid2()
    End Sub


    Protected Sub drlcb_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles drlcb.SelectedIndexChanged
        If drlcb.SelectedValue = "Cs" Then
            'Div_ChequeDetails.Visible = False
            txtbankname.Text = ""
            txtbankBranch.Text = ""
            txtchqno0.Text = ""
            txtdate0.Clear()
        ElseIf drlcb.SelectedValue = "Bn" Then
            'Div_ChequeDetails.Visible = True
        End If
        bindgrid()
    End Sub
    Private total As Decimal = 0.0
    Private paidamt As Decimal = 0.0

    Protected Sub Btn_clear_Click(sender As Object, e As System.EventArgs) Handles Btn_clear.Click
        Response.Redirect("paytran.aspx?type=" & Page.Request.QueryString("type"))
    End Sub
    Protected Sub GridView2_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridview2.RowCommand

        If e.CommandName = "d" Then
            GridView1.Enabled = True
            gridview2.Enabled = True
            ''  If MsgBox("Are you sure want to Delete?", MsgBoxStyle.YesNo + MsgBoxStyle.SystemModal, "Delete Confirmation") = MsgBoxResult.Yes Then
            ' execute command

            con.Open()
            Dim srno As Integer = e.CommandArgument
            'hftransactionmasterid.Value = srno
            Dim cmd1 As New SqlCommand("delete from transactiondetails where serno=" + srno.ToString() + "", con)
            cmd1.ExecuteNonQuery()
            MsgBox("Deleted", MsgBoxStyle.OkOnly + MsgBoxStyle.SystemModal + MsgBoxStyle.Information, "Delete")
            con.Close()
            bindgrid()
            bindgrid2()
            con.Open()
            con.Close()
            Txt_Amt.Enabled = True
            GridView1.Enabled = True
            gridview2.Enabled = True
            BtnSave.Enabled = True
            Lbl_unpaidAmt.Visible = True
            Lbl_unpaid.Visible = True

            'Else
            '    Exit Sub
            'End If


        End If
    End Sub

    Protected Sub gridview2_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridview2.RowCreated
        Dim cell As TableCell = e.Row.Cells(0)

        'Remove cell
        e.Row.Cells.RemoveAt(0)
        'Add at the end
        e.Row.Cells.Add(cell)
    End Sub

    Protected Sub GridView1_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        Dim cell As TableCell = e.Row.Cells(0)

        'Remove cell
        e.Row.Cells.RemoveAt(0)
        'Add at the end
        e.Row.Cells.Add(cell)
    End Sub

    Protected Sub GridView1_RowDataBound1(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Visible = False
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Visible = False
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(1).Visible = False
        End If

        'e.Row.Cells(1).Visible = False
    End Sub
End Class
